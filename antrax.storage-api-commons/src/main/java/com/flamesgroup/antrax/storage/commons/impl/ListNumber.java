/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

import java.io.Serializable;
import java.util.Objects;

abstract class ListNumber implements Serializable {

  private static final long serialVersionUID = 3287503824092298230L;

  private String number;
  private VoipAntiSpamListNumbersStatus status;
  private int routingRequestCount;

  public ListNumber() {
  }

  public ListNumber(final String number, final VoipAntiSpamListNumbersStatus status, final int routingRequestCount) {
    this.number = number;
    this.status = status;
    this.routingRequestCount = routingRequestCount;
  }

  public String getNumber() {
    return number;
  }

  public ListNumber setNumber(final String number) {
    this.number = number;
    return this;
  }

  public VoipAntiSpamListNumbersStatus getStatus() {
    return status;
  }

  public ListNumber setStatus(final VoipAntiSpamListNumbersStatus status) {
    this.status = status;
    return this;
  }

  public int getRoutingRequestCount() {
    return routingRequestCount;
  }

  public ListNumber setRoutingRequestCount(final int routingRequestCount) {
    this.routingRequestCount = routingRequestCount;
    return this;
  }

  protected void appendToStringProperty(final StringBuilder sb) {
    sb.append("n:'").append(number).append('\'');
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ListNumber)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final ListNumber that = (ListNumber) object;

    return number.equals(that.number);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(number);
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode()));
    sb.append('[');
    appendToStringProperty(sb);
    sb.append(']');
    return sb.toString();
  }

}
