/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.Serializable;

public abstract class CellEqualizerAlgorithm extends Domain {

  private static final long serialVersionUID = 6892468593276270046L;

  private long serverId;

  public CellEqualizerAlgorithm() {
  }

  public CellEqualizerAlgorithm(final long id) {
    setID(id);
  }

  public abstract Configuration getConfiguration();

  public long getServerId() {
    return serverId;
  }

  public CellEqualizerAlgorithm setServerId(final long serverId) {
    this.serverId = serverId;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellEqualizerAlgorithm)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final CellEqualizerAlgorithm that = (CellEqualizerAlgorithm) object;

    return serverId == that.serverId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Long.hashCode(serverId);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[serverId:" + serverId +
        ']';
  }

  public static class Configuration implements Serializable {

    private static final long serialVersionUID = -678888486804660107L;

  }

}
