/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

public class SimData extends Domain implements Serializable {

  private static final long serialVersionUID = 2620622027894634313L;

  private ICCID uid;

  private PhoneNumber phoneNumber;
  private IMEI imei;

  private SIMGroup simGroup;

  private boolean enabled;
  private boolean locked;
  private String lockReason;
  private long lockTime;

  private int totalCallsCount;
  private int successfulCallsCount;
  private long callDuration;
  private int incomingTotalCallsCount;
  private int incomingSuccessfulCallsCount;
  private long incomingCallDuration;

  private int successOutgoingSmsCount;
  private int totalOutgoingSmsCount;
  private int incomingSMSCount;
  private int countSmsStatuses;
  private String note;
  private boolean allowInternet = true;
  private long tariffPlanEndDate;

  private double balance;

  public SimData(final long id) {
    this.id = id;
  }

  public SimData() {
    simGroup = new SimNoGroup();
  }

  public ICCID getUid() {
    return uid;
  }

  public SimData setUid(final ICCID uid) {
    this.uid = uid;
    return this;
  }

  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

  public SimData setPhoneNumber(final PhoneNumber phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  public IMEI getImei() {
    return imei;
  }

  public SimData setImei(final IMEI imei) {
    this.imei = imei;
    return this;
  }

  public SIMGroup getSimGroup() {
    return simGroup;
  }

  public SimData setSimGroup(final SIMGroup simGroup) {
    this.simGroup = simGroup;
    return this;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public SimData setEnabled(final boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public boolean isLocked() {
    return locked;
  }

  public SimData setLocked(final boolean locked) {
    this.locked = locked;
    return this;
  }

  public String getLockReason() {
    return lockReason;
  }

  public SimData setLockReason(final String lockReason) {
    this.lockReason = lockReason;
    return this;
  }

  public long getLockTime() {
    return lockTime;
  }

  public SimData setLockTime(final long lockTime) {
    this.lockTime = lockTime;
    return this;
  }

  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  public SimData setTotalCallsCount(final int totalCallsCount) {
    this.totalCallsCount = totalCallsCount;
    return this;
  }

  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  public SimData setSuccessfulCallsCount(final int successfulCallsCount) {
    this.successfulCallsCount = successfulCallsCount;
    return this;
  }

  public long getCallDuration() {
    return callDuration;
  }

  public SimData setCallDuration(final long callDuration) {
    this.callDuration = callDuration;
    return this;
  }

  public int getIncomingTotalCallsCount() {
    return incomingTotalCallsCount;
  }

  public SimData setIncomingTotalCallsCount(final int incomingTotalCallsCount) {
    this.incomingTotalCallsCount = incomingTotalCallsCount;
    return this;
  }

  public int getIncomingSuccessfulCallsCount() {
    return incomingSuccessfulCallsCount;
  }

  public SimData setIncomingSuccessfulCallsCount(final int incomingSuccessfulCallsCount) {
    this.incomingSuccessfulCallsCount = incomingSuccessfulCallsCount;
    return this;
  }

  public long getIncomingCallDuration() {
    return incomingCallDuration;
  }

  public SimData setIncomingCallDuration(final long incomingCallDuration) {
    this.incomingCallDuration = incomingCallDuration;
    return this;
  }

  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  public SimData setSuccessOutgoingSmsCount(final int successOutgoingSmsCount) {
    this.successOutgoingSmsCount = successOutgoingSmsCount;
    return this;
  }

  public int getTotalOutgoingSmsCount() {
    return totalOutgoingSmsCount;
  }

  public SimData setTotalOutgoingSmsCount(final int totalOutgoingSmsCount) {
    this.totalOutgoingSmsCount = totalOutgoingSmsCount;
    return this;
  }

  public int getIncomingSMSCount() {
    return incomingSMSCount;
  }

  public SimData setIncomingSMSCount(final int incomingSMSCount) {
    this.incomingSMSCount = incomingSMSCount;
    return this;
  }

  public void incCountSmsStatuses() {
    countSmsStatuses++;
  }

  public SimData setCountSmsStatuses(final int countSmsStatuses) {
    this.countSmsStatuses = countSmsStatuses;
    return this;
  }

  public int getCountSmsStatuses() {
    return countSmsStatuses;
  }

  public String getNote() {
    return note;
  }

  public SimData setNote(final String note) {
    this.note = note;
    return this;
  }

  public boolean isAllowInternet() {
    return allowInternet;
  }

  public SimData setAllowInternet(final boolean allowInternet) {
    this.allowInternet = allowInternet;
    return this;
  }

  public long getTariffPlanEndDate() {
    return tariffPlanEndDate;
  }

  public SimData setTariffPlanEndDate(final long tariffPlanEndDate) {
    this.tariffPlanEndDate = tariffPlanEndDate;
    return this;
  }

  public double getBalance() {
    return balance;
  }

  public SimData setBalance(final double balance) {
    this.balance = balance;
    return this;
  }

}
