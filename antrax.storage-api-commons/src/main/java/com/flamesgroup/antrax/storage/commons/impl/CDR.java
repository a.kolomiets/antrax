/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.unit.PhoneNumber;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CDR extends Domain {

  private static final long serialVersionUID = 7663235853661999566L;

  private long setupTime;
  private long dialTime;
  private long alertTime;
  private long startTime;
  private long stopTime;
  private PhoneNumber callerPhoneNumber;
  private PhoneNumber calledPhoneNumber;
  private String voiceServerName;
  private CallType callType;
  private LinkedList<CallPath> callPath = new LinkedList<>();

  public CDR(final long id) {
    this.id = id;
  }

  public CDR addCallPath(final CallPath cp) {
    if (cp != null) {
      callPath.add(cp);
    }
    return this;
  }

  public List<CallPath> getCallPath() {
    return callPath;
  }

  public CDR setCallPath(final LinkedList<CallPath> cpList) {
    this.callPath = cpList;
    return this;
  }

  public long getDuration() {
    if (startTime <= 0) {
      return 0;
    }
    return stopTime - startTime;
  }

  public long getPDD() {
    if (alertTime <= 0) {
      return 0;
    }
    return alertTime - dialTime;
  }

  public long getRingTime() {
    if (alertTime > 0) {
      if (startTime > 0) {
        return startTime - alertTime;
      } else {
        return stopTime - alertTime;
      }
    } else {
      return 0;
    }
  }

  public PhoneNumber getCallerPhoneNumber() {
    return callerPhoneNumber;
  }

  public PhoneNumber getCalledPhoneNumber() {
    return calledPhoneNumber;
  }

  // TODO: refactor
  public CdrDropReason getDropReason() {
    CallPath cp = callPath.peekLast();
    if (cp != null) {
      return cp.getCdrDropReason();
    } else {
      return null;
    }
  }

  // TODO: refactor
  public short getDropCode() {
    CallPath cp = callPath.peekLast();
    if (cp != null) {
      return cp.getCdrDropCode();
    } else {
      return 0;
    }
  }

  public CallType getCallType() {
    return callType;
  }

  public CDR setCallerPhoneNumber(final PhoneNumber callerPhoneNumber) {
    this.callerPhoneNumber = callerPhoneNumber;
    return this;
  }

  public CDR setCalledPhoneNumber(final PhoneNumber calledPhoneNumber) {
    this.calledPhoneNumber = calledPhoneNumber;
    return this;
  }

  public CDR setCallType(final CallType callType) {
    this.callType = callType;
    return this;
  }

  public CDR setVoiceServerName(final String voiceServerName) {
    this.voiceServerName = voiceServerName;
    return this;
  }

  public String getVoiceServerName() {
    return voiceServerName;
  }

  @Override
  public String toString() {
    return "CDR<SETUP_TIME=" + getSetupTime() + "; STOP_TIME=" + getStopTime() + "; DURATION=" + getDuration() + "; PHOPNE_NUM="
        + getCalledPhoneNumber() + ">";
  }

  public CDR setSetupTime(final long setupTime) {
    this.setupTime = setupTime;
    return this;
  }

  public CDR setDialTime(final long dialTime) {
    this.dialTime = dialTime;
    return this;
  }

  public CDR setAlertTime(final long alertTime) {
    this.alertTime = alertTime;
    return this;
  }

  public CDR setStartTime(final long startTime) {
    this.startTime = startTime;
    return this;
  }

  public CDR setStopTime(final long stopTime) {
    this.stopTime = stopTime;
    return this;
  }

  public long getStopTime() {
    return stopTime;
  }

  public long getSetupTime() {
    return setupTime;
  }

  public long getDialTime() {
    return dialTime;
  }

  public long getAlertTime() {
    return alertTime;
  }

  public long getStartTime() {
    return startTime;
  }

  public Date getSetupDate() {
    return new Date(getSetupTime());
  }

  public Date getStartDate() {
    return (getStartTime() > 0) ? new Date(getStartTime()) : null;
  }

  public Date getStopDate() {
    return (getStopTime() > 0) ? new Date(getStopTime()) : null;
  }

  private CDR() {

  }

  public void closeCall(final CdrDropReason cdrDropReason, final byte cdrDropCode) {
    this.stopTime = System.currentTimeMillis();
    if (!callPath.isEmpty()) {
      this.callPath.peekLast().setCdrDropReason(cdrDropReason).setCdrDropCode((short) (cdrDropCode & 0xFF));
    }
  }

  public void startDial() {
    this.dialTime = System.currentTimeMillis();
  }

  public void startAlert() {
    this.alertTime = System.currentTimeMillis();
  }

  public void startCall() {
    this.startTime = System.currentTimeMillis();
  }

  public static CDR createCall(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber, final CallType callType) {
    CDR cdr = new CDR();
    cdr.setCallType(callType);
    cdr.setCallerPhoneNumber(callerPhoneNumber);
    cdr.setCalledPhoneNumber(calledPhoneNumber);
    cdr.setSetupTime(System.currentTimeMillis());
    return cdr;
  }

}
