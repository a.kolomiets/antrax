/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.util.Objects;

public class PhoneNumberPrefixList extends Domain {

  private static final long serialVersionUID = 5404848256228133295L;

  private PhoneNumberPrefix phoneNumberPrefix;
  private String phoneNumber;

  public PhoneNumberPrefixList() {
  }

  public PhoneNumberPrefixList(final long id) {
    this.id = id;
  }

  public PhoneNumberPrefix getPhoneNumberPrefix() {
    return phoneNumberPrefix;
  }

  public PhoneNumberPrefixList setPhoneNumberPrefix(final PhoneNumberPrefix phoneNumberPrefix) {
    this.phoneNumberPrefix = phoneNumberPrefix;
    return this;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public PhoneNumberPrefixList setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof PhoneNumberPrefixList)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final PhoneNumberPrefixList that = (PhoneNumberPrefixList) object;

    return Objects.equals(phoneNumberPrefix, that.phoneNumberPrefix)
        && Objects.equals(phoneNumber, that.phoneNumber);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(phoneNumberPrefix);
    result = prime * result + Objects.hashCode(phoneNumber);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[phoneNumberPrefix:" + phoneNumberPrefix +
        " phoneNumber:'" + phoneNumber + "'" +
        ']';
  }

}
