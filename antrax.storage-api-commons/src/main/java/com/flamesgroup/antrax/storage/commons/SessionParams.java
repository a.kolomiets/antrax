/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

public class SessionParams implements Serializable {

  private static final long serialVersionUID = -8847124884313597573L;

  private String clientUid;
  private String userName;
  private String group;
  private InetAddress ip;
  private Date sessionStartTime;
  private Date sessionLastAccessTime;
  private Date sessionEndTime;

  public Date getSessionLastAccessTime() {
    return sessionLastAccessTime;
  }

  public SessionParams setSessionLastAccessTime(final Date sessionLastAccessTime) {
    this.sessionLastAccessTime = sessionLastAccessTime;
    return this;
  }

  public SessionParams setClientUid(final String clientUid) {
    this.clientUid = clientUid;
    return this;
  }

  public SessionParams setUserName(final String userName) {
    this.userName = userName;
    return this;
  }

  public SessionParams setGroup(final String group) {
    this.group = group;
    return this;
  }

  public SessionParams setIp(final InetAddress ip) {
    this.ip = ip;
    return this;
  }

  public SessionParams setSessionStartTime(final Date sessionStartTime) {
    this.sessionStartTime = sessionStartTime;
    return this;
  }

  public SessionParams setSessionEndTime(final Date sessionEndTime) {
    this.sessionEndTime = sessionEndTime;
    return this;
  }

  public String getClientUid() {
    return clientUid;
  }

  public String getUserName() {
    return userName;
  }

  public String getGroup() {
    return group;
  }

  public InetAddress getIp() {
    return ip;
  }

  public Date getSessionStartTime() {
    return sessionStartTime;
  }

  public Date getSessionEndTime() {
    return sessionEndTime;
  }

}
