/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import java.io.Serializable;

public class JavaFileName implements Serializable {

  private static final long serialVersionUID = 934171971623208008L;

  public static final char PACKAGE_SEPARATOR = '.';

  private final String name;

  public JavaFileName(final Class<?> clazz) {
    this.name = clazz.getName();
  }

  public JavaFileName(final String path) {
    this.name = path;
  }

  public String getName() {
    return name;
  }

  public String getPackageName() {
    int lastSeparatorIndex = name.lastIndexOf(PACKAGE_SEPARATOR);
    if (lastSeparatorIndex < 0) {
      return null;
    }
    return name.substring(0, (lastSeparatorIndex < 0) ? 0 : lastSeparatorIndex);
  }

  public String getSimpleName() {
    int lastSeparatorIndex = name.lastIndexOf(PACKAGE_SEPARATOR);
    return name.substring((lastSeparatorIndex < 0) ? 0 : lastSeparatorIndex + 1, name.length());
  }

  @Override
  public int hashCode() {
    return name == null ? 0 : name.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof JavaFileName) {
      JavaFileName that = (JavaFileName) obj;
      return that.name.equals(name);
    }
    return false;
  }

  @Override
  public String toString() {
    return name;
  }

}
