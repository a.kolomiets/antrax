/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.connection;

import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection implements IDbConnection {

  private final Connection connection;
  private final DSLContext context;
  private final IDbConnectionPool dbConnectionPool;

  public DbConnection(final DBConnectionProps dbConnectionProps, final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
    try {
      Class.forName(dbConnectionProps.getDriver());
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
    try {
      connection = DriverManager.getConnection(dbConnectionProps.getUrl(), dbConnectionProps.getUsername(), dbConnectionProps.getPassword());
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    context = DSL.using(connection, SQLDialect.POSTGRES_9_5);
  }

  @Override
  public Connection getConnection() {
    return connection;
  }

  @Override
  public DSLContext getDSLContext() {
    return context;
  }

  @Override
  public void close() {
    dbConnectionPool.offerConnection(this);
  }

}
