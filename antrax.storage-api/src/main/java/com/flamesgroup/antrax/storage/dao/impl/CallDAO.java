/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.tables.CallPath.CALL_PATH;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.antrax.storage.dao.SQLConnectionException;
import com.flamesgroup.antrax.storage.dao.UpdateFailedException;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.CallPathRecord;
import com.flamesgroup.storage.jooq.tables.records.CdrRecord;
import com.flamesgroup.storage.map.CdrMapper;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.JoinType;
import org.jooq.Operator;
import org.jooq.Record;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CallDAO implements ICallDAO {

  private final Logger logger = LoggerFactory.getLogger(CallDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public CallDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public long insertCDR(final CDR cdr) throws SQLConnectionException, UpdateFailedException {
    logger.debug("[{}] - trying to insert cdr: [{}]", this, cdr);
    if (cdr == null) {
      throw new UpdateFailedException(String.format("[%s] - can't insert cdr (cdr is null)", this));
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      CDR insertedCdr = context.transactionResult(configuration -> {
        CdrRecord cdrRecord = context.newRecord(Tables.CDR);
        CdrMapper.mapCdrToCdrRecord(cdr, cdrRecord);

        cdrRecord.store();

        InsertQuery<CallPathRecord> insertQuery = context.insertQuery(CALL_PATH);
        cdr.getCallPath().forEach((callPath) -> {
          CallPathRecord callPathRecord = context.newRecord(CALL_PATH);
          CdrMapper.mapCallPathToCallPathRecord(cdrRecord.getId(), callPath, callPathRecord);
          insertQuery.addRecord(callPathRecord);
        });
        insertQuery.setReturning();

        insertQuery.execute();

        return CdrMapper.mapCdrRecordToCdr(cdrRecord, insertQuery.getReturnedRecords());
      });
      logger.info("[{}] - new cdr: [{}] was inserted", this, cdr);
      return insertedCdr.getID();
    } catch (DataAccessException e) {
      throw new SQLConnectionException(e);
    } finally {
      connection.close();
    }
  }

  @Override
  public CDR[] listCDRs(final long startTime, final long endTime, final String simGroupName, final String gsmGroupName, final String callerNumber, final String calledNumber, final ICCID iccid,
      final int limit, final int offset) {
    logger.debug("[{}] - trying to select cdr list (startTime: [{}], endTime: [{}], limit: [{}])", this, startTime, endTime, limit);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<CdrRecord> selectQuery = context.selectQuery(Tables.CDR);
        selectQuery.addJoin(CALL_PATH, JoinType.LEFT_OUTER_JOIN, CALL_PATH.CDR_ID.eq(Tables.CDR.as(Tables.CDR.getName()).ID));
        if (startTime >= 0) {
          selectQuery.addConditions(Tables.CDR.SETUP_TIME.ge(new Date(startTime)));
        }
        if (endTime >= 0) {
          selectQuery.addConditions(Tables.CDR.SETUP_TIME.le(new Date(endTime)));
        }
        if (callerNumber != null && !callerNumber.isEmpty()) {
          selectQuery.addConditions(Tables.CDR.CALLER_PHONE_NUMBER.like(callerNumber));
        }
        if (calledNumber != null && !calledNumber.isEmpty()) {
          selectQuery.addConditions(Tables.CDR.CALLED_PHONE_NUMBER.like(calledNumber));
        }

        if (simGroupName != null && !simGroupName.isEmpty()) {
          selectQuery.addConditions(CALL_PATH.SIM_GROUP_NAME.eq(simGroupName));
        }
        if (gsmGroupName != null && !gsmGroupName.isEmpty()) {
          selectQuery.addConditions(CALL_PATH.GSM_GROUP_NAME.eq(gsmGroupName));
        }
        if (iccid != null) {
          selectQuery.addConditions(CALL_PATH.ICCID.eq(iccid));
        }

        if (limit > 0) {
          if (offset > 0) {
            selectQuery.addLimit(offset, limit);
          } else {
            selectQuery.addLimit(limit);
          }
        }

        selectQuery.addOrderBy(Tables.CDR.as(Tables.CDR.getName()).SETUP_TIME.desc());
        return selectQuery.fetchGroups(Tables.CDR.fields(), CallPathRecord.class).entrySet().stream()
            .map(entry -> CdrMapper.mapCdrRecordToCdr(entry.getKey().into(CdrRecord.class),
                entry.getValue().stream().sorted(Comparator.comparingLong(CallPathRecord::getId))
                    .collect(Collectors.toCollection(LinkedList::new)))).toArray(CDR[]::new);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting cdr list(startTime: [{}], endTime: [{}], limit: [{}])", this, startTime, endTime, limit, e);
      return Stream.empty().toArray(CDR[]::new);
    } finally {
      connection.close();
    }
  }

  @Override
  public CDR getCDRById(final long id) {
    logger.debug("[{}] - trying to get cdr by id: [{}])", this, id);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<CdrRecord> selectQuery = context.selectQuery(Tables.CDR);
        selectQuery.addConditions(Tables.CDR.ID.eq(id));
        selectQuery.addJoin(CALL_PATH, JoinType.JOIN, CALL_PATH.CDR_ID.eq(Tables.CDR.as(Tables.CDR.getName()).ID));
        return selectQuery.fetchGroups(Tables.CDR.fields(), CallPathRecord.class).entrySet().stream()
            .map(entry -> CdrMapper.mapCdrRecordToCdr(entry.getKey().into(CdrRecord.class), entry.getValue())).findFirst().orElse(null);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting cdr by id: [{}]", this, id, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountNumbersInPeriod(final String number, final long durationTime, final long addTime) {
    logger.debug("[{}] - trying to get CountNumbersInPeriod number [{}], durationTime [{}], addTime [{}])", this, number, durationTime, addTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<CdrRecord> selectQuery = context.selectQuery(Tables.CDR);
        selectQuery.addConditions(Operator.AND,
            Tables.CDR.CALLED_PHONE_NUMBER.eq(new PhoneNumber(number)),
            Tables.CDR.STOP_TIME.minus(Tables.CDR.START_TIME).cast(String.class).lessThan(CdrMapper.convertDurationToString(durationTime)),
            Tables.CDR.SETUP_TIME.greaterOrEqual(new Date(addTime)));

        int res = context.fetchCount(selectQuery);
        logger.debug("[{}] - got CountNumbersInPeriod number [{}], durationTime [{}], addTime [{}])", this, number, durationTime, addTime);
        return res;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get CountNumbersInPeriod number [{}], durationTime [{}], addTime [{}])", this, number, durationTime, addTime, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public Map<ICCID, Set<PhoneNumber>> getSimCalledNumbers(final long fromTime) {
    logger.debug("[{}] - trying to get sim called numbers: fromTime [{}]", this, fromTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    Map<ICCID, Set<PhoneNumber>> simCalledNumbers = new HashMap<>();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<Record> selectQuery = context.selectQuery();

        selectQuery.addSelect(CALL_PATH.ICCID, Tables.CDR.CALLED_PHONE_NUMBER);

        selectQuery.addFrom(Tables.CDR);
        selectQuery.addJoin(CALL_PATH, JoinType.JOIN, CALL_PATH.CDR_ID.eq(Tables.CDR.as(Tables.CDR.getName()).ID));

        selectQuery.addConditions(CALL_PATH.ICCID.isNotNull());
        selectQuery.addConditions(Tables.CDR.SETUP_TIME.greaterOrEqual(new Date(fromTime)));
        selectQuery.addConditions(Tables.CDR.CALL_TYPE.eq(CallType.VOIP_TO_GSM));

        selectQuery.addGroupBy(CALL_PATH.ICCID, Tables.CDR.CALLED_PHONE_NUMBER);

        selectQuery.fetch().forEach(r -> {
          ICCID iccid = r.getValue(0, ICCID.class);
          PhoneNumber phoneNumber = r.getValue(1, PhoneNumber.class);

          simCalledNumbers.computeIfAbsent(iccid, n -> new HashSet<>()).add(phoneNumber);
        });

        return simCalledNumbers;
      });
    } catch (DataAccessException e) {
      logger.debug("[{}] - trying to get sim called numbers: fromDate [{}]", this, fromTime, e);
      return Collections.emptyMap();
    }
  }

  @Override
  public Map<PhoneNumber, Map<ICCID, Integer>> getCalledNumbersToSim(final long fromTime) {
    logger.debug("[{}] - trying to get sim called numbers: fromTime [{}]", this, fromTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    Map<PhoneNumber, Map<ICCID, Integer>> simCalledNumbers = new HashMap<>();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<Record> selectQuery = context.selectQuery();

        selectQuery.addSelect(Tables.CDR.CALLED_PHONE_NUMBER, CALL_PATH.ICCID, DSL.count(CALL_PATH.ICCID));

        selectQuery.addFrom(Tables.CDR);
        selectQuery.addJoin(CALL_PATH, JoinType.JOIN, CALL_PATH.CDR_ID.eq(Tables.CDR.as(Tables.CDR.getName()).ID));

        selectQuery.addConditions(CALL_PATH.ICCID.isNotNull());
        selectQuery.addConditions(Tables.CDR.SETUP_TIME.greaterOrEqual(new Date(fromTime)));
        selectQuery.addConditions(Tables.CDR.CALL_TYPE.eq(CallType.VOIP_TO_GSM));

        selectQuery.addGroupBy(CALL_PATH.ICCID, Tables.CDR.CALLED_PHONE_NUMBER);

        selectQuery.fetch().forEach(r -> {
          PhoneNumber phoneNumber = r.getValue(Tables.CDR.CALLED_PHONE_NUMBER);
          ICCID iccid = r.getValue(CALL_PATH.ICCID);
          Integer count = r.getValue(DSL.count().getName(), Integer.class);

          simCalledNumbers.computeIfAbsent(phoneNumber, n -> new HashMap<>()).put(iccid, count);
        });

        return simCalledNumbers;
      });
    } catch (DataAccessException e) {
      logger.debug("[{}] - trying to get sim called numbers: fromDate [{}]", this, fromTime, e);
      return Collections.emptyMap();
    }
  }

  public List<VoiceServerCallStatistic> listVoiceServerCallStatistic(final Date fromDate, final Date toDate, final String prefix, final String server) {
    logger.debug("[{}] - trying to get voice server call statistic fromDate: [{}], toDate: [{}], prefix: [{}], server: [{}]", this, fromDate, toDate, prefix, server);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<Record> selectQuery = context.selectQuery();

        selectQuery.addSelect(Tables.CDR.VOICE_SERVER_NAME.as("server"));

        selectQuery.addSelect(Tables.CDR.VOICE_SERVER_NAME.count().as("total"));
        selectQuery.addSelect(Tables.CDR.START_TIME.count().as("successful"));
        selectQuery.addSelect(DSL.field("extract(epoch from {0})", Long.class, Tables.CDR.STOP_TIME.minus(Tables.CDR.START_TIME)).sum().mul(1000).as("duration"));

        selectQuery.addFrom(Tables.CDR);

        selectQuery.addConditions(Tables.CDR.CALL_TYPE.eq(CallType.VOIP_TO_GSM));
        selectQuery.addConditions(Tables.CDR.SETUP_TIME.between(fromDate, toDate));

        if (prefix != null && !prefix.isEmpty()) {
          selectQuery.addConditions(Tables.CDR.CALLED_PHONE_NUMBER.like(prefix + "%"));
          selectQuery.addSelect(Tables.CDR.CALLED_PHONE_NUMBER.substring(1, prefix.length()).as("prefix"));
          selectQuery.addGroupBy(DSL.field("prefix"));
        }
        if (server != null && !server.isEmpty()) {
          selectQuery.addConditions(Tables.CDR.VOICE_SERVER_NAME.eq(server));
        }

        selectQuery.addGroupBy(Tables.CDR.VOICE_SERVER_NAME);

        return selectQuery.fetch().into(VoiceServerCallStatistic.class);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get voice server call statistic fromDate: [{}], toDate: [{}], prefix: [{}], server: [{}]", this, fromDate, toDate, prefix, server, e);
      return Collections.emptyList();
    } finally {
      connection.close();
    }
  }

}
