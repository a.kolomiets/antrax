/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.SIM_EVENT_LOG;

import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ISIMHistoryDAO;
import com.flamesgroup.antrax.storage.dao.SQLConnectionException;
import com.flamesgroup.antrax.storage.dao.UpdateFailedException;
import com.flamesgroup.antrax.storage.enums.SIMEvent;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.SimEventLogRecord;
import com.flamesgroup.storage.map.SimEventLogMapper;
import com.flamesgroup.unit.ICCID;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;

public class SIMHistoryDAO implements ISIMHistoryDAO {

  private final Logger logger = LoggerFactory.getLogger(SIMHistoryDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public SIMHistoryDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public long simEventOccurred(final ICCID iccid, final SIMEvent event, final long startTime, final String[] args) throws UpdateFailedException {
    logger.trace("[{}] - trying to insert new simEvent (ICCID: [{}], event: [{}], values: [{}])", this, iccid, event, Arrays.toString(args));
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty() || event == null) {
      throw new UpdateFailedException(String.format("[%s] - can't insert new simEvent (key: [%s], event: [%s]", this, key, event));
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      return context.transactionResult(configuration -> {
        SimEventLogRecord simEventLogRecord = context.newRecord(Tables.SIM_EVENT_LOG);
        SIMEventRec eventRec = new SIMEventRec(0)
            .setSimUid(iccid)
            .setEvent(event)
            .setArgs(args)
            .setStartTime(new Date(startTime));

        SimEventLogMapper.mapSimEventLogToSimEventLogRecord(eventRec, simEventLogRecord);

        simEventLogRecord.store();

        return simEventLogRecord.getId();
      });
    } catch (DataAccessException e) {
      throw new SQLConnectionException(e);
    } finally {
      connection.close();
    }
  }

  @Override
  public SIMEventRec[] listSIMEventsByUid(final ICCID iccid, final long startTime, final long endTime, final int limit) {
    logger.trace("[{}] - trying to select list sim events(simUid: [{}], startTime: [{}], endTime: [{}], limit: [{}])",
        this, iccid, startTime, endTime, limit);
    String key = (iccid == null) ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't select list sim events (simUid is not valid: [{}])", this, key);
      return new SIMEventRec[0];
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SimEventLogRecord> selectQuery = context.selectQuery(SIM_EVENT_LOG);
        selectQuery.addConditions(SIM_EVENT_LOG.ICCID.eq(iccid));
        if (startTime >= 0) {
          selectQuery.addConditions(SIM_EVENT_LOG.START_TIME.ge(new Date(startTime)));
        }
        if (endTime >= 0) {
          selectQuery.addConditions(SIM_EVENT_LOG.START_TIME.le(new Date(endTime)));
        }
        if (limit > 0) {
          selectQuery.addLimit(limit);
        }
        selectQuery.addOrderBy(SIM_EVENT_LOG.START_TIME);

        return selectQuery.fetchInto(SimEventLogRecord.class).stream().map(SimEventLogMapper::mapSimEventLogRecordToSimEventLog).toArray(SIMEventRec[]::new);
      });

    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list sim events(simUid: [{}], startTime: [{}], endTime: [{}], limit: [{}])", this, iccid, startTime, endTime, limit, e);
      return new SIMEventRec[0];
    } finally {
      connection.close();
    }
  }

}
