/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.util.List;

public interface IConfigViewDAO {

  List<IServerData> listServers();

  IServerData getServerByName(String name);

  LinkWithSIMGroup[] listSIMGSMLinks() throws DataSelectionException;

  LinkWithSIMGroup[] listSIMGSMLinks(GSMGroup gsmGroup) throws DataSelectionException;

  SimData getSimData(ICCID uid) throws DataSelectionException;

  SIMGroup[] listSIMGroups() throws DataSelectionException;

  SIMGroup getSimGroup(long simGroupId) throws DataSelectionException;

  GSMGroup[] listGSMGroups();

  GSMChannel getGSMChannel(ChannelUID channelUID);

  boolean isGSMChannelStored(ChannelUID channel);

  String[] listSimGroupNames();

  String[] listGsmGroupNames();

  ScriptDefinition[] listScriptDefinitions();

  ScriptFile getLastScriptFile();

  RouteConfig getRouteConfig();

}
