/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.storage.jooq.tables.records.GsmViewRecord;

import java.util.Objects;

public final class GsmViewMapper {

  private GsmViewMapper() {
  }

  public static GsmView mapGsmViewRecordToGsmView(final GsmViewRecord gsmViewRecord) {
    Objects.requireNonNull(gsmViewRecord, "gsmViewRecord mustn't be null");
    GsmView gsmView = new GsmView();
    CellKey cellKey = new CellKey(gsmViewRecord.getLac(), gsmViewRecord.getCellId(), gsmViewRecord.getBsic(), gsmViewRecord.getArfcn());
    gsmView.setCellKey(cellKey)
        .setNetworkSurveyLastRxLev(gsmViewRecord.getNetworkSurveyRxLev())
        .setCellInfoLastRxLev(gsmViewRecord.getCellInfoRxLev())
        .setLastMcc(gsmViewRecord.getMcc())
        .setLastMnc(gsmViewRecord.getMnc())
        .setNetworkSurveyNumberOccurrences(gsmViewRecord.getNetworkSurveyNumberOccurrences())
        .setServingNumberOccurrences(gsmViewRecord.getServingNumberOccurrences())
        .setNeighborsNumberOccurrences(gsmViewRecord.getNeighboursNumberOccurrences())
        .setSuccessfulCallsCount(gsmViewRecord.getSuccessfulCallsCount())
        .setTotalCallsCount(gsmViewRecord.getTotalCallsCount())
        .setCallsDuration(gsmViewRecord.getCallsDuration())
        .setOutgoingSmsCount(gsmViewRecord.getOutgoingSmsCount())
        .setIncomingSmsCount(gsmViewRecord.getIncomingSmsCount())
        .setUssdCount(gsmViewRecord.getUssdCount())
        .setAmountPdd(gsmViewRecord.getAmountPdd())
        .setNotes(gsmViewRecord.getNotes())
        .setFirstAppearance(gsmViewRecord.getFirstAppearance())
        .setLastAppearance(gsmViewRecord.getLastAppearance())
        .setTrusted(gsmViewRecord.getTrusted())
        .setServingCell(gsmViewRecord.getServingCell())
        .setServerId(gsmViewRecord.getServerId());
    return gsmView;
  }

  public static void mapGsmViewToGsmViewRecord(final GsmView gsmView, final GsmViewRecord gsmViewRecord) {
    gsmViewRecord.setArfcn(gsmView.getCellKey().getArfcn())
        .setBsic(gsmView.getCellKey().getBsic())
        .setCellInfoRxLev(gsmView.getCellInfoLastRxLev())
        .setNetworkSurveyRxLev(gsmView.getNetworkSurveyLastRxLev())
        .setMcc(gsmView.getLastMcc())
        .setMnc(gsmView.getLastMnc())
        .setLac(gsmView.getCellKey().getLac())
        .setCellId(gsmView.getCellKey().getCellId())
        .setNetworkSurveyNumberOccurrences(gsmView.getNetworkSurveyNumberOccurrences())
        .setServingNumberOccurrences(gsmView.getServingNumberOccurrences())
        .setNeighboursNumberOccurrences(gsmView.getNeighborsNumberOccurrences())
        .setSuccessfulCallsCount(gsmView.getSuccessfulCallsCount())
        .setTotalCallsCount(gsmView.getTotalCallsCount())
        .setCallsDuration(gsmView.getCallsDuration())
        .setOutgoingSmsCount(gsmView.getOutgoingSmsCount())
        .setIncomingSmsCount(gsmView.getIncomingSmsCount())
        .setUssdCount(gsmView.getUssdCount())
        .setAmountPdd(gsmView.getAmountPdd())
        .setNotes(gsmView.getNotes())
        .setFirstAppearance(gsmView.getFirstAppearance())
        .setLastAppearance(gsmView.getLastAppearance())
        .setTrusted(gsmView.isTrusted())
        .setServingCell(gsmView.isServingCell())
        .setServerId(gsmView.getServerId());
  }

}
