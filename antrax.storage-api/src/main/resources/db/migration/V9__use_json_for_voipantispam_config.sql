ALTER TABLE voip_anti_spam_configuration ADD COLUMN gray_list_config json;
ALTER TABLE voip_anti_spam_configuration ADD COLUMN black_list_config json;
ALTER TABLE voip_anti_spam_configuration ADD COLUMN gsm_alerting_config json;
ALTER TABLE voip_anti_spam_configuration ADD COLUMN voip_alerting_config json;
ALTER TABLE voip_anti_spam_configuration ADD COLUMN fas_config json;

UPDATE voip_anti_spam_configuration SET gray_list_config = (SELECT CASE
WHEN enable=true AND acd_analyze_enable=true THEN json_build_array(json_build_object(
  'period', gray_list_period,
  'maxRoutingRequestPerPeriod', gray_list_max_routing_request_per_period,
  'blockPeriod', gray_list_block_period,
  'acdConfig', json_build_object(
    'period', acd_period,
    'maxMinAcdCallPerPeriod', acd_max_min_acd_call_per_period,
    'minAcd', acd_min_acd)))
WHEN enable=true AND acd_analyze_enable=false THEN json_build_array(json_build_object(
  'period', gray_list_period,
  'maxRoutingRequestPerPeriod', gray_list_max_routing_request_per_period,
  'blockPeriod', gray_list_block_period))
ELSE '[]' END
FROM voip_anti_spam_configuration);

UPDATE voip_anti_spam_configuration SET black_list_config = (SELECT CASE
WHEN enable=true THEN json_build_array(json_build_object(
  'period', black_list_period,
  'maxRoutingRequestPerPeriod', black_list_max_routing_request_per_period,
  'maxBlockCountBeforeMoveToBlackList', black_list_max_block_count_before_move_to_black_list))
ELSE '[]' END
FROM voip_anti_spam_configuration);

UPDATE voip_anti_spam_configuration SET gsm_alerting_config = (SELECT CASE
WHEN gsm_alerting_analyze_enable=true THEN json_build_array(json_build_object(
  'alertingTime', gsm_alerting_time,
  'period', gsm_alerting_routing_period,
  'maxRoutingRequestPerPeriod', gsm_alerting_max_routing_request_per_period))
ELSE '[]' END
FROM voip_anti_spam_configuration);

UPDATE voip_anti_spam_configuration SET voip_alerting_config = (SELECT CASE
WHEN voip_alerting_analyze_enable=true THEN json_build_array(json_build_object(
  'alertingTime', voip_alerting_time,
  'period', voip_alerting_routing_period,
  'maxRoutingRequestPerPeriod', voip_alerting_max_routing_request_per_period))
ELSE '[]' END
FROM voip_anti_spam_configuration);

UPDATE voip_anti_spam_configuration SET fas_config = (SELECT CASE
WHEN fas_analyze_enable=true THEN json_build_array(json_build_object(
  'period', fas_routing_period,
  'maxRoutingRequestPerPeriod', fas_max_routing_request_per_period))
ELSE '[]' END
FROM voip_anti_spam_configuration);

ALTER TABLE voip_anti_spam_configuration
  DROP COLUMN gray_list_period,
  DROP COLUMN gray_list_max_routing_request_per_period,
  DROP COLUMN gray_list_block_period,
  DROP COLUMN black_list_period,
  DROP COLUMN black_list_max_routing_request_per_period,
  DROP COLUMN black_list_max_block_count_before_move_to_black_list,
  DROP COLUMN acd_period,
  DROP COLUMN acd_max_min_acd_call_per_period,
  DROP COLUMN acd_min_acd,
  DROP COLUMN gsm_alerting_time,
  DROP COLUMN gsm_alerting_routing_period,
  DROP COLUMN gsm_alerting_max_routing_request_per_period,
  DROP COLUMN voip_alerting_time,
  DROP COLUMN voip_alerting_routing_period,
  DROP COLUMN voip_alerting_max_routing_request_per_period,
  DROP COLUMN fas_routing_period,
  DROP COLUMN fas_max_routing_request_per_period;
