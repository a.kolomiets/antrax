-- remove triggers
DROP FUNCTION tr_aft_ins_sim() CASCADE;
DROP FUNCTION tr_registry_upd() CASCADE;
DROP FUNCTION tr_script_instance() CASCADE;
DROP FUNCTION tr_set_curr_rev() CASCADE;
DROP FUNCTION tr_upd_server_config_rev() CASCADE;

-- remove functions
DROP FUNCTION public.delete_by_id(text, bigint);
DROP FUNCTION public.get_max_table_rev(text);
DROP FUNCTION public.get_next_rev(text);
DROP FUNCTION public.validate(text, bigint);

DROP TABLE revisions;

-- remove rev columns
ALTER TABLE gsm_group DROP COLUMN rev;
ALTER TABLE sim_group DROP COLUMN rev;
ALTER TABLE link_with_sim_group DROP COLUMN rev;
ALTER TABLE registry DROP COLUMN rev;
ALTER TABLE script_definition DROP COLUMN rev;
ALTER TABLE script_file DROP COLUMN rev;
ALTER TABLE script_instance DROP COLUMN rev;
ALTER TABLE script_param DROP COLUMN rev;
ALTER TABLE script_param_def DROP COLUMN rev;
ALTER TABLE server DROP COLUMN rev;
