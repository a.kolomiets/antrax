/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CallHelper {

  private static final Logger logger = LoggerFactory.getLogger(CallHelper.class);

  private final PhoneNumberGenerator phoneNumberGenerator;

  private final Lock callLock = new ReentrantLock();
  private final Condition callCondition = callLock.newCondition();

  private CallStateChangeHandler.CallState callState;

  public enum CallStatus {
    ERROR("exception occured"),
    OK("ok"),
    NO_ACTIVE("no active but required"),
    SHORT_ACTIVE("active is too short");

    private final String msg;

    CallStatus(final String msg) {
      this.msg = msg;
    }

    @Override
    public String toString() {
      return msg;
    }
  }

  private TimeInterval minimumActiveDuration = new TimeInterval(TimePeriod.inSeconds(10), TimePeriod.inSeconds(15));
  private TimeInterval maximumActiveDuration = new TimeInterval(TimePeriod.inMinutes(1), TimePeriod.inMinutes(2));
  private TimeInterval maximumAttemptDuration = new TimeInterval(TimePeriod.inMinutes(1) + TimePeriod.inSeconds(30), TimePeriod.inMinutes(1) + TimePeriod.inSeconds(50));
  private boolean requireActive = true;

  public CallHelper(final PhoneNumberGenerator phoneNumberGenerator) {
    this.phoneNumberGenerator = phoneNumberGenerator;
  }

  public void setRequiresActive(final boolean requiresActive) {
    this.requireActive = requiresActive;
  }

  public void setMinimumActiveDuration(final TimeInterval timeInterval) {
    this.minimumActiveDuration = timeInterval;
  }

  public void setMaximumActiveDuration(final TimeInterval maximumActiveDuration) {
    this.maximumActiveDuration = maximumActiveDuration;
  }

  public void setMaximumAttemptDuration(final TimeInterval maximumAttemptDuration) {
    this.maximumAttemptDuration = maximumAttemptDuration;
  }

  public CallStatus call(final RegisteredInGSMChannel channel, final int attemptsCount) throws Exception {
    CallStatus callStatus = CallStatus.ERROR;
    for (int i = 1; i <= attemptsCount; i++) {
      callLock.lock();
      try {
        callState = CallStateChangeHandler.CallState.DIALING; // re-init previous state
      } finally {
        callLock.unlock();
      }

      try {
        if ((callStatus = singleCall(channel)) == CallStatus.OK) {
          break;
        }
      } catch (Exception e) {
        logger.info("[{}] - while calling", this, e);
        if (i == attemptsCount) {
          throw e;
        }
      }
      Thread.sleep(new TimeInterval(TimePeriod.inSeconds(8), TimePeriod.inSeconds(12)).random());
    }
    return callStatus;
  }

  private CallStatus singleCall(final RegisteredInGSMChannel channel) throws Exception {
    PhoneNumber number = phoneNumberGenerator.generate();
    logger.debug("[{}] - attempts to call to number {}", this, number);
    final AtomicLong activeStart = new AtomicLong(0);
    try {
      channel.dial(number, (newState, prevStates) -> {
        callLock.lock();
        try {
          switch (newState) {
            case ACTIVE:
              activeStart.set(System.currentTimeMillis());
            case DROP: {
              if (callState != CallStateChangeHandler.CallState.DROP) {
                callState = newState;
                callCondition.signal();
              }
            }
          }
        } finally {
          callLock.unlock();
        }
      });
    } catch (Exception e) {
      logger.info("[{}] - failed to call to number {}", this, number, e);
      return CallStatus.ERROR;
    }

    // wait ACTIVE
    CallStateChangeHandler.CallState callStateLocal;
    callLock.lock();
    try {
      callCondition.await(maximumAttemptDuration.random(), TimeUnit.MILLISECONDS);

      callStateLocal = callState;
      switch (callStateLocal) {
        case DIALING:
          callState = CallStateChangeHandler.CallState.DROP;
        case DROP:
          logger.debug("[{}] - call has no active (requireActive == {})", this, requireActive);
          break;
      }
    } finally {
      callLock.unlock();
    }

    if (callStateLocal != CallStateChangeHandler.CallState.ACTIVE) {
      if (callStateLocal != CallStateChangeHandler.CallState.DROP) {
        channel.dropCall();
      }
      return requireActive ? CallStatus.NO_ACTIVE : CallStatus.OK;
    }

    // wait DROP
    long activeStartLocal;
    long maxActive = maximumActiveDuration.random();
    callLock.lock();
    try {
      activeStartLocal = activeStart.get();
      long leftTime = maxActive - (System.currentTimeMillis() - activeStartLocal);
      if (leftTime > 0 && callState != CallStateChangeHandler.CallState.DROP) {
        callCondition.await(leftTime, TimeUnit.MILLISECONDS);
      }
      callStateLocal = callState;
      callState = CallStateChangeHandler.CallState.DROP;
    } finally {
      callLock.unlock();
    }

    if (callStateLocal != CallStateChangeHandler.CallState.DROP) {
      channel.dropCall();
    }

    long callDuration = System.currentTimeMillis() - activeStartLocal;
    long minActive = minimumActiveDuration.random();
    logger.trace("[{}] - active duraion [current: {}, min: {}, max: {}]", this, callDuration, minActive, maxActive);
    return (callDuration < minActive) ? CallStatus.SHORT_ACTIVE : CallStatus.OK;
  }

}
