/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.helper.business.ussd.USSDResponsePattern;
import com.flamesgroup.antrax.helper.editors.components.USSDResponsePatternPanel;

import java.awt.*;

public class USSDResponsePatternEditor extends BasePropertyEditor<USSDResponsePattern> {

  private final USSDResponsePatternPanel panel = new USSDResponsePatternPanel();

  @Override
  public Component getEditorComponent() {
    return panel;
  }

  @Override
  public Class<? extends USSDResponsePattern> getType() {
    return USSDResponsePattern.class;
  }

  @Override
  public USSDResponsePattern getValue() {
    return panel.getUSSDResponsePattern();
  }

  @Override
  public void setValue(final USSDResponsePattern value) {
    panel.setUSSDResponsePattern(value);
  }

}
