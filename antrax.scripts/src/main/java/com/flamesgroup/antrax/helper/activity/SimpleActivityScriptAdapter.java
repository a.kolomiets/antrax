/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

import java.io.Serializable;

/**
 * Adapter of {@link SimpleActivityScript}. It
 * should be used to wrap simple activity scripts as final scripts for
 * configuring antrax sim groups.
 * <p>
 * This adapter also is {@link StatefullScript}
 */
public class SimpleActivityScriptAdapter<T extends SimpleActivityScript> implements StatefullScript, SessionListener, ActivityListener, ActivityPeriodListener,
    SimDataListener, GenericEventListener, ActivityPeriodScript {

  private static final long serialVersionUID = 4124951003580116123L;

  @StateField
  private T adept;

  public SimpleActivityScriptAdapter(final T adept) {
    this.adept = adept;
  }

  public SimpleActivityScriptAdapter() {

  }

  protected void setAdept(final T adept) {
    this.adept = adept;
  }

  protected T getAdept() {
    return adept;
  }

  @Override
  public void handleActivityEnded() {
    adept.handleActivityEnded();
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    adept.handleActivityStarted(gsmGroup);
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    adept.handleGenericEvent(event, args);
  }

  @Override
  public void setSimData(final SimData simData) {
    adept.setSimData(simData);
  }

  @Override
  public boolean isReadyToStartPeriod() {
    return adept.isActivityAllowed();
  }

  @Override
  public boolean shouldStopPeriod() {
    return !adept.isActivityAllowed();
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", getClass().getSimpleName(), adept);
  }

  @Override
  public Prediction predictPeriodStop() {
    return adept.predictEnd();
  }

  @Override
  public Prediction predictPeriodStart() {
    return adept.predictStart();
  }

  @Override
  public void handlePeriodEnd() {
    adept.handlePeriodEnd();
  }

  @Override
  public void handlePeriodStart() {
    adept.handlePeriodStart();
  }

  @Override
  public void handleSessionEnded() {
    adept.handleSessionEnded();
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
    adept.handleSessionStarted(gateway);
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return adept.getScriptSaver();
  }

}
