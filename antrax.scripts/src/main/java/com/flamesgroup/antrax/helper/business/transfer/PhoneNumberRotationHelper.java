/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.transfer;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

public abstract class PhoneNumberRotationHelper {

  private static final Logger logger = LoggerFactory.getLogger(PhoneNumberRotationHelper.class);

  private static final int MAX_QUERY_SIZE = 100;

  private final int transfersCount;
  private final long usedPeriod;
  private final long oldPeriod;

  private final RegistryAccess access;
  private final String groupName;
  private final LinkedList<String> searchList;

  protected abstract boolean executeTransfer(RegisteredInGSMChannel srcChannel, String dstPhoneNumber) throws Exception;

  public PhoneNumberRotationHelper(final String groupName, final RegistryAccess access, final int transfersCount, final long usedPeriod, final long oldPeriod) {
    this.access = access;
    this.groupName = groupName;
    this.transfersCount = transfersCount;
    this.usedPeriod = usedPeriod;
    this.oldPeriod = oldPeriod;

    searchList = new LinkedList<>();
    searchList.add(groupName + ".main");
    for (int i = 1; i <= transfersCount - 1; i++) {
      searchList.add(groupName + "." + i);
    }
  }

  public void touch(final String phoneNumber, final boolean addNew) throws UnsyncRegistryEntryException, DataModificationException, TransferFromException {
    boolean touched = false;
    boolean found = false;

    if (null == groupName || groupName.equals("")) {
      throw new TransferFromException("store name isn't set");
    }

    for (String path : searchList) {
      touched = touch(path, phoneNumber);
      if (touched) {
        break;
      }
    }

    if (!touched) {
      //try to find in the used list but don't do touch
      for (RegistryEntry e : access.listEntries(groupName + ".used", MAX_QUERY_SIZE)) {
        if (phoneNumber.equals(e.getValue())) {
          logger.debug("[{}] - found phone number {} in the used path. Don't touch it", this, phoneNumber);
          found = true;
          break;
        }
      }
    }

    if (addNew && !touched && !found) {
      logger.debug("[{}] - add new phone number {}", this, phoneNumber);
      access.add(groupName + ".main", phoneNumber, MAX_QUERY_SIZE);
    }
  }

  public void transfer(final RegisteredInGSMChannel srcChannel) throws Exception {
    if (null == groupName || groupName.equals("")) {
      throw new TransferFromException("store name isn't set");
    }

    releaseUsed();
    if (oldPeriod > 0) {
      removeOld();
    }

    boolean transferred = false;
    for (String path : searchList) {
      transferred = transfer(srcChannel, path);
      if (transferred) {
        break;
      }
    }
    if (!transferred) {
      logger.warn("[{}] - no dst phone numbers", this);
      throw new TransferFromException("No dst phone numbers");
    }
  }

  private void releaseUsed() {
    moveAll(groupName + ".used", groupName + ".main", usedPeriod);
  }

  private void removeOld() throws UnsyncRegistryEntryException, DataModificationException {
    for (String path : searchList) {
      for (RegistryEntry e : access.listEntries(path, MAX_QUERY_SIZE)) {
        if (System.currentTimeMillis() - e.getUpdatingTime().getTime() > oldPeriod) {
          logger.debug("[{}] - remove old phone number {} on timeout", this, e.getValue());
          access.remove(e);
        }
      }
    }
  }

  private boolean touch(final String path, final String phoneNumber) throws UnsyncRegistryEntryException, DataModificationException {
    for (RegistryEntry e : access.listEntries(path, MAX_QUERY_SIZE)) {
      if (phoneNumber.equals(e.getValue())) {
        logger.debug("[{}] - touch existence number {} at path {}", this, phoneNumber, e.getPath());
        //move entry to the same path to change updated time
        access.move(e, e.getPath());
        return true;
      }
    }
    return false;
  }

  private boolean transfer(final RegisteredInGSMChannel srcChannel, final String path) throws Exception {
    boolean transferred = false;
    String srcNumber = srcChannel.getSimData().getPhoneNumber().getValue();

    for (RegistryEntry e : access.listEntries(path, MAX_QUERY_SIZE)) {
      if (e.getValue().equals(srcNumber)) {
        continue;
      }

      try {
        access.move(e, e.getPath() + ".taken");
      } catch (UnsyncRegistryEntryException e1) {
        continue;
      }

      try {
        transferred = executeTransfer(srcChannel, e.getValue());
      } catch (Exception e1) {
        access.move(e, path);
        throw e1;
      }

      if (transferred) {
        String nextPath = getNextPath(path);

        logger.debug("[{}] - move phone number {} to {}", this, e.getValue(), nextPath);
        access.move(e, nextPath);
        break;
      } else {
        access.move(e, path);
      }
    }
    return transferred;
  }

  private String getNextPath(final String path) {
    int index = path.lastIndexOf(".");
    String str;
    if (-1 == index) {
      return null;
    }

    str = path.substring(index + 1);
    if (str.equals("used")) {
      return path;
    }

    if (str.equals("main")) {
      if (transfersCount > 1) {
        return path.substring(0, index) + ".1";
      } else {
        return path.substring(0, index) + ".used";
      }
    } else {
      int num = Integer.parseInt(str);
      if (num < transfersCount - 1) {
        return path.substring(0, index) + "." + (num + 1);
      } else {
        return path.substring(0, index) + ".used";
      }
    }
  }

  private void moveAll(final String srcPath, final String dstPath, final long timeout) {
    for (RegistryEntry e : access.listEntries(srcPath, MAX_QUERY_SIZE)) {
      if (timeout > 0 && System.currentTimeMillis() - e.getUpdatingTime().getTime() > timeout) {
        try {
          logger.debug("[{}] - release used phone number {} on timeout", this, e.getValue());
          access.move(e, dstPath);
        } catch (UnsyncRegistryEntryException | DataModificationException e1) {
          logger.warn("[{}] - failed to move path", this, e1);
        }
      }
    }
  }
}
