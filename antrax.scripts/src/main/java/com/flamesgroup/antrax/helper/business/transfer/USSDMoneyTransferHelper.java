/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.transfer;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class USSDMoneyTransferHelper implements MoneyTransferHelper {

  private static final long serialVersionUID = 7284618093531416353L;

  private static final Logger logger = LoggerFactory.getLogger(USSDMoneyTransferHelper.class);

  private final String ussd;
  private final List<USSDResponseAnswer> ussdResponseAnswerList;

  public USSDMoneyTransferHelper(final String ussd, final List<USSDResponseAnswer> ussdResponseAnswerList) {
    this.ussd = ussd;
    this.ussdResponseAnswerList = ussdResponseAnswerList;
  }

  @Override
  public void transferMoney(final RegisteredInGSMChannel channel, final PhoneNumber number, final int moneyAmount) throws Exception {
    String request = ussd.replaceFirst("[$]", Integer.valueOf(moneyAmount).toString());
    request = request.replaceFirst("[@]", number.getValue());
    logger.debug("[{}] - going to transfer a {} money to number {}", this, moneyAmount, number);
    logger.debug("[{}] - Sending ussd: {}", this, request);
    String response = channel.sendUSSD(request);
    logger.debug("[{}] - got response: {}", this, response);
    for (USSDResponseAnswer usssdResponseAnswer : ussdResponseAnswerList) {
      if (!response.matches(usssdResponseAnswer.getResponsePattern())) {
        logger.debug("Wrong response: {}", response);
        throw new WrongResponseException("Wrong response: " + response);
      }
      Thread.sleep(new TimeInterval(TimePeriod.inSeconds(10), TimePeriod.inSeconds(12)).random());
      if (!usssdResponseAnswer.hasResponseAnswer()) {
        break;
      }
      request = response.replaceFirst(usssdResponseAnswer.getResponsePattern(), usssdResponseAnswer.getResponseAnswer());
      logger.debug("[{}] - sending ussd: {}", this, request);
      response = channel.sendUSSD(request);
      logger.debug("[{}] - got response: {}", this, response);
    }
    logger.debug("[{}] - transfer is ok", this);
  }

}
