/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.DefaultPropertyTableRenderer;
import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.editors.PropertyTableRenderer;
import com.flamesgroup.antrax.helper.editors.scriplet.EndBlockComponent;
import com.flamesgroup.antrax.helper.editors.scriplet.OperationComponent;
import com.flamesgroup.antrax.helper.editors.scriplet.ScripletComponent;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.BaseScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletAtom;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletOperation;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.EventObject;

import javax.swing.*;
import javax.swing.event.CellEditorListener;

public class ActivityScripletEditor extends JPanel implements PropertyEditor<ActivityScriplet> {

  private static final long serialVersionUID = -2739433839071736685L;

  public static final String[] EDIT_ICON = {
  /**/".+++.",
  /**/".+++.",
  /**/".+++.",
  /**/"..+..",
  /**/"..+.."};

  public static final String[] LEFT_ICON = {
  /**/"..++.",
  /**/".++..",
  /**/"++...",
  /**/".++..",
  /**/"..++."};

  public static final String[] RIGHT_ICON = {
  /**/".++..",
  /**/"..++.",
  /**/"...++",
  /**/"..++.",
  /**/".++.."};

  public static final String[] REMOVE_ICON = {
  /**/"+...+",
  /**/".+.+.",
  /**/"..+..",
  /**/".+.+.",
  /**/"+...+"};

  public static final String[] ADD_ICON = {
  /**/"..+..",
  /**/"..+..",
  /**/"+++++",
  /**/"..+..",
  /**/"..+.."};

  public static final String[] SPLIT_ICON = {
  /**/"+...+",
  /**/"++.++",
  /**/".+.+.",
  /**/"++.++",
  /**/"+...+"};

  public static final String[] JOIN_ICON = {
  /**/".+.+.",
  /**/"++.++",
  /**/"+...+",
  /**/"++.++",
  /**/".+.+."};

  public static Icon createIcon(final String[] icon) {
    BufferedImage image = new BufferedImage(icon.length, icon.length, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g = image.createGraphics();
    g.setColor(Color.BLACK);

    for (int y = 0; y < icon.length; ++y) {
      for (int x = 0; x < icon[y].length(); ++x) {
        if (icon[y].charAt(x) == '+') {
          g.drawLine(x, y, x, y);
        }
      }
    }
    g.dispose();

    return new ImageIcon(image);
  }

  public static JLabel createLabel(final String text, final boolean bold) {
    JLabel retval = new JLabel(text, JLabel.CENTER);
    if (bold) {
      retval.setFont(retval.getFont().deriveFont(Font.BOLD));
    }

    return retval;
  }

  private static final PropertyTableRenderer<Object> renderer = new DefaultPropertyTableRenderer<>();

  private ActivityScriplet value;

  private final Component editorComponent;

  public ActivityScripletEditor() {
    setLayout(new FlowLayout(FlowLayout.LEFT));
    setValue(new ScripletBlock(ScripletFactory.createScripletFor(ScripletFactory.listScripts()[0])));
    editorComponent = new JScrollPane(this, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    editorComponent.setPreferredSize(new Dimension(700, editorComponent.getPreferredSize().height));
  }

  @Override
  public int getClickCountToEdit() {
    return 1;
  }

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Component getRendererComponent(final ActivityScriplet value) {
    return renderer.getRendererComponent(value);
  }

  @Override
  public Class<? extends ActivityScriplet> getType() {
    return ActivityScriplet.class;
  }

  @Override
  public ActivityScriplet getValue() {
    return value;
  }

  @Override
  public void setValue(final ActivityScriplet value) {
    this.value = value;
    removeAll();
    addScriplet(value);
    revalidate();
    repaint();
  }

  private void addScriplet(final ActivityScriplet value) {
    if (value instanceof ScripletAtom) {
      add(new ScripletComponent((ScripletAtom) value, this));
    } else if (value instanceof ScripletBlock) {
      ScripletBlock block = (ScripletBlock) value;
      EndBlockComponent endBlockComponent = new EndBlockComponent((BaseScriplet) value, this);
      add(createBottomedLabel("(", endBlockComponent.getPreferredSize().height));
      addScriplet(block.getFirst());
      addSiblings(block);
      add(endBlockComponent);
    }
  }

  private void addSiblings(final ScripletBlock block) {
    ActivityScriplet prev = block.getFirst();
    for (Pair<ScripletOperation, ActivityScriplet> p : block.getSiblings()) {
      add(new OperationComponent(block, prev, p.second(), p.first(), this));
      addScriplet(p.second());
      prev = p.second();
    }
  }

  private JLabel createBottomedLabel(final String text, final int height) {
    JLabel retval = createLabel(text, false);
    retval.setVerticalAlignment(JLabel.BOTTOM);
    retval.setPreferredSize(new Dimension(retval.getPreferredSize().width, height));
    return retval;

  }

  @Override
  public boolean shouldSelectCell(final EventObject anEvent) {
    return true;
  }

  @Override
  public boolean stopEditing() {
    return true;
  }

  @Override
  public void addCellEditorListener(final CellEditorListener l) {
  }

  @Override
  public void removeCellEditorListener(final CellEditorListener l) {
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    final ActivityScripletEditor editor = new ActivityScripletEditor();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(editor);
    frame.setPreferredSize(new Dimension(500, 100));
    frame.pack();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

  public void refresh() {
    setValue(value);
  }

}
