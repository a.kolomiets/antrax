/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Script(name = "generate event after call", doc = "generate event after call")
public class GenerateEventAfterCall implements BusinessActivityScript, CallsListener, StatefullScript {

  private static final long serialVersionUID = 7490558400334364245L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventAfterCall.class);

  @StateField
  private volatile int callCount;
  @StateField
  private long callsLimitValue;
  @StateField
  private volatile int eventCount;

  private long latchLimit = 1000000;
  private VariableLong callsLimit = new VariableLong(20, 20);
  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event on fas")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "calls limit", doc = "after this count of calls event will be generated")
  public void setCallsLimit(final VariableLong callsLimit) {
    this.callsLimit = callsLimit;
    callsLimitValue = callsLimit.random();
    logger.debug("callsLimitValue set to {}", callsLimitValue);
  }

  public VariableLong getCallsLimit() {
    return callsLimit;
  }

  @ScriptParam(name = "latch limit", doc = "maximum number of event that script can generate")
  public void setLatchLimit(final long latchLimit) {
    this.latchLimit = latchLimit;
  }

  public long getLatchLimit() {
    return latchLimit;
  }

  @Override
  public String describeBusinessActivity() {
    return "event after call";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    eventCount++;
    logger.debug("[{}] - eventCount set to {}", this, eventCount);
    if (eventCount >= latchLimit) {
      logger.debug("No more events");
    }
    callCount = 0;
    callsLimitValue = callsLimit.random();
    logger.debug("[{}] - callsLimitValue set to {}", this, callsLimitValue);
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return eventCount < latchLimit && callCount >= callsLimitValue;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    callCount++;
    saver.save();
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}
