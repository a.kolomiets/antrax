/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.IvrListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "generate event on ivr detect", doc = "generate event when ivr was detected")
public class GenerateEventOnIvrDetect implements BusinessActivityScript, IvrListener, CallsListener {

  private GenericEvent event = GenericEvent.uncheckedEvent("ivrDetect");
  private VariableLong amountIvr = new VariableLong(1, 1);
  private String ivrTemplateNamePattern = ".*";
  private boolean consecutiveCount = false;

  private long amountIvrLimitValue;
  private long countIvrValue;
  private long countCalls;

  @ScriptParam(name = "event", doc = "name of generated event")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "ivr template name pattern", doc = "generate event only for this ivr template name pattern")
  public void setIvrTemplateNamePattern(final String ivrTemplateNamePattern) {
    this.ivrTemplateNamePattern = ivrTemplateNamePattern;
  }

  public String getIvrTemplateNamePattern() {
    return ivrTemplateNamePattern;
  }

  @ScriptParam(name = "amount ivr detects", doc = "after amount ivr detects will be generate event")
  public void setAmountIvr(final VariableLong amountIvr) {
    this.amountIvr = amountIvr;
    amountIvrLimitValue = amountIvr.random();
  }

  public VariableLong getAmountIvr() {
    return amountIvr;
  }

  @ScriptParam(name = "consecutive count", doc = "consecutive count ivr detects")
  public void setConsecutiveCount(final boolean consecutiveCount) {
    this.consecutiveCount = consecutiveCount;
  }

  public boolean getConsecutiveCount() {
    return consecutiveCount;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    countIvrValue = 0;
    countCalls = 0;
    amountIvrLimitValue = amountIvr.random();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (consecutiveCount && countCalls != countIvrValue) {
      countIvrValue = 1;
      countCalls = 1;
    }

    return countIvrValue >= amountIvrLimitValue;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event [" + event.getEvent() + "] on ivr detect";
  }

  @Override
  public void handleIvr(final String ivrTemplateName, final byte callDropCauseCode) {
    if (ivrTemplateName.matches(ivrTemplateNamePattern)) {
      countIvrValue++;
    }
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
    countCalls++;
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

}
