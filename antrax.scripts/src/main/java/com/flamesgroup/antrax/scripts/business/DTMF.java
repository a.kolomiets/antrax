/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.business.dtmf.DTMFScriplet;

import java.io.Serializable;

@Script(name = "DTMF", doc = "waits until event occured and call to specified number and sends specified DTMF's")
public class DTMF implements BusinessActivityScript, GenericEventListener {

  private DTMFScriplet scriplet = new DTMFScriplet(null);
  private int attemptsCount = 3;
  private String event = "dtmf";

  private volatile GenericEvent caughtEvent;

  @ScriptParam(name = "DTMF scriplet", doc = "Consists of number to call and DTMF actions (dial or timeout). Actions can be combined in a variety of ways")
  public void setDTMFScriplet(final DTMFScriplet scriplet) {
    this.scriplet = scriplet;
  }

  public DTMFScriplet getDTMFScriplet() {
    return scriplet;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on DTMF failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "event", doc = "event to start DTMF activity")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @Override
  public String describeBusinessActivity() {
    return "sending DTMF";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      int attemptCount = 0;
      while (true) {
        try {
          scriplet.execute(channel);
          break;
        } catch (Exception e) {
          attemptCount++;
          if (attemptCount >= attemptsCount) {
            caughtEvent.respondFailure(channel, e.getMessage());
            return;
          }
        }
      }
      caughtEvent.respondSuccess(channel);
    } finally {
      caughtEvent = null;
    }

  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}
