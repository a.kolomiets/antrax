/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


/*
 * This script should be activated with CallAction script only because
 * of using parameterized event. 
 * */

@Script(name = "ask for call back", doc = "ask for call back")
public class AskForCallBack implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(AskForCallBack.class);

  protected String event = "askForCallBack";
  private String actionName = "callFromTo";

  private boolean lockOnFail = false;

  private TimeInterval executeTimeout = new TimeInterval(TimePeriod.inMinutes(15), TimePeriod.inMinutes(15));

  private String eventOnSuccess = "";

  private String eventOnFailure = "";

  protected volatile GenericEvent incomingEvent = null;

  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "action", doc = "name of action, which can provide call from one number to another")
  public void setAction(final String action) {
    this.actionName = action;
  }

  public String getAction() {
    return actionName;
  }

  @ScriptParam(name = "lock on fail", doc = "lock card if call failed")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @ScriptParam(name = "execute time", doc = "time of execute call")
  public void setExecuteTime(final TimeInterval interval) {
    this.executeTimeout = interval;
  }

  public TimeInterval getExecuteTime() {
    return executeTimeout;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if call was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if call failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = eventOnFailure;
  }

  public String getEventOnFailure() {
    return eventOnFailure;
  }


  @Override
  public String describeBusinessActivity() {
    return "initiates call between defined phone numbers";
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return null != incomingEvent;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    String srcNumber = incomingEvent.getSuccessResponce();
    String dstNumber = incomingEvent.getErrorResponce();

    logger.debug("[{}] - my number: {}. Initiate call: {} -> {}", this, channel.getSimData().getPhoneNumber().getValue(), srcNumber, dstNumber);
    try {
      Action action = new Action(actionName, srcNumber, dstNumber);
      channel.executeAction(action, executeTimeout.random());
      incomingEvent.respondSuccess(channel);
      if (eventOnSuccess != null && !eventOnSuccess.isEmpty())
        channel.fireGenericEvent(eventOnSuccess);
    } catch (Exception e) {
      incomingEvent.respondFailure(channel, e.getMessage());
      if (lockOnFail) {
        channel.lock("ask for call back failed: " + e.getMessage());
      }
      logger.error("[{}] - failed to initiate call {} -> {}", this, srcNumber, dstNumber, e);
      if (eventOnFailure != null && !eventOnFailure.isEmpty())
        channel.fireGenericEvent(eventOnFailure);
    } finally {
      incomingEvent = null;
    }
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}
