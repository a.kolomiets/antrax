/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.transfer.MoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.PhoneNumberRotationHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDMoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


@Script(name = "Transfer to list", doc = "Transfer money from the current card to the card from list")
public class TransferToList implements BusinessActivityScript, GenericEventListener, RegistryAccessListener, StatefullScript, CallsListener {

  private static final long serialVersionUID = -5559926928766102062L;

  private static final Logger logger = LoggerFactory.getLogger(TransferToList.class);

  private final ScriptSaver saver = new ScriptSaver();
  private transient RegistryAccess registry;

  protected String event = "payed";
  protected String groupName;
  protected boolean addOnTouch = true;
  protected int transfersPerDay = 3;
  protected int removeOldPeriod = 5;
  private boolean lockOnFail = true;

  private CheckBalanceHelper checkBalanceHelper;
  private int amount = 5;
  private double remainsAmount = 5;
  private boolean transferAll = false;
  private String ussdRequest;
  private final List<USSDResponseAnswer> ussdResponseAnswerList = new LinkedList<>();


  @StateField
  protected boolean callEnded = false;

  protected volatile GenericEvent incomingEvent = null;


  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "add new number to the list on call", doc = "if checked the new phone number will be added to the list automatically on call")
  public void setAddOnTouch(final boolean addOnTouch) {
    this.addOnTouch = addOnTouch;
  }

  public boolean getAddOnTouch() {
    return addOnTouch;
  }

  @ScriptParam(name = "allowed transfers count per day", doc = "allowed transfers count per day")
  public void setTransfersPerDay(final int transfersPerDay) {
    this.transfersPerDay = transfersPerDay;
    if (this.transfersPerDay <= 0) {
      this.transfersPerDay = 1;
    }
  }

  public int getTransfersPerDay() {
    return transfersPerDay;
  }

  @ScriptParam(name = "remove numbers older than defined hours", doc = "remove numbers which weren't updated on call for defined period. set 0 to cancel automatic numbers removement")
  public void setRemoveOldPeriod(final int removeOldPeriod) {
    this.removeOldPeriod = removeOldPeriod;
  }

  public int getRemoveOldPeriod() {
    return removeOldPeriod;
  }

  @ScriptParam(name = "lock on fail", doc = "locks card on transfer failure")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }


  @ScriptParam(name = "amount", doc = "amount of money to transfer")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "USSD request", doc = "place $ for amount and @ for number. Example: *150*@*$#")
  public void setUSSDRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getUSSDRequest() {
    return "*124*$*@#";
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressins for parsing response and generating answers")
  public void addUSSDResponseAnswerList(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswerList.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "transfer all", doc = "transfer all except remain amount")
  public void setTransferAll(final boolean transferAll) {
    this.transferAll = transferAll;
  }

  public boolean getTransferAll() {
    return transferAll;
  }

  @ScriptParam(name = "remains amount", doc = "minimum amount of money which must remain after transfer")
  public void setRemainsAmount(final double amount) {
    this.remainsAmount = amount;
  }

  public double getRemainsAmount() {
    return remainsAmount;
  }

  @ScriptParam(name = "balance check data", doc = "balance checking data. Balance checking may take up to 4 minutes")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*rahunku: (\\d+\\.\\d{2}) ?grn.*");
  }


  @ScriptParam(name = "store name", doc = "unique name of registry path to store phone number list")
  public void setGroupName(final String groupName) {
    this.groupName = groupName;
  }

  public String getGroupName() {
    return groupName;
  }



  @Override
  public String describeBusinessActivity() {
    return "Transfer to list";
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (callEnded && null == incomingEvent) {
      return true;
    }
    if (!callEnded && null != incomingEvent) {
      return true;
    }
    return false;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) {
    if (channel.getSimData().getPhoneNumber() == null) {
      logger.debug("[{}] - there is no phone number, locking card", this);
      channel.lock("no PhoneNumber but using \"TransferToList\" script");
    }


    try {
      PhoneNumberRotationHelper helper = new PhoneNumberRotationHelper(groupName,
          registry,
          transfersPerDay,
          TimePeriod.inHours(24),
          TimePeriod.inHours(removeOldPeriod)) {

        @Override
        protected boolean executeTransfer(final RegisteredInGSMChannel srcChannel, final String dstPhoneNumber) throws Exception {

          logger.debug("[{}] - checking balance value...", this);
          double currentAmount = checkBalanceHelper.getBalanceValue(srcChannel, registry);
          logger.debug("[{}] - checked", this);
          int transferAmount;

          if (currentAmount > remainsAmount) {
            if (transferAll || amount > (int) Math.round(currentAmount - remainsAmount)) {
              transferAmount = (int) Math.round(currentAmount - remainsAmount); //transfer all money except reminder
            } else {
              transferAmount = amount;
            }
          } else {
            transferAmount = 0;
          }

          logger.debug("[{}] - {} going to transfer money [{}] to {}", this, srcChannel.getSimData().getPhoneNumber().getValue(), transferAmount, dstPhoneNumber);

          if (0 == transferAmount) {
            logger.info("[{}] - not enough money to make transfer. Channel {}", this, srcChannel);
          } else {
            MoneyTransferHelper moneyTransferHelper;
            moneyTransferHelper = new USSDMoneyTransferHelper(ussdRequest, ussdResponseAnswerList);

            moneyTransferHelper.transferMoney(srcChannel, new PhoneNumber(dstPhoneNumber), transferAmount);
          }
          return true;
        }
      };

      if (callEnded) {
        helper.touch(channel.getSimData().getPhoneNumber().getValue(), addOnTouch);
      }

      if (null != incomingEvent) {
        logger.debug("[{}] - going to transfer money from {}", this, channel);
        helper.transfer(channel);
      }
    } catch (Exception e) {
      if (callEnded) {
        logger.warn("[{}] - failed to touch", this, e);
      }

      if (null != incomingEvent) {
        logger.warn("[{}] - failed to transfer", this, e);
        incomingEvent.respondFailure(channel, e.getMessage());
        if (lockOnFail) {
          channel.lock("Transfer to list failed: " + e.getMessage());
        }
      }
    } finally {
      if (callEnded) {
        callEnded = false;
      }
      if (null != incomingEvent) {
        incomingEvent = null;
      }

      saver.save();
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
      saver.save();
    }
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    callEnded = true;
    saver.save();
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

}
