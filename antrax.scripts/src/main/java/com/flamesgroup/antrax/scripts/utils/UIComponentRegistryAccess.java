/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UIComponentRegistryAccess {

  private final RegistryAccess registry;

  private int max = 100;

  public UIComponentRegistryAccess(final RegistryAccess registry, final int... maxSize) {
    this.registry = registry;
    for (int i : maxSize) {
      max = i;
    }
  }

  private Set<String> getValues(final String path) {
    Set<String> retval = new HashSet<>();
    for (RegistryEntry e : registry.listEntries(path, max)) {
      String val = e.getValue();
      if (retval.contains(val)) {
        try {
          registry.remove(e);
        } catch (UnsyncRegistryEntryException | DataModificationException e1) {
        }
      } else {
        retval.add(val);
      }
    }
    return retval;
  }

  public String[] listValues(final String path) {
    Set<String> retval = getValues(path);
    return retval.toArray(new String[retval.size()]);
  }

  public void addValue(final String path, final String... value) {
    Set<String> alreadyExists = getValues(path);
    for (String v : value) {
      if (!alreadyExists.contains(v)) {
        registry.add(path, v, max);
      }
    }
  }

  /**
   * Lists filtered paths. PathPattern is used to check whether path is ok and
   * to transform it. If you want to transform path, use grouping in regex.
   * First group will be returned instead of whole path if specified
   *
   * @param pathPattern
   * @return
   */
  public String[] listPaths(final Pattern pathPattern) {
    List<String> retval = new LinkedList<>();
    for (String s : listAllPaths()) {
      Matcher matcher = pathPattern.matcher(s);
      if (matcher.matches()) {
        if (matcher.groupCount() == 0) {
          retval.add(s);
        } else {
          retval.add(matcher.group(1));
        }
      }
    }
    return retval.toArray(new String[retval.size()]);
  }

  public String[] listAllPaths() {
    return registry.listAllPaths();
  }

  public static void main(final String[] args) {
    UIComponentRegistryAccess registry = new UIComponentRegistryAccess(null) {
      @Override
      public String[] listAllPaths() {
        return new String[] {"vauchers.MTS.30.new", "vauchers.MTS.30.taken"};
      }
    };
    String[] paths = registry.listPaths(Pattern.compile("(vauchers[.][^.]*)[.].*"));
    System.out.println(Arrays.toString(paths));
  }

}
