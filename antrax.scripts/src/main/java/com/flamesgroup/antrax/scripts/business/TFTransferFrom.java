/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "TF Transfer from", doc = "TF Transfer from")
public class TFTransferFrom implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(TFTransferFrom.class);

  protected String event = "payed";
  private String actionName = "TFFindDstCardAction";

  private boolean lockOnFail = false;

  protected volatile GenericEvent incomingEvent = null;

  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "action", doc = "action to find destination card")
  public void setAction(final String action) {
    this.actionName = action;
  }

  public String getAction() {
    return actionName;
  }

  @ScriptParam(name = "lock on fail", doc = "locks card if failed to find destination card for the transfer")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @Override
  public String describeBusinessActivity() {
    return "TF Transfer from";
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return null != incomingEvent;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (channel.getSimData().getPhoneNumber() == null) {
      logger.debug("[{}] - there is no phone number, locking card", this);
      channel.lock("no PhoneNumber but using \"TransferFrom\" script");
    }
    try {
      logger.debug("[{}] - transfer from: {}", this, channel.getSimData().getPhoneNumber().getValue());

      channel.executeAction(new Action(actionName, channel.getSimData().getPhoneNumber().getValue()), TimePeriod.inMinutes(2));
      incomingEvent.respondSuccess(channel);
    } catch (Exception e) {
      logger.error("[{}] - failed to find destination card for the transfer", this, e);
      incomingEvent.respondFailure(channel, e.getMessage());
      if (lockOnFail) {
        channel.lock("TransferFrom failed: " + e.getMessage());
      }
    } finally {
      incomingEvent = null;
    }
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
    }
  }
}
