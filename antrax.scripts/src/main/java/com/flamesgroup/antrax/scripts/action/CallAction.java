/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "call action", doc = "Makes call")
public class CallAction extends CallBase {

  public CallAction() {
    super("call", LoggerFactory.getLogger(CallAction.class));
  }

  private boolean askForCallBack = false;
  private String askForCallBackEventName = "askForCallBack";

  private boolean callInsideOneGroup = true;

  @ScriptParam(name = "ask for call back", doc = "generate AskForCallBack event afer calls")
  public void setAskForCallBack(final boolean askForCallBack) {
    this.askForCallBack = askForCallBack;
  }

  public boolean getAskForCallBack() {
    return askForCallBack;
  }

  @ScriptParam(name = "ask for call back event", doc = "name of event to initiate call back")
  public void setAskForCallBackEventName(final String eventName) {
    this.askForCallBackEventName = eventName;
  }

  public String getAskForCallBackEventName() {
    return askForCallBackEventName;
  }

  @ScriptParam(name = "make calls inside one group", doc = "make calls inside one group only")
  public void setCallInsideOneGroup(final boolean callInsideOneGroup) {
    this.callInsideOneGroup = callInsideOneGroup;
  }

  public boolean getCallInsideOneGroup() {
    return callInsideOneGroup;
  }

  String extractPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length < 1) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    return args[0].toString();
  }

  PhoneNumber extractReplacedPhoneNumber(final Action action) {
    String phoneNumberStr = extractPhoneNumber(action);
    phoneNumberStr = replaceNumber(phoneNumberStr);
    return new PhoneNumber(phoneNumberStr);
  }

  @Override
  public boolean ensureCanExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    try {
      extractReplacedPhoneNumber(action);

      if (callInsideOneGroup) {
        Long simGroupId = extractSIMGroupID(action);
        return channel.getSimData().getSimGroup().getID() == simGroupId;
      } else {
        return true;
      }
    } catch (Exception e) {
      return false;
    }
  }

  Long extractSIMGroupID(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    String id = args[1].toString();
    return Long.parseLong(id);
  }

  @Override
  public void executeConcreteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - channel {} is executing action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
    PhoneNumber number = extractReplacedPhoneNumber(action);

    logger.debug("[{}] - channel {} is calling to {}", this, channel.getSimData().getPhoneNumber().toString(), number.toString());
    makeCall(channel, number);

    if (askForCallBack) {
      //We can't call another action from here because of channel.executeAction() is synchronous call and that action should be
      //performed by src channel which is currently performing this action. So, in case of usage actions we get deadlock.

      //Using events we can implement asynchronous call. Events don't support parameters.
      //This is a some kind of hack: passing src and dst phone numbers as Event successResponce and failureResponce
      //like action parameters

      //This event should initiate call back from number to this channel number
      String srcNumber = extractPhoneNumber(action);
      logger.debug("[{}] - channel {} is sending event to ask for call back from: {}", this, channel.getSimData().getPhoneNumber().toString(), srcNumber);
      GenericEvent event = GenericEvent.checkedEvent(askForCallBackEventName, srcNumber, channel.getSimData().getPhoneNumber().getValue());
      event.fireEvent(channel);
    }
  }

}
