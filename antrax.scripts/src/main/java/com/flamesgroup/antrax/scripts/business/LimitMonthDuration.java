/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.Tariffing;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

@Script(name = "limit month calls duration", doc = "drop call if it exceeds specified duration")
public class LimitMonthDuration implements BusinessActivityScript, CallsListener, StatefullScript {

  private static final long serialVersionUID = 3601390342461094591L;

  private static final Logger logger = LoggerFactory.getLogger(LimitMonthDuration.class);

  @StateField
  private volatile long durationLimitValue;
  @StateField
  private volatile long monthDuration;
  @StateField
  private volatile long nextMonthTimestamp;
  @StateField
  private volatile int dropCount;

  private TimeInterval durationLimit = new TimeInterval(60000, 120000);
  private int dropLimit = 2;
  private Tariffing tariffing = Tariffing.SECOND;
  private boolean tariffingFirstMinute = false;
  private String event = "event";

  private Integer startDayMonth = 1;

  private volatile Boolean inCall = false;
  private volatile long startCallTime;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "duration limit", doc = "limit call duration per month")
  public void setDurationLimit(final TimeInterval durationLimit) {
    this.durationLimit = durationLimit;
  }

  public TimeInterval getDurationLimit() {
    return durationLimit;
  }

  @ScriptParam(name = "drop limit", doc = "maximum numbers of dropped call, after that card will be locked")
  public void setDropLimit(final int dropLimit) {
    this.dropLimit = dropLimit;
  }

  public int getDropLimit() {
    return dropLimit;
  }

  @ScriptParam(name = "tariffing", doc = "tariffing per selected principle")
  public void setTariffing(final Tariffing tariffing) {
    this.tariffing = tariffing;
  }

  public Tariffing getTariffing() {
    return tariffing;
  }

  @ScriptParam(name = "tariffing first minute", doc = "tariffing only first minute, after tariffing per second")
  public void setTariffingFirstMinute(final boolean tariffingFirstMinute) {
    this.tariffingFirstMinute = tariffingFirstMinute;
  }

  public boolean getTariffingFirstMinute() {
    return tariffingFirstMinute;
  }

  @ScriptParam(name = "event", doc = "this event will be generated when exceeded drop limit value")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "first day of the month", doc = "starting month from this day")
  public void setStartDayMonth(final Integer startDayMonth) {
    this.startDayMonth = startDayMonth;
  }

  public Integer getStartDayMonth() {
    return startDayMonth;
  }

  @Override
  public String describeBusinessActivity() {
    return "drop call by month limit";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    channel.dropCall();
    dropCount++;
    saver.save();
    if (dropCount >= dropLimit) {
      channel.fireGenericEvent(event);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (getCurrentDate() > nextMonthTimestamp) {
      logger.debug("[{}] - new month", this);

      nextMonthTimestamp = getNextMonth();
      durationLimitValue = durationLimit.random();
      logger.debug("[{}] - durationLimitValue set to {}", this, durationLimitValue);
      monthDuration = 0;
      dropCount = 0;
      inCall = false;
      startCallTime = 0;

      saver.save();
    }

    if (inCall) {
      if (dropCount > 0) {
        return true;
      } else if (System.currentTimeMillis() - startCallTime > durationLimitValue - monthDuration) {
        logger.debug("[{}] - month call duration limit expired, drop call", this);
        return true;
      }
    }

    return dropCount <= 0 && monthDuration > durationLimitValue;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    inCall = false;
    startCallTime = 0;
    long durationCall = duration;
    long tariffingInMillis = tariffing.getTariffingInMillis();

    if (tariffingInMillis > 0) { // enable tariffing
      if (duration > TimePeriod.inMinutes(1) && tariffingFirstMinute) {
        // tariffing only first minutes
      } else {
        long mod = duration % tariffingInMillis;
        if (mod != 0) {
          durationCall = duration + tariffingInMillis - mod;
          logger.debug("[{}] - round call duration {}", this, durationCall);
        }
      }
    }

    monthDuration += durationCall;
    saver.save();
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
    inCall = true;
    startCallTime = System.currentTimeMillis();
    saver.save();
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  private long getCurrentDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.getTimeInMillis();
  }

  private long getNextMonth() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    if (cal.get(Calendar.DAY_OF_MONTH) >= startDayMonth) {
      cal.add(Calendar.MONTH, 1);
    }
    cal.set(Calendar.DAY_OF_MONTH, startDayMonth);
    return cal.getTimeInMillis();
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

}
