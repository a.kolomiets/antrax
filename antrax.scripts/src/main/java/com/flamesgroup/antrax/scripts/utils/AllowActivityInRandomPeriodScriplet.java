/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import java.io.Serializable;

public class AllowActivityInRandomPeriodScriplet implements Serializable {

  private static final long serialVersionUID = 7802981504408817801L;

  private final TimeInterval startPeriod;
  private final TimeInterval endPeriod;

  public AllowActivityInRandomPeriodScriplet(final TimeInterval startPeriod, final TimeInterval endPeriod) {
    this.startPeriod = startPeriod;
    this.endPeriod = endPeriod;
  }

  public TimeInterval getStartPeriod() {
    return startPeriod;
  }

  public TimeInterval getEndPeriod() {
    return endPeriod;
  }

  @Override
  public String toString() {
    return String.format("start: %s; end: %s", startPeriod, endPeriod);
  }

}
