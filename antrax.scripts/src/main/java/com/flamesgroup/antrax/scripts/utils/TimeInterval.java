/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import java.io.Serializable;

public class TimeInterval implements Serializable {

  private static final long serialVersionUID = -2545763218850401016L;

  private final TimePeriod min;
  private final TimePeriod max;

  public TimeInterval(final TimePeriod min, final TimePeriod max) {
    if (min.getPeriod() > max.getPeriod()) {
      this.max = min;
      this.min = max;
    } else {
      this.min = min;
      this.max = max;
    }
  }

  public TimeInterval(final long min, final long max) {
    this(new TimePeriod(min), new TimePeriod(max));
  }

  public TimePeriod getMax() {
    return max;
  }

  public TimePeriod getMin() {
    return min;
  }

  public long random() {
    if (min == max) {
      return max.getPeriod();
    }
    return min.getPeriod() + Math.round(Math.random() * (max.getPeriod() - min.getPeriod()));
  }

  @Override
  public String toString() {
    return String.format("%s..%s", min, max);
  }

}
