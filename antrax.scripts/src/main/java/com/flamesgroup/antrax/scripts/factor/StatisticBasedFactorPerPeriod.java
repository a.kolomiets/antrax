/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.factor;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.factor.StatisticField;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

import java.util.Calendar;

@Script(name = "statistic based factor per period", doc = "calculates factor basing on one of the statistic value in specified period of days")
public class StatisticBasedFactorPerPeriod extends FactorBase implements SimDataListener, StatefullScript {

  private static final long serialVersionUID = 8367378362641983472L;

  private transient SimData simData;

  private StatisticField field = StatisticField.CALLED_DURATION;
  private volatile long previousTime;
  private int period = 1;
  private boolean isFirstTimeCheck;
  private final ScriptSaver saver = new ScriptSaver();
  @StateField
  private int totalCallsCount;
  @StateField
  private int oldTotalCallsCount;
  @StateField
  private int successfullCallsCount;
  @StateField
  private int oldSuccessfullCallsCount;
  @StateField
  private long calledDuration;
  @StateField
  private long oldCalledDuration;
  @StateField
  private int totalSmsCount;
  @StateField
  private int oldTotalSmsCount;

  @ScriptParam(name = "time period", doc = "factor will be counted in this period of time")
  public void setPeriod(final int periodOfDays) {
    this.period = periodOfDays;
    this.previousTime = getCurrentMidnight();
    isFirstTimeCheck = true;
  }

  public int getPeriod() {
    return this.period;
  }

  @ScriptParam(name = "statistic value", doc = "sttaistic value used to count factor")
  public void setStatisticValue(final StatisticField field) {
    this.field = field;
  }

  public StatisticField getStatisticValue() {
    return field;
  }

  @Override
  public long countFactor() {
    return getPriority().getValue() + getMultiplier() * getFieldVal();
  }

  private int getFieldVal() {
    if (isFirstTimeCheck) {
      oldTotalCallsCount = simData.getTotalCallsCount();
      oldSuccessfullCallsCount = simData.getSuccessfulCallsCount();
      oldCalledDuration = simData.getCallDuration();
      oldTotalSmsCount = simData.getSuccessOutgoingSmsCount();
      isFirstTimeCheck = false;
      saver.save();
    } else if (periodExpired()) {
      oldTotalCallsCount = totalCallsCount;
      oldSuccessfullCallsCount = successfullCallsCount;
      oldCalledDuration = calledDuration;
      oldTotalSmsCount = totalSmsCount;
      totalCallsCount = 0;
      successfullCallsCount = 0;
      calledDuration = 0;
      totalSmsCount = 0;
      previousTime = getCurrentMidnight();
      saver.save();
    }

    switch (field) {
      case CALLED_DURATION:
        long calledDuration = simData.getCallDuration() - oldCalledDuration;
        if (this.calledDuration != calledDuration) {
          this.calledDuration = calledDuration;
          saver.save();
        }
        return (int) calledDuration;
      case SUCCESSFUL_CALLS_COUNT:
        int successfullCallsCount = simData.getSuccessfulCallsCount() - oldSuccessfullCallsCount;
        if (this.successfullCallsCount != successfullCallsCount) {
          this.successfullCallsCount = successfullCallsCount;
          saver.save();
        }
        return successfullCallsCount;
      case TOTAL_CALLS_COUNT:
        int totalCallsCount = simData.getTotalCallsCount() - oldTotalCallsCount;
        if (this.totalCallsCount != totalCallsCount) {
          this.totalCallsCount = totalCallsCount;
          saver.save();
        }
        return totalCallsCount;
      case TOTAL_SMS_COUNT:
        int totalSmsCount = simData.getSuccessOutgoingSmsCount() - oldTotalSmsCount;
        if (this.totalSmsCount != totalSmsCount) {
          this.totalSmsCount = totalSmsCount;
          saver.save();
        }
        return totalSmsCount;
    }
    return 0;
  }

  private boolean periodExpired() {
    long minutes = TimePeriod.inHours(period * 24);
    if ((getCurrentMidnight() - previousTime) > minutes) {
      previousTime = getCurrentMidnight();
      return true;
    }
    return false;
  }

  private long getCurrentMidnight() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.getTimeInMillis();
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
