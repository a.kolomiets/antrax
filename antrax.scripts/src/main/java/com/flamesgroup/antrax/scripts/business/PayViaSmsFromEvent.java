/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Script(name = "pay via SMS from event", doc = "pay phoneNumber via SMS from event, then analyzes incoming SMS and send SMS with code to the same number")
public class PayViaSmsFromEvent implements BusinessActivityScript, SMSListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(PayViaSmsFromEvent.class);

  private String event = "pay_via_sms";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));

  private String incomingTextRegex = "some text (\\d*)";
  private String smsNumber = "900";

  private String incomingSmsText;

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private PhoneNumber phoneNumber;
  private int amountForPay;
  private volatile GenericEvent caughtEvent = null;

  @ScriptParam(name = "event", doc = "start script after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  @ScriptParam(name = "incoming sms text regex", doc = "regex for parse text from incoming SMS")
  public void setIncomingTextRegex(final String incomingTextRegex) {
    this.incomingTextRegex = incomingTextRegex;
  }

  public String getIncomingTextRegex() {
    return incomingTextRegex;
  }

  @ScriptParam(name = "sms number", doc = "sms number")
  public void setSmsNumber(final String smsNumber) {
    this.smsNumber = smsNumber;
  }

  public String getSmsNumber() {
    return smsNumber;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && text != null && text.matches(incomingTextRegex)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches with pattern [{}]", this, phoneNumber, text, incomingTextRegex);
      smsWait.set(false);
      incomingSmsText = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      channel.sendSMS(new PhoneNumber(smsNumber), phoneNumber.getValue() + " " + amountForPay);
      smsWait.set(true);
    } catch (Exception e) {
      logger.error("[{}] - while send SMS", this, e);
      caughtEvent.respondFailure(channel, "Can't send SMS");
      return;
    }
    long startWaitingTime = System.currentTimeMillis();

    while (true) {
      if (incomingSmsText != null) {
        Matcher matcher = Pattern.compile(incomingTextRegex).matcher(incomingSmsText);
        String code = matcher.group();
        try {
          channel.sendSMS(new PhoneNumber(smsNumber), code);
        } catch (Exception e) {
          logger.error("[{}] - while send SMS", this, e);
          caughtEvent.respondFailure(channel, "Can't send SMS");
        }
        caughtEvent.respondSuccess(channel);
        incomingSmsText = null;
        break;
      } else if (smsWait.get() && System.currentTimeMillis() - startWaitingTime < smsTimeout.getPeriod()) {
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
      } else {
        logger.warn("[{}] - couldn't receive SMS", this);
        caughtEvent.respondFailure(channel, "Couldn't receive SMS");
        smsWait.set(false);
        break;
      }
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "pay phoneNumber via SMS";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      phoneNumber = (PhoneNumber) args[0];
      amountForPay = (int) args[1];
      if (phoneNumber == null) {
        logger.error("[{}] - phoneNumber mustn't be null", this);
        return;
      }
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}
