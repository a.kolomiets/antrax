/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.helper.callfilter.PhoneNumberMatcher;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;

import java.io.Serializable;

public abstract class FilterBase implements Serializable {

  private static final long serialVersionUID = -1939876188026933445L;

  protected transient final Logger logger;

  protected volatile String allowedBNumber;
  protected volatile String deniedBNumber;
  protected volatile String replaceBNumber;

  private transient PhoneNumberMatcher bPhoneNumberMatcher;

  public FilterBase(final Logger logger) {
    this.logger = logger;
  }

  protected PhoneNumberMatcher getBPhoneNumberMatcher(final PhoneNumber bPhoneNumber) {
    if (bPhoneNumberMatcher == null) {
      try {
        bPhoneNumberMatcher = new PhoneNumberMatcher(deniedBNumber, allowedBNumber, replaceBNumber);
        //try to substitute phone number to validate replacement string
        bPhoneNumberMatcher.substituteNumber(bPhoneNumber.getValue());
      } catch (Exception e) {
        logger.warn("[{}] - while compiling pattern", this, e);
        bPhoneNumberMatcher = new PhoneNumberMatcher(null, null, null);
      }
    }
    return bPhoneNumberMatcher;
  }

  public String getDeniedANumber() {
    return deniedBNumber;
  }

  @ScriptParam(name = "allowed B number", doc = "regular expression which will filter good B numbers")
  public void setAllowedBNumber(final String allowedBNumber) {
    this.allowedBNumber = allowedBNumber;
    bPhoneNumberMatcher = null;
  }

  public String getAllowedBNumber() {
    return allowedBNumber;
  }

  @ScriptParam(name = "denied B number", doc = "regular expression which will filter bad B numbers")
  public void setDeniedBNumber(final String deniedBNumbers) {
    this.deniedBNumber = deniedBNumbers;
    bPhoneNumberMatcher = null;
  }

  public String getDeniedBNumber() {
    return deniedBNumber;
  }

  @ScriptParam(name = "replace B number pattern", doc = "pattern which will replace B number taken values from allow matcher")
  public void setReplaceBNumber(final String replaceBNumberPattern) {
    this.replaceBNumber = replaceBNumberPattern;
    bPhoneNumberMatcher = null;
  }

  public String getReplaceBNumber() {
    return replaceBNumber;
  }

  protected boolean isBNumberMatches(final PhoneNumber bPhoneNumber) {
    return getBPhoneNumberMatcher(bPhoneNumber).matches(bPhoneNumber.getValue());
  }

  public PhoneNumber substituteBNumber(final PhoneNumber bPhoneNumber) {
    try {
      return new PhoneNumber(getBPhoneNumberMatcher(bPhoneNumber).substituteNumber(bPhoneNumber.getValue()));
    } catch (IllegalArgumentException e) {
      logger.error("[{}] - invalid replaced phone number", this, e);
    }
    return null;
  }

}
