/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "activity timeout generator", doc = "generates event for activity timeout, after registartion sim casrd in channel generates event after timeout activity passed")
public class ActivityTimeoutGenerator implements BusinessActivityScript, StatefullScript, GenericEventListener, ActivityListener, CallsListener {

  private static final long serialVersionUID = 7563720663750496218L;

  private static final Logger logger = LoggerFactory.getLogger(ActivityTimeoutGenerator.class);

  private final ScriptSaver saver = new ScriptSaver();

  private String event = "activity_timeout_generator";
  private GenericEvent genericEventAfterTimeoutActivityPassed = GenericEvent.uncheckedEvent("event_after_timeout_activity_passed");
  private GenericEvent genericEventToGenerateActivityTimeout = GenericEvent.uncheckedEvent("activity_timeout");
  private boolean dropCall;

  @StateField
  private boolean isTimeoutEvent;
  @StateField
  private boolean isEventAfterTimeout;
  private boolean isShouldStartBusinessActivity;
  private boolean isActiveCall;

  @ScriptParam(name = "event", doc = "trigger event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event after timeout activity passed", doc = "generate event when sim card will registered in channel after timeout activity passed")
  public void setEventAfterTimeout(final String event) {
    genericEventAfterTimeoutActivityPassed = GenericEvent.uncheckedEvent(event);
  }

  public String getEventAfterTimeout() {
    return genericEventAfterTimeoutActivityPassed.getEvent();
  }

  @ScriptParam(name = "event to generate activity timeout", doc = "generic event to generate activity timeout")
  public void setEventActivityTimeout(final String event) {
    genericEventToGenerateActivityTimeout = GenericEvent.uncheckedEvent(event);
  }

  public String getEventActivityTimeout() {
    return genericEventToGenerateActivityTimeout.getEvent();
  }

  @ScriptParam(name = "drop call", doc = "drop activity call when get event to generate activity timeout")
  public void setDropCall(final boolean dropCall) {
    this.dropCall = dropCall;
  }

  public boolean getDropCall() {
    return dropCall;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (dropCall) {
      logger.debug("[{}] - goin to drop call in channel {}", this, channel);
      channel.dropCall();
    }

    if (isTimeoutEvent && !isEventAfterTimeout) {
      logger.debug("[{}] - generate activity timeout event {} ", this, event);
      genericEventToGenerateActivityTimeout.fireEvent(channel);
      isShouldStartBusinessActivity = false;
    } else if (isTimeoutEvent) {
      logger.debug("[{}] - generate event after timeout {} ", this, event);
      genericEventAfterTimeoutActivityPassed.fireEvent(channel);
      isTimeoutEvent = false;
      isEventAfterTimeout = false;
      isShouldStartBusinessActivity = false;
      saver.save();
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (!dropCall && isActiveCall) {
      return false;
    } else {
      return isShouldStartBusinessActivity;
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "timeout";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      isTimeoutEvent = true;
      isShouldStartBusinessActivity = true;
      saver.save();
    }
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    if (isTimeoutEvent) {
      isEventAfterTimeout = true;
      isShouldStartBusinessActivity = true;
      saver.save();
    }
  }

  @Override
  public void handleActivityEnded() {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
    isActiveCall = true;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    isActiveCall = false;
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
    isActiveCall = false;
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

}
