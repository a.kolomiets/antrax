/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

@Script(name = "generate event on dial error", doc = "generates event on dial error cause")
public class GenerateEventOnDialError implements BusinessActivityScript, CallsListener, StatefullScript {

  private static final long serialVersionUID = -9117260359271316884L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnDialError.class);

  private int dialErrorsLimit;

  private final List<String> errorStatuses = new LinkedList<>();

  @StateField
  private volatile int dialErrorsCount;

  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event on dial error cause")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "dial errors limit", doc = "limit after which event will be generated")
  public void setDialErrorsLimit(final int dialErrorsLimit) {
    this.dialErrorsLimit = dialErrorsLimit;
  }

  public int getDialErrorsLimit() {
    return dialErrorsLimit;
  }

  @ScriptParam(name = "call error cause", doc = "call control connection management cause")
  public void addErrorStatus(final String errorStatus) {
    this.errorStatuses.add(errorStatus);
  }

  public String getErrorStatus() {
    return "";
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event on specified dial error cause code";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    dialErrorsCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return dialErrorsLimit > 0 && dialErrorsCount >= dialErrorsLimit;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (duration > 0) {
      dialErrorsCount = 0;
      saver.save();
    }
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  private boolean isErrorReportMatch(final String errorStatus) {
    if (errorStatuses.isEmpty()) {
      return false;
    }

    for (String status : errorStatuses) {
      if (status.isEmpty() || errorStatus.contains(status)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void handleDialError(final String errorStatus) {
    logger.debug("[{}] - got dial error status {}", this, errorStatus);
    if (isErrorReportMatch(errorStatus)) {
      logger.debug("[{}] - dial error '{}' match and increase dial error count: {}", this, errorStatus, ++dialErrorsCount);
      saver.save();
    }
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}
