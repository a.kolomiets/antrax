/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.payment.CallPaymentHelper;
import com.flamesgroup.antrax.helper.business.payment.PaymentHelper;
import com.flamesgroup.antrax.helper.business.payment.USSDPaymentHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "pay", doc = "pays money after event caught")
public class Pay implements BusinessActivityScript, RegistryAccessListener, GenericEventListener, SMSListener {

  private static final Logger logger = LoggerFactory.getLogger(Pay.class);

  private volatile GenericEvent eventCaugth;
  private transient RegistryAccess registry;
  private String event = "payMe";
  private int amount = 10;
  private double minimumAmount = 5;
  private boolean lockOnFail = true;
  private boolean skipIntermStates = true;
  private boolean confirmPayment;
  private PaymentHelper paymentHelper;
  private CheckBalanceHelper checkBalanceHelper;
  private GenericEvent payedEvent = GenericEvent.uncheckedEvent("payed");

  @ScriptParam(name = "balance check data", doc = "Check current balance of SIM card. Checking may take up to 4 minutes")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*");
  }

  @ScriptParam(name = "event", doc = "payment will be performed after this event occurs")
  public void setEvent(final String event) {
    if (event == null) {
      throw new NullPointerException();
    }
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "payment data", doc = "data for payment")
  public void setPaymentHelper(final PaymentHelper paymentHelper) {
    this.paymentHelper = paymentHelper;
  }

  public PaymentHelper getPaymentHelper() {
    return new USSDPaymentHelper("vauchers.mts", "*100*$#", "3APAXOBAHO.*", "BHECEHO.*", "Recharge.*");
  }

  @ScriptParam(name = "amount", doc = "amount of money to pay")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "lock on fail", doc = "locks card on payment failure")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @ScriptParam(name = "skip intermediate states", doc = "skip the intermediate states like failed.1, failed.2, failed.3")
  public void setSkipIntermStates(final boolean skipIntermStates) {
    this.skipIntermStates = skipIntermStates;
  }

  public boolean getSkipIntermStates() {
    return skipIntermStates;
  }


  @ScriptParam(name = "minimum amount", doc = "minimum amount of money which is enough to cancel payment. Put 0 here to discard balance check")
  public void setMinimumAmount(final double amount) {
    this.minimumAmount = amount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "paid event", doc = "this event generates on successfully paying")
  public void setPayedEvent(final String event) {
    this.payedEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getPayedEvent() {
    return payedEvent.getEvent();
  }

  @ScriptParam(name = "confirm payment", doc = "send voucher value as answer to confirm payment")
  public void setConfirmPayment(final boolean confirmPayment) {
    this.confirmPayment = confirmPayment;
  }

  public boolean getConfirmPayment() {
    return confirmPayment;
  }

  @Override
  public String describeBusinessActivity() {
    return "payment";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) {
    logger.debug("[{}] - going to pay {}", this, channel);
    try {
      if (minimumAmount < 1 || checkBalanceHelper.getBalanceValue(channel, registry) < minimumAmount) {
        paymentHelper.pay(channel, registry, amount, skipIntermStates, confirmPayment);
      }
      eventCaugth.respondSuccess(channel);
      payedEvent.fireEvent(channel);
    } catch (Exception e) {
      logger.warn("[{}] - failed to pay", this, e);
      eventCaugth.respondFailure(channel, e.getMessage());
      if (lockOnFail) {
        channel.lock("PaymentFailed: " + e.getMessage());
      }
    } finally {
      eventCaugth = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return eventCaugth != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      eventCaugth = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (paymentHelper instanceof CallPaymentHelper) {
      ((CallPaymentHelper) paymentHelper).handleIncomingSMS(phoneNumber, text);
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

}
