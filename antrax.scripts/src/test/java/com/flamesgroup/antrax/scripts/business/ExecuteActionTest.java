/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.PhoneNumber;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ExecuteActionTest {

  @Mocked
  private RegisteredInGSMChannel channel;

  @Test
  public void acceptanceTestSucess() throws Exception {
    ExecuteAction script = new ExecuteAction();
    script.setAttemptsCount(2);
    script.handleGenericEvent("exec_action", GenericEvent.EventType.CHECKED, "success", "failure");

    new Expectations() {{
      channel.getSimData();
      result = new SimData().setPhoneNumber(new PhoneNumber("111"));
      channel.executeAction(new Action("action", new PhoneNumber("111")), 100 * 1000);
      channel.fireGenericEvent("success");
    }};

    script.invokeBusinessActivity(channel);
  }

  @Test
  public void acceptanceTestNoNumber(@Injectable final RegisteredInGSMChannel channel) throws Exception {
    ExecuteAction script = new ExecuteAction();
    script.setAttemptsCount(2);
    script.handleGenericEvent("exec_action", GenericEvent.EventType.CHECKED, "success", "failure");

    new Expectations() {{
      channel.getSimData();
      result = new SimData();
      channel.lock(withMatch("Failed to execute action \\[action\\] cause .* has no number"));
    }};

    script.invokeBusinessActivity(channel);
  }

}
