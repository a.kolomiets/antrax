/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.activity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerActivity;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import org.junit.Test;

public class LimitCallAttemptsPerActivityTest {

  @Test
  public void testAllowedFirstTime() {
    LimitCallAttemptsPerActivity limit = new LimitCallAttemptsPerActivity();
    limit.setAttemptsLimit(new VariableLong(1, 1));
    limit.setSimData(new SimData());
    assertTrue(limit.isActivityAllowed());
    assertTrue(limit.isActivityAllowed());
    assertTrue(limit.isActivityAllowed());
  }

  @Test
  public void testAllowedFirstTimeWithNonZeroInitialValue() {
    LimitCallAttemptsPerActivity limit = new LimitCallAttemptsPerActivity();
    limit.setAttemptsLimit(new VariableLong(1, 1));
    SimData simData = new SimData();
    simData.setTotalCallsCount(10);
    limit.setSimData(simData);
    assertTrue(limit.isActivityAllowed());
    assertTrue(limit.isActivityAllowed());
    assertTrue(limit.isActivityAllowed());
  }

  @Test
  public void testForbidAfterCalls() {
    LimitCallAttemptsPerActivity limit = new LimitCallAttemptsPerActivity();
    limit.setAttemptsLimit(new VariableLong(5, 5));
    SimData simData = new SimData();
    simData.setTotalCallsCount(7);
    limit.setSimData(simData);
    limit.handlePeriodStart();
    assertTrue(limit.isActivityAllowed());
    simData.setTotalCallsCount(7 + 6);
    assertFalse(limit.isActivityAllowed());
  }

  @Test
  public void testForbidAfterExactlySameCalls() {
    LimitCallAttemptsPerActivity limit = new LimitCallAttemptsPerActivity();
    limit.setAttemptsLimit(new VariableLong(5, 5));
    SimData simData = new SimData();
    simData.setTotalCallsCount(7);
    limit.setSimData(simData);
    limit.handlePeriodStart();
    assertTrue(limit.isActivityAllowed());
    simData.setTotalCallsCount(7 + 5);
    assertFalse(limit.isActivityAllowed());
  }

  @Test
  public void testAllowsAfterSessionEnd() {
    LimitCallAttemptsPerActivity limit = new LimitCallAttemptsPerActivity();
    limit.setAttemptsLimit(new VariableLong(1, 1));
    SimData simData = new SimData();
    simData.setTotalCallsCount(0);
    limit.setSimData(simData);

    limit.handlePeriodStart();
    simData.setTotalCallsCount(2);
    limit.handlePeriodEnd();
    assertTrue(limit.isActivityAllowed());
  }
}
