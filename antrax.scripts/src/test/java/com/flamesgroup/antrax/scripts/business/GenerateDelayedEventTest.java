/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.junit.Test;

import java.io.Serializable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class GenerateDelayedEventTest {

  @Test
  public void testDelayedEventWithLock() throws Exception {
    final long timeout = 500;

    GenerateDelayedEvent script = new GenerateDelayedEvent();

    script.setTimeout(new TimePeriod(timeout));
    script.setReceiveEvent("activate");
    script.setSendEvent("sendMe");

    final boolean lock = true;
    script.setLockOnFailure(lock);

    script.handleGenericEvent("activate");
    assertTrue(script.shouldStartBusinessActivity());

    final Semaphore eventSem = new Semaphore(0);
    final AtomicReference<String> failureResponce = new AtomicReference<>(null);
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        if ("sendMe".equals(event)) {
          if (args.length == 3) {
            failureResponce.set((String) args[2]);
          }
          eventSem.release();
        }
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

    });
    assertTrue(eventSem.tryAcquire(timeout, TimeUnit.MILLISECONDS));

    assertTrue(failureResponce.get() != null);

    script.handleGenericEvent(failureResponce.get(), "unknown");
    assertTrue(script.shouldStartBusinessActivity());

    final AtomicReference<Boolean> locked = new AtomicReference<>(Boolean.FALSE);
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public void lock(final String reason) {
        locked.set(Boolean.TRUE);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

    });
    assertTrue(locked.get());
  }

  @Test
  public void testNonGreedyDelay() throws Exception {
    GenerateDelayedEvent script = new GenerateDelayedEvent();

    script.setTimeout(new TimePeriod(200));
    script.setReceiveEvent("activate");
    script.setSendEvent("sendMe");
    script.setGreedy(false);

    script.handleGenericEvent("activate");
    long time = System.currentTimeMillis();
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter());
    assertFalse(script.shouldStartBusinessActivity());
    assertTrue("Script should not sleep in thread", System.currentTimeMillis() - time < 200);
    Thread.sleep(250);
    assertTrue(script.shouldStartBusinessActivity());
  }

}
