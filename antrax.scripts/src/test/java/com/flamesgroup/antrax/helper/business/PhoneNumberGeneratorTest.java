/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PhoneNumberGeneratorTest {

  @Test
  public void testRegularNumber() {
    assertEquals("7", generate("7"));
  }

  @Test
  public void testSet() {
    assertTrue(generate("[123]").matches("[123]"));
    assertGeneratesNotSame("[123]");
  }

  @Test
  public void testAsterisk() {
    assertTrue(generate(".").matches("[0-9]"));
    assertGeneratesNotSame(".");
  }

  @Test
  public void testCombinations() {
    assertTrue(generate("8050....").matches("8050\\d{4}"));
    assertGeneratesNotSame("8050....");
    assertTrue(generate("8.[56][06]7").matches("8\\d[56][06]7"));
    assertGeneratesNotSame("8.[56][06]7");
  }

  @Test
  public void testVariations() {
    assertTrue(generate("8050|8066").matches("8050|8066"));
    assertGeneratesNotSame("8050|8066");
    assertTrue(generate(".34|255").matches("\\d34|255"));
    assertGeneratesNotSame(".34|255");
  }

  @Test
  public void testBlocks() {
    assertEquals("5372", generate("5(37)2"));
    assertEquals("5372", generate("5(37)(2)"));
    assertEquals("12345", generate("1(2(3(4(5))))"));
    assertEquals("12345", generate("(((((1)2)3)4)5)"));
    assertEquals("12345", generate("(1(2(3))4)5"));
    assertEquals("12", generate("(1|1)2"));
    assertEquals("12345", generate("1(2|2)345"));
    assertEquals("123", generate("(1|1)2(3|3)"));

    assertTrue(generate("5(3|1(2|6))7").matches("5(3|1(2|6))7"));

    assertTrue(generate("8(050|066)").matches("8050|8066"));
    assertGeneratesNotSame("8(050|066)");
    assertTrue(generate("8(050|066|040)...").matches("8050\\d{3}|8066\\d{3}|8040\\d{3}"));
  }

  private void assertGeneratesNotSame(final String pattern) {
    String generated = generate(pattern);
    for (int i = 0; i < 100; ++i) {
      if (!generated.equals(generate(pattern))) {
        return;
      }
    }
    fail("generation by pattern " + pattern + " produces same numbers");
  }

  private String generate(final String pattern) {
    return new PhoneNumberGenerator(pattern).generate().getValue();
  }
}
