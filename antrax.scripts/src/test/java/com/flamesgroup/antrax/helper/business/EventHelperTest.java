/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.business.RegisteredInGSMChannelAdapter;
import org.junit.Test;

public class EventHelperTest {

  @Test
  public void testSucceed() throws Exception {
    GenericEvent event = GenericEvent.checkedEvent("event", "event343 succed", "event343 failed");
    EventHelper helper = new EventHelper(event, 100);
    assertEquals(EventStatus.READY, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.generateEvent(new RegisteredInGSMChannelAdapter());
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    Thread.sleep(50);
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.handleEvent("event343 succed");
    assertEquals(EventStatus.SUCCEED, helper.getStatus());
    assertTrue(helper.isFinished());
  }

  @Test
  public void testFailed() throws Exception {
    GenericEvent event = GenericEvent.checkedEvent("event", "event343 succed", "event343 failed");
    EventHelper helper = new EventHelper(event, 100);
    assertEquals(EventStatus.READY, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.generateEvent(new RegisteredInGSMChannelAdapter());
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    Thread.sleep(50);
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.handleEvent("event343 failed", "failed");
    assertTrue(helper.isFinished());
    assertEquals(EventStatus.FAILED, helper.getStatus());
    assertEquals("event event failed cause failed", helper.getFailReason());
    assertTrue(helper.isFinished());
  }

  @Test
  public void testTimeout() throws Exception {
    GenericEvent event = GenericEvent.checkedEvent("event", "event343 succed", "event343 failed");
    EventHelper helper = new EventHelper(event, 100);
    assertEquals(EventStatus.READY, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.generateEvent(new RegisteredInGSMChannelAdapter());
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    Thread.sleep(50);
    assertEquals(EventStatus.WAITING, helper.getStatus());
    assertFalse(helper.isFinished());
    helper.handleEvent("wrong event");
    Thread.sleep(70);
    assertTrue(helper.isFinished());
    assertEquals(EventStatus.TIMEOUT, helper.getStatus());
    assertEquals("timeout for event event elapsed", helper.getFailReason());
    assertTrue(helper.isFinished());
  }

}
