/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.guiclient.commadapter.StatisticGatewaysAdapter;
import com.flamesgroup.antrax.control.guiclient.domain.ServerChannelRowKey;
import com.flamesgroup.antrax.control.guiclient.widgets.RefreshableTable;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelUidTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.PercentTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.VoiceServerStatisticTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.SearchFilter;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.SearchItem;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.ChannelUID;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class StatisticVoiceServersTable extends RefreshableTable implements SearchItem {

  private static final long serialVersionUID = 2532891035292808213L;

  private final StatisticVoiceServersTableModel model;

  private final List<ServerChannelRowKey> selections = new ArrayList<>();

  private final String[] columnToolTips = {
      VoiceServerStatisticTableTooltips.getVoiceServerTooltip(),
      VoiceServerStatisticTableTooltips.getMobileChannelTooltip(),
      VoiceServerStatisticTableTooltips.getAsrTooltip(),
      VoiceServerStatisticTableTooltips.getAcdTooltip(),
      VoiceServerStatisticTableTooltips.getSuccessfulCallsTooltip(),
      VoiceServerStatisticTableTooltips.getTotalCallsTooltip(),
      VoiceServerStatisticTableTooltips.getCallsDurationTooltip(),
      VoiceServerStatisticTableTooltips.getSuccessSmsTooltip(),
      VoiceServerStatisticTableTooltips.getTotalSmsTooltip(),
      VoiceServerStatisticTableTooltips.getSmsAsrTooltip(),
      VoiceServerStatisticTableTooltips.getIncomingTotalSmsTooltip()
  };

  public StatisticVoiceServersTable() {
    setName(StatisticVoiceServersTable.class.getSimpleName());

    setColumnToolTips(columnToolTips);

    model = new StatisticVoiceServersTableModel();
    setModel(model);

    for (int column = 0; column < model.getColumnCount(); column++) {
      if (model.getColumnClass(column) == ChannelUID.class) {
        getColumnModel().getColumn(column).setCellRenderer(new ChannelUidTCRenderer());
      }
    }

    TableColumnModel cm = getColumnModel();

    cm.getColumn(StatisticVoiceServersTableModel.Column.ASR.getColumnIndex()).setCellRenderer(new PercentTCRenderer());
    cm.getColumn(StatisticVoiceServersTableModel.Column.SMS_ASR.getColumnIndex()).setCellRenderer(new PercentTCRenderer());
    cm.getColumn(StatisticVoiceServersTableModel.Column.ACD.getColumnIndex()).setCellRenderer(new TimeoutTCRenderer());
    cm.getColumn(StatisticVoiceServersTableModel.Column.CALLS_DURATION.getColumnIndex()).setCellRenderer(new TimeoutTCRenderer());

    cm.getColumn(StatisticVoiceServersTableModel.Column.MOBILE_GATEWAY.getColumnIndex()).setPreferredWidth(80);
    cm.getColumn(StatisticVoiceServersTableModel.Column.ACD.getColumnIndex()).setPreferredWidth(75);
    cm.getColumn(StatisticVoiceServersTableModel.Column.CALLS_DURATION.getColumnIndex()).setPreferredWidth(75);
    cm.getColumn(StatisticVoiceServersTableModel.Column.ASR.getColumnIndex()).setPreferredWidth(40);

    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    cm.getColumn(StatisticVoiceServersTableModel.Column.TOTAL_CALLS.getColumnIndex()).setCellRenderer(renderer);

    renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    cm.getColumn(StatisticVoiceServersTableModel.Column.SUCCESS_CALLS.getColumnIndex()).setCellRenderer(renderer);

    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  @Override
  protected void refreshIndicators() {
  }

  @Override
  public void applySearchFilter(final SearchFilter searchFilter) {
  }

  @Override
  public String getSearchItemName() {
    return "Voice Servers statistic";
  }

  public void refreshData(final List<StatisticGatewaysAdapter> data) {
    model.refreshData(data);
    SwingUtilities.invokeLater(() -> {
      rememberSelections();
      model.refreshUI();
      restoreSelections();
    });
  }

  @Override
  public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
    Component cmp = super.prepareRenderer(renderer, row, column);
    boolean isServerRow = model.isServerRow(row);
    cmp.setFont(cmp.getFont().deriveFont(isServerRow ? Font.BOLD : Font.PLAIN));
    return cmp;
  }

  @Override
  public void setSearchItemName(final String name) {
    // TODO Auto-generated method stub
  }

  public IServerData getSelectedServer() {
    int row = getSelectedRow();
    if (row < 0) {
      return null;
    } else {
      return model.getServer(row);
    }
  }

  public boolean isSelectedServer(final int row) {
    return model.isServerRow(row);
  }

  private void rememberSelections() {
    selections.clear();
    int[] selectedRows = getSelectedRows();
    for (int row : selectedRows) {
      ServerChannelRowKey key = model.getKey(row);
      if (key != null) {
        selections.add(key);
      }
    }
  }

  private void restoreSelections() {
    for (ServerChannelRowKey key : selections) {
      int row = model.getRow(key);
      if (row > -1) {
        getSelectionModel().addSelectionInterval(row, row);
      }
    }
    selections.clear();
  }

}
