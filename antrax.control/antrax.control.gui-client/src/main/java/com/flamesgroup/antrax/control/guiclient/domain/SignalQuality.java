/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.domain;

import java.text.MessageFormat;

/**
 * Signal quality class for GSM module TC35i.
 */
public final class SignalQuality {

  public static final int MIN_SIGNAL_STRENGTH = -112;
  public static final int MAX_SIGNAL_STRENGTH = -51;

  private static final int NOT_DETICTABLE_BER = 99;
  private static final int NOT_DETICTABLE_RSSI = 99;

  private final int signalStrength;
  private final int bitErrorRate;

  private final double lowBER;
  private final double highBER;

  public SignalQuality(final int signalStrength, final int bitErrorRate) {
    this.signalStrength = signalStrength;
    this.bitErrorRate = bitErrorRate;

    double localLowBER = lowBERToPercent(bitErrorRate);
    highBER = highBERToPercent(bitErrorRate);
    if (Double.compare(localLowBER, lowBERToPercent(0)) == 0) {
      lowBER = 0.0d;
    } else {
      lowBER = localLowBER;
    }
  }

  private double lowBERToPercent(final int ber) {
    return Math.pow(2, ber) / 10.0d;
  }

  private double highBERToPercent(final int ber) {
    return 2 * lowBERToPercent(ber);
  }

  public int getBER() {
    return bitErrorRate;
  }

  public boolean isSignalStrengthDetectable() {
    return (signalStrength != NOT_DETICTABLE_RSSI);
  }

  public int getSignalStrength() {
    return signalStrength;
  }

  public boolean isBERDetectable() {
    return (bitErrorRate != NOT_DETICTABLE_BER);
  }

  public double getLowBERPercent() {
    return lowBER;
  }

  public double getHighBERPercent() {
    return highBER;
  }

  @Override
  public String toString() {
    StringBuilder dBmStr = new StringBuilder();
    StringBuilder berStr = new StringBuilder(", ber: ");
    if (isSignalStrengthDetectable()) {
      int dBm = getSignalStrength();
      if (dBm <= SignalQuality.MIN_SIGNAL_STRENGTH) {
        dBmStr.append("<= ");
      } else if (dBm >= SignalQuality.MAX_SIGNAL_STRENGTH) {
        dBmStr.append(">= ");
      }
      dBmStr.append(String.valueOf(dBm));
    } else {
      dBmStr.append('?');
    }

    if (isBERDetectable()) {
      if (lowBER == 0) {
        berStr.append(MessageFormat.format("< {0} %", getHighBERPercent()));
      } else {
        berStr.append(MessageFormat.format("{0}-{1} %", getLowBERPercent(), getHighBERPercent()));
      }
    }
    return String.format("%s dBm%s", dBmStr, berStr);
  }

}
