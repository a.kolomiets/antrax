/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.CallBackRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.guiclient.utils.UIConstants;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.TableFilterPanelBuilder;
import com.flamesgroup.antrax.control.transfer.AntraxTransferHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.*;
import javax.swing.border.Border;

/**
 * Base class for all editor panels consists of toolbar, context searcher, table
 * and editor box
 */
public abstract class BaseEditorPanel<T, K extends Comparable<K>> extends JPanel implements AppPanel {

  private static final long serialVersionUID = -4059373718747326496L;

  private static final Logger logger = LoggerFactory.getLogger(BaseEditorPanel.class);

  public volatile String selectElement;

  final class BaseEditorPanelTransferHandler extends AntraxTransferHandler {
    private static final long serialVersionUID = 3630791672477041992L;

    public void importObject(final String elem) {
      selectElement = elem;
      refresherThread.forceRefresh();
    }
  }

  public enum SplitterOrientation {
    VERTICAL,
    HORIZONTAL;

    public int getSwingValue() {
      switch (this) {
        case HORIZONTAL:
          return JSplitPane.HORIZONTAL_SPLIT;
        case VERTICAL:
          return JSplitPane.VERTICAL_SPLIT;
      }

      return -1;
    }
  }

  private RefresherThread refresherThread;
  private TransactionManager transactionManager;
  private final SplitterOrientation orientation;
  private JUpdatableTable<T, K> table;
  private BaseEditorPanelController<T> controller;
  private BeanCommunication<T> communication;
  private BaseEditorPanelPreprocessor<T> preprocessor;
  private BaseEditorBox<T> editorBox;
  private JButton addBtn;
  private JButton removeBtn;
  private JButton copyBtn;
  private CallBackRefresher<T[]> refresher;
  private final boolean showBorder;
  private Component toolbar;
  private boolean active = false;

  private Component[] toolbarComponents;

  private JContextSearch contextSearch;

  @Override
  public void setEditable(final boolean value) {
    logger.debug("[{}] - editable {}", this, value);
    checkEditingPossibility();

    getAddButton().setEnabled(value);
    getRemoveButton().setEnabled(value);
    getCopyButton().setEnabled(value);

    for (Component cmp : toolbarComponents) {
      if (isDisableAllowed(cmp)) {
        cmp.setEnabled(value);
      }
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void release() {
    table.getUpdatableTableProperties().saveProperties();
    if (refresher != null) {
      refresher.interrupt();
    }
  }

  @Override
  public void setActive(final boolean active) {
    setEditable(transactionManager.isEditTransactionStarted());

    if (active) {
      refresherThread.addRefresher(getRefresher());
      refreshEditors();
    } else {
      refresherThread.removeRefresher(getRefresher());
    }

    synchronized (this) {
      this.active = active;
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
    this.transactionManager = transactionManager;
    transactionManager.addListener(new TransactionListener() {

      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
        if (newState == TransactionState.ROLLBACK) {
          getController().clearChanges();
        }
        setEditable(transactionManager.isEditTransactionStarted());
        fireChangesMade();
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        return getController().syncChanges(beansPool, transactionId);
      }
    });
  }

  public BaseEditorPanel() {
    this(SplitterOrientation.VERTICAL, true);
    setTransferHandler(new BaseEditorPanelTransferHandler());
  }

  public BaseEditorPanel(final SplitterOrientation orientation, final boolean showBorder) {
    this.orientation = orientation;
    this.showBorder = showBorder;

    setLayout(new GridBagLayout());
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 1, 1));
    panel.add(getToolBar());

    TableFilterPanelBuilder<T> filterBuilder = new TableFilterPanelBuilder<>(table);
    setupFilters(filterBuilder);
    if (filterBuilder.getFilterCount() > 0) {
      panel.add(filterBuilder.getFilterPanel());
    }
    add(createSplitPane(getTable(), getEditorBox()), new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 1, 1));
    enableCopy(false);

    getCommunication();

    if (getEditorBox().supportsMultipleEditing()) {
      getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    } else {
      getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    getTable().setEnabled(enabled);
    getToolBar().setEnabled(enabled);
  }

  public void clear() {
    getTable().clearData();
  }

  protected abstract BaseEditorBox<T> createEditorBox(RefresherThread refresherThread, TransactionManager transactionManager);

  protected abstract TableBuilder<T, K> createTableBuilder();

  protected abstract BeanCommunication<T> createBeanCommunication();

  protected Component[] getToolbarComponents() {
    return new Component[0];
  }

  protected void setupFilters(final TableFilterPanelBuilder<T> filterBuilder) {
  }

  protected boolean isDisableAllowed(final Component comp) {
    return true;
  }

  protected EditingPreprocessor<T> createEditingPreprocessor() {
    return new DefaultEditingPreprocessor<>();
  }

  protected void addElement() {
    int transactionId = getTransactionId();

    logger.debug("[{}] - creating new elem", this);
    T elem = getController().createElem(getAddButton(), refresherThread, transactionId);
    if (elem == null) {
      return;
    }

    addElement(elem);
    fireChangesMade();
  }

  protected void addElement(T elem) {
    int transactionId = getTransactionId();
    logger.debug("[{}] - Preprocessing add new elem", this);
    try {
      elem = getPreprocessor().beforeAdd(elem, transactionId, BaseEditorPanel.this);
    } catch (OperationCanceledException ignored) {
      return;
    }

    logger.debug("[{}] - updating new elem: {}", this, elem);
    if (elem != null) {
      logger.debug("[{}] - insert in table new elem: {}", this, elem);
      getTable().selectRow(getTable().insertElem(elem));
    }
  }

  protected abstract boolean isEditorBoxStretched();

  protected Dimension getTableMinSize() {
    return new Dimension(100, 200);
  }

  protected Dimension getTablePreferredSize() {
    return null;
  }

  public void enableCopy(final boolean enable) {
    getCopyButton().setVisible(enable);
  }

  private Component createSplitPane(final JUpdatableTable<T, K> table, final BaseEditorBox<T> editorBox) {
    JScrollPane tableScrollPane = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(table);
    tableScrollPane.setMinimumSize(getTableMinSize());

    if (getTablePreferredSize() != null) {
      tableScrollPane.setPreferredSize(getTablePreferredSize());
    }

    if (isEditorBoxStretched()) {
      JScrollPane editorScrollPane = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(editorBox);

      JSplitPane splitPane = new JSplitPane(orientation.getSwingValue());
      switch (orientation) {
        case VERTICAL:
          splitPane.add(tableScrollPane, JSplitPane.TOP);
          splitPane.add(editorScrollPane, JSplitPane.BOTTOM);
          break;
        case HORIZONTAL:
          splitPane.add(tableScrollPane, JSplitPane.LEFT);
          splitPane.add(editorScrollPane, JSplitPane.RIGHT);
          break;
      }

      return splitPane;
    } else {
      if (showBorder) {
        Border border = BorderFactory.createMatteBorder(1, 1, 1, 0, UIConstants.BORDER_COLOR);
        editorBox.setBorder(border);
        tableScrollPane.setBorder(border);
      }

      JPanel panel = new JPanel(new BorderLayout());
      panel.add(tableScrollPane, BorderLayout.CENTER);
      switch (orientation) {
        case VERTICAL:
          panel.add(editorBox, BorderLayout.SOUTH);
          break;
        case HORIZONTAL:
          panel.add(editorBox, BorderLayout.EAST);
          break;
      }
      return panel;
    }
  }

  private Component getToolBar() {
    if (toolbar == null) {
      JContextSearch cs = getContextSearch();
      cs.registerSearchItem(getTable(), "main table");

      JReflectiveBar bar = new JReflectiveBar();
      bar.addToLeft(getAddButton());
      bar.addToLeft(getRemoveButton());
      bar.addToLeft(getCopyButton());

      toolbarComponents = getToolbarComponents();
      for (Component cmp : toolbarComponents) {
        bar.addToLeft(cmp);
      }

      bar.addToRight(cs);
      toolbar = bar;
    }
    return toolbar;
  }

  private JContextSearch getContextSearch() {
    if (contextSearch == null) {
      contextSearch = new JContextSearch();
    }
    return contextSearch;
  }

  protected int getTransactionId() {
    Integer id = transactionManager.getEditTransactionId();
    return (id == null) ? -1 : id;
  }

  protected BaseEditorPanelPreprocessor<T> getPreprocessor() {
    if (preprocessor == null) {
      preprocessor = new BaseEditorPanelPreprocessor<>(refresherThread, createEditingPreprocessor());
    }
    return preprocessor;
  }

  protected JButton getAddButton() {
    if (addBtn == null) {
      addBtn = createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"));
      addBtn.setToolTipText("Add");
      addBtn.addActionListener(e -> addElement());
    }
    return addBtn;
  }

  protected JButton getRemoveButton() {
    if (removeBtn == null) {
      removeBtn = createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"));
      removeBtn.setToolTipText("Remove");
      removeBtn.addActionListener(e -> {
        for (T elem : getSelectedElems()) {
          if (elem != null) {
            logger.debug("[{}] - preprocessing delete elem", this);
            try {
              getPreprocessor().beforeRemove(elem, getTransactionId(), getRemoveButton());
            } catch (OperationCanceledException ignored) {
              return;
            }

            logger.debug("[{}] removing {}", this, elem);
            getController().removeElem(elem);
            getTable().removeElem(elem);
            fireChangesMade();
            refreshEditors();
          }
        }
      });
    }
    return removeBtn;
  }

  protected JButton getCopyButton() {
    if (copyBtn == null) {
      copyBtn = createToolbarButton(IconPool.getShared("/img/buttons/button-copy.png"));
      copyBtn.setToolTipText("Copy");
      copyBtn.addActionListener(e -> {
        for (T elem : getSelectedElems()) {
          if (elem != null) {
            int transactionId = getTransactionId();

            logger.debug("[{}] - creating new elem for copy", this);
            T copyElem = getController().createElem(getAddButton(), refresherThread, transactionId);
            if (copyElem == null) {
              return;
            }

            logger.debug("[{}] - copying {}", this, elem);
            copyElem = getController().copyElem(elem, copyElem, copyBtn, refresherThread, transactionManager.getEditTransactionId());

            logger.debug("[{}] - copy element {}", this, copyElem);
            if (copyElem != null) {
              getTable().selectRow(getTable().insertElem(copyElem));
            }
          }
        }
      });
    }
    return copyBtn;
  }

  private List<T> getSelectedElems() {
    return getTable().getSelectedElems();
  }

  private JButton createToolbarButton(final Icon icon) {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
    retval.setEnabled(false);
    return retval;
  }

  BaseEditorBox<T> getEditorBox() {
    if (editorBox == null) {
      editorBox = createEditorBox(refresherThread, transactionManager);
      editorBox.addElementModifiedHandler(elem -> {
        if (!transactionManager.isEditTransactionStarted()) {
          logger.debug("[{}] - EditTransaction is not started", this);
          return;
        }
        logger.debug("[{}] - preprocessing add ElementModifiedHandler", this);
        try {
          getPreprocessor().beforeUpdate(elem, getTransactionId(), editorBox);
        } catch (OperationCanceledException ignored) {
          return;
        }

        logger.debug("[{}] - modified {}", this, elem);
        getTable().updateElemAt(elem, getTable().getViewRowOf(elem));
        if (transactionManager.isEditTransactionStarted()) {
          getController().updateElem(elem);
        }
        fireChangesMade();
      });
    }
    return editorBox;
  }

  protected JUpdatableTable<T, K> getTable() {
    if (table == null) {
      table = new JUpdatableTable<>(createTableBuilder());
      table.addElementSelectionListener((currSelection, prevSelection) -> refreshEditors());
      table.setName(getClass().getSimpleName());
      table.getUpdatableTableProperties().restoreProperties();
    }
    return table;
  }

  private void refreshEditors() {
    setEditable(transactionManager.isEditTransactionStarted());
    getEditorBox().setData(getSelectedElems());
  }

  protected BaseEditorPanelController<T> getController() {
    if (controller == null) {
      controller = new BaseEditorPanelController<>(getCommunication());
    }
    return controller;
  }

  protected BeanCommunication<T> getCommunication() {
    if (communication == null) {
      communication = createBeanCommunication();
    }
    return communication;
  }

  public synchronized boolean isActive() {
    return this.active;
  }

  private CallBackRefresher<T[]> getRefresher() {
    if (refresher == null) {
      refresher = new CallBackRefresher<T[]>(getClass().getSimpleName(), createActionHandler()) {
        private volatile boolean firstRead = true;

        {
          transactionManager.addListener(new TransactionListener() {

            @Override
            public void onTransactionStateChanged(final TransactionState newState) {
              if (newState == TransactionState.ROLLBACK) {
                logger.debug("[{}] - transaction rolled back, so need to reread changes", this);
                firstRead = true;
              }
            }

            @Override
            public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
              return Collections.emptyList();
            }
          });
        }

        @Override
        public void handleDisabled(final BeansPool beansPool) {
        }

        @Override
        public void handleEnabled(final BeansPool beansPool) {
        }

        @Override
        protected T[] performAction(final BeansPool beansPool) throws Exception {
          logger.debug("[{}] - refreshing", this);
          int id = getTransactionId();
          logger.debug("[{}] - reading whether we need to read anything", this);
          if (!firstRead && !getCommunication().checkReadRequired(beansPool, id)) {
            logger.debug("[{}] - we don't need to read anything", this);
            return null;
          }
          firstRead = false;
          logger.debug("[{}] - going to ask communication to list elem", this);
          T[] retval = getCommunication().listElems(beansPool, id);
          logger.debug("[{}] - listed {}", this, Arrays.toString(retval));
          return retval;
        }
      };
    }
    return refresher;
  }

  protected void refreshData(final T[] data) {
    try {
      if (data == null) { // if onActionRefresherFailure()
        return;
      }

      if (isActive()) {
        logger.debug("[{}] - updating table with {}", this, Arrays.toString(data));
        getTable().setData(data);

        if (!transactionManager.isEditTransactionStarted()) {
          T elem = getTable().getSelectedElem();
          if (elem == null) {
            getTable().clearSelection(true);
          } else {
            refreshEditors();
          }
        }
      } else {
        getTable().clearData();
      }
    } finally {
      if (selectElement != null) {
        getContextSearch().setText("\"" + selectElement + "\"");
        getContextSearch().applyFilter();
        if (table.getRowCount() > 0) {
          table.selectRow(0);
        }
        selectElement = null;
      }
    }
  }

  private ActionCallbackHandler<T[]> createActionHandler() {
    return new ActionCallbackHandler<T[]>() {
      private volatile T[] data;

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(BaseEditorPanel.this, "Failed: ","Failed: ", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final T[] result) {
        logger.debug("[{}] - got {} for update", this, Arrays.toString(result));
        this.data = result;
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
        if (data == null) {
          return;
        }
        refresherThread.removeRefresher(getRefresher());

        refreshData(data);

        if (isActive()) {
          refresherThread.addRefresher(getRefresher());
        }
      }
    };
  }

  private void checkEditingPossibility() {
    boolean editable = transactionManager.isEditTransactionStarted() && (!getSelectedElems().isEmpty());
    getEditorBox().setEditable(editable);
  }

  protected void fireChangesMade() {
    logger.debug("[{}] - forcing refresh", this);
    refresherThread.forceRefresh();
  }
}
