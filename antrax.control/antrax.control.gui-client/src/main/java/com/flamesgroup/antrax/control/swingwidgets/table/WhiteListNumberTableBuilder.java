/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WhiteListNumberTableBuilder implements TableBuilder<WhiteListNumber, Date> {

  private final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    columns.addColumn("number", String.class);
    columns.addColumn("status", String.class);
    columns.addColumn("add time", String.class);
    columns.addColumn("routing request count", Integer.class);
  }

  @Override
  public void buildRow(final WhiteListNumber src, final ColumnWriter<WhiteListNumber> dest) {
    dest.writeColumn(src.getNumber());
    dest.writeColumn(src.getStatus().toString());
    dest.writeColumn(formatter.format(src.getAddTime()));
    dest.writeColumn(src.getRoutingRequestCount());
  }

  @Override
  public Date getUniqueKey(final WhiteListNumber src) {
    return src.getAddTime();
  }

}
