/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManagerImpl;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.guiclient.widgets.StatusBar;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class MainWindow extends JFrame {
  private static final long serialVersionUID = 4801653349284361521L;
  private JPanel leftPanel;
  private JPanel rightPanel;
  private CardLayout cardLayout;
  private final TransactionManager transactionManager;
  private AppMenuTree menuTree;
  private StatusBar statusBar;
  private RefresherThread refresherThread;
  private Throwable caughtError;

  public MainWindow(final TransactionManager transactionManager) throws Exception {
    setTitle("Antrax Client");
    this.transactionManager = transactionManager;
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, getLeftPanel(), getRightPanel());
    splitPane.setOneTouchExpandable(true);
    getContentPane().add(splitPane, BorderLayout.CENTER);
    getContentPane().add(getStatusBar(), BorderLayout.SOUTH);
    setPreferredSize(new Dimension(1200, 650));
    pack();
    setLocationRelativeTo(null);

    getMenuTree().addMenuSelectedListener(new MenuSelectionListener() {
      private final Map<AppPanel, String> panels = new HashMap<>();
      private int lastIndex = 0;
      private AppPanel lastPanel;

      {
        getMenuTree().addPanelRemovedListener(new PanelRemovedListener() {
          @Override
          public void handlePanelRemoved(final AppPanel panel) {
            if (panels.remove(panel) != null) {
              getRightPanel().remove(panel.getComponent());
              getRightPanel().revalidate();
            }
          }
        });

        transactionManager.addListener(new TransactionListener() {
          @Override
          public void onTransactionStateChanged(final TransactionState newState) {
            if (lastPanel == null) {
              return;
            }
            if (newState == TransactionState.START) {
              lastPanel.setEditable(true);
            } else {
              lastPanel.setEditable(false);
            }
          }

          @Override
          public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
            return Collections.emptyList();
          }
        });
      }

      @Override
      public void handleMenuSelected(final AppPanel panel) {
        if (panel == lastPanel) {
          return;
        }
        if (!panels.containsKey(panel)) {
          String index = String.valueOf(lastIndex++);
          panels.put(panel, index);
          getRightPanel().add(panel.getComponent(), index);
          getRightPanel().revalidate();
          initializePanel(panel);
        }
        getCardLayout().show(getRightPanel(), panels.get(panel));
        if (lastPanel != null) {
          lastPanel.setActive(false);
        }
        panel.setActive(true);
        panel.setEditable(transactionManager.isEditTransactionStarted());
        lastPanel = panel;
        refresherThread.forceRefresh();
      }

      private void initializePanel(final AppPanel panel) {
        panel.postInitialize(refresherThread, transactionManager);
      }
    });
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent ev) {
        if (transactionManager != null && transactionManager.isEditTransactionStarted()) {
          JOptionPane.showConfirmDialog(null, "Edit transaction is started. Please save or cancel config.",
              "Edit transaction is started",
              JOptionPane.DEFAULT_OPTION,
              JOptionPane.QUESTION_MESSAGE);
        } else {
          ev.getWindow().dispose();
        }
      }
    });
  }

  private StatusBar getStatusBar() {
    if (statusBar == null) {
      statusBar = new StatusBar();
    }
    return statusBar;
  }

  public JPanel getLeftPanel() {
    if (leftPanel == null) {
      leftPanel = new JPanel();
      leftPanel.setLayout(new BorderLayout());
      leftPanel.setPreferredSize(new Dimension(200, 200));
    }
    return leftPanel;
  }

  public JPanel getRightPanel() {
    if (rightPanel == null) {
      rightPanel = new JPanel();
      rightPanel.setLayout(getCardLayout());
    }
    return rightPanel;
  }

  public CardLayout getCardLayout() {
    if (cardLayout == null) {
      cardLayout = new CardLayout();
    }
    return cardLayout;
  }

  public AppMenuTree getMenuTree() {
    if (menuTree == null) {
      menuTree = new AppMenuTree();
    }
    return menuTree;
  }

  @Override
  public void dispose() {
    getStatusBar().stopRefresh();
    menuTree.release();
    super.dispose();
  }

  public static void main(final String[] args) throws Exception {
    final MainWindow mainWindow = new MainWindow(new TransactionManagerImpl());
    mainWindow.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        mainWindow.setVisible(true);
      }
    });
  }

  public void setRefresherThread(final RefresherThread refresherThread) throws Exception {
    this.refresherThread = refresherThread;
    statusBar.setRefresherThread(refresherThread);
    refresherThread.addRefresher(new MainWindowPostInitializeRefresher(refresherThread, transactionManager, getMenuTree(), getRightPanel(), getCardLayout(), getLeftPanel(), this));
  }

  public void setCaughtError(final Throwable e) {
    caughtError = e;
  }

  public Throwable getCaughtError() {
    return caughtError;
  }
}
