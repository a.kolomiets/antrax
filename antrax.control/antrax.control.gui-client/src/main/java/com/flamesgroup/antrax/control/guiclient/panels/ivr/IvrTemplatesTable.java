/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.ivr;

import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.IvrTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.IvrTemplateWrapper;

public class IvrTemplatesTable extends JUpdatableTable<IvrTemplateWrapper, String> {

  private static final long serialVersionUID = -8621675776929229714L;

  private final String[] columnToolTips = {
      IvrTableTooltips.getIvrTemplatesNameTooltip(),
      IvrTableTooltips.getIvrTemplatesDropReasonTooltip(),
      IvrTableTooltips.getIvrTemplatesDropCodeTooltip()
  };

  public IvrTemplatesTable() {
    super(new TableBuilder<IvrTemplateWrapper, String>() {

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("name", String.class);
        columns.addColumn("call drop reason", String.class);
        columns.addColumn("call drop code", Integer.class);
      }

      @Override
      public void buildRow(final IvrTemplateWrapper src, final ColumnWriter<IvrTemplateWrapper> dest) {
        dest.writeColumn(src.getName());
        dest.writeColumn(src.getCallDropReason());
        dest.writeColumn(src.getCallDropCode());
      }

      @Override
      public String getUniqueKey(final IvrTemplateWrapper src) {
        return src.getName() + src.getCallDropReason() + src.getCallDropCode();
      }
    });
    setColumnToolTips(columnToolTips);
  }

}
