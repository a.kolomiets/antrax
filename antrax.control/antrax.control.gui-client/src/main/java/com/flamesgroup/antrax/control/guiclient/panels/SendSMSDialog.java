/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditCheckBox;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSEncoding;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SendSMSDialog extends JDialog {
  private static final long serialVersionUID = 4807045051246771668L;
  private final JTextField txtNumbers = new JTextField();

  private final DefaultListModel listNumbersModel = new DefaultListModel();
  private final JList listNumbers = new JList(listNumbersModel);

  private final JButton btnAdd = new JButton("Add");
  private final JButton btnDel = new JButton("Delete");
  private final JButton btnSend = new JButton("Send");
  private final JButton btnCancel = new JButton("Cancel");

  private final JLabel lblSymbolsCount = new JLabel("0", JLabel.TRAILING);
  private final JLabel lblMaxSymbols = new JLabel("/" + SMSEncoding.ENC7BIT.getLength(), JLabel.TRAILING);
  private final JLabel lblSMSCount = new JLabel("0", JLabel.TRAILING);

  private final JTextArea txtSMS = new JTextArea();

  private boolean applied = false;
  private final JPanel panel;

  public SendSMSDialog(final Window window) {
    super(window);
    setLayout(new BoxLayout(getContentPane(), 3));
    panel = createPanel();
    setContentPane(panel);
    initializeEvents();
    pack();
    //setResizable(false);
  }

  private JPanel createPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    retval.setPreferredSize(new Dimension(500, 400));

    JLabel lblNumbers = new JLabel("Number:", JLabel.TRAILING);
    lblNumbers.setLabelFor(txtNumbers);
    lblNumbers.setPreferredSize(new Dimension(lblNumbers.getPreferredSize().width, txtNumbers.getPreferredSize().height));
    resizeToWider(btnAdd, btnDel, btnDel.getPreferredSize().height);
    resizeToWider(btnSend, btnCancel, btnSend.getPreferredSize().height);
    btnAdd.setEnabled(false);
    btnDel.setEnabled(false);
    btnSend.setEnabled(false);

    listNumbers.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    listNumbers.setFocusable(false);

    JScrollPane listScroller = new JScrollPane(listNumbers);
    listScroller.setAlignmentX(LEFT_ALIGNMENT);
    listScroller.setPreferredSize(new Dimension(100, 100));

    JScrollPane txtSMSScroller = new JScrollPane(txtSMS);
    txtSMSScroller.setAlignmentX(LEFT_ALIGNMENT);
    txtSMSScroller.setPreferredSize(new Dimension(100, 100));
    txtSMS.setLineWrap(true);

    JLabel lblSymbolsLabel = new JLabel("Symbols:", JLabel.TRAILING);
    lblSymbolsLabel.setLabelFor(lblSymbolsCount);

    JLabel lblSMSLabel = new JLabel("Messages:", JLabel.TRAILING);
    lblSMSLabel.setLabelFor(lblSMSCount);

    layout.putConstraint(SpringLayout.WEST, lblNumbers, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, txtNumbers, 5, SpringLayout.EAST, lblNumbers);
    layout.putConstraint(SpringLayout.EAST, txtNumbers, -5, SpringLayout.EAST, retval);

    layout.putConstraint(SpringLayout.NORTH, btnAdd, 5, SpringLayout.SOUTH, lblNumbers);
    layout.putConstraint(SpringLayout.EAST, btnAdd, -2, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.NORTH, btnDel, 5, SpringLayout.SOUTH, lblNumbers);
    layout.putConstraint(SpringLayout.WEST, btnDel, 2, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.NORTH, listScroller, 5, SpringLayout.SOUTH, btnAdd);
    layout.putConstraint(SpringLayout.WEST, listScroller, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.EAST, listScroller, -5, SpringLayout.EAST, retval);

    layout.putConstraint(SpringLayout.NORTH, lblSymbolsLabel, 5, SpringLayout.SOUTH, listScroller);
    layout.putConstraint(SpringLayout.NORTH, lblSymbolsCount, 5, SpringLayout.SOUTH, listScroller);
    layout.putConstraint(SpringLayout.NORTH, lblMaxSymbols, 5, SpringLayout.SOUTH, listScroller);
    layout.putConstraint(SpringLayout.NORTH, lblSMSLabel, 5, SpringLayout.SOUTH, listScroller);
    layout.putConstraint(SpringLayout.NORTH, lblSMSCount, 5, SpringLayout.SOUTH, listScroller);

    layout.putConstraint(SpringLayout.WEST, lblSymbolsLabel, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, lblSymbolsCount, 5, SpringLayout.EAST, lblSymbolsLabel);
    layout.putConstraint(SpringLayout.WEST, lblMaxSymbols, 0, SpringLayout.EAST, lblSymbolsCount);

    layout.putConstraint(SpringLayout.EAST, lblSMSCount, -5, SpringLayout.EAST, retval);
    layout.putConstraint(SpringLayout.EAST, lblSMSLabel, -5, SpringLayout.WEST, lblSMSCount);

    layout.putConstraint(SpringLayout.NORTH, txtSMSScroller, 5, SpringLayout.SOUTH, lblSymbolsLabel);
    layout.putConstraint(SpringLayout.WEST, txtSMSScroller, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.EAST, txtSMSScroller, -5, SpringLayout.EAST, retval);

    layout.putConstraint(SpringLayout.NORTH, btnSend, 5, SpringLayout.SOUTH, txtSMSScroller);
    layout.putConstraint(SpringLayout.EAST, btnSend, -2, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.NORTH, btnCancel, 5, SpringLayout.SOUTH, txtSMSScroller);
    layout.putConstraint(SpringLayout.WEST, btnCancel, 2, SpringLayout.HORIZONTAL_CENTER, retval);

    //layout.putConstraint(SpringLayout.EAST, retval, 5, SpringLayout.EAST, txtNumbers);
    layout.putConstraint(SpringLayout.SOUTH, retval, 5, SpringLayout.SOUTH, btnCancel);

    retval.add(lblNumbers);
    retval.add(txtNumbers);
    retval.add(btnAdd);
    retval.add(btnDel);
    retval.add(listScroller);
    retval.add(lblSymbolsLabel);
    retval.add(lblSymbolsCount);
    retval.add(lblMaxSymbols);
    retval.add(lblSMSLabel);
    retval.add(lblSMSCount);
    retval.add(txtSMSScroller);
    retval.add(btnSend);
    retval.add(btnCancel);

    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    retval.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  private void addToList(final String number) {
    if (number.length() > 0) {
      String numbers[] = number.split(";|,");

      //check numbers
      for (String string : numbers) {
        if (string.length() > 0) {
          @SuppressWarnings("unused")
          PhoneNumber checkNumber = new PhoneNumber(string);
        }
      }

      //add numbers
      for (String string : numbers) {
        if (string.length() > 0) {
          listNumbersModel.addElement(string);
        }
      }
    }
  }

  private void deleteFromList(final int[] indeces) {
    if (indeces.length > 0) {
      Arrays.sort(indeces);
      for (int i = indeces.length - 1; i >= 0; i--) {
        listNumbersModel.remove(indeces[i]);
      }
      listNumbers.setSelectedIndex(0);
    }
  }

  private void initializeEvents() {
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        applied = false;
        txtNumbers.requestFocus();
      }
    });

    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnAdd.doClick();
      }
    };
    txtNumbers.addActionListener(actionListener);

    DocumentListener documentListener = new DocumentListener() {
      private void onChange() {
        if (txtNumbers.getText().length() > 0) {
          btnAdd.setEnabled(true);
        } else {
          btnAdd.setEnabled(false);
        }
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        onChange();
      }
    };
    txtNumbers.getDocument().addDocumentListener(documentListener);

    txtNumbers.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(final FocusEvent e) {
        if (!e.isTemporary() && e.getOppositeComponent() != btnAdd) {
          btnAdd.setEnabled(false);
        }
      }

      @Override
      public void focusGained(final FocusEvent e) {
        if (txtNumbers.getText().length() > 0) {
          btnAdd.setEnabled(true);
        } else {
          btnAdd.setEnabled(false);
        }
      }
    });

    DocumentListener documentListenerSMS = new DocumentListener() {
      private SMSEncoding encoding = SMSEncoding.ENC7BIT;
      private int smsCount = 0;
      private int symbolsCount = 0;


      private void resetCounters() {
        smsCount = 0;
        symbolsCount = 0;
      }

      private void updateEncoding(final String str) {
        encoding = SMSCodec.getTextEncoding(str);
      }

      private void updateCounters(final char symbol) {
        if (0 == smsCount) {
          smsCount = 1;
        }
        if (SMSEncoding.ENCUCS2 == encoding) {
          if (symbolsCount >= SMSEncoding.ENCUCS2.getLength()) {
            symbolsCount = 1;
            smsCount++;
          } else {
            symbolsCount++;
          }
        } else {

          if (SMSCodec.isDouble7BitSymbol(symbol)) {
            if (symbolsCount >= SMSEncoding.ENC7BIT.getLength() - 1) {
              symbolsCount = 2;
              smsCount++;
            } else {
              symbolsCount += 2;
            }
          } else {
            if (symbolsCount >= SMSEncoding.ENC7BIT.getLength()) {
              symbolsCount = 1;
              smsCount++;
            } else {
              symbolsCount++;
            }
          }
        }
      }

      private void updateLabels(final String str) {
        char charArr[] = str.toCharArray();
        resetCounters();
        updateEncoding(str);

        for (char strChar : charArr) {
          updateCounters(strChar);
        }

        if (SMSEncoding.ENCUCS2 == encoding) {
          lblMaxSymbols.setText("/" + SMSEncoding.ENCUCS2.getLength());
        } else {
          lblMaxSymbols.setText("/" + SMSEncoding.ENC7BIT.getLength());
        }
        lblSymbolsCount.setText(Integer.toString(symbolsCount));
        lblSMSCount.setText(Integer.toString(smsCount));
      }

      private void onChange() {
        updateLabels(txtSMS.getText());
        if (symbolsCount > 0 && listNumbersModel.getSize() > 0) {
          btnSend.setEnabled(true);
        } else {
          btnSend.setEnabled(false);
        }
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        onChange();
      }
    };
    txtSMS.getDocument().addDocumentListener(documentListenerSMS);

    KeyListener keyListener = new KeyListener() {
      @Override
      public void keyPressed(final KeyEvent key) {
      }

      @Override
      public void keyReleased(final KeyEvent key) {
        if (key.getKeyCode() == KeyEvent.VK_DELETE) {
          btnDel.doClick();
        }
      }

      @Override
      public void keyTyped(final KeyEvent key) {
      }
    };
    listNumbers.addKeyListener(keyListener);

    listNumbers.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(final FocusEvent e) {
        if (!e.isTemporary()) {
          if (e.getComponent() == listNumbers && e.getOppositeComponent() != btnDel) {
            listNumbers.clearSelection();
          }
        }
      }

      @Override
      public void focusGained(final FocusEvent e) {
      }
    });

    listNumbersModel.addListDataListener(new ListDataListener() {
      private void onChange() {
        if (listNumbersModel.getSize() > 0) {
          listNumbers.setFocusable(true);
        } else {
          listNumbers.setFocusable(false);
        }
        if (txtSMS.getText().length() > 0 && listNumbersModel.getSize() > 0) {
          btnSend.setEnabled(true);
        } else {
          btnSend.setEnabled(false);
        }
      }

      @Override
      public void intervalRemoved(final ListDataEvent e) {
        onChange();
      }

      @Override
      public void intervalAdded(final ListDataEvent e) {
        onChange();
      }

      @Override
      public void contentsChanged(final ListDataEvent e) {
        onChange();
      }
    });

    keyListener = new KeyListener() {
      @Override
      public void keyPressed(final KeyEvent key) {
      }

      @Override
      public void keyReleased(final KeyEvent key) {
        if (key.getKeyCode() == KeyEvent.VK_ENTER) {
          btnAdd.doClick();
        }
      }

      @Override
      public void keyTyped(final KeyEvent key) {
      }
    };
    btnAdd.addKeyListener(keyListener);

    ListSelectionListener selectionListener = new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          if (listNumbers.getSelectedIndex() == -1) {
            btnDel.setEnabled(false);
          } else {
            btnDel.setEnabled(true);
          }
        }
      }
    };
    listNumbers.addListSelectionListener(selectionListener);

    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (e.getSource() == btnAdd) {
          try {
            SendSMSDialog.this.addToList(txtNumbers.getText());
            txtNumbers.setText("");
            txtNumbers.requestFocus();
          } catch (IllegalArgumentException exception) {
            MessageUtils.showError(panel, "Send SMS error", exception.getMessage());
          }
        } else if (e.getSource() == btnDel) {
          deleteFromList(listNumbers.getSelectedIndices());
        }
      }
    };
    btnAdd.addActionListener(listener);
    btnDel.addActionListener(listener);

    ActionListener exitListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        applied = e.getSource() == btnSend;
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }
      }
    };
    btnSend.addActionListener(exitListener);
    btnCancel.addActionListener(exitListener);
  }

  public void setSMSText(final String smsText) {
    txtSMS.setText(smsText);
  }

  public String getSMSText() {
    return txtSMS.getText();
  }

  public List<String> getNumbers() {
    ArrayList<String> items = new ArrayList<>();
    int size = listNumbersModel.getSize();

    for (int i = 0; i < size; i++) {
      items.add((String) listNumbersModel.getElementAt(i));
    }
    return items;
  }

  public boolean showDialog() {
    setVisible(true);
    return applied;
  }

  public void clear() {
    txtSMS.setText("");
    txtNumbers.setText("");
    listNumbersModel.clear();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        SendSMSDialog dialog = new SendSMSDialog(null);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
        dialog.showDialog();
      }
    });
  }
}

