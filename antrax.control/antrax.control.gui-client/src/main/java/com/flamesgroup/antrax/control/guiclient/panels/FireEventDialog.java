/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class FireEventDialog extends JDialog {
  private static final long serialVersionUID = -8953770278632613871L;

  private final JTextField txtEvent = new JTextField(25);

  private final JButton btnOk = new JButton("Send");
  private final JButton btnCancel = new JButton("Cancel");

  private boolean applied = false;


  public JButton getOkBtn() {
    return btnOk;
  }

  public FireEventDialog(final Window window) {
    super(window);
    setLayout(new BoxLayout(getContentPane(), 3));
    setContentPane(createPanel());
    initializeEvents();
    pack();
    setResizable(false);
  }

  private JPanel createPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);

    JLabel lblEvent = new JLabel("Event:", JLabel.TRAILING);
    lblEvent.setLabelFor(txtEvent);

    lblEvent.setPreferredSize(new Dimension(lblEvent.getPreferredSize().width, txtEvent.getPreferredSize().height));
    resizeToWider(btnOk, btnCancel, btnOk.getPreferredSize().height);
    btnOk.setEnabled(false);

    layout.putConstraint(SpringLayout.NORTH, lblEvent, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.NORTH, txtEvent, 5, SpringLayout.NORTH, retval);

    layout.putConstraint(SpringLayout.WEST, lblEvent, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, txtEvent, 5, SpringLayout.EAST, lblEvent);

    layout.putConstraint(SpringLayout.NORTH, btnOk, 5, SpringLayout.SOUTH, txtEvent);
    layout.putConstraint(SpringLayout.EAST, btnOk, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.NORTH, btnCancel, 5, SpringLayout.SOUTH, txtEvent);
    layout.putConstraint(SpringLayout.WEST, btnCancel, 5, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.EAST, retval, 5, SpringLayout.EAST, txtEvent);
    layout.putConstraint(SpringLayout.SOUTH, retval, 5, SpringLayout.SOUTH, btnOk);

    retval.add(lblEvent);
    retval.add(txtEvent);
    retval.add(btnOk);
    retval.add(btnCancel);

    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    retval.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  private void initializeEvents() {
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        applied = false;
        txtEvent.requestFocus();
      }
    });

    DocumentListener documentListener = new DocumentListener() {
      private void onChange() {
        if (txtEvent.getText().length() > 0) {
          btnOk.setEnabled(true);
        } else {
          btnOk.setEnabled(false);
        }
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        onChange();
      }
    };
    txtEvent.getDocument().addDocumentListener(documentListener);

    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        btnOk.doClick();
      }
    };
    txtEvent.addActionListener(actionListener);



    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        applied = e.getSource() == btnOk;
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }
      }
    };
    btnCancel.addActionListener(listener);
    btnOk.addActionListener(listener);
  }

  public String getEventText() {
    return txtEvent.getText();
  }

  public void setEventText(final String event) {
    txtEvent.setText(event);
  }

  public void clear() {
    txtEvent.setText("");
  }

  public boolean showDialog() {
    setVisible(true);
    return applied;
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        boolean res;

        FireEventDialog dialog = new FireEventDialog(null);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
        res = dialog.showDialog();
        System.out.println(res);
      }
    });
  }

}
