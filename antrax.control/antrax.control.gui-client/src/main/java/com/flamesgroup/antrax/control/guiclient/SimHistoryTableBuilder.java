/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.guiclient.domain.SimEventRecRow;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.enums.SIMEvent;

import java.awt.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class SimHistoryTableBuilder implements TableBuilder<SimEventRecRow, Long> {

  final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
  final Calendar cal = Calendar.getInstance();
  final SimHistoryTableRenderer renderer = new SimHistoryTableRenderer();
  final DefaultTableCellRenderer defaultRenderer = new DefaultTableCellRenderer();
  final ParentEventTableRenderer parentRenderer = new ParentEventTableRenderer();

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    defaultRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    parentRenderer.setHorizontalAlignment(SwingConstants.CENTER);

    columns.addColumn("start time", String.class).setRenderer(defaultRenderer).setMaxWidth(150).setMinWidth(140);
    columns.addColumn("", String.class).setRenderer(parentRenderer).setMaxWidth(20).setMinWidth(20);
    columns.addColumn("", SIMEvent.class).setRenderer(renderer).setMaxWidth(25).setMinWidth(25);
    columns.addColumn("description", String.class);
  }

  @Override
  public void buildRow(final SimEventRecRow src, final ColumnWriter<SimEventRecRow> dest) {

    dest.writeColumn(calculateTime(src.getRecord().getStartTime().getTime()));
    dest.writeColumn(initParentIcon(src));
    dest.writeColumn(src.getRecord().getEvent());
    dest.writeColumn(SimHistoryEventFormatter.convertSimEventToText(src), getCDRIfPresent(src));

  }

  private Object[] getCDRIfPresent(final SimEventRecRow src) {
    if (src.getCDR() == null) {
      return new Object[0];
    }
    return new Object[] {src.getCDR()};
  }

  private String initParentIcon(final SimEventRecRow simEventRecRow) {

    if ((!simEventRecRow.isExpanded()) && (simEventRecRow.getRecord().getParent() != null)) {
      return "";
    } else if (simEventRecRow.getRecord().getEvent().hasChildren() || (simEventRecRow.getRecord().getParent() == null)) {
      return "*";
    }
    return "";
  }

  private String calculateTime(final long time) {
    if (time == 0) {
      return " - no - ";
    } else {
      cal.setTimeInMillis(time);
      return formatter.format(cal.getTime());
    }
  }

  @Override
  public Long getUniqueKey(final SimEventRecRow src) {
    return src.getRecord().getStartTime().getTime();
  }

  public class SimHistoryTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1000596214053822397L;

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
      Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

      String labelText = "";
      if (value instanceof SIMEvent) {
        Icon defaultIcon = IconPool.getShared("/img/whiteball.png");
        SIMEvent simEvent = (SIMEvent) value;
        switch (simEvent) {
          case OUTGOING_CALL_ENDED:
            setIcon(IconPool.getShared("/img/forward.png"));
            break;
          case INCOMING_CALL_ENDED:
            setIcon(IconPool.getShared("/img/arrow-turn-180-left.png"));
            break;
          case CALL_CHANNEL_BUILD:
            setIcon(IconPool.getShared("/img/build_channel.png"));
            break;
          case CALL_CHANNEL_RELEASED:
            setIcon(IconPool.getShared("/img/release_channel.png"));
            break;
          case SIM_LOCKED:
            setIcon(IconPool.getShared("/img/locked.png"));
            break;
          case SIM_ADDED:
            setIcon(IconPool.getShared("/img/card_plus.png"));
            break;
          case SIM_LOST:
            setIcon(IconPool.getShared("/img/card_minus.png"));
            break;
          case SIM_RETURNED:
            setIcon(IconPool.getShared("/img/card_arrow.png"));
            break;
          case SIM_STATUS_CHANGED:
            setIcon(IconPool.getShared("/img/card_export.png"));
            break;
          case SIM_FOUND:
            setIcon(IconPool.getShared("/img/card_find.png"));
            break;
          case SIM_TAKEN:
            setIcon(defaultIcon);
            break;

          case GSM_REGISTRATION:
            setIcon(IconPool.getShared("/img/registration.png"));
            break;

          case GSM_REGISTERED:
            setIcon(IconPool.getShared("/img/registered.png"));
            break;

          case GSM_NOT_REGISTERED:
            setIcon(IconPool.getShared("/img/notregistered.png"));
            break;

          case GSM_UNREGISTERED:
            setIcon(IconPool.getShared("/img/unregistered.png"));
            break;

          case IMEI_SETUPED:
          case IMEI_GENERATED:
          case RESET_IMEI:
            setIcon(IconPool.getShared("/img/barcode.png"));
            break;

          case SMS_SENT:
            setIcon(IconPool.getShared("/img/outbox.png"));
            break;

          case SMS_STATUS:
            setIcon(IconPool.getShared("/img/message.png"));
            break;

          case SMS_RECIEVED:
            setIcon(IconPool.getShared("/img/inbox.png"));
            break;

          case USSD:
            setIcon(IconPool.getShared("/img/document-horizontal-text.png"));
            break;

          case RESET_SCRIPTS:
            setIcon(IconPool.getShared("/img/drive_delete.png"));
            break;

          case RESET_STATISTIC:
            setIcon(IconPool.getShared("/img/buttons/clear.png"));
            break;

          case HTTP_REQUEST:
            setIcon(IconPool.getShared("/img/server_16.png"));
            break;

          case ALLOWED_INTERNET:
            setIcon(IconPool.getShared("/img/server_16.png"));
            break;

          case SET_TARIFF_PLAN_END_DATE:
            setIcon(IconPool.getShared("/img/tariff_end_date.png"));
            break;

          default:
            setIcon(defaultIcon);
            break;

        }
      } else {
        labelText = (value == null) ? "" : value.toString();
      }
      setText(labelText);
      return cmp;
    }

  }

  public class ParentEventTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1000596214053822397L;
    final Icon right = IconPool.getShared("/img/arrow-right.png");

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
      Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      String labelText = "";

      if (value instanceof String) {
        if (((String) value).compareTo("*") == 0) {
          setIcon(right);
        } else {
          setIcon(null);
        }

      } else {
        labelText = (value == null) ? "" : value.toString();
      }

      setText(labelText);
      return cmp;
    }
  }

}
