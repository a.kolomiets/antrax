/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.indicators;

public class JIntIndicator extends JIndicator<Integer> {

  private Integer value;
  private JIndicator<? extends Number> bindIndicator;

  public JIntIndicator(final Integer initialValue) {
    this(null, initialValue);
  }

  public JIntIndicator(final String caption, final Integer initialValue) {
    super(caption, initialValue);
  }

  @Override
  public Integer getValue() {
    return value;
  }

  @Override
  public void setValue(final Integer value) {
    this.value = value;
    updateUI();
  }

  public void bindPercentBase(final JIndicator<? extends Number> bindIndicator) {
    this.bindIndicator = bindIndicator;
    updateUI();
  }

  @Override
  protected String beforeRenderValue(final String labelText) {
    if (bindIndicator == null || bindIndicator.getValue() == null || getValue() == null) {
      return labelText;
    } else {
      int basePercent = bindIndicator.getValue().intValue();
      int percent = (basePercent == 0) ? 0 : (getValue() * 100) / basePercent;
      return String.format("%s (%s%%)", labelText, percent);
    }
  }

}
