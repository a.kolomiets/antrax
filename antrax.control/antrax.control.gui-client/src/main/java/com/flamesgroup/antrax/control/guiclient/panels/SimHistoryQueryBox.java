/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.domain.SimEventRecRow;
import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.control.guiclient.widgets.PhoneNumberField;
import com.flamesgroup.antrax.control.guiclient.widgets.SimUIDField;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.DateTableCellRenderer;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditPanel;
import com.flamesgroup.antrax.control.swingwidgets.field.AbstractField;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SimHistoryQueryBox extends JEditPanel {

  private static final long serialVersionUID = 7186310807982995158L;

  private final List<SimHistoryQueryActionListener> listeners = new LinkedList<>();

  private JTimePeriodSelector timePeriodSelector;
  private JTextField simChannel;
  private JTextField gsmChannel;
  private PhoneNumberField phoneNumber;
  private SimUIDField simUID;
  private JComboBox activeSim;
  private JComboBox enabledSim;
  private JComboBox simGroupFilter;
  private JButton executeButton;
  private JButton executeAppendButton;
  public JUpdatableTable<QueryInfo, String> table;
  private ListSelectionListener tableSelectionListener;
  private boolean allowQuery = true;

  private final List<QueryInfo> queryInfoData = new ArrayList<>();
  private final Lock queryInfoDataLock = new ReentrantLock();

  private JScrollPane scp;

  private final List<AbstractField> validationFields = new ArrayList<>();
  private transient final ChangeListener fieldChangeListener = new ChangeListener() {

    @Override
    public void stateChanged(final ChangeEvent e) {
      Object src = e.getSource();
      if (src instanceof AbstractField) {
        boolean isValid = ((AbstractField) src).getValidity();
        boolean enable = allowQuery && isValid;
        enableQueryButtons(enable);
      }
    }

  };

  public SimHistoryQueryBox() {
    createGUIElements();
    setupListeners();
  }

  private void enableQueryButtons(final boolean enabled) {
    executeButton.setEnabled(enabled);
    executeAppendButton.setEnabled(enabled);
  }

  public void setAllowQuery(final boolean allowQuery) {
    this.allowQuery = allowQuery;
    if (!allowQuery) {
      enableQueryButtons(false);
    } else {
      enableQueryButtons(isAllValid());
    }
  }

  private boolean isAllValid() {
    for (AbstractField field : validationFields) {
      if (!field.getValidity()) {
        return false;
      }
    }
    return true;
  }

  private void createGUIElements() {
    timePeriodSelector = new JTimePeriodSelector();
    timePeriodSelector.setOpaque(false);
    simChannel = new JTextField(10);
    gsmChannel = new JTextField(10);
    phoneNumber = new PhoneNumberField();
    simUID = new SimUIDField(false);
    executeButton = createButton("Execute");
    executeAppendButton = createButton("Execute (+)");

    activeSim = new JComboBox(new ThreeState[] {ThreeState.ZSTATE, ThreeState.SELECTED, ThreeState.UNSELECTED});
    activeSim.setRenderer(new ThreeStateLCR("Any", "Active", "Inactive"));
    enabledSim = new JComboBox(new ThreeState[] {ThreeState.ZSTATE, ThreeState.SELECTED, ThreeState.UNSELECTED});
    enabledSim.setRenderer(new ThreeStateLCR("Any", "Enabled", "Disabled"));

    simGroupFilter = new JComboBox();
    simGroupFilter.setEditable(true);

    table = new JUpdatableTable<>(new QueryInfoTableBuilder());
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    layoutComponents();
    table.setName(getClass().getSimpleName());
    table.getUpdatableTableProperties().restoreProperties();
  }

  private JButton createButton(final String text) {
    JButton button = new JButton(text);
    button.putClientProperty("JButton.buttonType", "textured");
    return button;
  }

  private void layoutComponents() {
    GridBagLayout gbl = new GridBagLayout();
    JPanel formPanel = new JPanel(gbl);
    formPanel.setOpaque(false);

    gbl.columnWidths = new int[] {0, 55, 35, 0, 125, 0, 0, 0, 0};
    gbl.rowHeights = new int[] {0, 0, 0, 0, 0, 0};
    gbl.columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
    gbl.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

    formPanel.add(timePeriodSelector, new GridBagConstraints(0, 0, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 0, 5, 5), 0, 0));

    formPanel.add(new JEditLabel("Sim channel"), new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
    formPanel.add(simChannel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(new JEditLabel("Phone number"), new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    formPanel.add(phoneNumber, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(new JEditLabel("Active"), new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    formPanel.add(activeSim, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(executeButton, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

    formPanel.add(new JEditLabel("Gsm channel"), new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
    formPanel.add(gsmChannel, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(new JEditLabel("SIM UID"), new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    formPanel.add(simUID, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(new JEditLabel("Enabled"), new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    formPanel.add(enabledSim, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    formPanel.add(executeAppendButton, new GridBagConstraints(7, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

    formPanel.add(new JEditLabel("SIM group"), new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 10, 5), 0, 0));
    formPanel.add(simGroupFilter, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 10, 5), 0, 0));

    setLayout(new BorderLayout());

    //    JPanel queryPanel = new JPanel(new BorderLayout());
    scp = new JScrollPane(table);
    scp.setMinimumSize(new Dimension(100, 100));
    add(formPanel, BorderLayout.NORTH);
    add(scp, BorderLayout.CENTER);

    //    JReflectiveBar bar = new JReflectiveBar();
    //    add(bar, BorderLayout.NORTH);
    //    add(queryPanel, BorderLayout.CENTER);
  }

  private void addValidationListener(final AbstractField field) {
    field.addChangeListener(fieldChangeListener);
    validationFields.add(field);
  }

  private void setupListeners() {
    addValidationListener(phoneNumber);
    addValidationListener(simUID);

    executeButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        fireQuery(readQueryParams(), false);
      }
    });

    executeAppendButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        fireQuery(readQueryParams(), true);
      }
    });

    tableSelectionListener = new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
          return;
        }

        int row = table.getSelectedRow();
        if (row < 0) {
          return;
        }

        QueryInfo queryInfo = table.getElemAt(row);
        if (queryInfo != null) {
          showQueryInfo(queryInfo);
        }
      }
    };
    addTableSelectionListener();
  }

  private void addTableSelectionListener() {
    table.getSelectionModel().addListSelectionListener(tableSelectionListener);
  }

  private void removeTableSelectionListener() {
    table.getSelectionModel().removeListSelectionListener(tableSelectionListener);
  }

  public void addQueryListener(final SimHistoryQueryActionListener l) {
    listeners.add(l);
  }

  public void removeQueryListener(final SimHistoryQueryActionListener l) {
    listeners.remove(l);
  }

  public void setSimGroupNames(final String[] simGroupNames) {
    Object oldValue = simGroupFilter.getEditor().getItem();
    simGroupFilter.removeAllItems();
    if (simGroupNames != null) {
      for (String groupName : simGroupNames) {
        simGroupFilter.addItem(groupName);
      }
    }
    simGroupFilter.getEditor().setItem(oldValue);
  }

  protected void fireQuery(final SimHistoryQuery query, final boolean appendResult) {
    if (listeners.size() > 0) {
      setAllowQuery(false);
    }
    for (SimHistoryQueryActionListener l : listeners) {
      l.handleQueryAction(query, appendResult);
    }
  }

  protected void fireShowHistory(final List<SimEventRecRow> records) {
    for (SimHistoryQueryActionListener l : listeners) {
      l.showHistory(records);
    }
  }

  protected SimHistoryQuery readQueryParams() {
    SimHistoryQuery query = new SimHistoryQuery();

    query.simChannel = simChannel.getText();
    query.gsmChannel = gsmChannel.getText();

    String phone = phoneNumber.getContent();
    if (phone != null && !phone.isEmpty()) {
      query.phoneNumber = new PhoneNumber(phoneNumber.getContent());
    }

    String uid = simUID.getContent().trim();
    if (!uid.isEmpty()) {
      query.simUID = new ICCID(uid);
    }

    query.active = ((ThreeState) activeSim.getSelectedItem()).asBool();
    query.enabled = ((ThreeState) enabledSim.getSelectedItem()).asBool();

    query.fromDate = timePeriodSelector.getFromDate();
    query.toDate = timePeriodSelector.getToDate();

    Object item = simGroupFilter.getEditor().getItem();
    if (item == null || item.toString().isEmpty()) {
      query.simGroupName = null;
    } else {
      query.simGroupName = item.toString();
    }
    return query;
  }

  protected void writeQueryParams(final SimHistoryQuery query) {
    if (query.getFromDate() != null) {
      timePeriodSelector.setFromDate(query.getFromDate());
    }
    if (query.getToDate() != null) {
      timePeriodSelector.setToDate(query.getToDate());
    }

    activeSim.setSelectedItem(ThreeState.boolToThreeState((query.isActive())));
    enabledSim.setSelectedItem(ThreeState.boolToThreeState((query.isEnabled())));

    phoneNumber.setContent(query.getPhoneNumber());

    if (query.getSimUID() != null) {
      simUID.setText(query.getSimUID().getValue());
    } else {
      simUID.setText("");
    }

    simChannel.setText(query.getSimChannel() == null ? "" : query.getSimChannel());
    gsmChannel.setText(query.getGsmChannel() == null ? "" : query.getGsmChannel());

    simGroupFilter.getEditor().setItem(query.getSimGroupName() == null ? "" : query.getSimGroupName());
  }

  protected void fireRootHistoryQuery(final QueryInfo queryInfo) {
    for (SimHistoryQueryActionListener l : listeners) {
      l.handleRootHistoryAction(queryInfo);
    }
  }

  public void addQueryResult(final List<QueryInfo> infos, final boolean appendResult) {
    queryInfoDataLock.lock();
    try {
      removeTableSelectionListener();

      if (!appendResult) {
        table.clearData();
        queryInfoData.clear();
      }

      queryInfoData.addAll(infos);
      table.setData(queryInfoData.toArray(new QueryInfo[queryInfoData.size()]));

      if (infos.size() > 0) {
        QueryInfo queryInfo = infos.get(0);

        int row = appendResult ? table.getViewRowOf(queryInfo) : 0;
        table.scrollToRow(row);
        table.selectRow(row);

        showQueryInfo(queryInfo);
      }

      addTableSelectionListener();
    } finally {
      queryInfoDataLock.unlock();
    }
  }

  public QueryInfo getQueryInfo(final SimEventRecRow recRows) {
    queryInfoDataLock.lock();
    try {
      if (recRows != null) {
        QueryInfo info = table.getSelectedElem();
        List<SimEventRecRow> simEventRec = info.getRows();
        if (simEventRec != null /* && root.equals(simEventRec) */) {
          return info;
        }
      }
      return null;
    } finally {
      queryInfoDataLock.unlock();
    }
  }

  public void generateQuery(final ICCID simUID, final Date fromDate, final Date toDate, final ChannelUID simChannel, final ChannelUID gsmChannel, final String simGroupName,
      final PhoneNumber phoneNumber, final Boolean active, final Boolean enabled) {
    SimHistoryQuery query = new SimHistoryQuery();
    query.simUID = simUID;
    query.fromDate = fromDate;
    query.toDate = toDate;

    if (simChannel != null) {
      query.simChannel = simChannel.getAliasCanonicalName();
    }

    if (gsmChannel != null) {
      query.gsmChannel = gsmChannel.getAliasCanonicalName();
    }

    query.active = active;
    query.enabled = enabled;
    query.simGroupName = simGroupName;

    if (phoneNumber != null && !phoneNumber.isPrivate()) {
      query.phoneNumber = phoneNumber;
    }

    fireQuery(query, true);
  }

  public void clearFilters() {
    writeQueryParams(new SimHistoryQuery());
  }

  public void clearQueries() {
    queryInfoDataLock.lock();
    try {
      removeTableSelectionListener();
      table.clearData();
      queryInfoData.clear();
      addTableSelectionListener();
    } finally {
      queryInfoDataLock.unlock();
    }
  }

  private void showQueryInfo(QueryInfo queryInfo) {
    writeQueryParams(queryInfo.getQuery());
    if (queryInfo.getRows().isEmpty()) {
      fireRootHistoryQuery(queryInfo);
    } else {
      fireShowHistory(queryInfo.getRows());
    }
  }

  public void release() {
    table.getUpdatableTableProperties().saveProperties();
  }

  /**
   * QueryBox action listener
   */
  public interface SimHistoryQueryActionListener {

    void handleQueryAction(SimHistoryQuery query, boolean appendResult);

    void handleRootHistoryAction(QueryInfo queryInfo);

    void showHistory(List<SimEventRecRow> rootRecordNode);

  }

  /**
   * Query table row builder
   */
  public static class QueryInfoTableBuilder implements TableBuilder<QueryInfo, String> {

    @Override
    public void buildRow(final QueryInfo queryInfo, final ColumnWriter<QueryInfo> dest) {
      SimHistoryQuery query = queryInfo.getQuery();
      dest.writeColumn(query.getSimUID().getValue());
      dest.writeColumn(query.getFromDate());
      dest.writeColumn(query.getToDate());
      dest.writeColumn(query.getSimChannel());
      dest.writeColumn(query.getGsmChannel());
      dest.writeColumn(query.getSimGroupName());
      dest.writeColumn(query.isActive());
      dest.writeColumn(query.isEnabled());
      if (query.getPhoneNumber() == null || query.getPhoneNumber().isPrivate()) {
        dest.writeColumn(null);
      } else {
        dest.writeColumn(query.getPhoneNumber().getValue());
      }
    }

    @Override
    public String getUniqueKey(final QueryInfo src) {
      return src.getID();
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      DateTableCellRenderer dateRenderer = new DateTableCellRenderer(TimeUtils.DEFAULT_DATE_FORMAT);
      columns.addColumn("Sim UID", String.class).setPreferredWidth(120).setWidth(120);
      columns.addColumn("From date", Date.class).setPreferredWidth(120).setWidth(120).setRenderer(dateRenderer);
      columns.addColumn("To date", Date.class).setPreferredWidth(120).setWidth(120).setRenderer(dateRenderer);
      columns.addColumn("Sim channel", String.class);
      columns.addColumn("Gsm channel", String.class);
      columns.addColumn("SIM group", String.class).setPreferredWidth(30);
      columns.addColumn("Active", Boolean.class).setPreferredWidth(30);
      columns.addColumn("Enabled", Boolean.class);
      columns.addColumn("Phone number", String.class);
    }

  }

  /**
   * Query parameters
   */
  public static class SimHistoryQuery {
    private ICCID simUID;
    private Date fromDate;
    private Date toDate;

    private String simChannel;
    private String gsmChannel;

    private Boolean active;
    private Boolean enabled;
    private PhoneNumber phoneNumber;
    private String simGroupName;

    public SimHistoryQuery() {
      super();
    }

    public SimHistoryQuery(final SimHistoryQuery other) {
      this.simUID = other.simUID;
      this.fromDate = other.fromDate;
      this.toDate = other.toDate;
      this.simChannel = other.simChannel;
      this.gsmChannel = other.gsmChannel;
      this.active = other.active;
      this.enabled = other.enabled;
      this.phoneNumber = other.phoneNumber;
      this.simGroupName = other.simGroupName;
    }

    public ICCID getSimUID() {
      return simUID;
    }

    public void setSimUID(final ICCID simUID) {
      this.simUID = simUID;
    }

    public String getSimChannel() {
      return simChannel;
    }

    public String getGsmChannel() {
      return gsmChannel;
    }

    public Boolean isActive() {
      return active;
    }

    public Boolean isEnabled() {
      return enabled;
    }

    public PhoneNumber getPhoneNumber() {
      return phoneNumber;
    }

    public Date getFromDate() {
      return fromDate;
    }

    public Date getToDate() {
      return toDate;
    }

    public String getSimGroupName() {
      return simGroupName;
    }
  }

  /**
   * Query information
   */
  public static class QueryInfo {
    private final String id;
    private List<SimEventRecRow> rows;
    private final SimHistoryQuery query;

    public QueryInfo(final SimHistoryQuery query) {
      this.id = UUID.randomUUID().toString(); // TODO: replace to UUID
      this.query = query;
      this.rows = new LinkedList<>();
    }

    public String getID() {
      return id;
    }

    public List<SimEventRecRow> getRows() {
      return rows;
    }

    public void setRows(final List<SimEventRecRow> node) {
      this.rows = new LinkedList<>(node);
    }

    public SimHistoryQuery getQuery() {
      return query;
    }
  }

  private enum ThreeState {
    SELECTED,
    UNSELECTED,
    ZSTATE;

    public Boolean asBool() {
      switch (this) {
        case ZSTATE:
          return null;
        case SELECTED:
          return true;
        case UNSELECTED:
          return false;
        default:
          return null;
      }
    }

    public static ThreeState boolToThreeState(final Boolean b) {
      if (b == null) {
        return ThreeState.ZSTATE;
      } else if (b) {
        return ThreeState.SELECTED;
      } else {
        return ThreeState.UNSELECTED;
      }
    }
  }

  private static class ThreeStateLCR extends DefaultListCellRenderer {

    private static final long serialVersionUID = -245724149525591058L;

    private final String zstate;
    private final String selectedState;
    private final String unselectedState;

    public ThreeStateLCR(final String zstate, final String selectedState, final String unselectedState) {
      super();
      this.zstate = zstate;
      this.selectedState = selectedState;
      this.unselectedState = unselectedState;
    }

    @Override
    public Component getListCellRendererComponent(final JList list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
      Component cmp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value instanceof ThreeState) {
        ThreeState ts = (ThreeState) value;
        switch (ts) {
          case ZSTATE:
            setText(zstate);
            break;
          case SELECTED:
            setText(selectedState);
            break;
          case UNSELECTED:
            setText(unselectedState);
            break;
          default:
            setText(String.valueOf(value));
            break;
        }
      } else {
        setText(String.valueOf(value));
      }
      return cmp;
    }
  }

}
