/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.guiclient.utils.ExportHelper;
import com.flamesgroup.antrax.control.guiclient.utils.ExportType;

import javax.swing.*;

public class JExportCsvButton extends JButton {

  private static final long serialVersionUID = -93692365243511199L;

  public JExportCsvButton(final JUpdatableTable table, final String filePrefix) {
    setIcon(ExportType.CSV.getIcon());
    setToolTipText("Export to CSV");
    addActionListener(e -> new ExportHelper().exportTable(table, ExportType.CSV, filePrefix, this.getRootPane()));
  }

}
