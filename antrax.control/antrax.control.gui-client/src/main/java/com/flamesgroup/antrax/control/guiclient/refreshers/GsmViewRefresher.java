/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.refreshers;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.configuration.diff.DiffUtil;
import com.flamesgroup.antrax.configuration.diff.ListContainer;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.panels.GsmViewPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GsmViewRefresher extends ExecutiveRefresher {

  private GsmView[] gsmViewData;
  private boolean isUpdating = false;
  private CellEqualizerAlgorithm cellEqualizerAlgorithm;

  private Map<String, List<GsmView>> prevGsmViewAll = new HashMap<>();

  public GsmViewRefresher(final RefresherThread refresherThread, final TransactionManager transactionManager, final Component invoker) {
    super("GsmViewRefresher", refresherThread, transactionManager, invoker);
  }

  public void release() {

  }

  public void clearPrevGsmView() {
    prevGsmViewAll.clear();
  }

  public void initializePermanentRefresher(final GsmViewPanel gsmViewPanel, final String server) {
    addPermanentExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "list GSM-view information";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        isUpdating = false;
        if (gsmViewData == null || !transactionManager.isEditTransactionStarted()) {
          List<GsmView> gsmView = new ArrayList<>();
          List<String> servers;
          if (server == null) {
            servers = beansPool.getControlBean().listVoiceServers().stream().map(IServerData::getName).collect(Collectors.toList());
          } else {
            servers = Collections.singletonList(server);
            cellEqualizerAlgorithm = beansPool.getGsmViewBean().getCellEqualizerAlgorithm(MainApp.clientUID, server);
          }
          for (String serverName : servers) {
            List<GsmView> prevGsmViews = prevGsmViewAll.getOrDefault(server, new ArrayList<>());
            if (prevGsmViews.isEmpty()) {
              prevGsmViews = beansPool.getGsmViewBean().getGsmView(MainApp.clientUID, serverName);
            } else {
              List<GraphComparator.Delta> gsmViewDiff = beansPool.getGsmViewBean().getGsmViewDiff(MainApp.clientUID, serverName);
              GraphComparator.applyDelta(new ListContainer(1, prevGsmViews), gsmViewDiff, DiffUtil.getIdFetcher(), GraphComparator.getJavaDeltaProcessor());
            }
            prevGsmViewAll.put(serverName, prevGsmViews);
            prevGsmViews.stream().forEach(e -> e.setServerName(serverName));
            gsmView.addAll(prevGsmViews);
          }
          gsmViewData = gsmView.toArray(new GsmView[gsmView.size()]);
          isUpdating = true;
        }
      }

      @Override
      public void updateGUIComponent() {
        if (isUpdating) {
          setGsmViewData(gsmViewPanel);
        }
      }

    });
  }

  public GsmView[] getGsmViewData() {
    return gsmViewData;
  }

  public CellEqualizerAlgorithm getCellEqualizerAlgorithm() {
    return cellEqualizerAlgorithm;
  }

  public GsmViewRefresher setCellEqualizerAlgorithm(final CellEqualizerAlgorithm cellEqualizerAlgorithm) {
    this.cellEqualizerAlgorithm = cellEqualizerAlgorithm;
    return this;
  }

  public void setGsmViewData(final GsmViewPanel gsmViewPanel) {
    if (gsmViewData == null) {
      gsmViewPanel.getGsmViewTable().setData();
    } else {
      gsmViewPanel.getGsmViewTable().setData(gsmViewData);
    }
    gsmViewPanel.updateCellEqualizerAlgorithm(cellEqualizerAlgorithm);
  }

  public void resetGsmViewStatistic(final List<GsmView> gsmViews, final String server) {
    addExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "resetGsmViewStatistic";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        beansPool.getGsmViewBean().resetGsmViewStatistic(MainApp.clientUID, gsmViews, server);
      }

      @Override
      public void updateGUIComponent() {
      }

    });

  }

}
