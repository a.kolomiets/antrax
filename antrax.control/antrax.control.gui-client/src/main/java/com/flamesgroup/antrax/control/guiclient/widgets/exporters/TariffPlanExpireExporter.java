package com.flamesgroup.antrax.control.guiclient.widgets.exporters;

import com.flamesgroup.antrax.control.swingwidgets.table.CellExporter;
import com.flamesgroup.commons.Pair;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class TariffPlanExpireExporter implements CellExporter {

  @Override
  public String getExportValue(final Object value) {
    if (value == null) {
      return "";
    }

    Long expireLong = (Long) ((Pair) value).first();
    if (expireLong <= 0) {
      return "";
    }

    LocalDate currentDate = Instant.ofEpochMilli((Long) ((Pair) value).second()).atZone(ZoneId.systemDefault()).toLocalDate();
    LocalDate expireDate = Instant.ofEpochMilli(expireLong).atZone(ZoneId.systemDefault()).toLocalDate();
    long daysBetween = ChronoUnit.DAYS.between(currentDate, expireDate);
    if (daysBetween > 0) {
      if (daysBetween > 1) {
        return "in " + daysBetween + " days";
      } else {
        return "in " + daysBetween + " day";
      }
    } else if (daysBetween < 0) {
      daysBetween = Math.abs(daysBetween);
      if (daysBetween > 1) {
        return daysBetween + " days ago";
      } else {
        return daysBetween + " day ago";
      }
    } else {
      return "today";
    }
  }

}
