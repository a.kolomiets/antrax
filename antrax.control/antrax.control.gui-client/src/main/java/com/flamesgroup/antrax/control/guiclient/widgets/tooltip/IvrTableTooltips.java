/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public final class IvrTableTooltips {

  private IvrTableTooltips() {
  }

  public static String getServerTooltip() {
    return new TooltipHeaderBuilder("Voice server").build();
  }

  public static String getSimGroupTooltip() {
    return new TooltipHeaderBuilder("SIM group").build();
  }

  public static String getIvrTemplatesNameTooltip() {
    return new TooltipHeaderBuilder("Name").build();
  }

  public static String getIvrTemplatesDropReasonTooltip() {
    return new TooltipHeaderBuilder("Drop reason").build();
  }

  public static String getIvrTemplatesDropCodeTooltip() {
    return new TooltipHeaderBuilder("Drop code").build();
  }
}
