/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.indicators;

import com.flamesgroup.antrax.control.guiclient.utils.SystemHelper;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class JIndicatorBar {

  private static final int SPACE_LENGTH_WIN = 3;
  private static final int SPACE_LENGTH_OTHER = 1;

  private final JPanel bar;
  private final Dimension spaceDim = new Dimension(SystemHelper.isWindows() ? SPACE_LENGTH_WIN : SPACE_LENGTH_OTHER, 0);
  private final List<JIndicator<?>> indicatorIndexes = new ArrayList<>();
  private final Map<JIndicator<?>, IndicatorProperties> indicators = new HashMap<>();

  public JIndicatorBar() {
    bar = new JPanel(new FlowLayout(FlowLayout.RIGHT));
  }

  public JComponent getComponent() {
    return bar;
  }

  public void addIndicator(final JIndicator<?> indicator) {
    IndicatorProperties prop = new IndicatorProperties();
    addInBar(indicator, null);
    //    JToolBar.Separator separator = appendInBar(indicator);
    //    if (separator != null) {
    //      prop.setSeparator(separator);
    //    }
    indicator.addVisibleListener(prop.getVisibleListener());
    indicators.put(indicator, prop);
    indicatorIndexes.add(indicator);
  }

  public void removeIndicator(final JIndicator<?> indicator) {
    IndicatorProperties prop = indicators.get(indicator);
    indicator.removeVisibleListener(prop.getVisibleListener());
    removeFromBar(indicator);
    indicators.remove(indicator);
    indicatorIndexes.remove(indicator);
  }

  private void removeFromBar(final JIndicator<?> indicator) {
    IndicatorProperties prop = indicators.get(indicator);
    if (prop.getSeparator() != null) {
      bar.remove(prop.getSeparator());
    }
    bar.remove(indicator.getComponent());
  }

  private void addInBar(final JIndicator<?> indicator, final Integer index) {
    if (bar.getComponentCount() == 0 || index == null) {
      if (bar.getComponentCount() == 0) {
        bar.add(indicator.getComponent());
      } else {
        JToolBar.Separator separator = new JToolBar.Separator(spaceDim);
        bar.add(separator);
        bar.add(indicator.getComponent());
      }
    } else {
      // convert index
      int posIndex = index;
      if (index > 0) {
        posIndex = 2 * index + 1;
      }

      if (posIndex > bar.getComponentCount()) {
        posIndex = bar.getComponentCount();
      }

      JToolBar.Separator separator = new JToolBar.Separator(spaceDim);
      if (posIndex == 0) {
        bar.add(indicator.getComponent(), posIndex);
        bar.add(separator, posIndex + 1);
      } else {
        bar.add(separator, posIndex);
        bar.add(indicator.getComponent(), posIndex + 1);
      }
    }
  }

  private static class IndicatorProperties {
    private JToolBar.Separator separator;
    private final JIndicator.IndicatorVisibleListener visibleListener = new ItemsIndicatorVisibleListener();

    public JToolBar.Separator getSeparator() {
      return separator;
    }

    public void setSeparator(final JToolBar.Separator separator) {
      this.separator = separator;
    }

    public JIndicator.IndicatorVisibleListener getVisibleListener() {
      return visibleListener;
    }
  }

  private static class ItemsIndicatorVisibleListener implements JIndicator.IndicatorVisibleListener {

    @Override
    public void onVisibleChanged(final JIndicator<?> indicator, final boolean oldValue, final boolean newValue) {
      //      if (oldValue != newValue) {
      //        if (newValue) { // visible
      //          int index = indicatorIndexes.indexOf(indicator);
      //          addInBar(indicator, index);
      //        } else { // invisible
      //          removeFromBar(indicator);
      //        }
      //      }
    }

  }

}
