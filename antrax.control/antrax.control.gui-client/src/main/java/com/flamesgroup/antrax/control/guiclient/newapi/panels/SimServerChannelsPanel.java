/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.indicators.SimChannelIndicators;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.simruntime.SimChannelsFilterPanelBuilder;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.simruntime.SimChannelsPanelTransferHandler;
import com.flamesgroup.antrax.control.guiclient.panels.SimChannelsTable;
import com.flamesgroup.antrax.control.guiclient.panels.TariffPlanEndDateDialog;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.ImportParserHelper;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.editor.ExtendedComboBox;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ElementSelectionListener;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.IMEIException;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.MaskFormatter;

public class SimServerChannelsPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 6422618656953463181L;

  private final String server;

  private RefresherThread refresherThread;
  private SimChannelsPanelRefresher refresher;

  private static final SimChannelsTable table = new SimChannelsTable();
  private static final JScrollPane scrollPaneTable = new JScrollPane(table);
  private static final JPanel filterPanel = new SimChannelsFilterPanelBuilder(table).getFiltersPanel();
  private static final JCheckBox showEmptyHolders = new JCheckBox("Show Empty Holders");
  private static final JCheckBox showUnLiveHolders = new JCheckBox("Show unavailable Holders");
  private static final JReflectiveBar showPanel = new JReflectiveBar();

  private static final AtomicBoolean loadProperties = new AtomicBoolean();
  private static final AtomicBoolean saveProperties = new AtomicBoolean();

  private final Box toolbar = new Box(BoxLayout.Y_AXIS);
  private final ExtendedComboBox simGroupBox = new ExtendedComboBox();
  private final SimChannelIndicators channelsIndicators = new SimChannelIndicators();
  private final GuiProperties guiProperties;
  private JTextField phoneNumberEditor;
  private JButton importSimUIDsButton;
  private JButton csvButton;
  private JButton enableButton;
  private JButton disableButton;
  private JButton lockButton;
  private JButton noteButton;
  private JButton invertAllowInternet;
  private JButton unlockButton;
  private JButton resetScriptStatesButton;
  private JButton resetImeiButton;
  private JButton tariffPlanEndDateButton;
  private JButton resetSimCardStatisticButton;
  private final JContextSearch searchField = new JContextSearch();
  private JFormattedTextField imeiEditor;
  private boolean editable;

  private abstract class BaseSimChannelsPanelExecution implements RefresherExecution {

    private final List<SimViewData> selectedElems;

    public BaseSimChannelsPanelExecution() {
      selectedElems = table.getSelectedElems();
    }

    protected String getSimData() {
      return selectedElems.toString();
    }

    protected ICCID[] getUIDS() {
      ICCID[] retval = new ICCID[selectedElems.size()];
      Iterator<SimViewData> dataIter = selectedElems.iterator();
      for (int i = 0; i < retval.length; ++i) {
        retval[i] = dataIter.next().getUid();
      }
      return retval;
    }

    protected List<ICCID> getUidsWithNotNullImei() {
      return selectedElems.stream().filter(simData -> simData.getImei() != null).map(SimViewData::getUid).collect(Collectors.toList());
    }

  }

  public SimServerChannelsPanel(final String server) {
    super(new BorderLayout());
    add(toolbar, BorderLayout.NORTH);
    toolbar.add(createToolbar1(), 0);
    toolbar.add(createToolbar2(), 1);
    add(channelsIndicators.getComponent(), BorderLayout.SOUTH);

    showPanel.addToLeft(showEmptyHolders);
    showEmptyHolders.addActionListener(e -> {
      if (refresher == null) {
        return;
      }
      refresher.setShowEmptyHolders(showEmptyHolders.isSelected());
      refresherThread.forceRefresh();
    });
    showPanel.addToLeft(showUnLiveHolders);
    showUnLiveHolders.addActionListener(e -> {
      if (refresher == null) {
        return;
      }
      refresher.setShowUnliveHolders(showUnLiveHolders.isSelected());
      refresherThread.forceRefresh();
    });
    this.server = server;

    table.addElementSelectionListener(new ElementSelectionListener<SimViewData>() {

      @Override
      public void handleSelectionChanged(final List<SimViewData> currSelection, final List<SimViewData> prevSelection) {
        if (simGroupBox.isFocusOwner()) {
          return;
        }

        String simGroupName = currSelection.stream().filter(d -> d.getSimGroupName() != null).map(SimViewData::getSimGroupName).findFirst().orElse(null);
        if (simGroupName == null) {
          simGroupBox.setSynteticSelectedItem(null);
          return;
        }

        for (int i = 0; i < simGroupBox.getItemCount(); ++i) {
          SimpleSimGroup group = (SimpleSimGroup) simGroupBox.getItemAt(i);
          if (group == null) {
            group = new SimpleSimGroup(new SimNoGroup());
          }

          if (group.getName().equals(simGroupName)) {
            simGroupBox.setSynteticSelectedIndex(i);
            return;
          }
        }
      }
    });

    table.addElementSelectionListener(new ElementSelectionListener<SimViewData>() {

      private void setEnableComponent(final boolean enable) {
        csvButton.setEnabled(enable);
        enableButton.setEnabled(enable);
        disableButton.setEnabled(enable);
        lockButton.setEnabled(enable);
        noteButton.setEnabled(enable);
        invertAllowInternet.setEnabled(enable);
        tariffPlanEndDateButton.setEnabled(enable);
        resetSimCardStatisticButton.setEnabled(enable);
        unlockButton.setEnabled(enable);
        resetScriptStatesButton.setEnabled(enable);
        resetImeiButton.setEnabled(enable);
        simGroupBox.setEnabled(enable);
      }

      private void checkForEnableButton(final List<SimViewData> selection) {
        for (SimViewData simViewData : selection) {
          if (simViewData.isEmpty()) {
            importSimUIDsButton.setEnabled(false);
            setEnableComponent(false);
            return;
          }
        }
        if (editable) {
          importSimUIDsButton.setEnabled(true);
        }
        setEnableComponent(true);
      }

      @Override
      public void handleSelectionChanged(final List<SimViewData> currSelection, final List<SimViewData> prevSelection) {
        channelsIndicators.setSelected(currSelection.size());
        checkForEnableButton(currSelection);
        if (currSelection.size() == 1) {
          PhoneNumber phone = currSelection.iterator().next().getPhoneNumber();
          IMEI imei = currSelection.iterator().next().getImei();
          if (!phoneNumberEditor.isFocusOwner() || (phoneNumberEditor.getText().isEmpty())) {
            phoneNumberEditor.setText(phone != null && !phone.isPrivate() ? phone.getValue() : "");
          }
          if (!imeiEditor.isFocusOwner()) {
            imeiEditor.setText(imei != null ? imei.getValue() : null);
          }
        } else {
          phoneNumberEditor.setText("");
          imeiEditor.setText(null);
        }
      }
    });

    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent e) {
        channelsIndicators.setTotal(table.getModel().getRowCount());
        channelsIndicators.setTotalLocked(countLocked(table.getElems()));
      }

      private int countLocked(final Collection<SimViewData> elems) {
        return (int) elems.stream().filter(SimViewData::isLocked).count();
      }

    });

    table.getRowSorter().addRowSorterListener(new RowSorterListener() {

      @Override
      public void sorterChanged(final RowSorterEvent e) {
        channelsIndicators.setFiltered(table.getRowSorter().getViewRowCount());
        channelsIndicators.setLocked(countLocked(table));

      }

      private int countLocked(final SimChannelsTable table) {
        int retval = 0;
        int rowCount = table.getRowSorter().getViewRowCount();
        for (int i = 0; i < rowCount; ++i) {
          if (table.getElemAt(i).isLocked()) {
            retval++;
          }
        }
        return retval;
      }
    });

    setTransferHandler(new SimChannelsPanelTransferHandler(searchField));
    guiProperties = new GuiProperties(SimServerChannelsPanel.class.getSimpleName()) {
      private static final String FILTERS = "filters";
      private static final String SHOW_EMPTY_HOLDERS = "showEmptyHolders";
      private static final String SHOW_UN_LIVE_HOLDERS = "showUnLiveHolders";


      @Override
      protected void restoreOutProperties() {
        String filtersProperty = properties.getProperty(FILTERS);
        if (filtersProperty != null) {
          Boolean[] filters = gson.fromJson(filtersProperty, Boolean[].class);
          int i = 0;
          for (Component component : filterPanel.getComponents()) {
            if (component instanceof JCheckBox) {
              ((JCheckBox) component).setSelected(filters[i]);
              i++;
            }
          }
        }
        String showEmptyHoldersString = properties.getProperty(SHOW_EMPTY_HOLDERS, "false");
        showEmptyHolders.setSelected(Boolean.valueOf(showEmptyHoldersString));
        String showUnLiveHoldersString = properties.getProperty(SHOW_UN_LIVE_HOLDERS, "false");
        showUnLiveHolders.setSelected(Boolean.valueOf(showUnLiveHoldersString));
      }

      @Override
      protected void saveOutProperties() {
        List<Boolean> filters = new ArrayList<>();
        for (Component component : filterPanel.getComponents()) {
          if (component instanceof JCheckBox) {
            filters.add(((JCheckBox) component).isSelected());
          }
        }
        properties.setProperty(FILTERS, gson.toJson(filters));
        properties.setProperty(SHOW_EMPTY_HOLDERS, String.valueOf(showEmptyHolders.isSelected()));
        properties.setProperty(SHOW_UN_LIVE_HOLDERS, String.valueOf(showUnLiveHolders.isSelected()));
      }
    };
    if (!loadProperties.getAndSet(true)) {
      table.getUpdatableTableProperties().restoreProperties();
      guiProperties.restoreProperties();
    }
  }

  private JReflectiveBar createToolbar1() {
    JReflectiveBar retval = new JReflectiveBar();
    searchField.registerSearchItem(table, "Sim Channels Table");
    retval.addToRight(searchField);
    retval.addToLeft(importSimUIDsButton = createImportSimButton());
    csvButton = createExportToCsvButton();
    retval.addToLeft(csvButton);
    enableButton = createEnableButton(true, "Enable selected sim cards", "/img/simstate/simstate_enable.png");
    retval.addToLeft(enableButton);
    disableButton = createEnableButton(false, "Disable selected sim cards", "/img/simstate/simstate_disable.png");
    retval.addToLeft(disableButton);
    lockButton = createLockButton(true, "Lock selected sim cards", "Enter lock sim card reason", "/img/buttons/button_lock.png");
    retval.addToLeft(lockButton);
    unlockButton = createLockButton(false, "Unlock selected sim cards", "Enter unlock sim card reason", "/img/buttons/button_unlock.png");
    retval.addToLeft(unlockButton);
    resetScriptStatesButton = createResetScriptStatesButton();
    retval.addToLeft(resetScriptStatesButton);
    resetImeiButton = createResetIMEIButton();
    retval.addToLeft(resetImeiButton);
    noteButton = createNoteButton();
    retval.addToLeft(noteButton);
    invertAllowInternet = createInvertAllowInternet();
    retval.addToLeft(invertAllowInternet);
    tariffPlanEndDateButton = createTariffPlanEndDateButton();
    retval.addToLeft(tariffPlanEndDateButton);
    resetSimCardStatisticButton = createResetSimCardStatisticButton();
    retval.addToLeft(resetSimCardStatisticButton);
    return retval;
  }

  private JButton createInvertAllowInternet() {
    final JButton retval = new JButton();
    retval.setToolTipText("Invert possibility to allow the internet");
    retval.setIcon(IconPool.getShared("/img/server_16.png"));
    retval.addActionListener(e -> {
      retval.setEnabled(false);
      refresher.addExecution(new BaseSimChannelsPanelExecution() {

        @Override
        public void updateGUIComponent() {
          retval.setEnabled(true);
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          beansPool.getActivationBean().invertAllowInternet(MainApp.clientUID, getUIDS());
        }

        @Override
        public String describeExecution() {
          return "Invert possibility to allow the internet: " + getSimData();
        }
      });
    });
    return retval;
  }

  private JButton createResetScriptStatesButton() {
    final JButton retval = new JButton();
    retval.setToolTipText("Reset scripts for the selected cards");
    retval.setIcon(IconPool.getShared("/img/drive_delete.png"));
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        if (JOptionPane.showConfirmDialog(retval, "This operation will erase all memory of scripts for selected cards.\n Do you realy want to reset scripts states?") != JOptionPane.YES_OPTION) {
          return;
        }
        retval.setEnabled(false);
        refresher.addExecution(new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
            retval.setEnabled(true);
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            beansPool.getActivationBean().resetScriptsStates(MainApp.clientUID, getUIDS());
          }

          @Override
          public String describeExecution() {
            return "Reset scripts states for: " + getSimData();
          }
        });
      }
    });
    return retval;
  }

  private JButton createResetIMEIButton() {
    final JButton resetIMEIButton = new JButton();
    resetIMEIButton.setToolTipText("Reset IMEI for the selected cards");
    resetIMEIButton.setIcon(IconPool.getShared("/img/barcode.png"));
    resetIMEIButton.addActionListener(e -> {
      if (JOptionPane.showConfirmDialog(resetIMEIButton, "This operation will reset IMEI for selected cards.\n Do you realy want to reset IMEI?") != JOptionPane.YES_OPTION) {
        return;
      }
      resetIMEIButton.setEnabled(false);
      refresher.addExecution(new BaseSimChannelsPanelExecution() {

        @Override
        public void updateGUIComponent() {
          resetIMEIButton.setEnabled(true);
          imeiEditor.setValue(null);
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          beansPool.getActivationBean().resetIMEI(MainApp.clientUID, getUidsWithNotNullImei());
        }

        @Override
        public String describeExecution() {
          return "Reset IMEI for: " + getSimData();
        }
      });
    });
    return resetIMEIButton;
  }

  private JButton createLockButton(final boolean lock, final String dialogTest, final String tooltip, final String iconPath) {
    final JButton retval = new JButton();
    retval.setToolTipText(tooltip);
    retval.setIcon(IconPool.getShared(iconPath));
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        final String lockReason = JOptionPane.showInputDialog(retval, dialogTest, "Manually by user");
        if (lockReason == null) {
          return;
        }
        retval.setEnabled(false);
        refresher.addExecution(new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
            retval.setEnabled(true);
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            beansPool.getActivationBean().lockSimUnit(MainApp.clientUID, getUIDS(), lock, lockReason);
          }

          @Override
          public String describeExecution() {
            return "Lock sim cards: " + getSimData();
          }
        });
      }
    });
    return retval;
  }

  private JButton createNoteButton() {
    final JButton retval = new JButton();
    retval.setToolTipText("Note for the selected sim cards");
    retval.setIcon(IconPool.getShared("/img/document.png"));
    retval.addActionListener(e -> {
      List<SimViewData> selectedElems = table.getSelectedElems();
      String noteValue = "";
      if (selectedElems.size() == 1) {
        noteValue = selectedElems.get(0).getNote();
      }
      final String note = JOptionPane.showInputDialog(retval, "Enter note", noteValue);
      if (note == null) {
        return;
      }
      retval.setEnabled(false);
      refresher.addExecution(new BaseSimChannelsPanelExecution() {

        @Override
        public void updateGUIComponent() {
          retval.setEnabled(true);
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          beansPool.getActivationBean().noteSimUnit(MainApp.clientUID, getUIDS(), note);
        }

        @Override
        public String describeExecution() {
          return "Note sim cards: " + getSimData();
        }
      });
    });
    return retval;
  }

  private JButton createTariffPlanEndDateButton() {
    final JButton retval = new JButton();
    retval.setToolTipText("Set the end date of the tariff plan");
    retval.setIcon(IconPool.getShared("/img/tariff_end_date.png"));
    retval.addActionListener(e -> {
      TariffPlanEndDateDialog dialog = new TariffPlanEndDateDialog(SwingUtilities.getWindowAncestor(SimServerChannelsPanel.this));
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
      dialog.setLocationRelativeTo(SimServerChannelsPanel.this);
      if (dialog.showDialog()) {
        long endDate = dialog.getEndDate();
        List<SimViewData> selected = table.getSelectedElems();
        if (!selected.isEmpty()) {
          retval.setEnabled(false);
          refresher.addExecution(new BaseSimChannelsPanelExecution() {
            @Override
            public String describeExecution() {
              return "Set the end date of the tariff plan for: " + getSimData();
            }

            @Override
            public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
              beansPool.getActivationBean().setTariffPlanEndDate(MainApp.clientUID, getUIDS(), endDate);
            }

            @Override
            public void updateGUIComponent() {
              retval.setEnabled(true);
            }
          });
        }
      }
    });

    return retval;
  }

  private JButton createResetSimCardStatisticButton() {
    final JButton resetSimCardStatisticButtonLocal = new JButton();
    resetSimCardStatisticButtonLocal.setToolTipText("Reset SIM card statistic for the selected cards");
    resetSimCardStatisticButtonLocal.setIcon(IconPool.getShared("/img/buttons/clear.png"));
    resetSimCardStatisticButtonLocal.addActionListener(e -> {
      if (JOptionPane.showConfirmDialog(resetSimCardStatisticButtonLocal, "This operation will reset all statistic for the selected cards.\n Do you really want to do this?")
          != JOptionPane.YES_OPTION) {
        return;
      }
      if (table.getSelectedElems().isEmpty()) {
        JOptionPane.showMessageDialog(resetSimCardStatisticButtonLocal, "Please select one or more SIM card(s)");
        return;
      }
      resetSimCardStatisticButtonLocal.setEnabled(false);
      refresher.addExecution(new BaseSimChannelsPanelExecution() {

        @Override
        public void updateGUIComponent() {
          resetSimCardStatisticButtonLocal.setEnabled(true);
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          beansPool.getActivationBean().resetSimCardStatistic(MainApp.clientUID, getUIDS());
        }

        @Override
        public String describeExecution() {
          return "Reset SIM card statistic for: " + getSimData();
        }
      });
    });
    return resetSimCardStatisticButtonLocal;
  }

  private JButton createEnableButton(final boolean enable, final String tooltip, final String iconPath) {
    final JButton retval = new JButton();
    retval.setToolTipText(tooltip);
    retval.setIcon(IconPool.getShared(iconPath));
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        retval.setEnabled(false);
        refresher.addExecution(new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
            retval.setEnabled(true);
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            beansPool.getActivationBean().enableSimUnit(MainApp.clientUID, getUIDS(), enable);
          }

          @Override
          public String describeExecution() {
            return "Enable sim cards: " + getSimData();
          }
        });
      }
    });
    return retval;
  }

  private JButton createImportSimButton() {
    final JButton retval = new JButton();
    final Icon importIcon = IconPool.getShared("/img/buttons/import.png");
    retval.setToolTipText("Import sim phone numbers");
    retval.setIcon(importIcon);
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        refresher.addExecution(new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            final JFileChooser fileOpen = new JFileChooser();
            if (fileOpen.showDialog(null, "Open file") != JFileChooser.APPROVE_OPTION) {
              return;
            }

            WaitingDialog wd = new WaitingDialog(importIcon, MainApp.getAppName(), "Import sim phone numbers...");
            wd.show(SimServerChannelsPanel.this, new Runnable() {

              @Override
              public void run() {
                try {
                  Map<ICCID, PhoneNumber> simUIDs = ImportParserHelper.parseSimUIDs(new FileInputStream(fileOpen.getSelectedFile()));
                  beansPool.getConfigBean().importSimUIDs(MainApp.clientUID, simUIDs);

                  MessageUtils.showInfo(SimServerChannelsPanel.this, MainApp.getAppName(), "Import completed");
                } catch (Exception e) {
                  MessageUtils.showError(SimServerChannelsPanel.this, "Error while, import sim phone numbers", e);
                }
              }
            });
          }

          @Override
          public String describeExecution() {
            return "Import sim phone numbers to db";
          }
        });
      }
    });
    return retval;
  }

  private JButton createExportToCsvButton() {
    return new JExportCsvButton(table, "simchannels");
  }


  private JPanel createToolbar2() {
    JPanel retval = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 0));
    retval.add(new JLabel("Pin Code"));
    retval.add(createPinCodeEditor());
    retval.add(new JLabel("Phone Number"));
    retval.add(phoneNumberEditor = createPhoneNumberEditor());
    retval.add(new JLabel("IMEI"));
    retval.add(imeiEditor = createIMEIEditor());
    retval.add(new JLabel("Sim Group"));
    retval.add(simGroupBox);

    simGroupBox.setPreferredSize(new Dimension(200, simGroupBox.getPreferredSize().height));

    simGroupBox.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        Object selectedItem = simGroupBox.getSelectedItem();
        SimpleSimGroup group = (selectedItem == null) ? new SimpleSimGroup(new SimNoGroup()) : (SimpleSimGroup) selectedItem;
        refresher.addExecution(new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            try {
              beansPool.getConfigBean().updateSimCardGroup(MainApp.clientUID, transactionManager.getGuaranteedTransactionId(), getUIDS(), group);
            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          @Override
          public String describeExecution() {
            return "Set group " + group + " for " + getSimData();
          }
        });
      }
    });
    return retval;
  }

  private JFormattedTextField createIMEIEditor() {
    try {
      final JFormattedTextField retval = new JFormattedTextField(new MaskFormatter("######''##''######")) {

        private static final long serialVersionUID = 9159098186956155345L;

        @Override
        public void setText(final String t) {
          try {
            if (t == null) {
              setText("000000'00'000000");
            } else if (t.length() == 15) {
              setImei(t.substring(0, 14));
            } else if (t.length() == 14) {
              setImei(t);
            } else if (t.length() == 16) {
              super.setText(t);
            }
          } catch (final IMEIException e1) {
            throw new RuntimeException(e1);
          }
        }

        private void setImei(final String t) throws IMEIException {
          IMEI imei = IMEI.valueOf(t);
          StringBuilder str = new StringBuilder();
          str.append(new String(imei.getTac())).append("'");
          str.append(new String(imei.getFac())).append("'");
          str.append(new String(imei.getSnr()));
          super.setText(str.toString());
        }

      };
      retval.setText(null);
      retval.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          String imeiString = retval.getText().replaceAll("'", "");
          IMEI imei;
          try {
            imei = IMEI.valueOf(imeiString);
          } catch (final IMEIException e1) {
            throw new RuntimeException(e1);
          }
          BaseSimChannelsPanelExecution execution = new BaseSimChannelsPanelExecution() {

            @Override
            public void updateGUIComponent() {
            }

            @Override
            public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
              for (ICCID uid : getUIDS()) {
                if (imei != null) {
                  beansPool.getConfigBean().updateIMEI(MainApp.clientUID, uid, imei);
                }
              }
            }

            @Override
            public String describeExecution() {
              return "Set imei " + imei + " for " + getSimData();
            }
          };

          moveSelectionFurther();
          retval.selectAll();
          refresher.addExecution(execution);
        }
      });
      return retval;
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  private JTextField createPhoneNumberEditor() {
    final JTextField retval = new JTextField(12);
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        final PhoneNumber phoneNumber;
        try {
          phoneNumber = getPhoneNumber();
        } catch (Exception ignored) {
          MessageUtils.showError(retval, "Bad number", "Wrong phone number: " + retval.getText());
          return;
        }

        BaseSimChannelsPanelExecution execution = new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            beansPool.getConfigBean().setPhoneNumber(MainApp.clientUID, transactionManager.getGuaranteedTransactionId(), getUIDS(), phoneNumber);
          }

          @Override
          public String describeExecution() {
            return "set phone number " + phoneNumber + " to: " + getSimData();
          }
        };
        moveSelectionFurther();
        retval.selectAll();
        refresher.addExecution(execution);
      }

      private PhoneNumber getPhoneNumber() {
        if (!retval.getText().isEmpty()) {
          return new PhoneNumber(retval.getText());
        }
        return null;
      }
    });
    return retval;
  }

  private Component createPinCodeEditor() {
    final JTextField field = new JTextField(4);
    field.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        final CHV chv1 = new CHV(field.getText());
        BaseSimChannelsPanelExecution execution = new BaseSimChannelsPanelExecution() {

          @Override
          public void updateGUIComponent() {
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            beansPool.getActivationBean().removeCHV(MainApp.clientUID, getUIDS(), chv1);
          }

          @Override
          public String describeExecution() {
            return "remove pin code for: " + getSimData();
          }
        };
        moveSelectionFurther();
        field.selectAll();
        refresher.addExecution(execution);
      }

    });
    return field;
  }

  private void moveSelectionFurther() {
    if (table.getSelectedRowCount() == 1) {
      int nextSelected = table.getSelectedRow() + 1;
      if (nextSelected < table.getRowCount()) {
        table.selectRow(nextSelected);
      }
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void postInitialize(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    this.refresherThread = refresherThread;
    this.refresher = new SimChannelsPanelRefresher(server, refresherThread, transactionManager, table, simGroupBox);
  }

  @Override
  public void release() {
    if (!saveProperties.getAndSet(true)) {
      table.getUpdatableTableProperties().saveProperties();
      guiProperties.saveProperties();
    }
    if (refresher != null) {
      refresher.interrupt();
    }
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      toolbar.add(filterPanel, 2);
      toolbar.add(showPanel, 1);
      add(scrollPaneTable, BorderLayout.CENTER);
      refresher.setShowEmptyHolders(showEmptyHolders.isSelected());
      refresher.setShowUnliveHolders(showUnLiveHolders.isSelected());
      refresher.updateSimGroups();
      refresher.updateSimData();
      phoneNumberEditor.setText("");
      imeiEditor.setText(null);
      searchField.applyFilter();
      refresherThread.addRefresher(refresher);
    } else {
      remove(scrollPaneTable);
      toolbar.remove(filterPanel);
      toolbar.remove(showPanel);
      refresher.clearPrevSimViewDataAll();
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void setEditable(final boolean editable) {
    this.editable = editable;
  }

}
