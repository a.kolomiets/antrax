/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.swingwidgets.JLabeledComponent;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class SimServerEditor {

  private final Box advancedPanel = new Box(BoxLayout.Y_AXIS);

  private final JCheckBox basicCheckBox = new JCheckBox();
  private final JCheckBox advancedCheckBox = new JCheckBox();

  private final JLabeledComponent lblBasicCheckBox;

  private final List<JLabeledComponent> components = new LinkedList<>();

  private final BasicAdvancedPanel panel;

  public SimServerEditor() {
    JLabeledComponent lblAdvancedCheckBox = r(new JLabeledComponent("Enable", advancedCheckBox));
    lblBasicCheckBox = r(new JLabeledComponent("Enable", basicCheckBox));

    advancedPanel.add(lblAdvancedCheckBox);

    basicCheckBox.addActionListener(e -> advancedCheckBox.setSelected(basicCheckBox.isSelected()));
    advancedCheckBox.addActionListener(e -> basicCheckBox.setSelected(advancedCheckBox.isSelected()));

    panel = new BasicAdvancedPanel(lblBasicCheckBox, advancedPanel);
  }

  private JLabeledComponent r(final JLabeledComponent jLabeledComponent) {
    components.add(jLabeledComponent);
    return jLabeledComponent;
  }

  public void setEditable(final boolean value) {
    basicCheckBox.setEnabled(value);
    advancedCheckBox.setEnabled(value);
  }

  public BasicAdvancedPanel getComponent() {
    return panel;
  }

  public void readEditorsToElem(final IServerData server) {
    server.setSimServerEnabled(basicCheckBox.isSelected());
  }

  public void writeElemToEditors(final IServerData server) {
    basicCheckBox.setSelected(server.isSimServerEnabled());
    advancedCheckBox.setSelected(server.isSimServerEnabled());
  }

  public List<JLabeledComponent> listLabeledComponents() {
    return components;
  }

  public Object exportToObject() {
    HashMap<Object, Object> retval = new HashMap<>();
    retval.put(advancedCheckBox, advancedCheckBox.isSelected());
    return retval;
  }
}
