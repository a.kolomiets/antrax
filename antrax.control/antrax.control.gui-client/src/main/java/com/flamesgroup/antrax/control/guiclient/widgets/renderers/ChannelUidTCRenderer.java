/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ChannelUidTCRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 1000596214053822397L;

  private final static Color LIGHT_COLOR = new Color(0xcdcdcd);
  private final static Color DARK_COLOR = Color.BLACK;

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    setForeground(isEmpty(value) ? LIGHT_COLOR : DARK_COLOR);
    Object valueLocal = getValue(value);
    if (valueLocal instanceof ChannelUID) {
      ChannelUID channelUID = (ChannelUID) valueLocal;
      setText(channelUID.getAliasCanonicalName());
      if (channelUID.getAliasDeviceUID() != null) {
        setFont(new Font(getFont().getName(), Font.ITALIC, getFont().getSize()));
        setToolTipText("Real channel UID: " + channelUID.getCanonicalName());
      } else {
        setToolTipText(null);
      }
    } else {
      setToolTipText("");
      setText("");
    }

    return component;
  }

  private Object getValue(final Object value) {
    if (value instanceof Pair<?, ?>) {
      Pair<?, ?> pair = (Pair<?, ?>) value;
      return pair.first();
    } else {
      return value;
    }
  }

  private boolean isEmpty(final Object value) {
    if (value instanceof Pair<?, ?>) {
      Pair<?, ?> pair = (Pair<?, ?>) value;
      return (Boolean) pair.second();
    } else {
      return false;
    }
  }

}
