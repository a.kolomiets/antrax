/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class GrayListDialog extends JDialog {

  private static final long serialVersionUID = 5187763398993178258L;

  private final JEditIntField period;
  private final JEditIntField maxRoutingRequestPerPeriod;
  private final JEditIntField blockPeriod;
  private final JEditIntField maxBlockCountBeforeMoveToBlackList;

  private final JCheckBox acdAnalyze;
  private final JEditIntField acdPeriod;
  private final JEditIntField maxMinAcdCallPerPeriod;
  private final JEditIntField minAcd;

  private final JLabel acdPeriodLabel;
  private final JLabel maxMinAcdCallPerPeriodLabel;
  private final JLabel minAcdLabel;

  private final JButton cancelButton;
  private final JButton okButton;

  private int componentsIndex;
  private boolean approved;

  public GrayListDialog(final Window owner, final GrayListConfig grayListConfig) {
    this(owner);

    period.setValue(grayListConfig.getPeriod());
    maxRoutingRequestPerPeriod.setValue(grayListConfig.getMaxRoutingRequestPerPeriod());
    blockPeriod.setValue(grayListConfig.getBlockPeriod());
    maxBlockCountBeforeMoveToBlackList.setValue(grayListConfig.getMaxBlockCountBeforeMoveToBlackList());

    AcdConfig acdConfig = grayListConfig.getAcdConfig();
    acdAnalyze.setSelected(acdConfig != null);
    setAcdConfigEditorEnable(acdConfig != null);
    if (acdConfig != null) {
      acdPeriod.setValue(acdConfig.getPeriod());
      maxMinAcdCallPerPeriod.setValue(acdConfig.getMaxMinAcdCallPerPeriod());
      minAcd.setValue(acdConfig.getMinAcd());
    }
  }

  public GrayListDialog(final Window owner) {
    super(owner);

    cancelButton = createCancelBtn();
    okButton = createOkBtn();

    GridBagLayout layout = new GridBagLayout();
    getContentPane().setLayout(layout);
    layout.rowWeights = new double[] {0, 0, 0, 1};

    add(new JLabel("Period: ", JLabel.RIGHT), period = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Max routing request per period: ", JLabel.RIGHT), maxRoutingRequestPerPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Block period: ", JLabel.RIGHT), blockPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Max block count before move to black list: ", JLabel.RIGHT), maxBlockCountBeforeMoveToBlackList = new JEditIntField(0, Integer.MAX_VALUE));

    add(new JLabel("ACD analyze: ", JLabel.RIGHT), acdAnalyze = new JCheckBox(), GridBagConstraints.EAST, new Insets(20, 2, 10, 2));

    add(acdPeriodLabel = new JLabel("Acd period: ", JLabel.RIGHT), acdPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(maxMinAcdCallPerPeriodLabel = new JLabel("Max min acd call per period: ", JLabel.RIGHT), maxMinAcdCallPerPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(minAcdLabel = new JLabel("Min acd: ", JLabel.RIGHT), minAcd = new JEditIntField(0, Integer.MAX_VALUE));

    add(createButtonsPanel(), new GridBagConstraints(0, componentsIndex, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

    setTitle("Gray list config");
    setMinimumSize(new Dimension(350, 150));
    setResizable(false);
    pack();

    ActionListener cancelListener = e -> cancelButton.doClick();
    ActionListener okListener = e -> okButton.doClick();

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    period.addActionListener(okListener);
    maxRoutingRequestPerPeriod.addActionListener(okListener);

    setAcdConfigEditorEnable(false);
    acdAnalyze.addItemListener(e -> setAcdConfigEditorEnable(e.getStateChange() == ItemEvent.SELECTED));
  }

  private void add(final JLabel lbl, final Component editor) {
    add(lbl, editor, GridBagConstraints.WEST, new Insets(2, 2, 2, 2));
  }

  private void add(final JLabel lbl, final Component editor, final int anchor, final Insets insets) {
    getContentPane().add(lbl, new GridBagConstraints(0, componentsIndex, 1, 1, 0, 0, anchor, GridBagConstraints.HORIZONTAL, insets, 0, 0));
    getContentPane().add(editor, new GridBagConstraints(1, componentsIndex, 1, 1, 1, 0, anchor, GridBagConstraints.HORIZONTAL, insets, 0, 0));
    componentsIndex++;
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    retval.add(okButton);
    retval.add(cancelButton);
    layout.putConstraint(SpringLayout.EAST, okButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, cancelButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, okButton, 0, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, cancelButton, 0, SpringLayout.SOUTH, retval);

    retval.setPreferredSize(okButton.getPreferredSize());
    return retval;
  }

  private JButton createCancelBtn() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(e -> setVisible(false));
    return retval;
  }

  private JButton createOkBtn() {
    final JButton retval = new JButton("OK");
    retval.addActionListener(e -> {
      approved = true;
      setVisible(false);
    });
    return retval;
  }

  private void setAcdConfigEditorEnable(final boolean enable) {
    acdPeriodLabel.setEnabled(enable);
    maxMinAcdCallPerPeriodLabel.setEnabled(enable);
    minAcdLabel.setEnabled(enable);
    acdPeriod.setEnabled(enable);
    maxMinAcdCallPerPeriod.setEnabled(enable);
    minAcd.setEnabled(enable);
  }

  private GrayListConfig getGrayListConfig() {
    if (approved) {
      if (acdAnalyze.isSelected()) {
        return new GrayListConfig(period.getIntValue(), maxRoutingRequestPerPeriod.getIntValue(), blockPeriod.getIntValue(), maxBlockCountBeforeMoveToBlackList.getIntValue(),
            new AcdConfig(acdPeriod.getIntValue(), maxMinAcdCallPerPeriod.getIntValue(), minAcd.getIntValue()));
      } else {
        return new GrayListConfig(period.getIntValue(), maxRoutingRequestPerPeriod.getIntValue(), blockPeriod.getIntValue(), maxBlockCountBeforeMoveToBlackList.getIntValue());
      }
    } else {
      return null;
    }
  }

  private static GrayListConfig getGrayListConfig(final GrayListDialog grayListDialog) {
    grayListDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    grayListDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    grayListDialog.setLocationRelativeTo(grayListDialog.getOwner());
    grayListDialog.setVisible(true);
    grayListDialog.dispose();

    return grayListDialog.getGrayListConfig();
  }

  public static GrayListConfig createNewGrayListConfig(final Component parent) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getGrayListConfig(new GrayListDialog(windowAncestor));
  }

  public static GrayListConfig createEditGrayListConfig(final Component parent, final GrayListConfig grayListConfig) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getGrayListConfig(new GrayListDialog(windowAncestor, grayListConfig));
  }

}
