/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class ScriptParamEditor extends JPanel {

  private static final long serialVersionUID = 5853980382777926185L;
  private final Map<PropertyEditor<?>, Object> values = new HashMap<>();
  private PropertyEditor<Object> editor;

  public ScriptParamEditor() {
    setLayout(new BorderLayout());
    add(new JLabel("Select type please"), BorderLayout.CENTER);
  }

  public void setScriptEditor(final PropertyEditor<Object> editor) {
    if (editor == null) {
      return;
    }
    // Saving old value
    if (this.editor != null) {
      values.put(editor, editor.getValue());
    }
    this.editor = editor;
    removeAll();

    add(editor.getEditorComponent());
    revalidate();
  }

  public Object getValue() {
    return editor.getValue();
  }

}
