/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.panels.registry.CreateRegistryEntryDialog;
import com.flamesgroup.antrax.control.guiclient.panels.registry.ImportValuesInKeyDialog;
import com.flamesgroup.antrax.control.guiclient.panels.registry.RegistryModel;
import com.flamesgroup.antrax.control.guiclient.panels.registry.RegistryPathTable;
import com.flamesgroup.antrax.control.guiclient.panels.registry.RegistryRefresher;
import com.flamesgroup.antrax.control.guiclient.panels.registry.RegistryValuesTable;
import com.flamesgroup.antrax.control.guiclient.panels.registry.SelectPathDialog;
import com.flamesgroup.antrax.control.guiclient.utils.ExportHelper;
import com.flamesgroup.antrax.control.guiclient.utils.ExportType;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ElementSelectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class CfgRegistryPanel extends JPanel implements AppPanel {
  private static final long serialVersionUID = -5477424850470722178L;

  private final Logger logger = LoggerFactory.getLogger(CfgRegistryPanel.class);

  private final RegistryModel model;
  private RegistryPathTable pathsList;
  private RegistryValuesTable valuesList;
  private RefresherThread refresherThread;
  private JComponent btnImport;
  private final JLabel counter = new JLabel();
  private final JLabel counterTotal = new JLabel();
  private final RegistryRefresher refresher;

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
  }

  @Override
  public void release() {
    valuesList.getUpdatableTableProperties().saveProperties();
    pathsList.getUpdatableTableProperties().saveProperties();
    refresher.interrupt();
  }

  public CfgRegistryPanel() {
    createUI();
    model = new RegistryModel(pathsList, valuesList);
    initializeListeners();
    refresher = new RegistryRefresher(model, pathsList, valuesList);
    valuesList.setName(getClass().getSimpleName() + "_valuesList");
    pathsList.setName(getClass().getSimpleName() + "_pathsList");
    valuesList.getUpdatableTableProperties().restoreProperties();
    pathsList.getUpdatableTableProperties().restoreProperties();
  }

  private void initializeListeners() {
    logger.debug("[{}] - setup listeners", this);
    pathsList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        if (refresher.isUpdating()) {
          return;
        }
        if (e.getValueIsAdjusting()) {
          return;
        }
        valuesList.setEnabled(false);

        model.showUpdatingMessage();
        refresherThread.forceRefresh();
      }
    });
    valuesList.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent arg0) {
        setTotalCounterText(valuesList.getModel().getRowCount());

      }
    });

    valuesList.addElementSelectionListener(new ElementSelectionListener<RegistryEntry>() {

      @Override
      public void handleSelectionChanged(final List<RegistryEntry> currSelection, final List<RegistryEntry> prevSelection) {
        if (currSelection.size() > 0) {
          setCounterText(currSelection.size());
          counter.setVisible(true);
        } else {
          counter.setVisible(false);
        }
      }
    });
  }

  private void setCounterText(final int rowCount) {
    StringBuilder counterText = new StringBuilder();
    counterText.append("<html>");
    counterText.append("<body>");
    counterText.append("<b>");
    counterText.append("Selected: ");
    counterText.append("</b>");
    counterText.append(rowCount);
    counterText.append("</body>");
    counterText.append("</html>");
    counter.setText(counterText.toString());
  }

  private void setTotalCounterText(final int rowCount) {
    StringBuilder counterText = new StringBuilder();
    counterText.append("<html>");
    counterText.append("<body>");
    counterText.append("<b>");
    counterText.append("Total: ");
    counterText.append("</b>");
    counterText.append(rowCount);
    counterText.append("</body>");
    counterText.append("</html>");
    counterTotal.setText(counterText.toString());
  }

  private void createUI() {
    logger.debug("[{}] - creating UI", this);
    pathsList = new RegistryPathTable();
    valuesList = new RegistryValuesTable();
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(pathsList), new JScrollPane(valuesList));
    splitPane.setDividerLocation(170);

    setLayout(new BorderLayout());

    add(splitPane, BorderLayout.CENTER);
    add(createToolbar(), BorderLayout.PAGE_START);
    setCounterText(0);
    setTotalCounterText(0);
    JPanel counterPanel = new JPanel();
    counterPanel.setPreferredSize(new Dimension(this.getPreferredSize().width + 200, counterPanel.getPreferredSize().height + 20));
    SpringLayout s = new SpringLayout();
    s.putConstraint(SpringLayout.EAST, counter, 300, SpringLayout.EAST, counterTotal);
    s.putConstraint(SpringLayout.EAST, counter, -150, SpringLayout.EAST, counterPanel);
    s.putConstraint(SpringLayout.WEST, counterTotal, -100, SpringLayout.EAST, counterPanel);

    counterPanel.setLayout(s);
    counterPanel.add(counterTotal);
    counterPanel.add(counter);
    add(counterPanel, BorderLayout.PAGE_END);
  }

  private Component createToolbar() {
    JContextSearch cs = new JContextSearch();
    cs.registerSearchItem(pathsList, "path");
    cs.registerSearchItem(valuesList, "values");

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(createAddButton());
    bar.addToLeft(createRemoveButton());
    bar.addToLeft(createMoveButton());
    bar.addToLeft(btnImport = createImportButton());
    JButton jExportCsvButton = new JButton();
    jExportCsvButton.setIcon(ExportType.CSV.getIcon());
    setToolTipText("Export to CSV");
    jExportCsvButton.addActionListener(e -> {
      String path = pathsList.getSelectedElem();
      if (path == null || path.isEmpty()) {
        MessageUtils.showInfo(this.getParent(), "Info", "Please select one item");
        return;
      }
      String filePrefix = "registry-" + path;
      new ExportHelper().exportTable(valuesList, ExportType.CSV, filePrefix, this.getRootPane());
    });
    bar.addToLeft(jExportCsvButton);
    bar.addToRight(new JLabel("<html><body><span color=red>NOTE</span>: There is <span color=red>NO TRANSACTION</span>s in registry"));
    bar.addToRight(cs);
    return bar;
  }

  private JComponent createMoveButton() {
    JButton moveButton = createToolbarButton(IconPool.getShared("/img/buttons/button-copy.png"));
    moveButton.setToolTipText("Move");
    moveButton.addActionListener(e -> {
      List<RegistryEntry> selectedElems = valuesList.getSelectedElems();
      if (selectedElems.isEmpty()) {
        return;
      }
      SelectPathDialog dialog = new SelectPathDialog(SwingUtilities.getWindowAncestor(CfgRegistryPanel.this));
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      dialog.setLocationRelativeTo(CfgRegistryPanel.this);
      if (dialog.showDialog(selectedElems.get(0).getPath(), model.getPaths())) {
        for (RegistryEntry selectedElem : selectedElems) {
          model.moveEntry(selectedElem, dialog.getPath());
        }
      }
    });
    return moveButton;
  }

  private JComponent createImportButton() {
    JButton retval = createToolbarButton(IconPool.getShared("/img/buttons/import.png"));
    retval.setToolTipText("Import");
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {

        ImportValuesInKeyDialog dlg = new ImportValuesInKeyDialog(SwingUtilities.getWindowAncestor(CfgRegistryPanel.this));
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dlg.setModalityType(ModalityType.DOCUMENT_MODAL);
        dlg.setLocationRelativeTo(CfgRegistryPanel.this);
        dlg.setVisible(true);
        if (dlg.isApplied()) {
          String key = dlg.getKey();
          String[] values = dlg.getValues();
          for (int i = 0; i < values.length; ++i) {
            model.addEntry(key, values[i]);
          }
          pathsList.selectElem(key);
        }
      }
    });
    return retval;
  }

  private JComponent createRemoveButton() {
    final JButton removeButton = createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"));
    removeButton.setToolTipText("Remove");

    removeButton.addActionListener(e -> {
      if (pathsList.hasFocus()) {
        if (pathsList.getSelectedRow() < 0) {
          return;
        }
        int response = JOptionPane.showConfirmDialog(CfgRegistryPanel.this, "Are you shure to delete all values of selected path",
            "Confirm removal", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (JOptionPane.OK_OPTION == response) {
          model.removeAllUnderPath(pathsList.getSelectedElem());
        }
      } else if (valuesList.hasFocus()) {
        List<RegistryEntry> selectedElems = valuesList.getSelectedElems();
        if (selectedElems.isEmpty()) {
          return;
        }
        for (RegistryEntry selectedElem : selectedElems) {
          model.removeValue(selectedElem);
        }
      }
    });
    return removeButton;
  }

  private JComponent createAddButton() {
    final JButton retval = createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"));
    retval.setToolTipText("Add");

    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        final CreateRegistryEntryDialog dialog = new CreateRegistryEntryDialog(SwingUtilities.getWindowAncestor(CfgRegistryPanel.this));
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setModalityType(ModalityType.DOCUMENT_MODAL);

        dialog.setLocationRelativeTo(CfgRegistryPanel.this);

        if (pathsList.getSelectedRow() >= 0) {
          dialog.setPath(pathsList.getSelectedElem());
        }

        dialog.getOkBtn().addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            int index = model.addEntry(dialog.getPath(), dialog.getValue());
            if (index != -1) {
              pathsList.selectRow(index);
            }
          }
        });
        dialog.showDialog();
      }
    });
    return retval;
  }

  private JButton createToolbarButton(final Icon icon) {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
    retval.setFocusable(false);
    return retval;
  }

}
