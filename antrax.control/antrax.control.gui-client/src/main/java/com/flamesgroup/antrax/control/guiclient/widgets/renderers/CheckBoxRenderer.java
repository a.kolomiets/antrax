/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import java.awt.*;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class CheckBoxRenderer extends JCheckBox implements TableCellRenderer, Serializable {

  private static final long serialVersionUID = 2059893188323247462L;

  private static final Border SAFE_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
  private static final Border DEFAULT_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
  protected static final Border noFocusBorder = DEFAULT_NO_FOCUS_BORDER;

  // We need a place to store the color the JLabel should be returned
  // to after its foreground and background colors have been set
  // to the selection background color.
  // These ivars will be made protected when their names are finalized.
  private Color unselectedForeground;
  private Color unselectedBackground;

  /**
   * Creates a default table cell renderer.
   */
  public CheckBoxRenderer() {
    super();
    setHorizontalAlignment(JLabel.CENTER);
    setOpaque(true);
    setBorder(getNoFocusBorder());
    setName("Table.cellRenderer");
  }

  private Border getNoFocusBorder() {
    Border border = UIManager.getBorder("Table.cellNoFocusBorder");
    if (System.getSecurityManager() != null) {
      if (border != null) {
        return border;
      }
      return SAFE_NO_FOCUS_BORDER;
    } else if (border != null) {
      if (noFocusBorder == null || noFocusBorder == DEFAULT_NO_FOCUS_BORDER) {
        return border;
      }
    }
    return noFocusBorder;
  }

  /**
   * Overrides <code>JComponent.setForeground</code> to assign
   * the unselected-foreground color to the specified color.
   *
   * @param c set the foreground color to this value
   */
  @Override
  public void setForeground(final Color c) {
    super.setForeground(c);
    unselectedForeground = c;
  }

  /**
   * Overrides <code>JComponent.setBackground</code> to assign
   * the unselected-background color to the specified color.
   *
   * @param c set the background color to this value
   */
  @Override
  public void setBackground(final Color c) {
    super.setBackground(c);
    unselectedBackground = c;
  }

  /**
   * Notification from the <code>UIManager</code> that the look and feel
   * [L&amp;F] has changed.
   * Replaces the current UI object with the latest version from the
   * <code>UIManager</code>.
   *
   * @see JComponent#updateUI
   */
  @Override
  public void updateUI() {
    super.updateUI();
    setForeground(null);
    setBackground(null);
  }

  // implements javax.swing.table.TableCellRenderer

  /**
   * Returns the default table cell renderer.
   * <p>
   * During a printing operation, this method will be called with
   * <code>isSelected</code> and <code>hasFocus</code> values of
   * <code>false</code> to prevent selection and focus from appearing
   * in the printed output. To do other customization based on whether
   * or not the table is being printed, check the return value from
   * {@link javax.swing.JComponent#isPaintingForPrint()}.
   *
   * @param table      the <code>JTable</code>
   * @param value      the value to assign to the cell at
   *                   <code>[row, column]</code>
   * @param isSelected true if cell is selected
   * @param hasFocus   true if cell has focus
   * @param row        the row of the cell to render
   * @param column     the column of the cell to render
   * @return the default table cell renderer
   * @see javax.swing.JComponent#isPaintingForPrint()
   */
  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value,
      boolean isSelected, final boolean hasFocus, final int row, final int column) {
    if (table == null) {
      return this;
    }

    Color fg = null;
    Color bg = null;

    JTable.DropLocation dropLocation = table.getDropLocation();
    if (dropLocation != null
        && !dropLocation.isInsertRow()
        && !dropLocation.isInsertColumn()
        && dropLocation.getRow() == row
        && dropLocation.getColumn() == column) {

      fg = UIManager.getColor("Table.dropCellForeground");
      bg = UIManager.getColor("Table.dropCellBackground");

      isSelected = true;
    }

    if (isSelected) {
      super.setForeground(fg == null ? table.getSelectionForeground()
          : fg);
      super.setBackground(bg == null ? table.getSelectionBackground()
          : bg);
    } else {
      Color background = unselectedBackground != null
          ? unselectedBackground
          : table.getBackground();
      if (background == null || background instanceof javax.swing.plaf.UIResource) {
        Color alternateColor = UIManager.getColor("Table.alternateRowColor");
        if (alternateColor != null && row % 2 != 0) {
          background = alternateColor;
        }
      }
      super.setForeground(unselectedForeground != null
          ? unselectedForeground
          : table.getForeground());
      super.setBackground(background);
    }

    setFont(table.getFont());

    if (hasFocus) {
      Border border = null;
      if (isSelected) {
        border = UIManager.getBorder("Table.focusSelectedCellHighlightBorder");
      }
      if (border == null) {
        border = UIManager.getBorder("Table.focusCellHighlightBorder");
      }
      setBorder(border);

      if (!isSelected && table.isCellEditable(row, column)) {
        Color col;
        col = UIManager.getColor("Table.focusCellForeground");
        if (col != null) {
          super.setForeground(col);
        }
        col = UIManager.getColor("Table.focusCellBackground");
        if (col != null) {
          super.setBackground(col);
        }
      }
    } else {
      setBorder(getNoFocusBorder());
    }

    setSelected((value != null && (Boolean) value));

    return this;
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public boolean isOpaque() {
    Color back = getBackground();
    Component p = getParent();
    if (p != null) {
      p = p.getParent();
    }

    // p should now be the JTable.
    boolean colorMatch = (back != null) && (p != null) &&
        back.equals(p.getBackground()) &&
        p.isOpaque();
    return !colorMatch && super.isOpaque();
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   *
   * @since 1.5
   */
  @Override
  public void invalidate() {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void validate() {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void revalidate() {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void repaint(final long tm, final int x, final int y, final int width, final int height) {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void repaint(final Rectangle r) {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   *
   * @since 1.5
   */
  @Override
  public void repaint() {
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  protected void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    // Strings get interned...
    if (propertyName == "text"
        || propertyName == "labelFor"
        || propertyName == "displayedMnemonic"
        || ((propertyName == "font" || propertyName == "foreground")
        && oldValue != newValue
        && getClientProperty(javax.swing.plaf.basic.BasicHTML.propertyKey) != null)) {

      super.firePropertyChange(propertyName, oldValue, newValue);
    }
  }

  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final boolean oldValue, final boolean newValue) {
  }


  /**
   * Sets the <code>String</code> object for the cell being rendered to
   * <code>value</code>.
   *
   * @param value the string value for this cell; if value is
   *              <code>null</code> it sets the text value to an empty string
   * @see JLabel#setText
   */
  protected void setValue(final Object value) {
    setText((value == null) ? "" : value.toString());
  }


  /**
   * A subclass of <code>DefaultTableCellRenderer</code> that
   * implements <code>UIResource</code>.
   * <code>DefaultTableCellRenderer</code> doesn't implement
   * <code>UIResource</code>
   * directly so that applications can safely override the
   * <code>cellRenderer</code> property with
   * <code>DefaultTableCellRenderer</code> subclasses.
   * <p>
   * <strong>Warning:</strong>
   * Serialized objects of this class will not be compatible with
   * future Swing releases. The current serialization support is
   * appropriate for short term storage or RMI between applications running
   * the same version of Swing.  As of 1.4, support for long term storage
   * of all JavaBeans&trade;
   * has been added to the <code>java.beans</code> package.
   * Please see {@link java.beans.XMLEncoder}.
   */
  public static class UIResource extends DefaultTableCellRenderer
      implements javax.swing.plaf.UIResource {
  }

}
