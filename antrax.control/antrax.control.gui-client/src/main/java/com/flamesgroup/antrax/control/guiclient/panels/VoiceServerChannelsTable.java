/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.Converters;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.VoiceServerChannelsTableTooltips;
import com.flamesgroup.antrax.control.guiclient.commadapter.CallChannelStateComparator;
import com.flamesgroup.antrax.control.guiclient.commadapter.CallChannelStateTimeComparator;
import com.flamesgroup.antrax.control.guiclient.commadapter.CallStateComparator;
import com.flamesgroup.antrax.control.guiclient.commadapter.CallStateTimeComparator;
import com.flamesgroup.antrax.control.guiclient.commadapter.VoiceServerChannelAdapter;
import com.flamesgroup.antrax.control.guiclient.domain.SignalQuality;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.CallChannelStateExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.CallStateCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.ChannelConfigCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.EnableCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.LiveCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.LockArfcnCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.LockCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.PairCallChannelStateTimeCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.PairChannelUIDCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.SignalQualityCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.TimePeriodCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CallChannelStateTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CallChannelStateTimeTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CallStateTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CallStateTimeRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelConfigRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelLiveRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelUidTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.EnableTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.GroupNameTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.HintTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.LockArfcnTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.LockTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.SignalQualityTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutDateHintTableCellRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.ContextSearchRowFilter;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumn;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.util.Comparator;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class VoiceServerChannelsTable extends JUpdatableTable<VoiceServerChannelAdapter, String> {

  private static final long serialVersionUID = 7636827545276289442L;

  private final String[] columnToolTips = {
      VoiceServerChannelsTableTooltips.getGsmChannelTooltip(),
      VoiceServerChannelsTableTooltips.getSimHolderTooltip(),
      VoiceServerChannelsTableTooltips.getSimCardStateTooltip(),
      VoiceServerChannelsTableTooltips.getSimCardLockStateTooltip(),
      VoiceServerChannelsTableTooltips.getLockTimeoutTooltip(),
      VoiceServerChannelsTableTooltips.getLockReasonTooltip(),
      VoiceServerChannelsTableTooltips.getCallChannelStateTooltip(),
      VoiceServerChannelsTableTooltips.getCallChannelStateTimeoutTooltip(),
      VoiceServerChannelsTableTooltips.getAdvancedInfoTooltip(),
      VoiceServerChannelsTableTooltips.getCallStateTooltip(),
      VoiceServerChannelsTableTooltips.getCallStateTimeoutTooltip(),
      VoiceServerChannelsTableTooltips.getPhoneNumberTooltip(),
      VoiceServerChannelsTableTooltips.getVoiceServerTooltip(),
      VoiceServerChannelsTableTooltips.getSimGroupTooltip(),
      VoiceServerChannelsTableTooltips.getGsmGroupTooltip(),
      VoiceServerChannelsTableTooltips.getSignalTooltip(),
      VoiceServerChannelsTableTooltips.getLockArfcnTooltip(),
      VoiceServerChannelsTableTooltips.getOkCallsTooltip(),
      VoiceServerChannelsTableTooltips.getTotalCallsTooltip(),
      VoiceServerChannelsTableTooltips.getSuccessSmsTooltip(),
      VoiceServerChannelsTableTooltips.getTotalSmsTooltip(),
      VoiceServerChannelsTableTooltips.getCallsDurationTooltip(),
      VoiceServerChannelsTableTooltips.getUssdTimeoutTooltip(),
      VoiceServerChannelsTableTooltips.getLastUssdTooltip(),
      VoiceServerChannelsTableTooltips.getChannelConnectionStateTooltip(),
      VoiceServerChannelsTableTooltips.getChannelLicenseStateTooltip()
  };

  public VoiceServerChannelsTable() {
    super(new VSChannelsTableBuilder());
    setName("Voice channels");
    setRowFilter(createRowFilter());
    setColumnToolTips(columnToolTips);
  }



  private static class VSChannelsTableBuilder implements TableBuilder<VoiceServerChannelAdapter, String> {

    // comparators
    private final CallStateComparator callStateComparator = new CallStateComparator();
    private final CallStateTimeComparator callStateTimeComparator = new CallStateTimeComparator();
    private final CallChannelStateComparator callChannelStateComparator = new CallChannelStateComparator();

    final Comparator callChannelStateTimeComparator = (Comparator<Pair<CallChannelState, Long>>) (o1, o2) -> new CallChannelStateTimeComparator().compare(o1, o2);
    final Comparator gsmChannelComparator = (Comparator<Pair<ChannelUID, Boolean>>) (o1, o2) -> Comparator.nullsLast(ChannelUID::compareTo).compare(o1.first(), o2.first());
    final Comparator lockArfcnComparator = (Comparator<Pair<Integer, ChannelUID>>) (o1, o2) -> Comparator.nullsFirst(Integer::compareTo).compare(o1.first(), o2.first());

    // renderer's
    private final ChannelLiveRenderer channelLiveRenderer = new ChannelLiveRenderer();
    private final ChannelConfigRenderer channelConfigRenderer = new ChannelConfigRenderer();
    private final ChannelUidTCRenderer channelUidRenderer = new ChannelUidTCRenderer();
    private final EnableTCRenderer enableTCRenderer = new EnableTCRenderer();
    private final LockTCRenderer lockTCRenderer = new LockTCRenderer();
    private final SignalQualityTCRenderer signalQualityRenderer = new SignalQualityTCRenderer();
    private final LockArfcnTCRenderer lockArfcnTCRenderer = new LockArfcnTCRenderer();
    private final CallChannelStateTCRenderer callChannelStateRenderer = new CallChannelStateTCRenderer();
    private final CallChannelStateTimeTCRenderer callChannelStateTimeRenderer = new CallChannelStateTimeTCRenderer();
    private final CallStateTCRenderer callStateRenderer = new CallStateTCRenderer();
    private final CallStateTimeRenderer callStateTimeRenderer = new CallStateTimeRenderer();
    private final TimeoutTCRenderer timeoutRenderer = new TimeoutTCRenderer();
    private final HintTCRenderer hintTCRenderer = new HintTCRenderer();
    private final TimeoutDateHintTableCellRenderer timeoutDateHintTableCellRenderer = new TimeoutDateHintTableCellRenderer();
    private final DefaultTableCellRenderer alignCenterRenderer = new DefaultTableCellRenderer();

    public VSChannelsTableBuilder() {
      alignCenterRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    }

    @Override
    public String getUniqueKey(final VoiceServerChannelAdapter src) {
      return src.getID();
    }

    @Override
    public void buildRow(final VoiceServerChannelAdapter src, final ColumnWriter<VoiceServerChannelAdapter> dest) {
      dest.writeColumn(new Pair<>(src.getMobileGatewayChannelUID(), !src.isLive()));
      dest.writeColumn(src.getSimChannelUID(), src);
      dest.writeColumn(src.isEnabled());
      dest.writeColumn(src.getGSMChannel().isLock());
      dest.writeColumn(src.getGSMChannel().getLockTime() > 0 ? src.getServerTimeMillis() - src.getGSMChannel().getLockTime() : 0);
      dest.writeColumn(src.getGSMChannel().getLockReason());
      dest.writeColumn(src.getCallChannelState());
      dest.writeColumn(new Pair<>(src.getCallChannelState(), src.getServerTimeMillis()));
      dest.writeColumn(src.getStateAdvInfo());
      dest.writeColumn(src.getCallState());
      dest.writeColumn(src.getCallState());
      dest.writeColumn(src.getPhoneNumber() == null ? "" : src.getPhoneNumber().getValue());
      dest.writeColumn(src.getVoiceServer());
      dest.writeColumn(src.getSIMGroupName(), src.getSIMGroupName());
      dest.writeColumn(src.getGSMGroupName(), src.getGSMGroupName());
      dest.writeColumn(new Pair<>(src.getSignalQuality(), src.getGSMNetworkInfo()));
      dest.writeColumn(new Pair<>(src.getGSMChannel().getLockArfcn(), src.getMobileGatewayChannelUID()));
      dest.writeColumn(src.getSuccessfulCallsCount());
      dest.writeColumn(src.getTotalCallsCount());
      dest.writeColumn(src.getSuccessOutgoingSmsCount());
      dest.writeColumn(src.getTotalOutgoingSmsCount());
      dest.writeColumn(src.getCallsDuration());
      dest.writeColumn(src.getLastUSSDTimeout());
      dest.writeColumn(src.getLastUSSD());
      dest.writeColumn(src.isLive());
      dest.writeColumn(new Pair<>(src.getChannelConfig(), src.getServerTimeMillis()));
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel builder) {
      GroupNameTCRenderer groupRenderer = new GroupNameTCRenderer();

      builder.addColumn("GSM channel", ChannelUID.class)
          /*       */.setMinWidth(110)
          /*       */.setPreferredWidth(110)
          /*       */.setComparator(gsmChannelComparator)
          /*       */.setCellExporter(new PairChannelUIDCellExporter())
          /*       */.setRenderer(channelUidRenderer);

      UpdateTableColumn columnSimHolder = builder.addColumn("SIM holder", ChannelUID.class)
          /*       */.setMinWidth(110)
          /*       */.setPreferredWidth(110)
          /*       */.setRenderer(channelUidRenderer);

      builder.addColumn("E", Boolean.class)
          /*       */.setMaxWidth(30)
          /*       */.setPreferredWidth(30)
          /*       */.setCellExporter(new EnableCellExporter())
          /*       */.setRenderer(enableTCRenderer);

      builder.addColumn("L", Boolean.class)
          /*       */.setMaxWidth(30)
          /*       */.setPreferredWidth(30)
          /*       */.setCellExporter(new LockCellExporter())
          /*       */.setRenderer(lockTCRenderer);

      builder.addColumn("Lock timeout", Long.class)
          /*       */.setRenderer(timeoutDateHintTableCellRenderer)
          /*       */.setCellExporter(new TimePeriodCellExporter())
          /*       */.setPreferredWidth(85);

      builder.addColumn("(Un)Lock reason", String.class)
          /*       */.setRenderer(hintTCRenderer)
          /*       */.setPreferredWidth(170);

      UpdateTableColumn columnCallChannelState = builder.addColumn("S", CallChannelState.class)
          /*       */.setMaxWidth(35)
          /*       */.setPreferredWidth(35)
          /*       */.setComparator(callChannelStateComparator)
          /*       */.setCellExporter(new CallChannelStateExporter())
          /*       */.setRenderer(callChannelStateRenderer);

      builder.addColumn("S timeout", CallChannelState.class)
          /*       */.setPreferredWidth(75)
          /*       */.setComparator(callChannelStateTimeComparator)
          /*       */.setCellExporter(new PairCallChannelStateTimeCellExporter())
          /*       */.setRenderer(callChannelStateTimeRenderer);

      builder.addColumn("adv info", String.class);

      UpdateTableColumn columnCallState = builder.addColumn("CS", CallState.class)
          /*       */.setMaxWidth(45)
          /*       */.setPreferredWidth(45)
          /*       */.setComparator(callStateComparator)
          /*       */.setCellExporter(new CallStateCellExporter())
          /*       */.setRenderer(callStateRenderer);

      builder.addColumn("CS timeout", CallState.class)
          /*       */.setPreferredWidth(75)
          /*       */.setComparator(callStateTimeComparator)
          /*       */.setCellExporter(new TimePeriodCellExporter())
          /*       */.setRenderer(callStateTimeRenderer);

      builder.addColumn("Phone number", String.class)
          /*       */.setPreferredWidth(125)
          /*       */.setRenderer(alignCenterRenderer);

      builder.addColumn("Voice Server", IServerData.class)
          /*       */.setPreferredWidth(100);

      builder.addColumn("SIM group", String.class)
          /*       */.setPreferredWidth(120)
          /*       */.setRenderer(groupRenderer);

      builder.addColumn("GSM group", String.class)
          /*       */.setPreferredWidth(90)
          /*       */.setRenderer(groupRenderer);

      builder.addColumn("Signal", SignalQuality.class)
          /*       */.setMinWidth(20)
          /*       */.setPreferredWidth(40)
          /*       */.setRenderer(signalQualityRenderer)
          /*       */.setCellExporter(new SignalQualityCellExporter());

      builder.addColumn("Lock arfcn", Integer.class)
          /*       */.setPreferredWidth(120)
          /*       */.setRenderer(lockArfcnTCRenderer)
          /*       */.setComparator(lockArfcnComparator)
          /*       */.setCellExporter(new LockArfcnCellExporter());

      builder.addColumn("OK calls", Integer.class)
          /*       */.setPreferredWidth(60)
          /*       */.setRenderer(alignCenterRenderer);

      builder.addColumn("Total calls", Integer.class)
          /*       */.setPreferredWidth(65)
          /*       */.setRenderer(alignCenterRenderer);

      builder.addColumn("Success SMS", Integer.class)
          /*       */.setPreferredWidth(65)
          /*       */.setRenderer(alignCenterRenderer);

      builder.addColumn("Total SMS", Integer.class)
          /*       */.setPreferredWidth(65)
          /*       */.setRenderer(alignCenterRenderer);

      builder.addColumn("Calls dur.", Long.class)
          /*       */.setPreferredWidth(75)
          /*       */.setCellExporter(new TimePeriodCellExporter())
          /*       */.setRenderer(timeoutRenderer);

      builder.addColumn("USSD timeout", Long.class)
          /*       */.setPreferredWidth(85)
          /*       */.setCellExporter(new TimePeriodCellExporter())
          /*       */.setRenderer(timeoutRenderer);

      builder.addColumn("Last USSD", String.class)
          /*       */.setPreferredWidth(320)
          /*       */.setRenderer(hintTCRenderer);

      builder.addColumn(null, Image.class)
          /*       */.setMaxWidth(30)
          /*       */.setPreferredWidth(30)
          /*       */.setCellExporter(new LiveCellExporter())
          /*       */.setRenderer(channelLiveRenderer);

      builder.addColumn(null, ChannelConfig.class)
          /*       */.setMaxWidth(30)
          /*       */.setMinWidth(30)
          /*       */.setCellExporter(new ChannelConfigCellExporter())
          /*       */.setRenderer(channelConfigRenderer);

      builder.addColumnSortSequence(columnSimHolder, SortOrder.DESCENDING);
      builder.addColumnSortSequence(columnCallChannelState, SortOrder.DESCENDING);
      builder.addColumnSortSequence(columnCallState, SortOrder.DESCENDING);
    }

  }

  private ContextSearchRowFilter createRowFilter() {
    return new ContextSearchRowFilter() {

      @Override
      protected String getEntryValue(final Entry<? extends Object, ? extends Object> entry, final int column) {
        String strValue;
        switch (column) {
          case 0:
            ChannelUID gsmChannel = (ChannelUID) ((Pair) entry.getValue(column)).first();
            strValue = gsmChannel.getAliasCanonicalName();
            break;
          case 1:
            ChannelUID simChannel = (ChannelUID) entry.getValue(column);
            strValue = simChannel == null ? "" : simChannel.getAliasCanonicalName();
            break;
          case 6:
            CallChannelState chs = (CallChannelState) entry.getValue(column);
            strValue = (chs == null) ? "" : Converters.asText(chs.getState());
            break;

          case 9:
            CallState cs = (CallState) entry.getValue(column);
            strValue = (cs == null) ? "" : Converters.asText(cs.getState());
            break;

          default:
            strValue = super.getEntryValue(entry, column);
            break;
        }
        return strValue;
      }
    };
  }

}
