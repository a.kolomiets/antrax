/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class BasicAdvancedPanel extends JPanel {
  private static final long serialVersionUID = 8013069504778561491L;

  private final Component basicPanel;
  private final Component advancedPanel;
  private final JButton basicButton;
  private final JButton advancedButton;

  public BasicAdvancedPanel(final Component basic, final Component advanced) {
    GridBagLayout layout = new GridBagLayout();
    setLayout(layout);
    layout.columnWeights = new double[] {1, 0};
    layout.columnWidths = new int[] {1, 1};
    layout.rowHeights = new int[] {1, 1};
    layout.rowWeights = new double[] {1, 0};

    this.basicPanel = basic;
    this.advancedPanel = advanced;

    basicButton = new JButton(IconPool.getShared("/img/bricks.png"));
    advancedButton = new JButton(IconPool.getShared("/img/brick.png"));

    basicButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        showBasic();
      }
    });

    advancedButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        showAdvanced();
      }
    });

    showBasic();
  }

  private void showBasic() {
    removeAll();
    add(basicPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    add(advancedButton, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    revalidate();
    repaint();
    advancedButton.requestFocus();
  }

  private void showAdvanced() {
    removeAll();
    add(advancedPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    add(basicButton, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    revalidate();
    repaint();
    basicButton.requestFocus();
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    frame.getContentPane().add(new BasicAdvancedPanel(new JLabel("simple"), new JLabel("coolest")));
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }
}
