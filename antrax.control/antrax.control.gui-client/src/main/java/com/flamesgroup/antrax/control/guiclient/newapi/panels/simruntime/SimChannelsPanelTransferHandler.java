/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.simruntime;

import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.transfer.AntraxTransferHandler;

public class SimChannelsPanelTransferHandler extends AntraxTransferHandler {

  private static final long serialVersionUID = 4660865340469664987L;
  private final JContextSearch contextSearch;

  public SimChannelsPanelTransferHandler(final JContextSearch contextSearch) {
    this.contextSearch = contextSearch;
  }

  public void importObject(final String str) {
    contextSearch.setText("\"" + str + "\"");
  }

}
