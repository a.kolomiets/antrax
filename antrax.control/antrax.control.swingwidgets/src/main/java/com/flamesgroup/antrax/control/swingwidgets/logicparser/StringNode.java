package com.flamesgroup.antrax.control.swingwidgets.logicparser;

/**
 * A node the represents a String value
 */
public class StringNode implements Node {

  private final StringBuffer value;

  public StringNode(final String value) {
    this.value = new StringBuffer(value);
  }

  public void appendValue(final String str) {
    value.append(str);
  }

  public String getValue() {
    return value.toString();
  }

  /**
   * Returns true if the string is not empty.
   */
  @Override
  public boolean evaluate() {
    return !(getValue().length() == 0);
  }

  @Override
  public String toString() {
    return value.toString();
  }

}
