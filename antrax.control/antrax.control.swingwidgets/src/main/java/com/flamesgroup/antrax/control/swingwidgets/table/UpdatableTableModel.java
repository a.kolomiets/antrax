package com.flamesgroup.antrax.control.swingwidgets.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * JTable which can be updated using arrays of data
 *
 * @param <T> type of table elements
 * @param <K> type of unique key of element
 */
public class UpdatableTableModel<T, K extends Comparable<K>> extends AbstractTableModel {
  private static final long serialVersionUID = 4377812460317848705L;
  private final TableBuilder<T, K> tableBuilder;
  private final UpdateTableColumnModel columnBuilder;

  private static class ThisColumnWriter<T, K extends Comparable<K>> implements ColumnWriter<T> {
    private int col;
    private final int maxColumnsCount;
    private final int row;
    private final ModelData<T, K> data;

    public ThisColumnWriter(final ModelData<T, K> data, final int row, final int maxColumnsCount) {
      this.row = row;
      this.col = 0;
      this.maxColumnsCount = maxColumnsCount;
      this.data = data;
    }

    @Override
    public void writeColumn(final Object val, final Object... transferableObjects) {
      if (col < maxColumnsCount) {
        data.setValueAt(val, transferableObjects, row, col++);
      } else {
        throw new IllegalStateException("Can't write any more columns (column size: " + maxColumnsCount + " is less than you want: " + col + ")");
      }
    }
  }

  private static class ModelData<T, K extends Comparable<K>> {
    private List<T> rows = new ArrayList<>();
    private ArrayList<ArrayList<Object>> values = new ArrayList<>();
    private List<List<Object>> transferableObjects = new ArrayList<>();
    private final int columnsCount;
    private final TableBuilder<T, K> tableBuilder;
    private final Comparator<T> comparator;

    private ModelData(final int columnsCount, final TableBuilder<T, K> tableBuilder) {
      this.columnsCount = columnsCount;
      this.tableBuilder = tableBuilder;
      comparator = new Comparator<T>() {
        @Override
        public int compare(final T o1, final T o2) {
          return tableBuilder.getUniqueKey(o1).compareTo(tableBuilder.getUniqueKey(o2));
        }
      };
    }

    private void removeElemAt(final int row) {
      rows.remove(row);
      values.remove(row);
      transferableObjects.remove(row);
    }

    private void insertElemAt(final T elem, final int row) {
      rows.add(row, elem);
      values.add(row, new ArrayList<>(Collections.nCopies(columnsCount, null)));
      transferableObjects.add(row, new ArrayList<>());
      tableBuilder.buildRow(elem, new ThisColumnWriter<>(this, row, columnsCount));
    }

    public void setValueAt(final Object aValue, final Object[] transferableObjects, final int rowIndex, final int columnIndex) {
      values.get(rowIndex).set(columnIndex, aValue);
      this.transferableObjects.get(rowIndex).addAll(Arrays.asList(transferableObjects));
    }

    public int size() {
      return values.size();
    }

    public T getElemAt(final int row) {
      return rows.get(row);
    }

    public Collection<T> getElems() {
      return Collections.unmodifiableCollection(rows);
    }

    public Object getValueAt(final int rowIndex, final int columnIndex) {
      return values.get(rowIndex).get(columnIndex);
    }

    public void setData(final T[] data) {
      Arrays.sort(data, comparator);
      values = new ArrayList<>();
      transferableObjects = new ArrayList<>();
      rows = new ArrayList<>();
      for (int i = 0; i < data.length; ++i) {
        insertElemAt(data[i], i);
      }
    }

    public int getRowOf(final T elem) {
      return Collections.binarySearch(rows, elem, comparator);
    }

    public void updateElemAt(final T elem, final int row) {
      tableBuilder.buildRow(elem, new ThisColumnWriter<>(this, row, columnsCount));
      rows.set(row, elem);
    }

    public List<Object> getTranferableObjects(final int row) {
      return transferableObjects.get(row);
    }
  }

  private final ModelData<T, K> data;

  public UpdatableTableModel(final TableBuilder<T, K> tableBuilder, final UpdateTableColumnModel columnBuilder) {
    this.tableBuilder = tableBuilder;
    this.columnBuilder = columnBuilder;
    this.tableBuilder.buildColumns(columnBuilder);
    data = new ModelData<>(columnBuilder.size(), tableBuilder);
  }

  @Override
  public boolean isCellEditable(final int row, final int column) {
    return false;
  }

  @Override
  public Class<?> getColumnClass(final int column) {
    return columnBuilder.getColumnAt(column).getType();
  }

  public T getElemAt(final int row) {
    return data.getElemAt(row);
  }

  public Collection<T> getElems() {
    return data.getElems();
  }

  public int getRowOf(final T elem) {
    return data.getRowOf(elem);
  }

  public void updateElemAt(final T elem, final int row) {
    K newKey = tableBuilder.getUniqueKey(elem);
    T oldElem = getElemAt(row);
    K oldKey = tableBuilder.getUniqueKey(oldElem);
    if (newKey.equals(oldKey)) {
      data.updateElemAt(elem, row);
      fireTableRowsUpdated(row, row);
    } else {
      removeElem(oldElem);
      insertElem(elem);
    }
  }

  public int insertElem(final T elem) {
    int row = Math.abs(getRowOf(elem) + 1);
    data.insertElemAt(elem, row);
    fireTableRowsInserted(row, row);
    return row;
  }

  public void removeElem(final T elem) {
    int row = getRowOf(elem);
    if (row < 0) {
      throw new IllegalArgumentException("Can't find element to remove: " + elem);
    }
    data.removeElemAt(row);
    fireTableRowsDeleted(row, row);
  }

  public void setData(final T... data) {
    this.data.setData(data);
    fireTableDataChanged();
  }

  @Override
  public int getColumnCount() {
    return columnBuilder.size();
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return columnBuilder.getColumnAt(columnIndex).getName();
  }

  @Override
  public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
    data.setValueAt(aValue, new Object[] {aValue}, rowIndex, columnIndex);
    fireTableCellUpdated(rowIndex, columnIndex);
  }

  @Override
  public int getRowCount() {
    return data.size();
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return data.getValueAt(rowIndex, columnIndex);
  }

  public String getExportValueAt(final int rowIndex, final int columnIndex) {
    return columnBuilder.getColumnAt(columnIndex).getCellExporter().getExportValue(getValueAt(rowIndex, columnIndex));
  }

  public List<Object> getTransferableObjects(final int row) {
    return data.getTranferableObjects(row);
  }
}
