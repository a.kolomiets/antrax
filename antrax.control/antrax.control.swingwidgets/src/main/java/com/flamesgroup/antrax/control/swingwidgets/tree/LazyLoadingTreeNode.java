package com.flamesgroup.antrax.control.swingwidgets.tree;

import javax.swing.*;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;

public interface LazyLoadingTreeNode extends MutableTreeNode {

  void setAllowsChildren(boolean allows);

  boolean isChildrenLoaded();

  void setChildren(MutableTreeNode... nodes);

  /**
   * This method will be executed in a background thread. If you have to do
   * some GUI stuff use {@link SwingUtilities#invokeLater(Runnable)}
   *
   * @param model the tree model
   * @return the created nodes
   */
  MutableTreeNode[] loadChildren(TreeModel model);

}
