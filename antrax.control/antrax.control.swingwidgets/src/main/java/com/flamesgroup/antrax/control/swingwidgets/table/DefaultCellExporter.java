package com.flamesgroup.antrax.control.swingwidgets.table;

public class DefaultCellExporter implements CellExporter {

  @Override
  public String getExportValue(final Object value) {
    return value == null ? "" : value.toString();
  }

}
