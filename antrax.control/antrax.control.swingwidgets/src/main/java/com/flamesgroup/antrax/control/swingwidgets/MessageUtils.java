package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.*;

public class MessageUtils {

  public static void showError(final Component invoker, final String title, final String message) {
    JOptionPane.showMessageDialog(invoker, wrapLines(message, 80, 25), title, JOptionPane.ERROR_MESSAGE);
  }

  public static void showWarn(final Component invoker, final String title, final String message) {
    JOptionPane.showMessageDialog(invoker, wrapLines(message, 80, 25), title, JOptionPane.WARNING_MESSAGE);
  }

  public static void showInfo(final Component invoker, final String title, final String message) {
    JOptionPane.showMessageDialog(invoker, wrapLines(message, 80, 25), title, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void showError(final Component invoker, final String title, final String message, final Throwable e) {
    JOptionPane.showMessageDialog(invoker, wrapLines(message + translateException(e), 80, 25), title, JOptionPane.ERROR_MESSAGE);
  }

  public static void showError(final Component invoker, final String message, final Throwable e) {
    showError(invoker, "Error occured", message, e);
  }

  public static boolean confirm(final Component invoker, final String title, final String message) {
    return JOptionPane.showConfirmDialog(invoker, message, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
  }

  private static String translateException(final Throwable e) {
    StringWriter stringWriter = new StringWriter();
    e.printStackTrace(new PrintWriter(stringWriter));
    return stringWriter.toString();
  }

  static String wrapLines(final String string, final int lineLength, int lineLimit) {
    StringBuilder retval = new StringBuilder();
    for (String line : string.split("\n", lineLimit)) {
      if (lineLimit <= 0) {
        break;
      }
      lineLimit = wrapLine(line, lineLength, retval, lineLimit);
    }
    return retval.toString();
  }

  static int wrapLine(final String line, final int lineLength, final StringBuilder retval, int lineLimit) {
    if (lineLimit <= 0) {
      return 0;
    }
    if (line.length() > lineLength) {
      retval.append(line.substring(0, lineLength)).append('\n');
      --lineLimit;
      if (lineLimit > 0) {
        retval.append("  ");
        lineLimit = wrapLine(line.substring(lineLength), lineLength, retval, lineLimit);
      }
    } else {
      retval.append(line).append('\n');
      --lineLimit;
    }
    return lineLimit;
  }

}
