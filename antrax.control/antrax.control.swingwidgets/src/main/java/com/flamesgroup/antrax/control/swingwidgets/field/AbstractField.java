package com.flamesgroup.antrax.control.swingwidgets.field;

import com.flamesgroup.antrax.control.swingwidgets.binding.ValueEditor;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * An advanced text-field component supporting validation, binding, formatting,
 * auto-completion, and water-marking
 */
public abstract class AbstractField extends JTextField implements ValueEditor<Object> {

  private static final long serialVersionUID = 2483440804535750652L;

  private final List<ValidationListener> listeners = new LinkedList<>();

  private boolean required;
  private String watermark;
  private transient boolean lock;
  private transient boolean valid;

  /**
   * Creates a field
   */
  protected AbstractField() {
    setDocument(new FieldDocument());
    valid = true;
  }

  /**
   * Updates the content, auto-completion turned off
   *
   * @param text the new content
   */
  @Override
  public void setText(final String text) {
    lock = true;
    super.setText(text);
    lock = false;
  }

  /**
   * Sets the required property, used for validation
   *
   * @param required true for non empty field requirement
   */
  public void setRequired(final boolean required) {
    this.required = required;
    checkValidity();
  }

  /**
   * Text painted gray in empty field, set null to switch this feature off
   *
   * @param watermark text painted in the background
   */
  public void setWatermark(final String watermark) {
    this.watermark = watermark;
    repaint();
  }

  /**
   * Overwrite this method to use a custom validation
   *
   * @param text the actual content
   * @return true if the text was valid
   * @see #remove(String, String)
   */
  protected boolean isValid(final String text) {
    return true;
  }

  /**
   * Overwrite this method to update the content as typed, e.g. to convert the
   * typed text upper case
   *
   * @param offset the location of this substring
   * @param text   the substring typed
   * @return the updated content
   */
  protected String prepareText(final int offset, final String text) {
    return text;
  }

  /**
   * Overwrite this method to support auto-completion
   *
   * @param text the current content of the field
   * @return the text to append and select
   * @see #getAutoCompleteValue(Set, String)
   */
  protected String getAutoCompleteValue(final String text) {
    return null;
  }

  /**
   * Helper method to find the first possible completion text
   *
   * @param values list of possible values
   * @param text   the current content of the field
   * @return the text to append and select
   * @see #getAutoCompleteValue(String)
   */
  protected final String getAutoCompleteValue(final Set<String> values, final String text) {
    for (String complete : values) {
      if (complete.regionMatches(true, 0, text, 0, text.length())) {
        return complete.substring(text.length());
      }
    }
    return null;
  }

  /**
   * Overwrite this method to format the text when the focus was lost, e.g.
   * add separators to a number
   *
   * @param text the current content of the field
   * @return the formatted text
   * @see #remove(String, String)
   */
  protected String getFormattedText(final String text) {
    return text;
  }

  /**
   * Helper method to remove chars from a text, e.g. used for account number
   * formatting and validation
   *
   * @param text  the current content of the field
   * @param chars characters to remove
   * @return the text excluding the given chars
   * @see #isValid(String)
   * @see #getFormattedText(String)
   */
  protected final String remove(final String text, final String chars) {
    StringBuffer sb = null;
    for (int i = 0; i < text.length(); i++) {
      char c = text.charAt(i);
      if (chars.indexOf(c) != -1) {
        if (sb == null)
          sb = new StringBuffer(text.substring(0, i));
      } else if (sb != null) {
        sb.append(c);
      }
    }
    return (sb != null) ? sb.toString() : text;
  }

  /**
   * @see ValueEditor#setContent(Object)
   */
  @Override
  public void setContent(final Object value) {
    setText((value != null) ? value.toString() : null);
  }

  /**
   * @see ValueEditor#getContent()
   */
  @Override
  public Object getContent() {
    String text = getText();
    return (text.length() > 0) ? text : null;
  }

  /**
   * @see ValueEditor#getValidity()
   */
  @Override
  public boolean getValidity() {
    return valid;
  }

  /**
   * @see ValueEditor#addChangeListener(ChangeListener)
   */
  @Override
  public void addChangeListener(final ChangeListener listener) {
    listenerList.add(ChangeListener.class, listener);
  }

  /**
   * @see ValueEditor#removeChangeListener(ChangeListener)
   */
  @Override
  public void removeChangeListener(final ChangeListener listener) {
    listenerList.remove(ChangeListener.class, listener);
  }

  private class FieldDocument extends PlainDocument implements FocusListener {

    private static final long serialVersionUID = 7742273056815036584L;

    private FieldDocument() {
      addFocusListener(this);
    }

    @Override
    public void insertString(final int offset, String string, final AttributeSet attributes) throws BadLocationException {
      string = prepareText(offset, string);

      int selectionStart = -1, selectionEnd = -1;
      if (!lock && (offset == getLength())) {
        String text = getText(0, getLength()) + string;
        String complete = getAutoCompleteValue(text);
        if (complete != null) {
          selectionStart = text.length();
          selectionEnd = selectionStart + complete.length();
          string += complete;
        }
      }
      super.insertString(offset, string, attributes);
      if (selectionStart != -1) {
        select(selectionStart, selectionEnd);
      }
      checkValidity();
    }

    @Override
    public void remove(final int offset, final int length) throws BadLocationException {
      super.remove(offset, length);
      checkValidity();
    }

    @Override
    public void focusGained(final FocusEvent e) {
    }

    @Override
    public void focusLost(final FocusEvent e) {
      if (valid) {
        String text = AbstractField.this.getText().trim();
        String formatted = (text.length() == 0) ? ""
            : getFormattedText(text);
        if (!formatted.equals(text))
          setText(formatted);
      }
    }
  }

  private void checkValidity() {
    String text = getText().trim();
    boolean v = (text.length() == 0) ? !required : isValid(text);
    if (valid != v) {
      valid = v;
      for (ValidationListener l : listeners) {
        l.onValidate(valid);
      }
      repaint();
    }

    // fire change event to the listeners
    for (ChangeListener listener : listenerList
        .getListeners(ChangeListener.class)) {
      listener.stateChanged(new ChangeEvent(this));
    }
  }

  /**
   * Overrides the method to paint water-mark and status icon
   */
  @Override
  public void paint(final Graphics g) {
    super.paint(g);

    if ((watermark != null) && (getText().length() == 0)) {
      g.setColor(Color.LIGHT_GRAY);
      Insets is = getInsets();
      g.drawString(watermark, is.left, is.top
          + g.getFontMetrics().getAscent());
    }

    if (!valid) {
      paintStatus(g, this, required && (getText().trim().length() == 0));
    }
  }

  private static Graphics2D initGC(final Graphics g) {
    Graphics2D g2 = (Graphics2D) g;

    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

    return g2;
  }

  /**
   * Paints the validation status for an invalid component
   *
   * @param g         graphics context
   * @param component the component
   * @param warning   true for warning, false for error colors
   */
  public static void paintStatus(final Graphics g, final JComponent component,
      final boolean warning) {
    initGC(g);
    int diameter = 8;
    int y = component.getHeight() - diameter;
    g.setColor(warning ? new Color(0xFFDE46) : Color.RED);
    g.fillOval(0, y, diameter, diameter);
    g.setColor(warning ? Color.GRAY : Color.WHITE);
    g.drawLine(2, y + 2, 5, y + 5);
    g.drawLine(5, y + 2, 2, y + 5);
  }

  /**
   * Helper method to calculate the field's preferred size based on the given
   * text not character number
   *
   * @param text the reference content for calculation
   * @return the calculated size
   */
  protected final Dimension getSize(final String text) {
    Insets is = getInsets();
    FontMetrics fm = getFontMetrics(getFont());
    return new Dimension(fm.stringWidth(text) + is.left + is.right + 2, fm
        .getAscent()
        + fm.getDescent() + is.top + is.bottom);
  }

  /**
   * Overwritten to return preferred size for box layout
   */
  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

  /**
   * Overwritten to return preferred size for box layout
   */
  @Override
  public Dimension getMaximumSize() {
    return getPreferredSize();
  }

  public void addValidationListener(final ValidationListener l) {
    listeners.add(l);
  }

  public void removeValidationListener(final ValidationListener l) {
    listeners.remove(l);
  }

  public interface ValidationListener {
    void onValidate(boolean valid);
  }

  /*
   * public void setRolloverBorder() { setBorder(new RolloverBorder(this)); }
   *
   * private static class RolloverBorder implements Border, FocusListener,
   * MouseListener {
   *
   * private JTextField textField; private Border border; private boolean
   * focused, inside, pressed;
   *
   * private RolloverBorder(JTextField textField) { this.textField =
   * textField; border = textField.getBorder();
   * textField.addFocusListener(this); textField.addMouseListener(this); }
   *
   * public Insets getBorderInsets(Component comp) { return
   * border.getBorderInsets(comp); }
   *
   * public boolean isBorderOpaque() { return border.isBorderOpaque(); }
   *
   * public void paintBorder(Component comp, Graphics g, int x, int y, int
   * width, int height) { if (focused || inside || pressed)
   * border.paintBorder(comp, g, x, y, width, height); }
   *
   * public void focusGained(FocusEvent e) { focused = true;
   * textField.repaint(); } public void focusLost(FocusEvent e) { focused =
   * false; textField.repaint(); } public void mouseClicked(MouseEvent e) {}
   * public void mouseEntered(MouseEvent e) { inside = true;
   * textField.repaint(); } public void mouseExited(MouseEvent e) { inside =
   * false; textField.repaint(); } public void mousePressed(MouseEvent e) {
   * pressed = true; textField.repaint(); } public void
   * mouseReleased(MouseEvent e) { pressed = false; textField.repaint(); } }
   */
}
