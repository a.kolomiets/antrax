package com.flamesgroup.antrax.control.transfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AntraxTransferable implements Transferable {

  private final Map<DataFlavor, List<Object>> flavours = new HashMap<>();

  public AntraxTransferable(final Object... transferObjects) {
    if (transferObjects == null) {
      return;
    }

    for (Object o : transferObjects) {
      if (o == null) {
        continue;
      }

      DataFlavor dataFlavor = new DataFlavor(o.getClass(), o.getClass().getCanonicalName());
      if (!flavours.containsKey(dataFlavor)) {
        flavours.put(dataFlavor, new ArrayList<>());
      }
      flavours.get(dataFlavor).add(o);
    }
  }

  @Override
  public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    return flavours.get(flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return flavours.keySet().toArray(new DataFlavor[flavours.size()]);
  }

  @Override
  public boolean isDataFlavorSupported(final DataFlavor flavor) {
    return flavours.containsKey(flavor);
  }

}
