package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class SwingHelper {

  // Suppress default constructor, ensuring non-instantiability
  private SwingHelper() {
    throw new AssertionError();
  }

  public static final String OS_NAME = System.getProperty("os.name");
  public static final String FRAME_ACTIVE_PROPERTY = "Frame.active";

  private static Boolean isWindows;

  private static boolean isWindows() {
    if (isWindows == null) {
      isWindows = OS_NAME.toLowerCase().startsWith("win");
    }
    return isWindows;
  }

  private static void centerOnScreen(final Component cmp) {
    // Get the screen size
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();

    // Calculate the frame location
    int x = (screenSize.width - cmp.getWidth()) / 2;
    int y = (screenSize.height - cmp.getHeight()) / 2;
    cmp.setLocation(x, y);
  }

  public static void onTop4Screen(final Component cmp) {
    // Get the screen size
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();

    // Calculate the frame location
    int y = (screenSize.height / 4) - cmp.getHeight();
    if (y < 0) {
      centerOnScreen(cmp);
    } else {
      int x = (screenSize.width - cmp.getWidth()) / 2;
      cmp.setLocation(x, y);
    }
  }

  public static void expandToLast(final JTree tree) {
    // expand to the last leaf from the root
    Object rootObj = tree.getModel().getRoot();
    if (rootObj instanceof DefaultMutableTreeNode) {
      DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel().getRoot();
      tree.scrollPathToVisible(new TreePath(root.getLastLeaf().getPath()));
    }
  }

  public static void drawString(final Graphics2D gc, final String str, final int x, int y) {
    if (isWindows()) {
      y--;
    }
    gc.drawString(str, x, y);
  }

  public static void drawString(final Graphics2D gc, final String str, final float x, float y) {
    if (isWindows()) {
      y--;
    }
    gc.drawString(str, x, y);
  }

  public static void expandAll(final JTree tree) {
    for (int row = 0; row < tree.getRowCount(); row++) {
      tree.expandRow(row);
    }
  }

  public static int calculateColumnsCount(final JComponent textField, final int preferredWidth) {
    FontMetrics metrics = textField.getFontMetrics(textField.getFont());
    int columnWidth = metrics.charWidth('m');
    Insets insets = textField.getInsets();
    if (columnWidth == 0) {
      return 0;
    }
    return Math.round((preferredWidth - insets.left - insets.right) / (float) columnWidth);
  }

  /**
   * Installs a listener on the given {@link JComponent}'s parent {@link Window} that repaints
   * the given component when the parent window's focused state changes. If the given component
   * does not have a parent at the time this method is called, then an ancestor listener will be
   * installed that installs a window listener when the components parent changes.
   *
   * @param component the {@code JComponent} to add the repaint focus listener to.
   */
  public static void installJComponentRepainterOnWindowFocusChanged(final JComponent component) {
    // TODO check to see if the component already has an ancestor.
    component.addAncestorListener(createAncestorListener(component,
        createWindowListener(component)));
  }

  private static WindowListener createWindowListener(
      final JComponent component) {
    return new WindowAdapter() {
      @Override
      public void windowActivated(final WindowEvent e) {
        component.repaint();
      }

      @Override
      public void windowDeactivated(final WindowEvent e) {
        component.repaint();
      }
    };
  }

  private static AncestorListener createAncestorListener(
      final JComponent component, final WindowListener windowListener) {
    return new AncestorListener() {
      @Override
      public void ancestorAdded(final AncestorEvent event) {
        Window window = SwingUtilities.getWindowAncestor(component);
        if (window != null) {
          window.removeWindowListener(windowListener);
          window.addWindowListener(windowListener);
        }
      }

      @Override
      public void ancestorRemoved(final AncestorEvent event) {
        // no implementation
      }

      @Override
      public void ancestorMoved(final AncestorEvent event) {
        // no implementation
      }
    };
  }

  public static boolean isParentWindowFocused(final Component component) {
    Window window = SwingUtilities.getWindowAncestor(component);
    return (window != null && window.isFocused());
  }

}
