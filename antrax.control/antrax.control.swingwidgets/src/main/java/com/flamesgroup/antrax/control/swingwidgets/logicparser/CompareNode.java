package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public abstract class CompareNode extends OperatorNode {

  protected int compareBranches() {
    String val1 = ((StringNode) getLeft()).getValue();
    String val2 = ((StringNode) getRight()).getValue();
    //Comparable
    return val1.compareTo(val2);
  }

}
