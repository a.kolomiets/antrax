package com.flamesgroup.antrax.control.swingwidgets.calendar;

import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JXTimeView extends JPanel {
  private static final long serialVersionUID = 946117349844825481L;

  private final List<TimeChangerListener> listeners = new LinkedList<>();

  private final JSpinner spinnerHour;
  private final JSpinner spinnerMin;
  private final JSpinner spinnerSec;

  public JXTimeView(Date time) {
    if (time == null) {
      time = new Date();
    }
    spinnerHour = createTimePartSpinner(time, Calendar.HOUR_OF_DAY, "HH");
    spinnerMin = createTimePartSpinner(time, Calendar.MINUTE, "mm");
    spinnerSec = createTimePartSpinner(time, Calendar.SECOND, "ss");

    setLayout(new FlowLayout());
    add(spinnerHour);
    add(spinnerMin);
    add(spinnerSec);
  }

  private JSpinner createTimePartSpinner(final Date time, final int calendarField, final String dateFormatPattern) {
    SpinnerModel dateModel = new SpinnerDateModel(time, null, null, calendarField);
    JSpinner spinner = new JSpinner();
    spinner.setModel(dateModel);
    spinner.setEditor(new JSpinner.DateEditor(spinner, dateFormatPattern));
    spinner.addChangeListener(new SpinnerListener());
    return spinner;
  }

  public void addListener(final TimeChangerListener l) {
    listeners.add(l);
  }

  public void removeListener(final TimeChangerListener l) {
    listeners.remove(l);
  }

  private void fireTimeChanged(final Calendar timeField) {
    for (TimeChangerListener l : listeners) {
      l.onTimeChanged(timeField);
    }
  }

  private int getTimePart(final Date date, final int timeField) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(timeField);
  }

  public Calendar getTime() {
    int hour = getTimePart((Date) spinnerHour.getValue(), Calendar.HOUR_OF_DAY);
    int minutes = getTimePart((Date) spinnerMin.getValue(), Calendar.MINUTE);
    int seconds = getTimePart((Date) spinnerSec.getValue(), Calendar.SECOND);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime((Date) spinnerHour.getValue());
    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, minutes);
    calendar.set(Calendar.SECOND, seconds);
    return calendar;
  }

  public interface TimeChangerListener {
    void onTimeChanged(Calendar time);
  }

  public class SpinnerListener implements ChangeListener {
    @Override
    public void stateChanged(final ChangeEvent evt) {
      fireTimeChanged(getTime());
    }
  }

}
