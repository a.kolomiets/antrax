package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.swingwidgets.ColorUtils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class TableFilterPanelBuilder<T> {

  private static final long serialVersionUID = -3446851773797437122L;

  private final List<Filter<T>> filters = new LinkedList<>();

  private final UpdatableTableFilter<T> tableFilter;

  private final ActionListener actionListener;

  private final JPanel panel;

  public <K extends Comparable<K>> TableFilterPanelBuilder(final JUpdatableTable<T, K> table) {
    this.tableFilter = new UpdatableTableFilter<T>() {
      @Override
      public boolean isAccepted(final T elem) {
        if (isAllDisabled()) {
          return true;
        }
        for (Filter<T> f : filters) {
          if (f.isFilterEnabled() && f.isAccepted(elem)) {
            return true;
          }
        }
        return false;
      }

      private boolean isAllDisabled() {
        for (Filter<T> f : filters) {
          if (f.isFilterEnabled()) {
            return false;
          }
        }
        return true;
      }
    };
    this.actionListener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        table.setFilter(tableFilter);
      }
    };
    panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 0));
    panel.setBorder(BorderFactory.createLineBorder(panel.getBackground().darker()));
    panel.setBackground(ColorUtils.darker(panel.getBackground()));
    panel.add(new JLabel("Filters:"));
  }

  public JPanel getFilterPanel() {
    actionListener.actionPerformed(null);
    return panel;
  }

  public void addBooleanFilter(final String text, final boolean selected, final UpdatableTableFilter<T> filter) {
    final JCheckBox checkBox = new JCheckBox(text);
    checkBox.setOpaque(false);
    checkBox.setSelected(selected);
    checkBox.addActionListener(actionListener);
    panel.add(checkBox);
    filters.add(new Filter<T>() {

      @Override
      public boolean isFilterEnabled() {
        return checkBox.isSelected();
      }

      @Override
      public boolean isAccepted(final T elem) {
        return filter.isAccepted(elem);
      }

    });
  }

  public void addBooleanFilter(final String text, final UpdatableTableFilter<T> filter) {
    addBooleanFilter(text, false, filter);
  }

  private interface Filter<T> extends UpdatableTableFilter<T> {
    boolean isFilterEnabled();
  }

  public int getFilterCount() {
    return filters.size();
  }

}
