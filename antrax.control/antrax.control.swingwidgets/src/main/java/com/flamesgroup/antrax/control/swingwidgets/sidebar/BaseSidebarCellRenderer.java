package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import java.awt.*;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.tree.TreeCellRenderer;

public abstract class BaseSidebarCellRenderer extends JLabel implements TreeCellRenderer {

  private static final long serialVersionUID = 3936580449073538502L;

  /**
   * Last tree the renderer was painted in.
   */
  protected JTree tree;

  /**
   * True if has focus.
   */
  protected boolean hasFocus;

  //  private boolean isDropCell;
  //
  //  /** True if draws focus border around icon as well. */
  //  private boolean drawsFocusBorderAroundIcon;

  /**
   * Is the value currently selected.
   */
  private boolean selected;

  private SidebarNode node;

  // Icons

  /**
   * Icon used to show non-leaf nodes that aren't expanded.
   */
  transient protected Icon closedIcon;

  /**
   * Icon used to show leaf nodes.
   */
  transient protected Icon leafIcon;

  /**
   * Icon used to show non-leaf nodes that are expanded.
   */
  transient protected Icon openIcon;

  // Colors

  /**
   * Color to use for the foreground for selected nodes.
   */
  protected Color textSelectionColor;

  /**
   * Color to use for the foreground for non-selected nodes.
   */
  protected Color textNonSelectionColor;

  /**
   * Color to use for the background when a node is selected.
   */
  protected Color backgroundSelectionColor;

  /**
   * Color to use for the background when the node isn't selected.
   */
  protected Color backgroundNonSelectionColor;

  /**
   * Color to use for the focus indicator when the node has focus.
   */
  protected Color borderSelectionColor;

  //
  // Internal properties
  //

  public SidebarNode getNode() {
    return node;
  }

  public void setNode(final SidebarNode node) {
    this.node = node;
  }

  public boolean isSelected() {
    return selected;
  }

  protected void setSelected(final boolean selected) {
    this.selected = selected;
  }

  /**
   * Returns the default icon, for the current laf, that is used to represent
   * non-leaf nodes that are expanded.
   */
  public Icon getDefaultOpenIcon() {
    return UIManager.getIcon("Tree.openIcon");
  }

  /**
   * Returns the default icon, for the current laf, that is used to represent
   * non-leaf nodes that are not expanded.
   */
  public Icon getDefaultClosedIcon() {
    return UIManager.getIcon("Tree.closedIcon");
  }

  /**
   * Returns the default icon, for the current laf, that is used to represent
   * leaf nodes.
   */
  public Icon getDefaultLeafIcon() {
    return UIManager.getIcon("Tree.leafIcon");
  }

  /**
   * Sets the icon used to represent non-leaf nodes that are expanded.
   */
  public void setOpenIcon(final Icon newIcon) {
    openIcon = newIcon;
  }

  /**
   * Returns the icon used to represent non-leaf nodes that are expanded.
   */
  public Icon getOpenIcon() {
    return openIcon;
  }

  /**
   * Sets the icon used to represent non-leaf nodes that are not expanded.
   */
  public void setClosedIcon(final Icon newIcon) {
    closedIcon = newIcon;
  }

  /**
   * Returns the icon used to represent non-leaf nodes that are not expanded.
   */
  public Icon getClosedIcon() {
    return closedIcon;
  }

  /**
   * Sets the icon used to represent leaf nodes.
   */
  public void setLeafIcon(final Icon newIcon) {
    leafIcon = newIcon;
  }

  /**
   * Returns the icon used to represent leaf nodes.
   */
  public Icon getLeafIcon() {
    return leafIcon;
  }

  /**
   * Sets the color the text is drawn with when the node is selected.
   */
  public void setTextSelectionColor(final Color newColor) {
    textSelectionColor = newColor;
  }

  /**
   * Returns the color the text is drawn with when the node is selected.
   */
  public Color getTextSelectionColor() {
    return textSelectionColor;
  }

  /**
   * Sets the color the text is drawn with when the node isn't selected.
   */
  public void setTextNonSelectionColor(final Color newColor) {
    textNonSelectionColor = newColor;
  }

  /**
   * Returns the color the text is drawn with when the node isn't selected.
   */
  public Color getTextNonSelectionColor() {
    return textNonSelectionColor;
  }

  /**
   * Sets the color to use for the background if node is selected.
   */
  public void setBackgroundSelectionColor(final Color newColor) {
    backgroundSelectionColor = newColor;
  }

  /**
   * Returns the color to use for the background if node is selected.
   */
  public Color getBackgroundSelectionColor() {
    return backgroundSelectionColor;
  }

  /**
   * Sets the background color to be used for non selected nodes.
   */
  public void setBackgroundNonSelectionColor(final Color newColor) {
    backgroundNonSelectionColor = newColor;
  }

  /**
   * Returns the background color to be used for non selected nodes.
   */
  public Color getBackgroundNonSelectionColor() {
    return backgroundNonSelectionColor;
  }

  /**
   * Sets the color to use for the border.
   */
  public void setBorderSelectionColor(final Color newColor) {
    borderSelectionColor = newColor;
  }

  /**
   * Returns the color the border is drawn.
   */
  public Color getBorderSelectionColor() {
    return borderSelectionColor;
  }

  //
  // TreeCellRenderer
  //

  @Override
  public Component getTreeCellRendererComponent(final JTree tree, final Object value,
      final boolean selected, final boolean expanded, final boolean leaf, final int row,
      final boolean hasFocus) {

    String stringValue = tree.convertValueToText(value, selected, expanded,
        leaf, row, hasFocus);

    this.tree = tree;
    this.hasFocus = hasFocus;
    setSelected(selected);
    setText(stringValue);
    setNode((value != null && value instanceof SidebarNode) ? (SidebarNode) value : null);

    // Determine foreground color
    Color fgColor;
    //    isDropCell = false;

    JTree.DropLocation dropLocation = tree.getDropLocation();
    if (dropLocation != null
        && dropLocation.getChildIndex() == -1
        && tree.getRowForPath(dropLocation.getPath()) == row) {

      Color dropCellColor = UIManager.getColor("Tree.dropCellForeground");
      if (dropCellColor != null) {
        fgColor = dropCellColor;
      } else {
        fgColor = getTextSelectionColor();
      }

      //      isDropCell = true;
    } else if (selected) {
      fgColor = getTextSelectionColor();
    } else {
      fgColor = getTextNonSelectionColor();
    }
    setForeground(fgColor);

    // There needs to be a way to specify disabled icons.
    //    if (!tree.isEnabled()) {
    //        setEnabled(false);
    //        if (leaf) {
    //          setDisabledIcon(getLeafIcon());
    //        } else if (expanded) {
    //          setDisabledIcon(getOpenIcon());
    //        } else {
    //          setDisabledIcon(getClosedIcon());
    //        }
    //    }
    //    else {
    //        setEnabled(true);
    //        if (leaf) {
    //          setIcon(getLeafIcon());
    //        } else if (expanded) {
    //          setIcon(getOpenIcon());
    //        } else {
    //          setIcon(getClosedIcon());
    //        }
    //    }

    setComponentOrientation(tree.getComponentOrientation());

    return this;
  }

  //
  // JComponent
  //

  /**
   * Subclassed to map <code>FontUIResource</code>s to null. If
   * <code>font</code> is null, or a <code>FontUIResource</code>, this has the
   * effect of letting the font of the JTree show through. On the other hand,
   * if <code>font</code> is non-null, and not a <code>FontUIResource</code>,
   * the font becomes <code>font</code>.
   */
  @Override
  public void setFont(Font font) {
    if (font instanceof FontUIResource)
      font = null;
    super.setFont(font);
  }

  /**
   * Gets the font of this component.
   *
   * @return this component's font; if a font has not been set for this
   * component, the font of its parent is returned
   */
  @Override
  public Font getFont() {
    Font font = super.getFont();
    if (font == null && tree != null) {
      // Strive to return a non-null value, otherwise the html support
      // will typically pick up the wrong font in certain situations.
      font = tree.getFont();
    }
    return font;
  }

  /**
   * Subclassed to map <code>ColorUIResource</code>s to null. If
   * <code>color</code> is null, or a <code>ColorUIResource</code>, this has
   * the effect of letting the background color of the JTree show through. On
   * the other hand, if <code>color</code> is non-null, and not a
   * <code>ColorUIResource</code>, the background becomes <code>color</code>.
   */
  @Override
  public void setBackground(Color color) {
    if (color instanceof ColorUIResource)
      color = null;
    super.setBackground(color);
  }

  @Override
  protected abstract void paintComponent(Graphics g);

  //
  // Empty override
  //

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void validate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   *
   * @since 1.5
   */
  @Override
  public void invalidate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void revalidate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void repaint(final long tm, final int x, final int y, final int width, final int height) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void repaint(final Rectangle r) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   *
   * @since 1.5
   */
  @Override
  public void repaint() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  protected void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    // Strings get interned...
    if ("text".equals(propertyName)
        || (("font".equals(propertyName) || "foreground".equals(propertyName))
        && oldValue != newValue
        && getClientProperty(javax.swing.plaf.basic.BasicHTML.propertyKey) != null)) {

      super.firePropertyChange(propertyName, oldValue, newValue);
    }
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final byte oldValue,
      final byte newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final char oldValue,
      final char newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final short oldValue,
      final short newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final int oldValue,
      final int newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final long oldValue,
      final long newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final float oldValue,
      final float newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final double oldValue,
      final double newValue) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final boolean oldValue,
      final boolean newValue) {
  }

}
