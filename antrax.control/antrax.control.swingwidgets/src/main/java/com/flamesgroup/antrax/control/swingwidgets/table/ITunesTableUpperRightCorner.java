package com.flamesgroup.antrax.control.swingwidgets.table;

import java.awt.*;

import javax.swing.*;

public class ITunesTableUpperRightCorner extends JLabel {

  private static final long serialVersionUID = 6873403880365230592L;

  private static final Color BORDER_LEFT = new Color(0xd8d8d8);

  protected Image headerImg = loadImage("itunes_table_header.png");

  private static Image loadImage(final String name) {
    return new ImageIcon(ITunesTableHeaderRenderer.class.getResource(name))
        .getImage();
  }

  @Override
  public void paint(final Graphics g) {
    Graphics2D g2 = (Graphics2D) g.create();
    try {
      Dimension vSize = getSize();
      int x = 0;
      int y = 0;
      int h = vSize.height;
      int w = vSize.width;

      // Draw background
      g2.drawImage(headerImg, x, y + 1, x + w, y + 1 + h - 2, 0, 0,
          headerImg.getWidth(null), headerImg.getHeight(null), null);

      g2.setColor(BORDER_LEFT);
      g2.drawLine(x, y, x, y + h - 1);
      g2.setColor(ITunesTableHeaderRenderer.BORDER_COLOR);
      g2.drawLine(x, y, x + w - 1, y);
      g2.drawLine(x, y + h - 1, x + w, y + h - 1);

    } finally {
      g2.dispose();
    }
  }

}
