package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.PatternSyntaxException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class TestFilterTable {
  public static void main(final String[] args) {
    Runnable runner = new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame("Sorting JTable");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Object rows[][] = {
            {"AMZN", "Amazon", 41.28},
            {"EBAY", "eBay", 41.57},
            {"GOOG", "Google", 388.33},
            {"MSFT", "Microsoft", 26.56},
            {"NOK", "Nokia Corp", 17.13},
            {"ORCL", "Oracle Corp.", 12.52},
            {"SUNW", "Sun Microsystems", 3.86},
            {"TWX", "Time Warner", 17.66},
            {"VOD", "Vodafone Group", 26.02},
            {"YHOO", "Yahoo!", 37.69}};
        Object columns[] = {"Symbol", "Name", "Price"};
        TableModel model = new DefaultTableModel(rows, columns) {
          private static final long serialVersionUID = 1L;

          @Override
          public Class<?> getColumnClass(final int column) {
            Class<?> returnValue;
            if ((column >= 0) && (column < getColumnCount())) {
              returnValue = getValueAt(0, column).getClass();
            } else {
              returnValue = Object.class;
            }
            return returnValue;
          }
        };

        // JTable: -- Symbol -- Name -- Price --
        JTable table = new JTable(model);
        final TableRowSorter<TableModel> sorter = new TableRowSorter<>(model);
        table.setRowSorter(sorter);
        JScrollPane pane = new JScrollPane(table);
        frame.add(pane, BorderLayout.CENTER);

        // Filter: [SUN___________]
        JPanel panel = new JPanel(new BorderLayout());
        JLabel label = new JLabel("Filter:");
        panel.add(label, BorderLayout.WEST);
        final JTextField filterText = new JTextField("SUN");
        panel.add(filterText, BorderLayout.CENTER);
        frame.add(panel, BorderLayout.NORTH);
        //        < Filter >
        JButton button = new JButton("Filter");
        button.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            String text = filterText.getText();
            if (text.length() == 0) {
              sorter.setRowFilter(null);
            } else {
              try {
                sorter.setRowFilter(RowFilter.regexFilter(text));
              } catch (PatternSyntaxException ignored) {
                System.err.println("Bad regex pattern");
              }
            }
          }
        });
        frame.add(button, BorderLayout.SOUTH);
        frame.setSize(300, 250);
        frame.setVisible(true);
      }
    };
    EventQueue.invokeLater(runner);
  }
}
