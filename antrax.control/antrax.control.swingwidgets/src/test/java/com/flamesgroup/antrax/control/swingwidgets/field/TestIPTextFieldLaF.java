package com.flamesgroup.antrax.control.swingwidgets.field;

import java.awt.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.*;

public class TestIPTextFieldLaF {

  public static void main(final String[] args) throws UnknownHostException {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    IPTextField ipField = new IPTextField();

    JPanel panel = new JPanel(new FlowLayout());
    ipField.setText("192.168.10.1");
    InetAddress addr = InetAddress.getByName("172.16.10.1");
    ipField.setInetAddress(addr);

    panel.add(ipField);
    panel.add(new JButton("Test"));
    panel.add(new JTextField("Test"));

    frame.add(panel);
    frame.setSize(500, 250);
    frame.setVisible(true);
    frame.setLocationRelativeTo(null);
  }

}
