/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.engine;

import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.listeners.SimCallHistoryListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.scripts.IMEIGeneratorScript;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.commons.BaseEngine;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.control.simserver.core.Configuration;
import com.flamesgroup.antrax.control.simserver.core.DefaultActivityPeriodScript;
import com.flamesgroup.antrax.control.simserver.core.DefaultGatewaySelectorScript;
import com.flamesgroup.antrax.control.simserver.core.DefaultIMEIGeneratorScript;
import com.flamesgroup.antrax.control.simserver.core.DefaultSIMCardFactorScript;
import com.flamesgroup.antrax.control.simserver.core.ISimUnitScriptUpdater;
import com.flamesgroup.antrax.control.simserver.core.SimUnit;
import com.flamesgroup.antrax.control.simserver.core.SimUnitPool;
import com.flamesgroup.antrax.control.simserver.dao.ConfigurationDAO;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.util.Objects;

// TODO: check to copy past and review in future
public class ConfigurationEngine extends BaseEngine implements ISimUnitScriptUpdater {

  private static final Logger logger = LoggerFactory.getLogger(ConfigurationEngine.class);

  private final Configuration cfg;
  private final ConfigurationDAO dao;
  private final SimUnitPool simUnitPool;
  private final ISimCallHistory simCallHistory;

  private final RegistryAccess registryAccess;

  private final long timeout;

  public ConfigurationEngine(final Configuration cfg, final ConfigurationDAO dao, final SimUnitPool simUnitPool, final RegistryAccess registry, final ISimCallHistory simCallHistory) {
    super("ConfigurationEngine");
    this.cfg = cfg;
    this.dao = dao;
    this.simUnitPool = simUnitPool;
    this.registryAccess = registry;
    this.simCallHistory = simCallHistory;

    this.timeout = ControlPropUtils.getInstance().getControlServerProperties().getReconfigurationTimeout();
  }

  @Override
  protected long countSleepTime() {
    return timeout;
  }

  @Override
  protected void invokeRoutine() throws Exception {
    updateGatewaysData();
    if (!isInterrupted()) {
      updateGsmToSimGroupLinks();
    }
    boolean storeUpdated = false;
    if (!isInterrupted()) {
      storeUpdated = updatePluginsStore();
    }
    if (!isInterrupted()) {
      updateUnits(storeUpdated);
    }
  }

  private void updateGatewaysData() {
    logger.trace("[{}] - going to update gateways data", this);
    long gatewaysRevision = Revisions.getInstance().getServersRevision();

    if (cfg.getGatewaysRevision() >= gatewaysRevision) {
      logger.trace("[{}] - gateways is already up to date", this);
      return;
    }
    cfg.setGateways(dao.loadGatewaysData(), gatewaysRevision);
    logger.trace("[{}] - update of gateways complete. Gateways is in {} revision now", this, gatewaysRevision);
  }

  private boolean updatePluginsStore() throws Exception {
    logger.trace("[{}] - going to update plugins store", this);
    long revision = Revisions.getInstance().getScriptFilesRevision();
    if (cfg.getPluginsStore().getRevision() < revision) {
      logger.trace("[{}] - analyzing plugins store", this);
      ScriptFile scriptFile = dao.getLastScriptFile();
      if (scriptFile != null) {
        cfg.getPluginsStore().analyze(new ByteArrayInputStream(scriptFile.getData()), revision);
        return true;
      }
    }
    return false;
  }

  void updateUnits(final boolean requiresUpdate) {
    logger.trace("[{}] - starting updating units {}", this, simUnitPool.listUnits());
    for (SimUnit u : simUnitPool.listUnits()) {
      updateUnit(u, requiresUpdate);
    }
    logger.trace("[{}] - update units complete", this);
  }

  private void updateUnit(final SimUnit u, final boolean requiresUpdate) {
    SimData storedSimData = dao.loadSimData(u.getUID()); // TODO: need refactor
    if (!u.getSimData().isLocked() && storedSimData.isLocked()) {
      u.getSimData().setLocked(true).setLockTime(storedSimData.getLockTime()).setLockReason(storedSimData.getLockReason());
      u.setIndicationMode(IndicationMode.BUSINESS_PROBLEM);
    }

    if (u.isPinEnabled()) {
      return;
    }

    boolean resetScripts = dao.shouldResetSsScripts(u.getUID());
    try {
      if (requiresUpdate || simDataUpdated(u.getSimData(), storedSimData) || simGroupUpdated(u.getSIMGroup()) || resetScripts) {
        logger.trace("[{}] - found that {} requires update of data. Updating it", this, u);
        u.setSimData(storedSimData);
      } else {
        return;
      }

      SIMGroup simGroup = u.getSIMGroup();
      if (simGroup instanceof SimNoGroup) {
        logger.trace("[{}] - sim {} has no group, ignoring it", this, u.getChannelUID());
        if (u.isTaken()) {
          u.forceSessionEnd();
        }
        return;
      }

      logger.trace("[{}] - found that unit {} has out of date simGroup \"{}\". Updating it", this, u, u.getSIMGroup());
      updateScripts(u, simGroup, resetScripts);

      u.getSimRuntimeData().setReconfigureTime(System.currentTimeMillis());
      ensureSimCardAllowedByGroupLinks(u);
    } catch (Exception e) {
      logger.warn("[{}] - failed to configure unit {}", this, u, e);
    }
  }

  private boolean simGroupUpdated(final SIMGroup simGroup) {
    return !(simGroup instanceof SimNoGroup) && simGroup.getRevision() < Revisions.getInstance().getSimGroupRevision(simGroup);
  }

  private boolean simDataUpdated(final SimData runtime, final SimData stored) {
    return !Objects.equals(runtime.getSimGroup(), stored.getSimGroup())
        || !Objects.equals(runtime.getPhoneNumber(), stored.getPhoneNumber())
        || !Objects.equals(runtime.getImei(), stored.getImei());
  }

  private void updateSessionScript(final SimUnit u, final SIMGroup simGroup, final boolean resetScripts) {
    logger.trace("[{}] - updating session script for \"{}\" from \"{}\"", this, u, simGroup);
    AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
    if (!u.hasSessionScript()) {
      logger.trace("[{}] - found that we need to restore script state", this);
      StatefullScript[] deserialize = pluginsStore.deserialize(StatefullScript[].class, dao.getSessionScript(u.getUID()));
      if (deserialize != null) {
        logger.trace("[{}] - setting state", this);
        initialize(deserialize[0], u.getSimData(), registryAccess);
        u.setSessionScript(pluginsStore, false, (ActivityPeriodScript) deserialize[0]);
      } else {
        logger.trace("[{}] - script has no state", this);
      }
    }

    ActivityPeriodScript latest;
    if (simGroup.getSessionScript() != null) {
      try {
        latest = pluginsStore.instantiate(simGroup.getSessionScript(), ActivityPeriodScript.class, simGroup.listScriptCommons());
        initialize(latest, u.getSimData(), registryAccess);
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate session script for {}", this, u, e);
        latest = new DefaultActivityPeriodScript();
      }
    } else {
      latest = new DefaultActivityPeriodScript();
    }
    u.setSessionScript(pluginsStore, resetScripts, latest);
  }

  private void updateGatewaySelectorScript(final SimUnit u, final SIMGroup simGroup, final boolean resetScripts) {
    logger.trace("[{}] - updating gateway selection script for \"{}\" from \"{}\"", this, u, simGroup);
    AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
    if (!u.hasGatewaySelectionScript()) {
      logger.trace("[{}] - found that we need to restore script state", this);
      StatefullScript[] deserialize = pluginsStore.deserialize(StatefullScript[].class, dao.getGatewaySelectorScript(u.getUID()));
      if (deserialize != null) {
        logger.trace("[{}] - setting state", this);
        initialize(deserialize[0], u.getSimData(), registryAccess);
        u.setGatewaySelectorScript(pluginsStore, false, (GatewaySelectorScript) deserialize[0]);
      } else {
        logger.trace("[{}]  script has no state", this);
      }
    }

    GatewaySelectorScript latest;
    if (simGroup.getGWSelectorScript() != null) {
      try {
        latest = cfg.getPluginsStore().instantiate(simGroup.getGWSelectorScript(), GatewaySelectorScript.class, simGroup.listScriptCommons());
        initialize(latest, u.getSimData(), registryAccess);
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate gateway selector script for {}", this, u, e);
        latest = new DefaultGatewaySelectorScript();
      }
    } else {
      latest = new DefaultGatewaySelectorScript();
    }
    u.setGatewaySelectorScript(pluginsStore, resetScripts, latest);
  }

  private void updateFactorScript(final SimUnit u, final SIMGroup simGroup, final boolean resetScripts) {
    logger.trace("[{}] - updating factor script for \"{}\" from \"{}\"", this, u, simGroup);
    AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
    if (!u.hasFactorScript()) {
      logger.trace("[{}] - found that we need to restore script state", this);
      StatefullScript[] deserialize = pluginsStore.deserialize(StatefullScript[].class, dao.getFactorEvaluationScript(u.getUID()));
      if (deserialize != null) {
        logger.trace("[{}] - setting state", this);
        initialize(deserialize[0], u.getSimData(), registryAccess);
        u.setFactorScript(pluginsStore, false, (FactorEvaluationScript) deserialize[0]);
      } else {
        logger.trace("[{}] script has no state", this);
      }
    }

    FactorEvaluationScript latest;
    if (simGroup.getFactorScript() != null) {
      try {
        latest = cfg.getPluginsStore().instantiate(simGroup.getFactorScript(), FactorEvaluationScript.class, simGroup.listScriptCommons());
        initialize(latest, u.getSimData(), registryAccess);
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate factor script for {}", this, u, e);
        latest = new DefaultSIMCardFactorScript();
      }
    } else {
      latest = new DefaultSIMCardFactorScript();
    }
    u.setFactorScript(pluginsStore, resetScripts, latest);
  }

  private void updateActivityScript(final SimUnit u, final SIMGroup simGroup, final boolean resetScripts) {
    logger.trace("[{}] - updating activity script for \"{}\" from \"{}\"", this, u, simGroup);
    AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
    if (!u.hasActivityScript()) {
      logger.trace("[{}] - found that we need to restore script state", this);
      StatefullScript[] deserialize = pluginsStore.deserialize(StatefullScript[].class, dao.getActivityScript(u.getUID()));
      if (deserialize != null) {
        logger.trace("[{}] - setting state", this);
        initialize(deserialize[0], u.getSimData(), registryAccess);
        u.setActivityScript(pluginsStore, false, (ActivityPeriodScript) deserialize[0]);
      } else {
        logger.trace("[{}] - script has no state", this);
      }
    }

    ActivityPeriodScript latest;
    if (simGroup.getActivityPeriodScript() != null) {
      try {
        latest = cfg.getPluginsStore().instantiate(simGroup.getActivityPeriodScript(), ActivityPeriodScript.class, simGroup.listScriptCommons());
        initialize(latest, u.getSimData(), registryAccess);
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate activity script for {}", this, u, e);
        latest = new DefaultActivityPeriodScript();
      }
    } else {
      latest = new DefaultActivityPeriodScript();
    }
    u.setActivityScript(pluginsStore, resetScripts, latest);
  }

  private void updateIMEIGeneratorScript(final SimUnit u, final SIMGroup simGroup, final boolean resetScripts) {
    logger.trace("[{}] - updating imei generator script for \"{}\" from \"{}\"", this, u, simGroup);
    AntraxPluginsStore pluginsStore = cfg.getPluginsStore();
    if (!u.hasIMEIGeneratorScript()) {
      logger.trace("[{}] - found that we need to restore script state", this);
      StatefullScript[] deserialize = pluginsStore.deserialize(StatefullScript[].class, dao.getIMEIGeneratorScript(u.getUID()));
      if (deserialize != null) {
        logger.trace("[{}] - setting state", this);
        initialize(deserialize[0], u.getSimData(), registryAccess);
        u.setIMEIGeneratorScript(pluginsStore, false, (IMEIGeneratorScript) deserialize[0]);
      } else {
        logger.trace("[{}] - script has no state", this);
      }
    }

    IMEIGeneratorScript latest;
    if (simGroup.getIMEIGeneratorScript() != null) {
      try {
        latest = cfg.getPluginsStore().instantiate(simGroup.getIMEIGeneratorScript(), IMEIGeneratorScript.class, simGroup.listScriptCommons());
        initialize(latest, u.getSimData(), registryAccess);
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate imei generator script for {}", this, u, e);
        latest = new DefaultIMEIGeneratorScript();
      }
    } else {
      latest = new DefaultIMEIGeneratorScript();
    }
    u.setIMEIGeneratorScript(pluginsStore, resetScripts, latest);
  }

  private void initialize(final Object script, final SimData simData, final RegistryAccess registry) {
    if (script instanceof SimDataListener) {
      ((SimDataListener) script).setSimData(simData);
    }
    if (script instanceof SimCallHistoryListener) {
      ((SimCallHistoryListener) script).setSimCallHistory(simCallHistory);
    }
    if (script instanceof RegistryAccessListener) {
      ((RegistryAccessListener) script).setRegistryAccess(registry);
    }
  }

  private void updateGsmToSimGroupLinks() {
    logger.trace("[{}] - going to update links of sim to gsm groups", this);
    long revision = Revisions.getInstance().getGsmToSimGroupLinksRevision();
    if (revision <= cfg.getGroupLinksRevision()) {
      logger.trace("[{}] - links of gsm to sim groups is up to date and does not requires update", this);
      return;
    }
    cfg.setLinks(dao.loadGsmToSimLinks(), revision);
    // We should break all channels which does not apply current configuration (probably, when some links were removed or sim cards start to belong to other groups)
    for (SimUnit unit : simUnitPool.listUnits()) {
      ensureSimCardAllowedByGroupLinks(unit);
    }
    logger.trace("[{}] - update of links of gsm to sim groups completed. Links is in {} revision", this, revision);
  }

  private void ensureSimCardAllowedByGroupLinks(final SimUnit u) {
    synchronized (u) {
      if (!u.isTaken()) {
        return;
      }
      if (cfg.applies(u.getCurrentGSMGroup(), u.getSIMGroup())) {
        return;
      }
      logger.trace("[{}] found that {} was taken by previous configuration rules which now does not aplied. Forsing it's session end", this, u);
      u.forceSessionEnd();
    }
  }

  @Override
  public void updateScripts(final SimUnit simUnit, final SIMGroup simGroup, final boolean resetScripts) {
    updateActivityScript(simUnit, simGroup, resetScripts);
    updateFactorScript(simUnit, simGroup, resetScripts);
    updateGatewaySelectorScript(simUnit, simGroup, resetScripts);
    updateSessionScript(simUnit, simGroup, resetScripts);
    updateIMEIGeneratorScript(simUnit, simGroup, resetScripts);
    if (resetScripts) {
      logger.info("[{}] - {}: script states was reset", this, simUnit);
      simUnit.clearResetSsScriptsFlag();
    }
  }

}
