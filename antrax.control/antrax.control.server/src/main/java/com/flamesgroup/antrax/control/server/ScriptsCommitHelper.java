/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.control.communication.ScriptRemovalConflictNotResolvedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.dao.IConfigEditDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ScriptsCommitHelper {

  private final Logger logger = LoggerFactory.getLogger(ScriptsCommitHelper.class);

  private final IConfigEditDAO editDAO;

  public ScriptsCommitHelper(final IConfigEditDAO editDAO) {
    this.editDAO = editDAO;
  }

  private void validateSimGroupConflicts(final SimGroupScriptModificationConflict[] resolvedConflicts) throws ScriptRemovalConflictNotResolvedException {
    for (SimGroupScriptModificationConflict c : resolvedConflicts) {
      if (!c.isFullyResolved()) {
        throw new ScriptRemovalConflictNotResolvedException("Conflict of " + c.getUnresolvedInfo() + " is not resolved for " + c.getOldGroup().getName());
      }
    }
  }

  public void commit(final ScriptFile scriptFile, final SimGroupScriptModificationConflict[] simGroupConflicts) throws Exception {
    AntraxPluginsStore ps = analyzeJavaFiles(new ByteArrayInputStream(scriptFile.getData()));
    validateSimGroupConflicts(simGroupConflicts);
    // Normalizing conflicts scripts (Using just inserted definitions, because they have an ids)
    normalizeScripts(simGroupConflicts, ps);
    // Resolve sim groups conflicts
    logger.debug("[{}] - resolving sim group conflicts", this);
    try {
      List<SIMGroup> newSimGroup = Arrays.stream(simGroupConflicts).map(SimGroupScriptModificationConflict::getNewGroup).collect(Collectors.toList());
      editDAO.applyScriptConflicts(scriptFile, newSimGroup, ps.listDefinitions());
      Revisions.getInstance().incrementAndGetScriptFilesRevision();
    } catch (DataModificationException e) {
      logger.warn("[{}] - while commit code base", this, e);
      throw new StorageException("Error while commit code base: " + e.getMessage(), e);
    }
  }

  private void normalizeScripts(final SimGroupScriptModificationConflict[] simGroupConflicts, final AntraxPluginsStore ps) {
    for (SimGroupScriptModificationConflict c : simGroupConflicts) {
      SIMGroup group = c.getNewGroup();
      group.setBusinessActivityScripts(normalizeScripts(group.getBusinessActivityScripts(), ps));
      group.setActionProviderScripts(normalizeScripts(group.getActionProviderScripts(), ps));
      group.setActivityPeriodScript(normalizeScript(group.getActivityPeriodScript(), ps));
      group.setFactorScript(normalizeScript(group.getFactorScript(), ps));
      group.setGWSelectorScript(normalizeScript(group.getGWSelectorScript(), ps));
      group.setIncomingCallManagementScript(normalizeScript(group.getIncomingCallManagementScript(), ps));
      group.setSessionScript(normalizeScript(group.getSessionScript(), ps));
      group.setVSFactorScript(normalizeScript(group.getVSFactorScript(), ps));
      group.setCallFilterScripts(normalizeScripts(group.getCallFilterScripts(), ps));
      group.setSmsFilterScripts(normalizeScripts(group.getSmsFilterScripts(), ps));
      group.setIMEIGeneratorScript(normalizeScript(group.getIMEIGeneratorScript(), ps));
    }
  }

  private ScriptInstance[] normalizeScripts(final ScriptInstance[] scripts, final AntraxPluginsStore ps) {
    ScriptInstance[] retval = new ScriptInstance[scripts.length];
    for (int i = 0; i < scripts.length; ++i) {
      retval[i] = normalizeScript(scripts[i], ps);
    }
    return retval;
  }

  private ScriptInstance normalizeScript(final ScriptInstance script, final AntraxPluginsStore ps) {
    if (script == null) {
      return null;
    }
    ScriptDefinition def = ps.getDefinition(script.getDefinition().getName());
    ScriptInstance retval = def.createInstance();
    retval.setParameters(script);
    return retval;
  }

  private AntraxPluginsStore analyzeJavaFiles(final InputStream inputStream) throws Exception {
    AntraxPluginsStore ps = new AntraxPluginsStore();
    ps.analyze(inputStream, 1);
    return ps;
  }

}
