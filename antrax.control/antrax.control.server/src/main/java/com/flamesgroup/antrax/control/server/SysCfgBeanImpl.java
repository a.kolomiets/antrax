/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RemoteRegistryAccess;
import com.flamesgroup.antrax.configuration.IPingServerManager;
import com.flamesgroup.antrax.configuration.NoSuchServerException;
import com.flamesgroup.antrax.configuration.SysCfgBean;
import com.flamesgroup.antrax.control.communication.IActivityRemoteLogger;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.simserver.GsmUnitManager;
import com.flamesgroup.antrax.control.simserver.IGsmUnitManger;
import com.flamesgroup.antrax.control.simserver.ISimServerManager;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.simserver.SimUnitManager;
import com.flamesgroup.antrax.storage.commons.IServerData;

public class SysCfgBeanImpl implements SysCfgBean {

  private final DAOProvider daoProvider;
  private final PingServersManager pingServersManager;
  private final IActionExecutionManager actionExecutionManager;

  private final IRemoteHistoryLogger historyLogger;
  private final ISmsManager smsManager;
  private final IGsmViewManager gsmViewManager;

  private final IRemoteVSConfigurator remoteVSConfigurator;
  private final RemoteRegistryAccess remoteRegistryAccess;
  private final ISimCallHistory simCallHistory;
  private final SimUnitManager simUnitManager;
  private final GsmUnitManager gsmUnitManager;

  private final IActivityRemoteLogger activityRemoteLogger;

  public SysCfgBeanImpl(final DAOProvider daoProvider, final PingServersManager pingServersManager, final IActionExecutionManager actionExecutionManager,
      final IRemoteHistoryLogger historyLogger, final ISmsManager smsManager,
      final IGsmViewManager gsmViewManager, final IRemoteVSConfigurator remoteVSConfigurator, final RemoteRegistryAccess remoteRegistryAccess,
      final ISimCallHistory simCallHistory, final SimUnitManager simUnitManager, final GsmUnitManager gsmUnitManager, final IActivityRemoteLogger activityRemoteLogger) {
    this.daoProvider = daoProvider;
    this.pingServersManager = pingServersManager;
    this.actionExecutionManager = actionExecutionManager;
    this.historyLogger = historyLogger;
    this.smsManager = smsManager;
    this.gsmViewManager = gsmViewManager;
    this.remoteVSConfigurator = remoteVSConfigurator;
    this.remoteRegistryAccess = remoteRegistryAccess;
    this.simCallHistory = simCallHistory;
    this.simUnitManager = simUnitManager;
    this.gsmUnitManager = gsmUnitManager;
    this.activityRemoteLogger =activityRemoteLogger;
  }

  @Override
  public IServerData getServer(final String serverName) throws NoSuchServerException {
    IServerData retval = daoProvider.getConfigViewDAO().getServerByName(serverName);
    if (retval == null) {
      throw new NoSuchServerException(serverName);
    }
    return retval;
  }

  @Override
  public long getControlServerCurrentTime() {
    return System.currentTimeMillis();
  }

  @Override
  public IPingServerManager getPingServerManager() {
    return pingServersManager;
  }

  @Override
  public IActionExecutionManager getActionExecutionManager() {
    return actionExecutionManager;
  }

  @Override
  public IRemoteHistoryLogger getHistoryLogger() {
    return historyLogger;
  }

  @Override
  public IRemoteVSConfigurator getRemoteVSConfigurator() {
    return remoteVSConfigurator;
  }

  @Override
  public RemoteRegistryAccess getRemoteRegistryAccess() {
    return remoteRegistryAccess;
  }

  @Override
  public ISmsManager getSmsManager() {
    return smsManager;
  }

  @Override
  public IGsmViewManager getGsmViewManager() {
    return gsmViewManager;
  }

  @Override
  public ISimUnitManager getSimUnitManager() {
    return simUnitManager;
  }

  @Override
  public ISimServerManager getSimServerManager() {
    return simUnitManager;
  }

  @Override
  public IGsmChannelManagerHandler getGsmChannelManagerHandler() {
    return gsmUnitManager;
  }

  @Override
  public IGsmUnitManger getGsmUnitManager() {
    return gsmUnitManager;
  }

  @Override
  public ISimCallHistory getSimCallHistory() {
    return simCallHistory;
  }

  @Override
  public IActivityRemoteLogger getActivityRemoteLogger() {
    return activityRemoteLogger;
  }

}
