/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.autoactions;

import com.flamesgroup.antrax.autoactions.ActionExecutionStatus;
import com.flamesgroup.antrax.autoactions.ExecutionStatus;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.control.commons.BaseEngine;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// TODO: check to remove extends BaseEngine
public class ActionExecutionManager extends BaseEngine implements IActionExecutionManager {

  private static final Logger logger = LoggerFactory.getLogger(ActionExecutionManager.class);

  private final long cleanupTimeout;

  private final Lock executionLock = new ReentrantLock();
  private final Map<UUID, Action> actions = new HashMap<>();
  private final Map<UUID, ActionExecution> actionExecutions = new HashMap<>();
  private final Map<UUID, Set<String>> executionRoutes = new HashMap<>();
  private final Map<String, Long> servers = new ConcurrentHashMap<>();

  public ActionExecutionManager() {
    super("ExpiredActionsCleanup");
    this.cleanupTimeout = ControlPropUtils.getInstance().getControlServerProperties().getExpireActionTimeout();
  }

  @Override
  public UUID execute(final Action action, final long maxTimeout) throws RemoteException {
    logger.trace("[{}] - asked to execute action {}", this, action);
    UUID executionID = UUID.randomUUID();
    executionLock.lock();
    try {
      actions.put(executionID, action);
      actionExecutions.put(executionID, new ActionExecution(maxTimeout));
      executionRoutes.put(executionID, new HashSet<>());
    } finally {
      executionLock.unlock();
    }
    logger.trace("[{}] - executionID={}", this, executionID);
    return executionID;
  }

  @Override
  public ExecutionStatus getExecutionStatus(final UUID executionID) throws RemoteException {
    logger.trace("[{}] - asked for status for {}", this, executionID);
    executionLock.lock();
    try {
      if (!actionExecutions.containsKey(executionID)) {
        return ExecutionStatus.FAILED; // execution was expired by cleanup method
      }

      ActionExecution actionExecution = actionExecutions.get(executionID);
      if (actionExecution.isExpired()) {
        logger.trace("[{}] - execution {} was expired", this, executionID);
        actionExecution.setExecutionStatus(ExecutionStatus.FAILED);
      }

      ExecutionStatus executionStatus = actionExecution.getExecutionStatus();
      if (executionStatus == ExecutionStatus.FINISHED || executionStatus == ExecutionStatus.FAILED) {
        actions.remove(executionID);
        actionExecutions.remove(executionID);
        executionRoutes.remove(executionID);
      }

      logger.trace("[{}] - execution {} status {}", this, executionID, executionStatus);
      return executionStatus;
    } finally {
      executionLock.unlock();
    }
  }

  @Override
  public Pair<UUID, Action> getExecution(final String serverName) throws RemoteException {
    servers.put(serverName, System.currentTimeMillis());

    logger.trace("[{}] - voice-server wants to execute action", this);
    executionLock.lock();
    try {
      for (Entry<UUID, Action> entry : actions.entrySet()) {
        UUID executionID = entry.getKey();
        ActionExecution actionExecution = actionExecutions.get(executionID);
        if (executionRoutes.get(executionID).contains(serverName) || actionExecution.isExpired()) {
          continue;
        }

        if (actionExecution.getExecutionStatus() == ExecutionStatus.READY) {
          actionExecution.setExecutionStatus(ExecutionStatus.IN_PROGRESS);
          executionRoutes.get(executionID).add(serverName);
          logger.trace("[{}] found {} for it", this, executionID);
          return new Pair<>(executionID, entry.getValue());
        }
      }
    } finally {
      executionLock.unlock();
    }
    return null;
  }

  @Override
  public void handleActionExecutionStatus(final String serverName, final UUID executionID, final ActionExecutionStatus actionExecutionStatus)
      throws RemoteException {
    logger.trace("[{}] - execution {} was {}", this, executionID, actionExecutionStatus);
    executionLock.lock();
    try {
      if (!actionExecutions.containsKey(executionID)) {
        return; // execution was expired
      }

      ActionExecution actionExecution = actionExecutions.get(executionID);
      switch (actionExecutionStatus) {
        case FINISHED:
          actionExecution.setExecutionStatus(ExecutionStatus.FINISHED);
          break;
        case CANNOT_EXECUTE:
          actionExecution.setExecutionStatus(ExecutionStatus.READY);
          if (!executionRoutes.get(executionID).equals(servers.keySet())) {
            break; // still have a server to execute action
          }
        case FAILED:
          actionExecution.setExecutionStatus(ExecutionStatus.FAILED);
          break;
        case BUSY:
          actionExecution.setExecutionStatus(ExecutionStatus.READY);
          executionRoutes.get(executionID).remove(serverName);
          break;
      }
    } finally {
      executionLock.unlock();
    }
  }

  @Override
  public void invokeRoutine() throws Exception {
    logger.trace("[{}] - starting cleanup of expired actions", this);
    Set<UUID> expired = new HashSet<>();
    executionLock.lock();
    try {
      actionExecutions.entrySet().stream().filter(entry -> entry.getValue().isExpired()).forEach(entry -> {
        UUID executionID = entry.getKey();
        logger.trace("[{}] - {} was expired", this, executionID);
        expired.add(executionID);
      });

      actionExecutions.keySet().removeAll(expired);
      actions.keySet().removeAll(expired);
      executionRoutes.keySet().removeAll(expired);
    } finally {
      executionLock.unlock();
    }

    logger.trace("[{}] - starting cleanup of expired servers", this);
    Set<String> removeServers = new HashSet<>();
    for (Entry<String, Long> entry : servers.entrySet()) {
      String server = entry.getKey();
      long expiringTimeout = System.currentTimeMillis() - entry.getValue();
      if (expiringTimeout > 10) {
        removeServers.add(server);
      }
    }
    servers.keySet().removeAll(removeServers);
    logger.trace("[{}] - cleanup finished", this);
  }

  @Override
  protected long countSleepTime() {
    return cleanupTimeout;
  }

}
