/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rest.alaris.sms;

import com.flamesgroup.antrax.control.properties.AlarisMultiUser;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.properties.IReloadListener;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class AlarisSmsManager implements IReloadListener {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsManager.class);

  private static final String SAR_TEMPLATE = "(?<id>\\p{XDigit}+)/(?<parts>\\d+)/(?<partNumber>\\d+)";
  private static final Pattern sarResponsePattern = Pattern.compile(SAR_TEMPLATE);

  private static final AlarisSmsManager instance = new AlarisSmsManager();

  private DAOProvider daoProvider;

  private AlarisSmsService alarisSmsService;

  private final Map<String, FakeSmsStatusStatistic> fakeSmsStatusStatistics = new ConcurrentHashMap<>();

  private AlarisSmsManager() {
  }

  public UUID sendSms(final String clientIp, final String ani, final String dnis, final String message, final String serviceType, final AlarisSms.LongMessageMode longMessageMode,
      final Integer dataCoding, final String sar) {
    if (!alarisSmsService.canSendAlarisSms(clientIp)) {
      throw new AlarisSmsException("NO ROUTES");
    }
    if (!PhoneNumber.isValid(dnis) || dnis.length() > 15) {
      throw new AlarisSmsException(String.format("Invalid phoneNumber: [%s] of param: [dnis]", dnis));
    }
    Date currentTime = new Date();
    UUID uuid = UUID.randomUUID();
    AlarisSms.Sar alarisSmsSar = null;
    if (sar != null && !sar.isEmpty()) {
      Matcher matcher = sarResponsePattern.matcher(sar);
      if (!matcher.matches()) {
        throw new AlarisSmsException(String.format("can't match sar: [%s] to SAR_TEMPLATE: [%s]", sar, SAR_TEMPLATE));
      }
      String id = matcher.group("id");
      try {
        int parts = Integer.parseInt(matcher.group("parts"));
        int partNumber = Integer.parseInt(matcher.group("partNumber"));
        alarisSmsSar = new AlarisSms.Sar(id, parts, partNumber);
      } catch (NumberFormatException e) {
        throw new AlarisSmsException("Bad parts or partNumber parameter", e);
      }
    }
    AlarisSms alarisSms = new AlarisSms(uuid, ani, dnis, message, serviceType, longMessageMode, dataCoding, currentTime, alarisSmsSar, clientIp);
    alarisSms.setLastUpdateTime(currentTime);
    alarisSms.setStatus(AlarisSms.Status.ENROUTE);
    try {
      daoProvider.getAlarisSmsDAO().insertSms(alarisSms);
    } catch (DataModificationException e) {
      throw new AlarisSmsException("NO ROUTES", e);
    }

    alarisSmsService.sendAlarisSms(alarisSms);

    SMSEncoding messageEncoding = SMSCodec.getTextEncoding(alarisSms.getMessage());
    if (alarisSms.getMessage().length() > messageEncoding.getLength()) {
      throw new AlarisSmsException(String.format("Incorrect message length [%d], it must be lower or equal [%d], for encoding [%s]", alarisSms.getMessage().length(), messageEncoding.getLength(), messageEncoding));
    }
    return uuid;
  }

  public AlarisSms.Status getSmsStatus(final String smsId, final String username) {
    UUID smsUuid;
    try {
      smsUuid = UUID.fromString(smsId);
    } catch (IllegalArgumentException ignore) {
      return null;
    }
    AlarisSms alarisSms = daoProvider.getAlarisSmsDAO().getById(smsUuid);
    logger.debug("[{}] - found alarisSms: [{}]", this, alarisSms);
    if (alarisSms == null) {
      return null;
    } else if (!alarisSms.getStatus().equals(AlarisSms.Status.SENT)) {
      if(AlarisSms.Status.UNDELIV.equals(alarisSms.getStatus()) && ControlPropUtils.getInstance().getControlServerProperties().isAlternativeResponse()) {
        throw new AlarisSmsException("Bad Request");
      }
      return alarisSms.getStatus();
    } else {
      FakeSmsStatusStatistic fakeSmsStatusStatistic = fakeSmsStatusStatistics.get(username);
      if (fakeSmsStatusStatistic == null) {
        fakeSmsStatusStatistic = new FakeSmsStatusStatistic();
        fakeSmsStatusStatistics.put(username, fakeSmsStatusStatistic);
      }
      fakeSmsStatusStatistic.incTotalSmsStatus();
      AlarisMultiUser alarisMultiUser = ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsServerMultiUser().get(username);
      int res = (fakeSmsStatusStatistic.getTotalSmsStatus() * alarisMultiUser.getPercentageRealStatus()) / 100;
      if (res >= fakeSmsStatusStatistic.getTotalRealSmsStatus()) {
        fakeSmsStatusStatistic.incTotalRealSmsStatus();
        logger.debug("[{}] - return real status [{}] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, alarisSms.getStatus(), username, smsId, fakeSmsStatusStatistic);
        return alarisSms.getStatus();
      } else {
        logger.debug("[{}] - return fake status [DELIVRD] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, username, smsId, fakeSmsStatusStatistic);
        return AlarisSms.Status.DELIVRD;
      }
    }
  }

  public AlarisSmsManager setDaoProvider(final DAOProvider daoProvider) {
    this.daoProvider = daoProvider;
    return this;
  }

  public AlarisSmsManager setAlarisSmsService(final AlarisSmsService alarisSmsService) {
    this.alarisSmsService = alarisSmsService;
    return this;
  }

  public static AlarisSmsManager getInstance() {
    return instance;
  }

  @Override
  public String getName() {
    return AlarisSmsManager.class.getSimpleName();
  }

  @Override
  public boolean reload() {
    fakeSmsStatusStatistics.clear();
    logger.debug("[{}] - reloaded configuration, clean fakeSmsStatusStatistic", this);
    return true;
  }

  private class FakeSmsStatusStatistic {

    private int totalSmsStatus = 0;
    private int totalRealSmsStatus = 1;

    public int getTotalSmsStatus() {
      return totalSmsStatus;
    }

    public FakeSmsStatusStatistic setTotalSmsStatus(final int totalSmsStatus) {
      this.totalSmsStatus = totalSmsStatus;
      return this;
    }

    public FakeSmsStatusStatistic incTotalSmsStatus() {
      this.totalSmsStatus++;
      return this;
    }

    public int getTotalRealSmsStatus() {
      return totalRealSmsStatus;
    }

    public FakeSmsStatusStatistic setTotalRealSmsStatus(final int totalRealSmsStatus) {
      this.totalRealSmsStatus = totalRealSmsStatus;
      return this;
    }

    public FakeSmsStatusStatistic incTotalRealSmsStatus() {
      this.totalRealSmsStatus++;
      return this;
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
          "[totalSmsStatus:" + totalSmsStatus +
          " totalRealSmsStatus:" + totalRealSmsStatus +
          ']';
    }

  }

}
