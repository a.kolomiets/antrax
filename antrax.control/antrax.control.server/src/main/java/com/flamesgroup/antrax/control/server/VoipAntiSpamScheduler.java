/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.control.server.config.ControlServerConfig;
import com.flamesgroup.antrax.control.server.config.VoipAntiSpamConfig;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class VoipAntiSpamScheduler {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpamScheduler.class);

  private static final long ONCE_PER_DAY = 1000 * 60 * 60 * 24;
  private static final int HOUR_TIME = 24;
  private static final int MINUTES_TIME = 0;

  private final IVoipAntiSpamDAO voipAntiSpamDAO;
  private final VoipAntiSpamConfig voipAntiSpamConfig;
  private final VoipAntiSpam voipAntiSpam;
  private final Timer timer;

  public VoipAntiSpamScheduler(final DAOProvider daoProvider, final ControlServerConfig controlServerConfig) {
    Objects.requireNonNull(daoProvider, "daoProvider mustn't be null");
    Objects.requireNonNull(controlServerConfig, "controlServerConfig mustn't be null");
    this.voipAntiSpamDAO = daoProvider.getVoipAntiSpamDAO();
    this.voipAntiSpamConfig = controlServerConfig.getVoipAntiSpamConfig();
    voipAntiSpam = new VoipAntiSpam(daoProvider);
    timer = new Timer();
  }

  public void start() {
    timer.scheduleAtFixedRate(new RemindHistoryTask(), getHistoryStartTime(), ONCE_PER_DAY);
    timer.scheduleAtFixedRate(new RemindWhiteListTask(), getAnalyzeCDRStartTime(), voipAntiSpamConfig.getCdrAnalyzePeriod());
  }

  public void stop() {
    timer.cancel();
    timer.purge();
  }

  private class RemindHistoryTask extends TimerTask {

    @Override
    public void run() {
      VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
      if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
        return;
      }
      logger.info("[{}] - try to execute RemindHistoryTask", this);
      try {
        voipAntiSpamDAO.clearHistoryBlockNumbers();
        voipAntiSpamDAO.clearHistoryRoutingRequest();
      } catch (DataModificationException e) {
        logger.warn("[{}] - can't execute RemindHistoryTask", this, e);
      }
      logger.info("[{}] - executed RemindHistoryTask", this);
    }

  }

  class RemindWhiteListTask extends TimerTask {

    @Override
    public void run() {
      VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
      if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
        return;
      }

      for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
        AcdConfig acdConfig = grayListConfig.getAcdConfig();
        if (acdConfig == null) {
          continue;
        }

        logger.info("[{}] - try to execute RemindWhiteListTask", this);
        long period = System.currentTimeMillis() - voipAntiSpamConfig.getCdrAnalyzePeriod();
        voipAntiSpam.analyzeCdr(period, voipAntiSpamConfig.getCdrNormalAcd(), voipAntiSpamConfig.getCdrMaxNormalCallPerPeriod());
        logger.info("[{}] - executed RemindWhiteListTask", this);
        return;
      }
    }

  }

  private Date getHistoryStartTime() {
    Calendar tomorrow = new GregorianCalendar();
    tomorrow.add(Calendar.DATE, 0);
    Calendar result = new GregorianCalendar(
        tomorrow.get(Calendar.YEAR),
        tomorrow.get(Calendar.MONTH),
        tomorrow.get(Calendar.DATE),
        HOUR_TIME,
        MINUTES_TIME
    );
    return result.getTime();
  }

  private Date getAnalyzeCDRStartTime() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(calendar.getTimeInMillis() + voipAntiSpamConfig.getCdrAnalyzePeriod());
    return calendar.getTime();
  }

}
