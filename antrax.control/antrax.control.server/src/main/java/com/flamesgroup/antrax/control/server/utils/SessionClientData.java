/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.utils;

import com.flamesgroup.antrax.control.authorization.ClientUID;

public class SessionClientData {

  private final String userName;
  private final String userIP;
  private final String userGroup;
  private final ClientUID clientUID;

  public SessionClientData(final ClientUID clientUID, final String userName, final String groupName, final String ipAddress) {
    this.userName = userName;
    this.userGroup = groupName;
    this.userIP = ipAddress;
    this.clientUID = clientUID;
  }

  public ClientUID getClientUID() {
    return clientUID;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserIP() {
    return userIP;
  }

  public String getUserGroup() {
    return userGroup;
  }

}
