/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.activity.ISimHistoryLogger;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.simserver.ISimActivityLogger;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;

public class SimUnitConfigurator {

  private final ISimActivityLogger simActivityLogger;
  private final ISimpleConfigEditDAO simpleConfigEditDAO;
  private final IConfigViewDAO configViewDAO;
  private final ISimHistoryLogger simHistoryLogger;
  private final ScriptStateSaver scriptStateSaver;

  public SimUnitConfigurator(final ISimActivityLogger simActivityLogger, final ISimpleConfigEditDAO simpleConfigEditDAO, final IConfigViewDAO configViewDAO, final ISimHistoryLogger simHistoryLogger,
      final ScriptStateSaver scriptStateSaver) {
    this.simActivityLogger = simActivityLogger;
    this.simpleConfigEditDAO = simpleConfigEditDAO;
    this.simHistoryLogger = simHistoryLogger;
    this.scriptStateSaver = scriptStateSaver;
    this.configViewDAO = configViewDAO;
  }

  public void configure(final SimUnit simUnit) {
    simUnit.setSaver(scriptStateSaver);
    simUnit.setSimActivityLogger(simActivityLogger);
    simUnit.setSimHistoryLogger(simHistoryLogger);
    simUnit.setSimpleConfigEditDAO(simpleConfigEditDAO);
    simUnit.setConfigViewDAO(configViewDAO);
  }

}
