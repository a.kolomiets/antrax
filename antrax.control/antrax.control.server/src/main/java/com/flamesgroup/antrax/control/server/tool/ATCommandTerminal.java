/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.tool;

import static com.flamesgroup.rmi.RemoteServiceExporter.destroyAll;
import static com.flamesgroup.rmi.RemoteServiceExporter.export;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.flamesgroup.antrax.configuration.AntraxConfigurationHelper;
import com.flamesgroup.antrax.control.server.tool.parameter.ChannelUIDConverter;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannels;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannelsHandler;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannels;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannelsHandler;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IRawATSubChannel;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannelHandler;
import com.flamesgroup.device.gsmb.RawATSubChannel2ATSubChannelStackedAdapter;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.protocol.cmux.CMUXConnection;
import com.flamesgroup.device.protocol.cmux.ICMUXVirtualPort;
import com.flamesgroup.device.protocol.remotesim.IRemoteSimConnection;
import com.flamesgroup.device.protocol.remotesim.IRemoteSimConnectionHandler;
import com.flamesgroup.device.protocol.remotesim.RemoteSimConnection;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.sc.SCEmulatorSubChannelsJoiningException;
import com.flamesgroup.device.sc.SCReaderSubChannelsJoiningException;
import com.flamesgroup.device.sc.SCSubChannelsJoining;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.device.tool.ToolUncaughtExceptionHandler;
import com.flamesgroup.device.tool.parameter.CommonParameters;
import com.flamesgroup.rmi.RemoteExporter;
import com.flamesgroup.unit.ms.ITelitMobileStationTraceHandler;
import com.flamesgroup.unit.ms.RemoteSimConnection2SCEmulatorSubChannelAdapter;
import com.flamesgroup.unit.ms.TelitMobileStationCMUXVirtualPortTraceCollector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ATCommandTerminal {

  private static final int ERR_PARAMS = -1;
  private static final int ERR_EXCEPTION = -2;
  private static final int ERROR_TERMINAL_STOP_SIGNAL = -3;

  private static final Charset atCharset = Charset.forName("ISO-8859-1");

  private static final char CR = 0x0D;
  private static final char CTRLZ = 0x1A;
  private static final char ESC = 0x1B;

  private static final String EXIT_STRING = "EXIT";
  private static final String CTRLZ_STRING = "CTRLZ";
  private static final String ESC_STRING = "ESC";

  private static final RawATSubChannelsHandler rawATSubChannelsHandler = new RawATSubChannelsHandler();
  private static final SCReaderSubChannelsHandler scReaderSubChannelsHandler = new SCReaderSubChannelsHandler();
  private static final SCEmulatorSubChannelsHandler scEmulatorSubChannelsHandler = new SCEmulatorSubChannelsHandler();

  private static final Lock shutdownHookLock = new ReentrantLock();
  private static boolean shutdownHook = false;

  private static IRawATSubChannel rawATSubChannel;
  private static SCSubChannelsJoining scSubChannelsJoining;

  public static void main(final String[] args) throws Exception {
    final JCommander clp = new JCommander();

    clp.setProgramName(MethodHandles.lookup().lookupClass().getSimpleName());

    ATTerminalCommonParameters commonParameters = new ATTerminalCommonParameters();

    clp.addObject(commonParameters);

    Thread.setDefaultUncaughtExceptionHandler(new HardExitUncaughtExceptionHandler(commonParameters));

    if (args.length == 0) {
      extendedUsage(clp);
    } else {
      clp.parse(args);
      if (commonParameters.isHelp()) {
        extendedUsage(clp);
      } else {
        ChannelUID gsmChannelUid = commonParameters.getGsmChannelUID();
        ChannelUID simChannelUid = commonParameters.getSimChannelUID();
        String traceMask = commonParameters.getTraceMask();
        Path tracePath = commonParameters.getTracePath();
        if (EnumSet.of(DeviceType.GSMB, DeviceType.GSMB2).contains(gsmChannelUid.getDeviceUID().getDeviceType()) && (traceMask != null || tracePath != null)) {
          throw new ParameterException("Trace supported only for Telit module");
        }
        if ((traceMask != null && tracePath == null) || (traceMask == null && tracePath != null)) {
          throw new ParameterException("traceMask and tracePath must be set together");
        }

        main(gsmChannelUid, simChannelUid, traceMask, tracePath);
      }
    }
  }

  private static void main(final ChannelUID gsmChannelUid, final ChannelUID simChannelUid, final String traceMask, final Path tracePath) throws Exception {
    IRawATSubChannels rawATSubChannels;
    {
      rawATSubChannels = AntraxConfigurationHelper.getInstance().getGsmUnitManager().getRawATSubChannels(gsmChannelUid);

      if (rawATSubChannels == null) {
        System.err.println("Can't find RawATSubChannels for " + gsmChannelUid);
        System.exit(ERR_EXCEPTION);
      }
    }

    ISCReaderSubChannel scReaderSubChannel = null;
    if (simChannelUid != null) {
      ISCReaderSubChannels scReaderSubChannels = AntraxConfigurationHelper.getInstance().getSimUnitManager().getSCReaderSubChannels(simChannelUid);

      if (scReaderSubChannels == null) {
        System.err.println("Can't find SCReaderSubChannels for " + simChannelUid);
        System.exit(ERR_EXCEPTION);
      }

      scReaderSubChannel = new SCReaderSubChannelAdapter(simChannelUid, scReaderSubChannels);
    }

    ISCEmulatorSubChannel scEmulatorSubChannel = null;
    IRawATSubChannel rawATSubChannelLocal = new RawATSubChannelAdapter(gsmChannelUid, rawATSubChannels);
    if (EnumSet.of(DeviceType.GSMB3, DeviceType.GSMBOX4, DeviceType.GSMBOX8).contains(gsmChannelUid.getDeviceUID().getDeviceType())) {
      ITelitMobileStationTraceHandler traceFileWriterHandler;
      if (tracePath != null) {
        traceFileWriterHandler = new TelitMobileStationTraceFileWriterHandler(tracePath);
      } else {
        traceFileWriterHandler = null;
      }

      RawATSubChannel2CMUXVirtualPortAndRemoteSimConnectionAdapter adapter = new RawATSubChannel2CMUXVirtualPortAndRemoteSimConnectionAdapter(rawATSubChannelLocal, traceMask, traceFileWriterHandler);
      rawATSubChannelLocal = adapter;
      if (scReaderSubChannel != null) {
        scEmulatorSubChannel = new RemoteSimConnection2SCEmulatorSubChannelAdapter(adapter);
      }
    } else if (EnumSet.of(DeviceType.GSMB, DeviceType.GSMB2).contains(gsmChannelUid.getDeviceUID().getDeviceType())) {
      if (scReaderSubChannel != null) {
        ISCEmulatorSubChannels scEmulatorSubChannels = AntraxConfigurationHelper.getInstance().getGsmUnitManager().getSCEmulatorSubChannels(gsmChannelUid);

        if (scEmulatorSubChannels == null) {
          System.err.println("Can't find SCEmulatorSubChannels for " + gsmChannelUid);
          System.exit(ERR_EXCEPTION);
        }

        scEmulatorSubChannel = new SCEmulatorSubChannelAdapter(gsmChannelUid, scEmulatorSubChannels);
      }
    } else {
      System.err.println("Illegal device type for gsm channel " + gsmChannelUid);
      System.exit(ERR_PARAMS);
    }

    export(new RemoteExporter.Builder(rawATSubChannelsHandler, IRawATSubChannelsHandler.class).build());
    if (scReaderSubChannel != null) {
      export(new RemoteExporter.Builder(scReaderSubChannelsHandler, ISCReaderSubChannelsHandler.class).build());
      if (EnumSet.of(DeviceType.GSMB, DeviceType.GSMB2).contains(gsmChannelUid.getDeviceUID().getDeviceType())) {
        export(new RemoteExporter.Builder(scEmulatorSubChannelsHandler, ISCEmulatorSubChannelsHandler.class).build());
      }
    }

    addShutdownHook();

    shutdownHookLock.lock();
    try {
      if (shutdownHook) {
        return;
      }

      rawATSubChannelLocal.start((ByteBuffer buffer) -> System.out.print(atCharset.decode(buffer)));
      System.out.println("AT Terminal on " + gsmChannelUid + (simChannelUid == null ? "" : "and " + simChannelUid));
      System.out.println("(special strings: \"" + EXIT_STRING + "\" to exit; \"" + CTRLZ_STRING + "\" and \"" + ESC_STRING + "\" at the end replaced with proper char)");

      SCSubChannelsJoining scSubChannelsJoiningLocal = null;
      if (scReaderSubChannel != null) {
        scSubChannelsJoiningLocal = new SCSubChannelsJoining(scReaderSubChannel, scEmulatorSubChannel);
        try {
          scSubChannelsJoiningLocal.join();
        } catch (SCReaderSubChannelsJoiningException | SCEmulatorSubChannelsJoiningException | InterruptedException e) {
          try {
            rawATSubChannelLocal.stop();
          } catch (ChannelException | InterruptedException ex) {
            ex.addSuppressed(e);
            throw ex;
          }
          throw e;
        }
      }

      rawATSubChannel = rawATSubChannelLocal;
      scSubChannelsJoining = scSubChannelsJoiningLocal;
    } finally {
      shutdownHookLock.unlock();
    }

    while (true) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      String command;
      command = reader.readLine();

      // After pressing 'Ctrl + C + C', readLine() method returns null
      if (command == null) {
        System.exit(ERROR_TERMINAL_STOP_SIGNAL);
      }

      if (command.isEmpty()) {
        continue;
      } else if (command.toUpperCase().equals(EXIT_STRING)) {
        System.exit(0);
      } else if (command.toUpperCase().endsWith(CTRLZ_STRING)) {
        command = command.toUpperCase().replace(CTRLZ_STRING, "") + CTRLZ;
      } else if (command.toUpperCase().endsWith(ESC_STRING)) {
        command = command.toUpperCase().replace(ESC_STRING, "") + ESC;
      }

      rawATSubChannelLocal.writeRawATData(atCharset.encode(command + CR));
    }
  }

  private static void addShutdownHook() {
    Runtime.getRuntime().addShutdownHook(new Thread("ShutdownHook") {

      @Override
      public void run() {
        shutdownHookLock.lock();
        try {
          shutdownHook = true;

          if (scSubChannelsJoining != null) {
            try {
              scSubChannelsJoining.disjoin();
            } catch (SCReaderSubChannelsJoiningException | SCEmulatorSubChannelsJoiningException | InterruptedException ignored) {
              System.out.println("Can't disjoin from SCSubChannelsJoining, will terminate");
            }
          }
          if (rawATSubChannel != null) {
            try {
              rawATSubChannel.stop();
            } catch (ChannelException | InterruptedException ignored) {
              System.out.println("Can't stop RawATSubChannel, will terminate");
            }
          }
        } finally {
          shutdownHookLock.unlock();
        }

        destroyAll();
      }
    });
  }

  private static void extendedUsage(final JCommander clp) {
    StringBuilder sb = new StringBuilder();
    clp.usage(sb);
    sb.append("\n");
    sb.append("Channel UID template: <device type>-<device number>:<channel number>\n");
    sb.append("For example: ATCommandTerminal GSMB2-100:0 SIMBOX120-0:0\n");
    JCommander.getConsole().println(sb.toString());
  }

  private static class RawATSubChannelAdapter implements IRawATSubChannel {

    private final ChannelUID channel;
    private final IRawATSubChannels rawATSubChannels;

    public RawATSubChannelAdapter(final ChannelUID channel, final IRawATSubChannels rawATSubChannels) {
      this.channel = channel;
      this.rawATSubChannels = rawATSubChannels;
    }

    @Override
    public void start(final IRawATSubChannelHandler atRawSubChannelHandler) throws ChannelException, InterruptedException {
      rawATSubChannelsHandler.putRawATSubChannelHandler(channel, atRawSubChannelHandler);
      try {
        rawATSubChannels.start(channel, rawATSubChannelsHandler);
      } catch (RemoteException | IllegalChannelException e) {
        rawATSubChannelsHandler.removeRawATSubChannelHandler(channel);
        throw new ChannelException(e);
      }
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      try {
        rawATSubChannels.stop(channel);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      } finally {
        rawATSubChannelsHandler.removeRawATSubChannelHandler(channel);
      }
    }

    @Override
    public void writeRawATData(final ByteBuffer atDataBuffer) throws ChannelException, InterruptedException {
      try {
        rawATSubChannels.writeRawATData(channel, toByteArray(atDataBuffer));
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      }
    }

    private byte[] toByteArray(final ByteBuffer atDataBuffer) {
      byte[] bytes = new byte[atDataBuffer.remaining()];
      atDataBuffer.get(bytes);
      return bytes;
    }

  }

  private static class SCReaderSubChannelAdapter implements ISCReaderSubChannel {

    private final ChannelUID channel;
    private final ISCReaderSubChannels scReaderSubChannels;

    public SCReaderSubChannelAdapter(final ChannelUID channel, final ISCReaderSubChannels scReaderSubChannels) {
      this.channel = channel;
      this.scReaderSubChannels = scReaderSubChannels;
    }

    @Override
    public ATR start(final ISCReaderSubChannelHandler scReaderSubChannelHandler) throws ChannelException, InterruptedException {
      ATR atr;
      try {
        atr = scReaderSubChannels.start(channel, scReaderSubChannelsHandler);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      }

      scReaderSubChannelsHandler.putSCReaderSubChannelHandler(channel, scReaderSubChannelHandler);
      return atr;
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      try {
        scReaderSubChannels.stop(channel);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      } finally {
        scReaderSubChannelsHandler.removeSCReaderSubChannelHandler(channel);
      }
    }

    @Override
    public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
      try {
        scReaderSubChannels.writeAPDUCommand(channel, command);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      }
    }

    @Override
    public SCReaderState getState() throws ChannelException, InterruptedException {
      throw new UnsupportedOperationException("Not supported");
    }

  }

  private static class SCEmulatorSubChannelAdapter implements ISCEmulatorSubChannel {

    private final ChannelUID channel;
    private final ISCEmulatorSubChannels scEmulatorSubChannels;

    public SCEmulatorSubChannelAdapter(final ChannelUID channel, final ISCEmulatorSubChannels scEmulatorSubChannels) {
      this.channel = channel;
      this.scEmulatorSubChannels = scEmulatorSubChannels;
    }

    @Override
    public void start(final ATR atr, final ISCEmulatorSubChannelHandler scEmulatorSubChannelHandler) throws ChannelException, InterruptedException {
      try {
        scEmulatorSubChannels.start(channel, atr, scEmulatorSubChannelsHandler);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      }
      scEmulatorSubChannelsHandler.putSCEmulatorSubChannelHandler(channel, scEmulatorSubChannelHandler);
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      try {
        scEmulatorSubChannels.stop(channel);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      } finally {
        scEmulatorSubChannelsHandler.removeSCEmulatorSubChannelHandler(channel);
      }
    }

    @Override
    public void writeAPDUResponse(final APDUResponse response) throws ChannelException, InterruptedException {
      try {
        scEmulatorSubChannels.writeAPDUResponse(channel, response);
      } catch (RemoteException | IllegalChannelException e) {
        throw new ChannelException(e);
      }
    }

    @Override
    public SCEmulatorState getState() throws ChannelException, InterruptedException {
      throw new UnsupportedOperationException("Not supported");
    }

  }

  private static class RawATSubChannel2CMUXVirtualPortAndRemoteSimConnectionAdapter implements IRawATSubChannel, IRemoteSimConnection {

    private final IRawATSubChannel rawATSubChannelDelegate;
    private final String traceMask;
    private final ITelitMobileStationTraceHandler telitTraceHandler;

    private IRawATSubChannel rawATSubChannel;
    private CMUXConnection cmuxConnection;
    private ICMUXVirtualPort atCMUXVirtualPort;
    private IRemoteSimConnection remoteSimConnection;
    private TelitMobileStationCMUXVirtualPortTraceCollector traceCollector;

    public RawATSubChannel2CMUXVirtualPortAndRemoteSimConnectionAdapter(final IRawATSubChannel rawATSubChannelDelegate, final String traceMask,
        final ITelitMobileStationTraceHandler traceHandler) {
      this.rawATSubChannelDelegate = rawATSubChannelDelegate;
      this.traceMask = traceMask;
      this.telitTraceHandler = traceHandler;
    }

    @Override
    public void start(final IRawATSubChannelHandler atRawSubChannelHandler) throws ChannelException, InterruptedException {
      rawATSubChannel = new RawATSubChannel2ATSubChannelStackedAdapter(rawATSubChannelDelegate);
      rawATSubChannel.start(atRawSubChannelHandler);

      init();

      cmuxConnection = new CMUXConnection(rawATSubChannel, 10000);
      try {
        cmuxConnection.connect();

        atCMUXVirtualPort = cmuxConnection.getVirtualPorts().get(0);
        traceCollector = new TelitMobileStationCMUXVirtualPortTraceCollector(cmuxConnection.getVirtualPorts().get(3)); // in CMUX configuration Trace Access point connect to VC4 (COM6)

        try {
          atCMUXVirtualPort.open((ByteBuffer buffer) -> System.out.print(atCharset.decode(buffer)));
          traceCollector.start(telitTraceHandler);
        } catch (ChannelException | InterruptedException e) {
          try {
            if (atCMUXVirtualPort.isOpen()) {
              atCMUXVirtualPort.close();
            }
            cmuxConnection.disconnect();
          } catch (ChannelException | InterruptedException ex) {
            ex.addSuppressed(e);
            throw ex;
          }
          throw e;
        }
      } catch (ChannelException | InterruptedException e) {
        try {
          rawATSubChannel.stop();
        } catch (ChannelException | InterruptedException ex) {
          ex.addSuppressed(e);
          throw ex;
        }
        throw e;
      }
    }

    @Override
    public void stop() throws ChannelException, InterruptedException {
      Exception le = null;
      try {
        atCMUXVirtualPort.close();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        traceCollector.stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        cmuxConnection.disconnect();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      try {
        rawATSubChannel.stop();
      } catch (ChannelException | InterruptedException e) {
        le = e;
      }
      if (le != null) {
        if (le instanceof ChannelException) {
          throw (ChannelException) le;
        } else {
          throw (InterruptedException) le;
        }
      }
    }

    @Override
    public void writeRawATData(final ByteBuffer atDataBuffer) throws ChannelException, InterruptedException {
      atCMUXVirtualPort.sendData(atDataBuffer);
    }

    private void init() throws ChannelException, InterruptedException {
      try {
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT+IPR=115200" + CR));
        Thread.sleep(2000);
        if (traceMask != null) {
          rawATSubChannel.writeRawATData(atCharset.encode("AT#RTDE=" + traceMask + CR));
          Thread.sleep(2000);
        }
        rawATSubChannel.writeRawATData(atCharset.encode("AT#CPUMODE=3" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("ATV1" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT&K3" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT&D2" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT#BCCHLOCK=1024" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT#CMUXMODE=5" + CR));
        Thread.sleep(2000);
        rawATSubChannel.writeRawATData(atCharset.encode("AT+CMUX=0" + CR));
        Thread.sleep(2000);
      } catch (ChannelException | InterruptedException e) {
        try {
          rawATSubChannel.stop();
        } catch (ChannelException | InterruptedException ex) {
          ex.addSuppressed(e);
          throw ex;
        }
        throw e;
      }
    }

    @Override
    public boolean isConnected() {
      return remoteSimConnection != null && remoteSimConnection.isConnected();
    }

    @Override
    public void connect(final ATR atr, final IRemoteSimConnectionHandler remoteSimChannelHandler) throws ChannelException, InterruptedException {
      remoteSimConnection = new RemoteSimConnection(cmuxConnection.getVirtualPorts().get(1));
      remoteSimConnection.connect(atr, remoteSimChannelHandler);

      writeRawATData(atCharset.encode("AT#RSEN=1,1,0,2,0,0" + CR));
    }

    @Override
    public void disconnect() throws ChannelException, InterruptedException {
      remoteSimConnection.disconnect();
      remoteSimConnection = null;
    }

    @Override
    public void transferAPDUResponse(final APDUResponse apduResponse) throws ChannelException, InterruptedException {
      remoteSimConnection.transferAPDUResponse(apduResponse);
    }

  }

  public static class ATTerminalCommonParameters extends CommonParameters {

    private static final String gsmChannelUidParameterName = "GSM channel UID";
    private static final String simChannelUidParameterName = "SIM channel UID";

    @Parameter(description = "<" + gsmChannelUidParameterName + "> [<" + simChannelUidParameterName + ">]", required = true, converter = ChannelUIDConverter.class)
    private final List<ChannelUID> channelUIDs = new ArrayList<>();

    @Parameter(names = "--traceMask", description = "Mask for trace (used in AT#RTDE)")
    private String traceMask;

    @Parameter(names = "--tracePath", description = "Path to trace file")
    private Path tracePath;

    public ChannelUID getGsmChannelUID() {
      if (channelUIDs.size() >= 1) {
        return channelUIDs.get(0);
      } else {
        throw new ParameterException("<" + gsmChannelUidParameterName + "> parameter specify incorrectly");
      }
    }

    public ChannelUID getSimChannelUID() {
      if (channelUIDs.size() == 2) {
        return channelUIDs.get(1);
      } else if (channelUIDs.size() == 1) {
        return null;
      } else {
        throw new ParameterException("<" + simChannelUidParameterName + "> parameter specify incorrectly");
      }
    }

    public String getTraceMask() {
      return traceMask;
    }

    public Path getTracePath() {
      return tracePath;
    }

  }

  private static class HardExitUncaughtExceptionHandler extends ToolUncaughtExceptionHandler {

    public HardExitUncaughtExceptionHandler(final ATTerminalCommonParameters commonParameters) {
      super(commonParameters);
    }

    @Override
    public void uncaughtException(final Thread t, final Throwable e) {
      super.uncaughtException(t, e);
      System.exit(ERR_EXCEPTION); // need hard exit because RMI non daemon threads is still running
    }

  }

  private static class TelitMobileStationTraceFileWriterHandler implements ITelitMobileStationTraceHandler {

    private final Path tracePath;
    private ByteChannel traceFileByteChannel;

    private TelitMobileStationTraceFileWriterHandler(final Path tracePath) {
      this.tracePath = tracePath;
    }

    @Override
    public void handleOpen() {
      try {
        traceFileByteChannel = Files.newByteChannel(tracePath, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public void handleClose() {
      try {
        traceFileByteChannel.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      } finally {
        traceFileByteChannel = null;
      }
    }

    @Override
    public void handleTraceData(final ByteBuffer traceData) {
      while (traceData.hasRemaining()) {
        try {
          traceFileByteChannel.write(traceData);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }

  }

}
