/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rest.alaris.sms;

import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SmsStatistic;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.ControlBeanModel;
import com.flamesgroup.antrax.control.server.PingServersManager;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SmsAllocationAlgorithm;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.properties.IReloadListener;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AlarisSmsService implements IConfigurationHandler, IReloadListener {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsService.class);

  private final ControlBeanModel controlBeanModel;
  private final PingServersManager pingServersManager;
  private final DAOProvider daoProvider;

  private final ThreadPoolExecutor threadPoolExecutor = new WaitThreadPoolExecutor(5, 100, 30, TimeUnit.SECONDS, new SynchronousQueue<>(), r -> new Thread(r, "AlarisSendSmsExecutor"));
  private final Map<String, Map<String, List<AlarisSms>>> multiPartAlarisSms = new ConcurrentHashMap<>();

  private final AtomicInteger countOfSendingSms = new AtomicInteger();
  private final AtomicInteger countOfSendSmsToday = new AtomicInteger();
  private final ConcurrentMap<String, Integer> countOfSendingSmsTodayByIp = new ConcurrentHashMap<>();
  private final ConcurrentMap<String, Integer> countOfSendSmsTodayByIp = new ConcurrentHashMap<>();
  private final Timer timer = new Timer();

  private List<IServerData> serverData = new ArrayList<>();
  private final Lock routeLock = new ReentrantLock();

  private final AtomicReference<SmsAllocationAlgorithm> smsAllocationAlgorithm = new AtomicReference<>();

  public AlarisSmsService(final ControlBeanModel controlBeanModel, final PingServersManager pingServersManager, final DAOProvider daoProvider) {
    this.controlBeanModel = controlBeanModel;
    this.pingServersManager = pingServersManager;
    this.daoProvider = daoProvider;
  }

  public void sendAlarisSms(final AlarisSms alarisSms) {
    if (alarisSms.getSar() == null) {
      SMSEncoding messageEncoding = SMSCodec.getTextEncoding(alarisSms.getMessage());
      if (alarisSms.getMessage().length() <= messageEncoding.getLength()) {
        threadPoolExecutor.execute(new ProcessAlarisSms(Collections.singletonList(alarisSms)));
      } else {
        alarisSms.setLastUpdateTime(new Date());
        alarisSms.setStatus(AlarisSms.Status.REJECTD);
        logger.info("[{}] - incorrect message length [{}], it must be lower or equal [{}], for encoding [{}]. Skip this message [{}]", this, alarisSms.getMessage().length(),
            messageEncoding.getLength(), messageEncoding, alarisSms);
        try {
          daoProvider.getAlarisSmsDAO().updateSms(alarisSms);
        } catch (DataModificationException e) {
          logger.warn("[{}] - can't update status of alaris sms: [{}]", this, alarisSms, e);
        }
      }
    } else {
      Map<String, List<AlarisSms>> serverSmses = multiPartAlarisSms.get(String.valueOf(alarisSms.getClientIp()));
      if (serverSmses == null) {
        serverSmses = new HashMap<>();
      }
      List<AlarisSms> alarisSmses = serverSmses.get(alarisSms.getMultiPartSmsKey());
      if (alarisSmses == null) {
        alarisSmses = new ArrayList<>();
      }
      alarisSmses.add(alarisSms);
      if (alarisSmses.size() != alarisSms.getSar().getParts()) {
        serverSmses.put(alarisSms.getMultiPartSmsKey(), alarisSmses);
      } else {
        serverSmses.remove(alarisSms.getMultiPartSmsKey());
        boolean badEncoding = false;
        for (AlarisSms sms : alarisSmses) {
          SMSEncoding messageEncoding = SMSCodec.getTextEncoding(sms.getMessage());
          if (sms.getMessage().length() > messageEncoding.getLength()) {
            logger.info("[{}] - incorrect message length [{}], it must be lower or equal [{}], for encoding [{}]. Skip this message [{}]", this, sms.getMessage().length(),
                messageEncoding.getLength(), messageEncoding, sms);
            badEncoding = true;
          }
        }
        if (!badEncoding) {
          threadPoolExecutor.execute(new ProcessAlarisSms(alarisSmses));
        } else {
          for (AlarisSms sms : alarisSmses) {
            sms.setLastUpdateTime(new Date());
            sms.setStatus(AlarisSms.Status.REJECTD);
            try {
              daoProvider.getAlarisSmsDAO().updateSms(sms);
            } catch (DataModificationException e) {
              logger.warn("[{}] - can't update status of alaris sms: [{}]", this, sms, e);
            }
          }
        }
      }
      multiPartAlarisSms.put(String.valueOf(alarisSms.getClientIp()), serverSmses);
    }
  }

  public boolean canSendAlarisSms(final String clientIp) {
    if (countOfSendingSms.get() >= ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsSenderBuffer()) {
      logger.info("[{}] - NO ROUTES, overflow senderBuffer, current countOfSendingSms: [{}], smsSenderBuffer: [{}] ", this, countOfSendingSms.get(),
          ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsSenderBuffer());
      return false;
    }
    if (countOfSendSmsToday.get() >= ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDay()) {
      logger.info("[{}] - NO ROUTES, sender sms limits for today, current countOfSendSmsToday: [{}], limitPearDay: [{}] ", this, countOfSendSmsToday.get(),
          ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDay());
      return false;
    }
    Integer countOfSendSmsForClient = countOfSendingSmsTodayByIp.get(clientIp);
    Integer limitForClient = ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDayByIp().get(clientIp);

    if (ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsAllowLimitPearDayByIp()) {
      if (countOfSendSmsForClient == null || limitForClient == null) {
        logger.info("[{}] - NO ROUTES, client IP: [{}], denied send sms from this client IP. For allow send sms from this client, configure it at properties", this, clientIp);
        return false;
      }
      if (countOfSendSmsForClient >= limitForClient) {
        logger.info("[{}] - NO ROUTES, sender sms limits for today, current countOfSendSmsToday: [{}], limitPearDay: [{}] for clientIP: [{}]", this, countOfSendSmsForClient, limitForClient, clientIp);
        return false;
      }
    }
    return true;
  }

  public void start() {
    daoProvider.getAlarisSmsDAO().listSmsAtRouting().forEach(this::sendAlarisSms);
    countOfSendSmsToday.set(daoProvider.getAlarisSmsDAO().getCountOfSendSmsToday(null));
    for (String clientIp : ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDayByIp().keySet()) {
      int countOfSendSmsToday = daoProvider.getAlarisSmsDAO().getCountOfSendSmsToday(clientIp);
      countOfSendingSmsTodayByIp.put(clientIp, countOfSendSmsToday);
      countOfSendSmsTodayByIp.put(clientIp, countOfSendSmsToday);
    }
    timer.scheduleAtFixedRate(new ResetCountOfSendSmsToday(), Date.from(LocalDateTime.now().withHour(23).withMinute(59).atZone(ZoneId.systemDefault()).toInstant()), TimeUnit.DAYS.toMillis(1));
  }

  public void stop() {
    threadPoolExecutor.shutdown();
    timer.cancel();
    timer.purge();
  }

  @Override
  public void reconfigure(final IConfigViewDAO configViewDAO) {
    RouteConfig routeConfig = configViewDAO.getRouteConfig();
    if (routeConfig == null) {
      logger.warn("[{}] - no route config found for servers", this);
      return;
    }

    smsAllocationAlgorithm.set(routeConfig.getSmsAllocationAlgorithm());
    routeLock.lock();
    try {
      serverData = configViewDAO.listServers().stream().filter(e -> e.isVoiceServerEnabled() && e.getVoiceServerConfig().getSmsRouteConfig() != null).collect(Collectors.toList());
      logger.info("[{}] - servers for sms routing: {}", this, serverData.stream().map(IServerData::getName).collect(Collectors.toList()));
    } finally {
      routeLock.unlock();
    }
  }

  @Override
  public String getName() {
    return AlarisSmsService.class.getSimpleName();
  }

  @Override
  public boolean reload() {
    for (String s : ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDayByIp().keySet()) {
      countOfSendingSmsTodayByIp.putIfAbsent(s, 0);
      countOfSendSmsTodayByIp.putIfAbsent(s, 0);
    }
    return true;
  }

  public List<SmsStatistic> getSmsStatistic() {
    List<SmsStatistic> smsStatistics = new ArrayList<>();
    Map<String, Integer> alarisSmsLimitPearDayByIp = ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDayByIp();
    for (Map.Entry<String, Integer> ipAddressLimit : alarisSmsLimitPearDayByIp.entrySet()) {
      smsStatistics.add(new SmsStatistic(ipAddressLimit.getKey(), countOfSendSmsTodayByIp.get(ipAddressLimit.getKey()), ipAddressLimit.getValue()));
    }
    return smsStatistics;
  }

  private class ProcessAlarisSms implements Runnable {

    private final Long WAITING_TIME = TimeUnit.SECONDS.toMillis(5);

    private final List<AlarisSms> alarisSmses;
    private final UUID objectUuid = UUID.randomUUID();

    public ProcessAlarisSms(final List<AlarisSms> alarisSmses) {
      this.alarisSmses = alarisSmses;
    }

    @Override
    public void run() {
      int countIteration = 1;
      alarisSmses.sort(Comparator.comparingInt(o -> o.getSar().getPartNumber()));
      VsSmsStatus vsSmsStatus = new VsSmsStatus();
      countOfSendingSms.addAndGet(alarisSmses.size());
      countOfSendSmsToday.addAndGet(alarisSmses.size());
      countOfSendingSmsTodayByIp.compute(alarisSmses.get(0).getClientIp(), (s, i) -> i == null ? alarisSmses.size() : i + alarisSmses.size());
      while (true) {
        try {
          logger.info("[{}] - try send alaris sms: [{}], objectUuid: [{}]", this, alarisSmses, objectUuid);
          List<IServerData> localServerData;
          routeLock.lock();
          try {
            localServerData = serverData;
          } finally {
            routeLock.unlock();
          }
          List<String> workingServers = pingServersManager.getVoiceServersStatus().entrySet().stream()
              .filter(svs -> svs.getValue().getShortInfo().getStatus() == ServerStatus.STARTED).map(svs -> svs.getKey().getName())
              .collect(Collectors.toList());

          List<IServerData> routerServerData = localServerData.stream().filter(createFilterPredicate(workingServers)).collect(Collectors.toList());
          switch (smsAllocationAlgorithm.get()) {
            case RANDOM:
              Collections.shuffle(routerServerData);
              break;
            case UNIFORMLY_TOTAL:
              Collections.sort(routerServerData, createFactorComparator(this::countUniformlyTotalFactor));
              break;
            case UNIFORMLY_SUCCESS:
              Collections.sort(routerServerData, createFactorComparator(this::countUniformlySuccessFactor));
              break;
          }

          logger.debug("[{}] - routerServerData: {}", this, routerServerData);

          for (IServerData serverData : routerServerData) {
            try {
              logger.debug("[{}] - try send alaris sms: [{}], via server: [{}], objectUuid: [{}]", this, alarisSmses, serverData.getName(), objectUuid);
              vsSmsStatus = controlBeanModel.sendSMS(serverData, alarisSmses, objectUuid);

              for (int i = 0; i < 100; i++) {
                if (vsSmsStatus.getStatus() != VsSmsStatus.Status.SENDING) {
                  break;
                }
                logger.debug("[{}] - waiting SMS will be sent", this);
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                vsSmsStatus = controlBeanModel.getSMSStatus(serverData, objectUuid);
              }

              if (vsSmsStatus.getStatus() == VsSmsStatus.Status.SENT) {
                logger.debug("[{}] - sent alaris sms: [{}], via server: [{}]", this, alarisSmses, serverData.getName());
                controlBeanModel.removeSMSStatus(serverData, objectUuid);
                break;
              }
              logger.debug("[{}] - last SMS status is: [{}], with message: [{}]", this, vsSmsStatus.getStatus(), vsSmsStatus.getMessage());
            } catch (RemoteAccessException e) {
              logger.warn("[{}] - connection was lost with server: [{}]", this, serverData.getName());
              logger.debug("[{}] - ", this, e);
            }
          }
          if (vsSmsStatus.getStatus() == VsSmsStatus.Status.SENT) {
            for (AlarisSms alarisSms : alarisSmses) {
              alarisSms.setLastUpdateTime(new Date());
              alarisSms.setStatus(AlarisSms.Status.SENT);
              alarisSms.setSmsId(vsSmsStatus.getUuid());
              try {
                daoProvider.getAlarisSmsDAO().updateSms(alarisSms);
                logger.info("[{}] - send alaris sms: [{}]", this, alarisSms);
              } catch (DataModificationException e) {
                logger.warn("[{}] - can't update status of alaris sms: [{}]", this, alarisSms, e);
              }
            }
            countOfSendSmsTodayByIp.compute(alarisSmses.get(0).getClientIp(), (s, i) -> i + alarisSmses.size());
            break;
          } else {
            if (countIteration >= ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsResentAmount()) {
              for (AlarisSms alarisSms : alarisSmses) {
                alarisSms.setLastUpdateTime(new Date());
                alarisSms.setStatus(AlarisSms.Status.UNDELIV);
                try {
                  daoProvider.getAlarisSmsDAO().updateSms(alarisSms);
                  logger.info("[{}] - can't send alaris sms: [{}], after trying [{}] times", this, alarisSms, countIteration);
                } catch (DataModificationException e) {
                  logger.warn("[{}] - can't update status of alaris sms: [{}]", this, alarisSms, e);
                }
              }
              countOfSendSmsToday.getAndAccumulate(alarisSmses.size(), (left, right) -> left - right);
              countOfSendingSmsTodayByIp.compute(alarisSmses.get(0).getClientIp(), (s, i) -> i == null ? 0 : i - alarisSmses.size());
              break;
            }
            countIteration++;
            logger.info("[{}] - can't send alaris sms: [{}], NO ROUTE. Try sent at next iteration", this, alarisSmses);
            Thread.sleep(WAITING_TIME);
          }
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        }
      }
      countOfSendingSms.updateAndGet(prev -> prev - alarisSmses.size());
    }

    private Comparator<IServerData> createFactorComparator(final Function<IServerData, Integer> factorFunction) {
      return (o1, o2) -> {
        int priority1 = o1.getVoiceServerConfig().getSmsRouteConfig().getPriority();
        int priority2 = o2.getVoiceServerConfig().getSmsRouteConfig().getPriority();

        int cmp = Integer.compare(priority2, priority1); // sort descending
        if (cmp == 0) {
          cmp = Integer.compare(priority1 + factorFunction.apply(o1), priority2 + factorFunction.apply(o2));
        }
        return cmp;
      };
    }

    private int countUniformlyTotalFactor(final IServerData serverData) {
      return pingServersManager.getVoiceServerStatus(new Server(serverData.getName(), ServerType.VOICE_SERVER)).getChannelsInfo().stream()
          .mapToInt(IVoiceServerChannelsInfo::getTotalOutgoingSmsCount).sum();
    }

    private int countUniformlySuccessFactor(final IServerData serverData) {
      return pingServersManager.getVoiceServerStatus(new Server(serverData.getName(), ServerType.VOICE_SERVER)).getChannelsInfo().stream()
          .mapToInt(IVoiceServerChannelsInfo::getSuccessOutgoingSmsCount).sum();
    }

    private Predicate<IServerData> createFilterPredicate(final List<String> workingServers) {
      return r -> workingServers.contains(r.getName()) && alarisSmses.get(0).getDnis().matches(r.getVoiceServerConfig().getSmsRouteConfig().getDnisRegex());
    }

  }

  private class ResetCountOfSendSmsToday extends TimerTask {

    @Override
    public void run() {
      countOfSendSmsToday.set(0);
      countOfSendingSmsTodayByIp.clear();
      countOfSendSmsTodayByIp.clear();
      for (String s : ControlPropUtils.getInstance().getControlServerProperties().getAlarisSmsLimitPearDayByIp().keySet()) {
        countOfSendingSmsTodayByIp.put(s, 0);
        countOfSendSmsTodayByIp.put(s, 0);
      }
    }

  }

}
