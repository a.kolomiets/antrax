/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ConfigurationBean;
import com.flamesgroup.antrax.control.communication.GsmGroupEditInfo;
import com.flamesgroup.antrax.control.communication.GsmGroupEditInfoImpl;
import com.flamesgroup.antrax.control.communication.NoSuchSimException;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.server.utils.EditTransactionManager;
import com.flamesgroup.antrax.control.simserver.ChangeGroupFailedError;
import com.flamesgroup.antrax.control.simserver.ISimUnitManager;
import com.flamesgroup.antrax.control.simserver.NoSuchSimError;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.plugins.core.ScriptsHelper;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.dao.IConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigurationBeanImpl implements ConfigurationBean {

  private static final Logger logger = LoggerFactory.getLogger(ConfigurationBeanImpl.class);

  private final EditTransactionManager transactionManager;
  private final DAOProvider daoProvider;
  private final PluginsStoreCache pluginsStoreCache;
  private final ControlBeanModel controlBeanModel;
  private final List<IConfigurationHandler> configurationHandlers;
  private final ISimUnitManager simUnitManager;

  public ConfigurationBeanImpl(final EditTransactionManager transactionManager, final DAOProvider daoProvider, final List<IConfigurationHandler> configurationHandlers,
      final ISimUnitManager simUnitManager, final ControlBeanModel controlBeanModel) {
    this.transactionManager = transactionManager;
    this.daoProvider = daoProvider;
    this.configurationHandlers = configurationHandlers;
    this.simUnitManager = simUnitManager;
    pluginsStoreCache = new PluginsStoreCache(daoProvider);
    this.controlBeanModel = controlBeanModel;
    try {
      pluginsStoreCache.getPluginsStore(-1);
    } catch (Exception e) {
      logger.warn("[{}] - failed to analyze plugins store", this, e);
    }
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return true;
  }

  //
  // Edit transaction
  //

  @Override
  public int startEditTransaction(final ClientUID client) throws NotPermittedException, TransactionException {
    return transactionManager.startEditTransaction(client);
  }

  @Override
  public void commitEditTransaction(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException {
    transactionManager.commitEditTransaction(client, transactionId);

    IConfigViewDAO configViewDAO = daoProvider.getConfigViewDAO();

    for (IConfigurationHandler configurationHandler : configurationHandlers) {
      configurationHandler.reconfigure(configViewDAO);
    }
  }

  @Override
  public void rollbackEditTransaction(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException {
    transactionManager.rollbackEditTransaction(client, transactionId);
  }

  @Override
  public void retainEditTransaction(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException {
    transactionManager.retainEditTransaction(client, transactionId);

  }

  //
  // GSM group CRUD methods
  //

  @Override
  public GSMGroup[] listGSMGroups(final ClientUID client) throws NotPermittedException, StorageException {
    IConfigViewDAO cfgViewDAO = daoProvider.getConfigViewDAO();
    return cfgViewDAO.listGSMGroups();
  }

  @Override
  public String[] listGsmGroupNames(final ClientUID client) throws NotPermittedException, TransactionException, StorageException {
    return daoProvider.getConfigViewDAO().listGsmGroupNames();
  }

  //
  // GsmGroupEditInfo
  //

  @Override
  public GsmGroupEditInfo createGsmGroupEditInfo(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);
    try {
      return new GsmGroupEditInfoImpl(dao.createGSMGroup());
    } catch (DataModificationException e) {
      logger.warn("[{}] - create GSMGroup error", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public GsmGroupEditInfo[] listGsmGroupEditInfo(final ClientUID client) throws NotPermittedException, StorageException {
    IConfigViewDAO cfgViewDAO = daoProvider.getConfigViewDAO();
    GSMGroup[] gsmGroups = cfgViewDAO.listGSMGroups();

    GsmGroupEditInfo[] infos = new GsmGroupEditInfo[gsmGroups.length];
    int i = 0;
    for (GSMGroup gsmGroup : gsmGroups) {
      infos[i++] = new GsmGroupEditInfoImpl(gsmGroup, listLinks(client, gsmGroup));
    }
    return infos;
  }

  @Override
  public void deleteGsmGroupEditInfo(final ClientUID client, final int transactionId, final GsmGroupEditInfo gsmGroupInfo) throws NotPermittedException, TransactionException,
      StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);

    try {
      dao.deleteGSMGroup(gsmGroupInfo.getGsmGroup());
    } catch (DataModificationException e) {
      throw new StorageException(e.getMessage());
    }
  }

  @Override
  public GsmGroupEditInfo updateGsmGroupEditInfo(final ClientUID client, final int transactionId, final GsmGroupEditInfo gsmGroupInfo) throws NotPermittedException,
      TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);

    try {
      dao.updateGSMGroup(gsmGroupInfo.getGsmGroup(), gsmGroupInfo.getLinkedSimGroups());
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't get updateGsmGroupEditInfo", this, e);
      throw new StorageException(e);
    }
    return gsmGroupInfo;
  }

  //
  // GSM - SIM link
  //

  @Override
  public LinkWithSIMGroup[] listLinks(final ClientUID client, final GSMGroup gsmGroup) throws NotPermittedException, StorageException {
    IConfigViewDAO cfgViewDAO = daoProvider.getConfigViewDAO();
    try {
      return cfgViewDAO.listSIMGSMLinks(gsmGroup);
    } catch (DataSelectionException e) {
      logger.warn("[{}] - can't get listLinks", this, e);
      return new LinkWithSIMGroup[0];
    }
  }

  //
  // SIM group CRUD
  //

  @Override
  public String[] listSimGroupNames(final ClientUID client) throws NotPermittedException, TransactionException, StorageException {
    IConfigViewDAO cfgViewDAO = daoProvider.getConfigViewDAO();
    return cfgViewDAO.listSimGroupNames();
  }

  @Override
  public SIMGroup createSIMGroup(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);
    try {
      SIMGroup simGroup = dao.createSIMGroup();
      Revisions.getInstance().putSimGroupRevision(simGroup);
      return simGroup;
    } catch (DataModificationException e) {
      logger.warn("[{}] - create SIMGroup error", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public SIMGroup updateSIMGroup(final ClientUID client, final int transactionId, final SIMGroup simGroup) throws NotPermittedException, TransactionException, StorageException {
    logger.debug("[{}] - asked to update sim group: {}", this, simGroup);
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);
    try {
      dao.updateSIMGroup(simGroup);
      Revisions.getInstance().incrementAndGetSimGroupRevision(simGroup);
    } catch (DataModificationException e) {
      throw new StorageException(e);
    }
    return simGroup;
  }

  @Override
  public void updateIMEI(final ClientUID client, final ICCID sim, final IMEI imei) throws StorageException {
    daoProvider.getSimpleConfigEditDAO().updateIMEI(sim, imei);
  }

  @Override
  public void deleteSIMGroup(final ClientUID client, final int transactionId, final SIMGroup simGroup) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(client, transactionId);
    try {
      dao.deleteSIMGroup(simGroup);
      Revisions.getInstance().removeSimGroupRevision(simGroup);
    } catch (DataModificationException e) {
      throw new StorageException(e.getMessage());
    }
  }

  @Override
  public SIMGroup[] listSIMGroups(final ClientUID client, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
    IConfigViewDAO cfgViewDAO = daoProvider.getConfigViewDAO();
    try {
      return cfgViewDAO.listSIMGroups();
    } catch (DataSelectionException ignored) {
      return new SIMGroupImpl[0];
    }
  }

  @Override
  public SIMGroup[] listSIMGroups(final ClientUID client) throws NotPermittedException, StorageException, DataSelectionException {
    return daoProvider.getConfigViewDAO().listSIMGroups();
  }

  @Override
  public SimpleSimGroup[] listSimpleSIMGroups(final ClientUID client) throws NotPermittedException, StorageException, DataSelectionException {
    SIMGroup[] simGroups = daoProvider.getConfigViewDAO().listSIMGroups();
    SimpleSimGroup[] simpleSimGroups = new SimpleSimGroup[simGroups.length];
    for (int i = 0; i < simGroups.length; i++) {
      simpleSimGroups[i] = new SimpleSimGroup(simGroups[i]);
    }
    return simpleSimGroups;
  }

  //
  // SIM card CRUD
  //

  @Override
  public void updateSimCardGroup(final ClientUID client, final int transactionId, final ICCID[] simUIDs, final SimpleSimGroup simGroup) throws NotPermittedException, TransactionException,
      NoSuchSimException, StorageException {
    for (ICCID simUID : simUIDs) {
      if (simUID == null) {
        logger.warn("[{}] - While change SIM group, SIM UID is null", this);
        return;
      }

      try {
        simUnitManager.changeSimGroup(simUID, simGroup.getId());
      } catch (RemoteException ignored) {
      } catch (NoSuchSimError e) {
        throw new NoSuchSimException(e.getMessage());
      } catch (ChangeGroupFailedError e) {
        logger.warn("[{}] - can't update sim group {} for {}", this, simGroup.getName(), simUID, e);
      }
    }
  }

  @Override
  public void setPhoneNumber(final ClientUID client, final int transactionId, final ICCID[] simUID, final PhoneNumber phoneNumber) throws NotPermittedException, TransactionException,
      StorageException, NoSuchSimException {
    ISimpleConfigEditDAO simpleConfigEditDAO = daoProvider.getSimpleConfigEditDAO();
    for (ICCID iccid : simUID) {
      simpleConfigEditDAO.updatePhoneNumber(iccid, phoneNumber);
    }
  }

  //
  // Servers CRUD
  //

  @Override
  public List<IServerData> listServers(final ClientUID client) throws NotPermittedException, TransactionException, StorageException {
    return daoProvider.getConfigViewDAO().listServers();
  }

  @Override
  public long getServersRevision(final ClientUID client) throws StorageException {
    return Revisions.getInstance().getServersRevision();
  }

  @Override
  public IServerData createServer(final ClientUID clientUID, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(clientUID, transactionId);
    try {
      return dao.createServer();
    } catch (DataModificationException e) {
      logger.warn("[{}] - create server error", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public void deleteServer(final ClientUID clientUID, final int transactionId, final IServerData server) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(clientUID, transactionId);
    try {
      dao.deleteServer(server);
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete server error", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public IServerData updateServer(final ClientUID clientUID, final int transactionId, final IServerData server) throws NotPermittedException, TransactionException,
      StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(clientUID, transactionId);
    try {
      dao.updateServer(server);
    } catch (DataModificationException e) {
      logger.warn("[{}] - update server error", this, e);
      throw new StorageException(e);
    }
    controlBeanModel.updateAudioCaptureEnable(server);
    return server;
  }

  @Override
  public RouteConfig getRouteConfig(final ClientUID clientUID) throws NotPermittedException, TransactionException, StorageException {
    return daoProvider.getConfigViewDAO().getRouteConfig();
  }

  @Override
  public void updateRouteConfig(final ClientUID clientUID, final int transactionId, final RouteConfig routeConfig) throws NotPermittedException, TransactionException, StorageException {
    IConfigEditDAO dao = transactionManager.getConfigEditDAO(clientUID, transactionId);
    try {
      dao.updateRouteConfig(routeConfig);
    } catch (DataModificationException e) {
      logger.warn("[{}] - update route config error", this, e);
      throw new StorageException(e);
    }
  }

  //
  // Scripting
  //

  @Override
  public String getVersionOfAntrax(final ClientUID client) throws NotPermittedException {
    try {
      return ScriptsHelper.getAntraxVersion();
    } catch (IOException | URISyntaxException e) {
      logger.warn("[{}] - can't get version of antrax", this, e);
    }
    return null;
  }

  @Override
  public ScriptFile getScriptFile(final ClientUID client) throws NotPermittedException {
    return daoProvider.getConfigViewDAO().getLastScriptFile();
  }

  @Override
  public ScriptDefinition[] listScriptDefinitions(final ClientUID client, final ScriptType... type) throws NotPermittedException, StorageException {
    Set<ScriptType> types = new HashSet<>(Arrays.asList(type));
    List<ScriptDefinition> retval = new ArrayList<>();
    for (ScriptDefinition d : daoProvider.getConfigViewDAO().listScriptDefinitions()) {
      if (types.contains(d.getType())) {
        retval.add(d);
      }
    }
    return retval.toArray(new ScriptDefinition[retval.size()]);
  }

  @Override
  public ScriptConflict[] getScriptConflicts(final ClientUID client, final int transactionId, final byte[] inputByteArray) throws Exception {
    AntraxPluginsStore newPS = analyzeJavaFileClasses(client, transactionId, inputByteArray);
    AntraxPluginsStore oldPS;
    try {
      oldPS = getPluginsStore(client, transactionId);
    } catch (Exception e) {
      logger.warn("[{}] - failed to get plugins store. Using new one", this, e);
      oldPS = newPS;
    }

    IConfigViewDAO viewDAO = daoProvider.getConfigViewDAO();
    return new SimGroupConflictsHelper().createConflicts(viewDAO.listSIMGroups(), oldPS, newPS);
  }

  @Override
  public void commitCodeBase(final ClientUID client, final int transactionId, final ScriptFile scriptFile, final SimGroupScriptModificationConflict[] conflicts) throws Exception {
    try {
      IConfigEditDAO editDAO = transactionManager.getConfigEditDAO(client, transactionId);
      ScriptsCommitHelper commitHelper = new ScriptsCommitHelper(editDAO);
      commitHelper.commit(scriptFile, conflicts);
    } catch (DataSelectionException e) {
      logger.warn("[{}] - while commit code base", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public AntraxPluginsStore analyzeJavaFileClasses(final ClientUID client, final int transactionId, final byte[] inputByteArray) throws Exception {
    AntraxPluginsStore retval = new AntraxPluginsStore();
    retval.analyze(new ByteArrayInputStream(inputByteArray), 1);
    return retval;
  }

  @Override
  public AntraxPluginsStore getPluginsStore(final ClientUID client, final int transactionId) throws Exception {
    return pluginsStoreCache.getPluginsStore(transactionId);
  }

  @Override
  public boolean isScriptsOk(final ClientUID client) throws NotPermittedException {
    try {
      pluginsStoreCache.getPluginsStore(-1);
      return true;
    } catch (Exception ignored) {
      return false;
    }
  }

  @Override
  public ScriptDefinition getScriptDefinition(final ClientUID client, final String name) throws NotPermittedException, StorageException {
    for (ScriptDefinition def : daoProvider.getConfigViewDAO().listScriptDefinitions()) {
      if (def.getName().equalsIgnoreCase(name)) {
        return def;
      }
    }
    return null;
  }

  @Override
  public long getPluginsStoreRevision(final ClientUID client, final int transactionId) {
    logger.debug("[{}] - asked to get plugins store revision", this);
    return Revisions.getInstance().getScriptFilesRevision();
  }

  @Override
  public long getSIMGroupsRevision(final ClientUID client, final int transactionId) {
    logger.debug("[{}] - asked to get sim groups revision", this);
    return Revisions.getInstance().getLastSimGroupRevision();
  }

  @Override
  public long getGSMGroupsRevision(final ClientUID client, final int transactionId) {
    logger.debug("[{}] - asked to get gsm groups revision", this);
    return Revisions.getInstance().getGsmGroupsRevision();
  }

  @Override
  public void incGsmToSimGroupLinksRevision(final ClientUID client) {
    logger.debug("[{}] - asked to inc gsm to sim group links revision", this);
    Revisions.getInstance().incrementAndGetGsmGroupsRevision();
    Revisions.getInstance().incrementAndGetGsmToSimGroupLinksRevision();
  }

  @Override
  public void incServersRevision(final ClientUID client) {
    logger.debug("[{}] - asked to inc servers revision", this);
    Revisions.getInstance().incrementAndGetServersRevision();
  }

  @Override
  public void importSimUIDs(final ClientUID client, final Map<ICCID, PhoneNumber> simUIDs) throws NotPermittedException, StorageException {
    daoProvider.getSimpleConfigEditDAO().importSimUIDs(simUIDs);
  }

  @Override
  public VoipAntiSpamConfiguration getVoipAntiSpamConfiguration(final ClientUID clientuid) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getVoipAntiSpamConfiguration();
  }

  @Override
  public void saveVoipAntiSpamConfiguration(final ClientUID clientuid, final int transactionId,
      final VoipAntiSpamConfiguration voipAntiSpamConfiguration) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().upsertVoipAntiSpamConfiguration(voipAntiSpamConfiguration);
    } catch (DataModificationException e) {
      logger.warn("[{}] - update VoipAntiSpamConfiguration error", this, e);
      throw new StorageException("Can't update voipAntiSpamConfiguration", e);
    }
  }

}
