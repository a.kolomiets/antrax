/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.dao;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.util.Map;
import java.util.Set;

public interface ConfigurationDAO {

  Map<GSMGroup, Set<SIMGroup>> loadGsmToSimLinks();

  Map<String, IServerData> loadGatewaysData();

  SimData loadSimData(ICCID simUID);

  byte[] getSessionScript(ICCID simUID);

  byte[] getGatewaySelectorScript(ICCID simUID);

  byte[] getFactorEvaluationScript(ICCID simUID);

  byte[] getActivityScript(ICCID simUID);

  byte[] getIMEIGeneratorScript(ICCID simUID);

  ScriptFile getLastScriptFile();

  boolean shouldResetSsScripts(ICCID simUid);

}
