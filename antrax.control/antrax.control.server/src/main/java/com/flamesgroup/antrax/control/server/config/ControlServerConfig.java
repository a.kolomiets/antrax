/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.config;

import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.properties.ControlServerProperties;
import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;

public class ControlServerConfig {

  private final DBConnectionProps dbConnectionProps;
  private final int controlServerServicePort;
  private final String rmiServerHostname;
  private final VoipAntiSpamConfig voipAntiSpamConfig;

  public ControlServerConfig() {
    ControlServerProperties controlServerProperties = ControlPropUtils.getInstance().getControlServerProperties();
    controlServerServicePort = controlServerProperties.getRmiServicePort();
    rmiServerHostname = controlServerProperties.getRmiServerHostname();
    dbConnectionProps = new DBConnectionProps(controlServerProperties.getDbConfigDriver(), controlServerProperties.getDbConfigUrl(), controlServerProperties.getDbConfigUsername(),
        controlServerProperties.getDbConfigPassword(), controlServerProperties.getDbConfigMaxConnection());
    voipAntiSpamConfig = new VoipAntiSpamConfig();
    voipAntiSpamConfig.setCdrAnalyzePeriod(controlServerProperties.getVoipantispamCdrAnalyzePeriod());
    voipAntiSpamConfig.setCdrMaxNormalCallPerPeriod(controlServerProperties.getVoipantispamCdrMaxNormalCallPerPeriod());
    voipAntiSpamConfig.setCdrNormalAcd(controlServerProperties.getVoipantispamCdrNormalAcd());
  }

  public DBConnectionProps getDbConnectionProps() {
    return dbConnectionProps;
  }

  public int getControlServerServicePort() {
    return controlServerServicePort;
  }

  public String getRmiServerHostname() {
    return rmiServerHostname;
  }

  public VoipAntiSpamConfig getVoipAntiSpamConfig() {
    return voipAntiSpamConfig;
  }

}
