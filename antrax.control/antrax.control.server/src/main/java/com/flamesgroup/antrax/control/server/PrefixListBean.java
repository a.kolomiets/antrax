/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.IPrefixListBean;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefix;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class PrefixListBean implements IPrefixListBean {

  private static final Logger logger = LoggerFactory.getLogger(PrefixListBean.class);

  private final DAOProvider daoProvider;

  private final AtomicBoolean updating = new AtomicBoolean();
  private Exception lastUpdatingException;

  public PrefixListBean(final DAOProvider daoProvider) {
    this.daoProvider = daoProvider;
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return true;
  }


  @Override
  public List<PhoneNumberPrefix> listPhoneNumberPrefixes(final ClientUID clientUid) throws NotPermittedException {
    return daoProvider.getPhoneNumberPrefixDAO().listPhoneNumberPrefixes();
  }

  @Override
  public void addPhoneNumberPrefix(final ClientUID clientUID, final PhoneNumberPrefix newElem) throws NotPermittedException, StorageException {
    try {
      daoProvider.getPhoneNumberPrefixDAO().insertPhoneNumberPrefix(newElem);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public void editPhoneNumberPrefix(final ClientUID clientUID, final PhoneNumberPrefix editElem) throws NotPermittedException, StorageException {
    try {
      daoProvider.getPhoneNumberPrefixDAO().updatePhoneNumberPrefix(editElem);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public void deletePhoneNumberPrefix(final ClientUID clientUID, final PhoneNumberPrefix deleteElem) throws NotPermittedException, StorageException {
    try {
      daoProvider.getPhoneNumberPrefixDAO().deletePhoneNumberPrefix(deleteElem);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public void importPhoneNumberPrefix(final ClientUID clientUID, final Set<String> numbers, final long phoneNumberPrefixId,
      final boolean ignoreOrUpdateOnDuplicate) throws NotPermittedException, StorageException {
    try {
      daoProvider.getPhoneNumberPrefixDAO().insertAllNumbers(numbers, phoneNumberPrefixId, ignoreOrUpdateOnDuplicate);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public int clearPhoneNumberPrefixes(final ClientUID clientUID, final PhoneNumberPrefix selectedElem) throws NotPermittedException, StorageException {
    try {
      return daoProvider.getPhoneNumberPrefixDAO().clearPhoneNumberPrefix(selectedElem);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

  @Override
  public void updatePhoneNumberPrefixesCount(final ClientUID clientUID, final PhoneNumberPrefix selectedElem, final Boolean ignoreOrUpdateOnDuplicate)
      throws NotPermittedException, StorageException {
    new Thread(new Runnable() {
      @Override
      public void run() {
        updating.set(true);
        lastUpdatingException = null;
        try {
          if (ignoreOrUpdateOnDuplicate) {
            daoProvider.getPhoneNumberPrefixDAO().updatePhoneNumberPrefixesCount(selectedElem);
          } else {
            List<PhoneNumberPrefix> phoneNumberPrefixes = daoProvider.getPhoneNumberPrefixDAO().listPhoneNumberPrefixes();
            for (PhoneNumberPrefix phoneNumberPrefix : phoneNumberPrefixes) {
              daoProvider.getPhoneNumberPrefixDAO().updatePhoneNumberPrefixesCount(phoneNumberPrefix);
            }
          }
        } catch (DataModificationException e) {
          logger.warn("[{}] - can't execute", this, e);
          lastUpdatingException = e;
        } finally {
          updating.set(false);
        }
      }
    }).start();
  }

  @Override
  public Pair<Boolean, Exception> checkPhoneNumberPrefixesCountExecuting(final ClientUID clientUID) {
    return new Pair<>(updating.get(), lastUpdatingException);
  }

  @Override
  public String exportNumbers(final ClientUID clientUID, final PhoneNumberPrefix selectedElem) throws NotPermittedException {
    List<String> strings = daoProvider.getPhoneNumberPrefixDAO().listPhoneNumberForPrefix(selectedElem.getID());
    return String.join("\n", strings);
  }

  @Override
  public PhoneNumberPrefixConfig getPrefixConfig(final ClientUID clientUID) throws NotPermittedException {
    return daoProvider.getPhoneNumberPrefixDAO().getPrefixConfig();
  }

  @Override
  public void savePrefixConfig(final ClientUID clientUID, final PhoneNumberPrefixConfig phoneNumberPrefixConfig) throws NotPermittedException, StorageException {
    try {
      daoProvider.getPhoneNumberPrefixDAO().savePrefixConfig(phoneNumberPrefixConfig);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't execute", this, e);
      throw new StorageException(e);
    }
  }

}
