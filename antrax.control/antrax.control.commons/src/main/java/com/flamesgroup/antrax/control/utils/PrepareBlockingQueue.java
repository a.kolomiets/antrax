/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.utils;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class PrepareBlockingQueue<E> extends AbstractQueue<E> implements BlockingQueue<E> {

  private final LinkedList<E> queue = new LinkedList<>();

  abstract protected void prepareOffer(E e);

  final Lock lock = new ReentrantLock();
  private final Condition notEmpty = lock.newCondition();

  @Override
  public E poll() {
    lock.lock();
    try {
      return queue.poll();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public boolean offer(final E e) {
    Objects.requireNonNull(e);
    lock.lock();
    try {
      prepareOffer(e);
      if (queue.offer(e)) {
        notEmpty.signal();
        return true;
      } else {
        return false;
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public E take() throws InterruptedException {
    lock.lockInterruptibly();
    try {
      while (queue.isEmpty()) {
        notEmpty.await();
      }
      return queue.poll();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public E poll(final long timeout, final TimeUnit unit) throws InterruptedException {
    long nanos = unit.toNanos(timeout);
    lock.lockInterruptibly();
    try {
      while (queue.isEmpty() && nanos > 0) {
        nanos = notEmpty.awaitNanos(nanos);
      }
      return queue.poll();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public Iterator<E> iterator() {
    lock.lock();
    try {
      return queue.iterator();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public int size() {
    lock.lock();
    try {
      return queue.size();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public E peek() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void put(final E e) throws InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean offer(final E e, final long timeout, final TimeUnit unit) throws InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  public int remainingCapacity() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int drainTo(final Collection<? super E> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int drainTo(final Collection<? super E> c, final int maxElements) {
    throw new UnsupportedOperationException();
  }

}
