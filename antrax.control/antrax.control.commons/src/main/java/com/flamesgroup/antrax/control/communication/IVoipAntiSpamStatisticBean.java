/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.commons.Pair;

import java.util.List;
import java.util.Set;

import javax.swing.*;

public interface IVoipAntiSpamStatisticBean extends PermissionChecker {

  @PermitTo(groups = {UserGroup.ADMIN}, description = "insert WhiteListNumbers")
  void insertWhiteListNumbers(ClientUID clientuid, Set<WhiteListNumber> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "import WhiteListNumbers")
  void importWhiteListNumbers(ClientUID clientuid, Set<String> numbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete all WhiteListNumbers")
  void deleteAllWhiteListNumbers(ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete WhiteListNumbers")
  void deleteWhiteListNumbers(ClientUID clientuid, Set<String> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "List all WhiteListNumbers")
  List<WhiteListNumber> listWhiteListNumbers(ClientUID clientuid, String search, Pair<String, SortOrder> sort, int offset, int limit) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Get count WhiteListNumbers")
  int getCountWhiteListNumbers(ClientUID clientuid, String search) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Export all WhiteListNumbers")
  StringBuilder exportWhiteListNumbers(ClientUID clientuid, Set<String> columns) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete all GrayListNumbers")
  void deleteAllGrayListNumbers(ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete GrayListNumbers")
  void deleteGrayListNumbers(ClientUID clientuid, Set<String> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Move to BlackListNumbers")
  void moveToBlackListNumbers(ClientUID clientuid, Set<String> grayListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "List all GrayListNumbers")
  List<GrayListNumber> listGrayListNumbers(ClientUID clientuid, String search, Pair<String, SortOrder> sort, int offset, int limit) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Get count GrayListNumbers")
  int getCountGrayListNumbers(ClientUID clientuid, String search) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "insert BlackListNumbers")
  void insertBlackListNumbers(ClientUID clientuid, Set<BlackListNumber> blackListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "import BlackListNumbers")
  void importBlackListNumbers(ClientUID clientuid, Set<String> numbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete all BlackListNumbers")
  void deleteAllBlackListNumbers(ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Delete BlackListNumbers")
  void deleteBlackListNumbers(ClientUID clientuid, Set<String> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "List all BlackListNumbers")
  List<BlackListNumber> listBlackListNumbers(ClientUID clientuid, String search, Pair<String, SortOrder> sort, int offset, int limit) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Get count BlackListNumbers")
  int getCountBlackListNumbers(ClientUID clientuid, String search) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Export all BlackListNumbers")
  StringBuilder exportBlackListNumbers(ClientUID clientuid, Set<String> columns) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Get VoipAntiSpamPlotData for routing request")
  List<VoipAntiSpamPlotData> getPlotDataForRoutingRequest(ClientUID clientuid) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "Get VoipAntiSpamPlotData for block number")
  List<VoipAntiSpamPlotData> getPlotDataForBlockNumber(ClientUID clientuid) throws NotPermittedException;

}
