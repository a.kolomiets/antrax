/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.control.voiceserver.ActivityLogger;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ActivityRemoteLogger implements IActivityRemoteLogger {

  private final Map<Server, IActivityLogger> activityLoggers = new ConcurrentHashMap<>();

  public List<IVoiceServerChannelsInfo> getChannelsInfo(final Server server) {
    return activityLoggers.get(server).getVoiceServerChannelsInfos();
  }

  public IVoiceServerShortInfo getShortServerInfo(final Server server) {
    return activityLoggers.get(server).getShortServerInfo();
  }

  @Override
  public void changeServerStatus(final Server server, final ServerStatus status) throws RemoteException {
    if (status == ServerStatus.STARTING) {
      activityLoggers.put(server, new ActivityLogger());
    }
    activityLoggers.get(server).changeServerStatus(status);
  }

  @Override
  public void logDeclaredGsmUnit(final Server server, final GSMChannel channel) throws RemoteException {
    activityLoggers.get(server).logDeclaredGsmUnit(channel);
  }

  @Override
  public void logActiveGsmUnit(final Server server, final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) throws RemoteException {
    activityLoggers.get(server).logActiveGsmUnit(gsmUnit, gsmChannel, channelConfig);
  }

  @Override
  public void updateChannelConfig(final Server server, final ChannelUID gsmUnit, final GSMChannel gsmChannel, final ChannelConfig channelConfig) throws RemoteException {
    activityLoggers.get(server).updateChannelConfig(gsmUnit, gsmChannel, channelConfig);
  }

  @Override
  public void logGsmUnitChangedGroup(final Server server, final ChannelUID gsmUnit, final GSMGroup group) throws RemoteException {
    activityLoggers.get(server).logGsmUnitChangedGroup(gsmUnit, group);
  }

  @Override
  public void logGsmUnitLock(final Server server, final ChannelUID gsmUnit, final boolean lock, final String lockReason) throws RemoteException {
    activityLoggers.get(server).logGsmUnitLock(gsmUnit, lock, lockReason);
  }

  @Override
  public void logGSMUnitReleased(final Server server, final ChannelUID gsmUnit) throws RemoteException {
    activityLoggers.get(server).logGSMUnitReleased(gsmUnit);
  }

  @Override
  public void logSIMTakenFromSimServer(final Server server, final ChannelUID simUnit, final ICCID simUid) throws RemoteException {
    activityLoggers.get(server).logSIMTakenFromSimServer(simUnit, simUid);
  }

  @Override
  public void logSIMChangedEnableStatus(final Server server, final ChannelUID simUnit, final boolean enable) throws RemoteException {
    activityLoggers.get(server).logSIMChangedEnableStatus(simUnit, enable);
  }

  @Override
  public void logSIMChangedGroup(final Server server, final ChannelUID simUnit, final String group) throws RemoteException {
    activityLoggers.get(server).logSIMChangedGroup(simUnit, group);
  }

  @Override
  public void logSIMReturnedToSimServer(final Server server, final ChannelUID simUnit) throws RemoteException {
    activityLoggers.get(server).logSIMReturnedToSimServer(simUnit);
  }

  @Override
  public void logCallChannelCreated(final Server server, final ChannelUID gsmUnit, final ChannelUID simUnit) throws RemoteException {
    activityLoggers.get(server).logCallChannelCreated(gsmUnit, simUnit);
  }

  @Override
  public void logCallChannelChangedState(final Server server, final ChannelUID simUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) throws RemoteException {
    activityLoggers.get(server).logCallChannelChangedState(simUnit, state, advInfo, periodPrediction);
  }

  @Override
  public void logCallChannelChangedAdvInfo(final Server server, final ChannelUID simUnit, final String advInfo) throws RemoteException {
    activityLoggers.get(server).logCallChannelChangedAdvInfo(simUnit, advInfo);
  }

  @Override
  public void logGsmUnitChangedState(final Server server, final ChannelUID gsmUnit, final CallChannelState.State state, final String advInfo, final long periodPrediction) throws RemoteException {
    activityLoggers.get(server).logGsmUnitChangedState(gsmUnit, state, advInfo, periodPrediction);
  }

  @Override
  public void logCallChannelStartActivity(final Server server, final ChannelUID simUnit) throws RemoteException {
    activityLoggers.get(server).logCallChannelStartActivity(simUnit);
  }

  @Override
  public void logCallChannelReleased(final Server server, final ChannelUID simUnit) throws RemoteException {
    activityLoggers.get(server).logCallChannelReleased(simUnit);
  }

  @Override
  public void logCallStarted(final Server server, final ChannelUID simUnit, final PhoneNumber phoneNumber) throws RemoteException {
    activityLoggers.get(server).logCallStarted(simUnit, phoneNumber);
  }

  @Override
  public void logCallChangeState(final Server server, final ChannelUID simUnit, final CallState.State callState) throws RemoteException {
    activityLoggers.get(server).logCallChangeState(simUnit, callState);
  }

  @Override
  public void logCallEnded(final Server server, final ChannelUID simUnit, final long duration) throws RemoteException {
    activityLoggers.get(server).logCallEnded(simUnit, duration);
  }

  @Override
  public void logCallChannelSendUSSD(final Server server, final ChannelUID simUnit, final String ussd, final String response) throws RemoteException {
    activityLoggers.get(server).logCallChannelSendUSSD(simUnit, ussd, response);
  }

  @Override
  public void logCallChannelSuccessSentSMS(final Server server, final ChannelUID simUnit, final String number, final String text, final int parts) throws RemoteException {
    activityLoggers.get(server).logCallChannelSuccessSentSMS(simUnit, number, text, parts);
  }

  @Override
  public void logCallChannelFailSentSMS(final Server server, final ChannelUID simUnit, final String number, final String text, final int totalSmsParts, final int successSendSmsParts)
      throws RemoteException {
    activityLoggers.get(server).logCallChannelFailSentSMS(simUnit, number, text, totalSmsParts, successSendSmsParts);
  }

  @Override
  public void logCallChannelReceivedSMS(final Server server, final ChannelUID simUnit, final String number, final String text, final int parts) throws RemoteException {
    activityLoggers.get(server).logCallChannelReceivedSMS(simUnit, number, text, parts);
  }

  @Override
  public void logSignalQualityChange(final Server server, final ChannelUID gsmUnit, final int signalStrength, final int bitErrorRate) throws RemoteException {
    activityLoggers.get(server).logSignalQualityChange(gsmUnit, signalStrength, bitErrorRate);
  }

  @Override
  public void logGSMNetworkInfoChange(final Server server, final ChannelUID gsmUnit, final GSMNetworkInfo info) throws RemoteException {
    activityLoggers.get(server).logGSMNetworkInfoChange(gsmUnit, info);
  }

  @Override
  public void logPdd(final Server server, final ChannelUID simUnit, final long pdd) throws RemoteException {
    activityLoggers.get(server).logPdd(simUnit, pdd);
  }

  @Override
  public void resetStatistic(final Server server) throws RemoteException {
    activityLoggers.get(server).resetStatistic();
  }

  @Override
  public void logGsmUnitLockToArfcn(final Server server, final ChannelUID channel, final Integer arfcn) throws RemoteException {
    activityLoggers.get(server).logGsmUnitLockToArfcn(channel, arfcn);
  }

  @Override
  public void logCallSetup(final Server server, final ChannelUID simUID, final PhoneNumber phoneNumber) throws RemoteException {
    activityLoggers.get(server).logCallSetup(simUID, phoneNumber);
  }

  @Override
  public void logCallRelease(final Server server, final ChannelUID simUID) throws RemoteException {
    activityLoggers.get(server).logCallRelease(simUID);
  }

}
