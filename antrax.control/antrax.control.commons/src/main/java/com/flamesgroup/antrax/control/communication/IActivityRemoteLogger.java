/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IActivityRemoteLogger extends Remote {

  void changeServerStatus(Server server, ServerStatus status) throws RemoteException;

  void logDeclaredGsmUnit(Server server, GSMChannel channel) throws RemoteException;

  void logActiveGsmUnit(Server server, ChannelUID gsmUnit, GSMChannel gsmChannel, ChannelConfig channelConfig) throws RemoteException;

  void updateChannelConfig(Server server, ChannelUID gsmUnit, GSMChannel gsmChannel, ChannelConfig channelConfig) throws RemoteException;

  void logGsmUnitChangedGroup(Server server, ChannelUID gsmUnit, GSMGroup group) throws RemoteException;

  void logGsmUnitLock(Server server, ChannelUID gsmUnit, boolean lock, String lockReason) throws RemoteException;

  void logGSMUnitReleased(Server server, ChannelUID gsmUnit) throws RemoteException;

  void logSIMTakenFromSimServer(Server server, ChannelUID simUnit, ICCID simUid) throws RemoteException;

  void logSIMChangedEnableStatus(Server server, ChannelUID simUnit, boolean enable) throws RemoteException;

  void logSIMChangedGroup(Server server, ChannelUID simUnit, String group) throws RemoteException;

  void logSIMReturnedToSimServer(Server server, ChannelUID simUnit) throws RemoteException;

  void logCallChannelCreated(Server server, ChannelUID gsmUnit, ChannelUID simUnit) throws RemoteException;

  void logCallChannelChangedState(Server server, ChannelUID simUnit, CallChannelState.State state, String advInfo, long periodPrediction) throws RemoteException;

  void logCallChannelChangedAdvInfo(Server server, ChannelUID simUnit, String advInfo) throws RemoteException;

  void logGsmUnitChangedState(Server server, ChannelUID gsmUnit, CallChannelState.State state, String advInfo, long periodPrediction) throws RemoteException;

  void logCallChannelStartActivity(Server server, ChannelUID simUnit) throws RemoteException;

  void logCallChannelReleased(Server server, ChannelUID simUnit) throws RemoteException;

  void logCallStarted(Server server, ChannelUID simUnit, PhoneNumber phoneNumber) throws RemoteException;

  void logCallChangeState(Server server, ChannelUID simUnit, CallState.State callState) throws RemoteException;

  void logCallEnded(Server server, ChannelUID simUnit, long duration) throws RemoteException;

  void logCallChannelSendUSSD(Server server, ChannelUID simUnit, String ussd, String response) throws RemoteException;

  void logCallChannelSuccessSentSMS(Server server, ChannelUID simUnit, String number, String text, int parts) throws RemoteException;

  void logCallChannelFailSentSMS(Server server, ChannelUID simUnit, String number, String text, int totalSmsParts, int successSendSmsParts) throws RemoteException;

  void logCallChannelReceivedSMS(Server server, ChannelUID simUnit, String number, String text, final int parts) throws RemoteException;

  void logSignalQualityChange(Server server, ChannelUID gsmUnit, int signalStrength, int bitErrorRate) throws RemoteException;

  void logGSMNetworkInfoChange(Server server, ChannelUID gsmUnit, GSMNetworkInfo info) throws RemoteException;

  void logPdd(Server server, ChannelUID simUnit, long pdd) throws RemoteException;

  void resetStatistic(Server server) throws RemoteException;

  void logGsmUnitLockToArfcn(Server server, ChannelUID channel, Integer arfcn) throws RemoteException;

  void logCallSetup(Server server, ChannelUID simUID, PhoneNumber phoneNumber) throws RemoteException;

  void logCallRelease(Server server, ChannelUID simUID) throws RemoteException;

}
