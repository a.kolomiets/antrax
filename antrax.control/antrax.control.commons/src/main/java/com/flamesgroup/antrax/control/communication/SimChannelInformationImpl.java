/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

public class SimChannelInformationImpl implements SimChannelInformation {

  private static final long serialVersionUID = 5859309894359194588L;

  private final ChannelUID channelUID;
  private final ICCID simUID;

  private boolean enabled;

  private String lastUSSDResponse;

  private long lastUSSDTime;

  private final TimeProvider timeProvider = new ServerSyncTimeProvider();

  private String group;

  public SimChannelInformationImpl(final ChannelUID channelUID, final ICCID simUID) {
    this.channelUID = channelUID;
    this.simUID = simUID;
  }

  @Override
  public ChannelUID getSimChannelUID() {
    return channelUID;
  }

  @Override
  public ICCID getSimUID() {
    return simUID;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public String getLastUSSDResponse() {
    return lastUSSDResponse;
  }

  @Override
  public long getLastUSSDTimeout() {
    return lastUSSDResponse == null ? 0 : timeProvider.currentTimeMillis() - lastUSSDTime;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }

  public void setLastUSSDResponse(final String lastUSSDResponse) {
    this.lastUSSDResponse = lastUSSDResponse;
    this.lastUSSDTime = System.currentTimeMillis();
  }

  @Override
  public String getGroup() {
    return group;
  }

  public void setGroup(final String group) {
    this.group = group;
  }

}
