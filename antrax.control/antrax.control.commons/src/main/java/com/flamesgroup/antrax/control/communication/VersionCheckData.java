/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;

/**
 * This class is used to check compilation compatibility of client and server
 * <p>
 * Note, that we don't have serialVersionUID assigned letting java generate it.
 */
@SuppressWarnings("serial")
public class VersionCheckData implements Serializable {

  public String getVersion() {
    return "Version";
  }

}
