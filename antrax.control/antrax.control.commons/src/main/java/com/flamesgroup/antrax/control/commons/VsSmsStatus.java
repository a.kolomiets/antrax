/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.commons;

import java.io.Serializable;
import java.util.UUID;

public class VsSmsStatus implements Serializable {

  private static final long serialVersionUID = 1401370900784868727L;

  private UUID uuid;
  private Status status;
  private String message;


  public enum Status {
    SENT, SENDING, FAIL;
  }

  public UUID getUuid() {
    return uuid;
  }

  public VsSmsStatus setUuid(final UUID uuid) {
    this.uuid = uuid;
    return this;
  }

  public Status getStatus() {
    return status;
  }

  public VsSmsStatus setStatus(final Status status) {
    this.status = status;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public VsSmsStatus setMessage(final String message) {
    this.message = message;
    return this;
  }

}
