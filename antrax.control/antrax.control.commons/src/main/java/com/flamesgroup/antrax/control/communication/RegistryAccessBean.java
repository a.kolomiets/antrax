/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

public interface RegistryAccessBean extends PermissionChecker {

  @PermitTo(groups = {UserGroup.ADMIN}, description = "add registry entry")
  void add(ClientUID clientUID, String path, String value, int maxSize) throws TransactionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "remove registry entry")
  void remove(ClientUID clientUID, RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException,
      TransactionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "move registry entry")
  RegistryEntry move(ClientUID clientUID, RegistryEntry entry, String newPath) throws UnsyncRegistryEntryException, DataModificationException,
      TransactionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "list registry entry")
  RegistryEntry[] listEntries(ClientUID clientUID, String path, int maxSize) throws TransactionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "remove whole path")
  void removePath(ClientUID clientUid, String path) throws DataModificationException, TransactionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "list all paths")
  String[] listAllPaths(ClientUID clientUid) throws TransactionException;

}
