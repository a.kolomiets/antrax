/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ISimUnitManager extends Remote {

  Pair<ChannelUID, UUID> takeSimUnit(String serverName, GSMGroup gsmGroup) throws RemoteException;

  ISCReaderSubChannels takeSCReaderSubChannels(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  void returnSimUnit(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitPlugged(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  Pair<SimData, IMSI> getSimData(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  void lockSimUnit(UUID sessionUuid, boolean lockStatus, String reason) throws RemoteException, IllegalSessionUuidException;

  void enableSimUnit(UUID sessionUuid, boolean enableStatus) throws RemoteException, IllegalSessionUuidException;

  void handleSimUnitStartedActivity(UUID sessionUuid, ChannelUID gsmChannel, GSMGroup gsmGroup) throws RemoteException, IllegalSessionUuidException;

  void handleSimUnitStoppedActivity(UUID sessionUuid, final String stopReason) throws RemoteException, IllegalSessionUuidException;

  void handleSimUnitCallEnded(ICCID uid, long callDuration) throws RemoteException, NoSuchSimError;

  void handleSimUnitIncomingCallEnded(ICCID uid, long callDuration) throws RemoteException, NoSuchSimError;

  void handleSimUnitSMSStatus(ICCID uid) throws RemoteException, NoSuchSimError;

  void handleSimUnitSentSMS(ICCID uid, int parts) throws RemoteException, NoSuchSimError;

  void handleSimUnitFailSentSMS(ICCID uid, int totalSmsParts, int successSendSmsParts) throws RemoteException, NoSuchSimError;

  void handleSimUnitIncomingSMS(ICCID uid, int parts) throws RemoteException, NoSuchSimError;

  void handleGenericEvent(ICCID uid, String event, byte[]... serializedArgs) throws RemoteException, NoSuchSimError;

  void addUserMessage(UUID sessionUuid, String message) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitShouldStartActivity(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitShouldStopActivity(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitShouldStopSession(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitRequiresReconfiguration(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  // Non VoiceServer management
  void lockSimUnit(ICCID[] uid, boolean lockStatus, String reason) throws RemoteException;

  void enableSimUnit(ICCID[] uid, boolean enableStatus) throws RemoteException;

  void removeCHV(ICCID[] uid, CHV chv1) throws RemoteException, PINRemoveFailedException;

  SIMGroup changeSimGroup(ICCID uid, long simGroup) throws RemoteException, NoSuchSimError, ChangeGroupFailedError;

  void resetIMEI(ICCID ui, String message) throws RemoteException, NoSuchSimError;

  void resetIMEI(UUID sessionUuid, String message) throws RemoteException, IllegalSessionUuidException;

  boolean isSimUnitShouldGenerateIMEI(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  IMEI generateIMEI(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  void gsmRegistration(UUID sessionUuid, int attemptNumber) throws RemoteException, IllegalSessionUuidException;

  void gsmRegistered(UUID sessionUuid) throws RemoteException, IllegalSessionUuidException;

  void updatePhoneNumber(UUID sessionUuid, PhoneNumber phoneNumber) throws RemoteException, IllegalSessionUuidException;

  // service channels
  ISCReaderSubChannels getSCReaderSubChannels(ChannelUID channel) throws RemoteException;

  void noteSimUnit(ICCID[] simUIDs, String note) throws RemoteException;

  void handleAllowedInternet(ICCID uid, boolean allowed) throws RemoteException, NoSuchSimError;

  void invertAllowInternet(ICCID[] uids) throws RemoteException, NoSuchSimError;

  void setTariffPlanEndDate(ICCID uid, long endDate) throws RemoteException, NoSuchSimError;

  void setBalance(ICCID uid, double balance) throws RemoteException, NoSuchSimError;

  void resetSimCardStatistic(ICCID[] iccids) throws RemoteException;
}
