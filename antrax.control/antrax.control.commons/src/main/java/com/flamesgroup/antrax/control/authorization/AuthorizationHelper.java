/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.authorization;

import com.flamesgroup.antrax.control.communication.AuthorizationBean;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AuthorizationHelper {

  public static void authorize(final ClientUID client, final String name, final String passwd, final AuthorizationBean bean) throws AuthorizationFailedException {
    AuthorizationData secData = bean.requestAuthorization(client, new AuthorizationData(name, passwd));
    secData.setPasswd(passwd);
    bean.confirmAuthorization(client, secData);
  }

  public static String digest(final String passwd) {
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("SHA");
      md.update(passwd.getBytes());
      return new BigInteger(md.digest()).toString(16);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  public static String digest(final char[] passwd) {
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("SHA");
      for (int i = 0; i < passwd.length; ++i) {
        md.update((byte) passwd[i]);
      }
      return new BigInteger(md.digest()).toString(16);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

}
