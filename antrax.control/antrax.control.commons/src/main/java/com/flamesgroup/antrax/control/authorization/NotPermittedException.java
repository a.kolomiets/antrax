/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.authorization;

import java.lang.reflect.Method;

public class NotPermittedException extends Exception {

  private static final long serialVersionUID = 349158430604144552L;

  private final String message;

  public NotPermittedException(final Method source) {
    if (!source.isAnnotationPresent(PermitTo.class))
      throw new IllegalArgumentException("Method caused this exception should be annotated with " + PermitTo.class.getName() + ": " + source);
    PermitTo permitTo = source.getAnnotation(PermitTo.class);
    message = "You are not permitted to " + permitTo.description();
  }

  public NotPermittedException(final String msg) {
    message = msg;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public String getLocalizedMessage() {
    return message;
  }

}
