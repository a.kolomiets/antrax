/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.statefulscripts;

import com.flamesgroup.antrax.automation.annotations.StateField;

import java.io.Serializable;

/**
 * Interface which marks scripts and objects that should be stored in database
 * and be restated.
 * <p>
 * Scripts with states, which should live longer than sim server's JVM should
 * implements this interface.
 * </p>
 * <p>
 * All fields which contains state of this class should be marked as
 * {@linkplain StateField @StateField}.
 * </p>
 * <p>
 * If you wish some property to be restated the same way as script, mark it with
 * {@link StatefullScript} too.
 * </p>
 */
public interface StatefullScript extends Serializable {
  /**
   * This method called each time script is initializing. Pay attention, that
   * script saver is not Serializable, so should be marked as transient
   */
  ScriptSaver getScriptSaver();

}
