/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.scripts;

import com.flamesgroup.unit.PhoneNumber;

/**
 * Controls voip to gsm call
 * <p>
 * You can control what numbers is allowed to call. Or made limitations of how
 * much calls can be made to specified pattern.
 * </p>
 */
public interface CallFilterScript {
  /**
   * Determines whether to allow outgoing call
   * <p>
   * Called each time voice server wants to use card for outgoing call. If
   * true returned then {@link #substituteBNumber(PhoneNumber)} is called.
   * </p>
   *
   * @param aPhoneNumber number of caller
   * @param bPhoneNumber number of future call
   * @return true if phone number is accepted
   */
  boolean isCallAccepted(PhoneNumber aPhoneNumber, PhoneNumber bPhoneNumber);

  /**
   * Substitute number with another one
   * <p>
   * This can be done to cut long prefixes, or country code, etc.
   * </p>
   * <p>
   * Simplest implementation should return number passed as argument.
   * </p>
   *
   * @param bPhoneNumber source number
   * @return number which is used to make call
   */
  PhoneNumber substituteBNumber(PhoneNumber bPhoneNumber);

}
