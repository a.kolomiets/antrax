/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.ui.guiclient.widgets.table;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

public class NumericPropertyEditor<T extends Number> extends BasePropertyEditor<T> {

  private final Class<T> type;
  private final JSpinner spinner = new JSpinner();

  public NumericPropertyEditor(final Class<T> type) {
    this.type = type;

    DecimalFormat decimalFormat;
    if (isFloatingPointType(type)) {
      double fraction = 0.1;

      decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance();
      decimalFormat.setGroupingUsed(false);
      decimalFormat.setMinimumFractionDigits(1);

      JSpinner.NumberEditor numEditor = new JSpinner.NumberEditor(spinner);
      spinner.setEditor(numEditor);

      NumberFormatter numFormatter = new NumberFormatter(decimalFormat);
      numFormatter.setValueClass(Double.class);

      JFormattedTextField jftf = numEditor.getTextField();
      jftf.setFormatterFactory(new DefaultFormatterFactory(numFormatter));

      SpinnerNumberModel spinnerModel = new SpinnerNumberModel(0.0d, -Double.MIN_VALUE, Double.MAX_VALUE, fraction);
      spinner.setModel(spinnerModel);
    } else {
      decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance();
      decimalFormat.setDecimalSeparatorAlwaysShown(false);
      decimalFormat.setMinimumFractionDigits(0);
      decimalFormat.setGroupingUsed(false);

      JSpinner.NumberEditor numEditor = new JSpinner.NumberEditor(spinner, decimalFormat.toPattern());
      spinner.setEditor(numEditor);
    }
    ((JSpinner.NumberEditor) spinner.getEditor()).getTextField().addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        fireEditingStopped();
      }
    });
  }

  private static boolean isFloatingPointType(final Class<?> type) {
    return (Float.class.equals(type) || Double.class.equals(type));
  }

  @Override
  public Class<? extends T> getType() {
    return type;
  }

  @Override
  public Component getEditorComponent() {
    return spinner;
  }

  @Override
  public void setValue(final T value) {
    spinner.setValue(value);
  }

  @Override
  public T getValue() {
    JSpinner.NumberEditor editor = (JSpinner.NumberEditor) spinner.getEditor();
    String textValue = editor.getTextField().getText();
    Number parsedValue;
    try {
      parsedValue = editor.getFormat().parse(textValue);
    } catch (ParseException ignored) {
      return null;
    }
    try {
      if (Byte.class.equals(type)) {
        return type.cast(parsedValue.byteValue());
      } else if (Short.class.equals(type)) {
        return type.cast(parsedValue.shortValue());
      } else if (Integer.class.equals(type)) {
        return type.cast(parsedValue.intValue());
      } else if (Long.class.equals(type)) {
        return type.cast(parsedValue.longValue());
      } else if (Float.class.equals(type)) {
        return type.cast(parsedValue.floatValue());
      } else if (Double.class.equals(type)) {
        return type.cast(parsedValue.doubleValue());
      }
    } catch (NumberFormatException ignored) {
      return null;
    }

    return null;
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    //final NumericPropertyEditor<Integer> numericPropertyEditor = new NumericPropertyEditor<Integer>(Integer.class);
    final NumericPropertyEditor<Float> numericPropertyEditor = new NumericPropertyEditor<>(Float.class);

    frame.getContentPane().add(numericPropertyEditor.getEditorComponent());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent e) {
        System.out.println(numericPropertyEditor.getValue());
      }
    });

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

}
