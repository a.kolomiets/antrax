/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.ui.guiclient.widgets.table;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class BooleanPropertyEditor extends BasePropertyEditor<Boolean> {

  private final JCheckBox editorComponent;
  private final BooleanPropertyTableRenderer renderer;

  public BooleanPropertyEditor() {
    this.editorComponent = new JCheckBox();
    this.editorComponent.setHorizontalAlignment(JLabel.CENTER);
    this.editorComponent.setRequestFocusEnabled(false);
    this.renderer = new BooleanPropertyTableRenderer();
    editorComponent.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        fireEditingStopped();
      }
    });
  }

  @Override
  public Class<? extends Boolean> getType() {
    return Boolean.class;
  }

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Boolean getValue() {
    return editorComponent.isSelected();
  }

  @Override
  public void setValue(final Boolean value) {
    editorComponent.setSelected(value);
  }

  @Override
  public Component getRendererComponent(final Boolean value) {
    return renderer.getRendererComponent(value);
  }

}
