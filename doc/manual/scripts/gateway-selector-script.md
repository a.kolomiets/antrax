## Gateway Selector Script

**Gateway Selector Script** is responsible for Voice server selection for SIM card. Can permit or deny the work of SIM card on particular Voice server. Sim card rotation among servers can be arranged with its help, for instance, card can sequentially move among stated Voice servers or register on any Voice server, except previous one. Gateway Selector Script is performed on SIM server.

![gw_menu](gateway-selector-script.assets/gw_menu.png)

### Any except previous

SIM card registers on any Voice server, except stated quantity of the last ones, where it was previously registered.

![gw_any_except_previous](gateway-selector-script.assets/gw_any_except_previous.png)

### Any gateway

SIM card is registered on any  Voice server.

![gw_any_gateway](gateway-selector-script.assets/gw_any_gateway.png)

### Specified order

SIM card is registered according to stated sequence list on Voice servers.

![gw_specified_order](gateway-selector-script.assets/gw_specified_order.png)

Sequence list management of Voice servers is performed by context menu. Actions with selected item from the list are available:

| Name| Description|
| -------- | -------- |
|  Up | move selected item one up |
|  Add | add the item after selected |
|  Down | move selected item one down |
|  Remove | delete selected item |
