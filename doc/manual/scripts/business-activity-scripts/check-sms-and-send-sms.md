## check SMS and send SMS

This script enables to process the incoming SMS and send SMS to same number.

![check-sms-and-send-sms](check-sms-and-send-sms.assets/check-sms-and-send-sms.png)

### Parameters

| name| description|
| -------- | -------- |
| incoming sms text regex | pattern check incoming  SMS |
| send sms text | text for send SMS after receiving incoming SMS |
| sms number pattern | regex for check incoming SMS number |
| sms number replace pattern | the pattern for replacing incoming SMS number. Instead '$1' will be paste value from 'sms number pattern' which place between '(' and ')' |
| sms timeout | time for waiting for incoming SMS after receive event |
| event | event for start executing this script |

