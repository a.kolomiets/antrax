## USSD dialog pay

Check balance and if less than minimum amount pays via USSD from a registry

![ussd_dialog_check_balance](ussd-dialog-pay.assets/ussd-dialog-pay.png)

### Parameters

| name| description|
| -------- | -------- |
|  ussd | number of USSD request |
|  event | event essential for script activation |
|  minimum amount | minimum amount of money which is enough to cancel payment. Put 0 here to discard balance check |
|  ussd responses | regular expressing for parsing ussd response and generating answers. Allowed regex '$(\d{1,2}+\d{1,2})?' to send split or all digits of vouchers |
|  payed responses | regular expressions for parsing payed response and generating events |
|  balance check data | check current balance of SIM card. Checking may take up to 4 minutes |
|  amount | amount of money to pay |
|  lock on fail | locks card on payment failure |
|  skip intermediate states | skip the intermediate states like failed.1, failed.2, failed.3 |
|  operator | operator name is used to determine registry key where to look for vouchers |
