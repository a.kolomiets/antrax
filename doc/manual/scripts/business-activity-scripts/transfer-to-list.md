## Transfer to list

The script is used for money transferring from source-card to one of receiver-cards, selected from the list.

![transfer_to_list](transfer-to-list.assets/transfer_to_list.png)

## Parameters

| name| description|
| -------- | -------- |
|  event | event essential for script activation |
|  bill check data | balance check parameters. |
|  amount | amount of money for transfer |
|  lock on fail | need of blocking card in the case of failure |
|  USSD request | combination which initiates the transfer of given amount of money (inserted instead of '$')\\ to the specified phone number (inserted instead of '@') |
|  response patterns list | regular expressions to analyze the response from operator and send confirmation of money transfer |
|  remains amount | amount of money that must remain on balance  after transfer |
|  transfer all | transfer all money with exception of given amount remaining on balance |
|  store name | unique name of phone numbers' storage in registry. It is recommended\\ to use name of SIM group as a value if there are no SIM groups in the system with the same\\ names |
|  add new number to the list on call | add new phone numbers to the list automatically during call |
|  allowed transfers count per day | amount of transfers that can be made to each card per day |
|  remove numbers older than defined hours | if card has not processed any call during stated period of time it is regarded outdated or its phone number is removed from the list. For deactivation of automatic removal of phone numbers set to 0 |

Script supports several lists with phone numbers. Names of lists are based on **store name** parameter value. Amount of lists depends on **allowed transfers count per day** parameter value. For example, if the parameter values are the following:

store name = group
allowed transfers count per day = 3

lists in the register will appear as follows:
 
```
group.main
group.1
group.2
group.used
```

It should be noted that if the list has no phone number, it will not be displayed in the Registry tab from  UTILITY section.

 
### Money transfer

Search of card-receiver is carried out in all lists with exception of group.used list. At first list group.main s checked, then group.1, etc. Numbers from group.used list with more than one day state are moved to group.main list before each search of card-receiver.

For example, there are two numbers XXX and YYY in group.main list.

```
group.main
    XXX
    YYY
group.1
group.2
group.used
```

After first money transfer from XXX card to YYY card state of groups appears as following:

```
group.main
    XXX
group.1
    YYY
group.2
group.used
```

It will fall down into the lower list until gets to the group.used list after every money transfer into card with YYY number.\\ State of the lists after 3 transfers into YYY card:

```
group.main
    XXX
group.1
group.2
group.used
    YYY
```

### Automatic rotation of numbers in the lists

Implementation of the script provides an automatic addition of new and automatic removal of outdated numbers.

Number will be automatically added into the group.main list after any call of this card if **add new number to the list on call** parameter was selected in the script settings (only in case when phone number is not situated in any list).

Phone number state of the card, which has processed call, upgrades in the list every time during call processing. If the count of the **remove numbers older than defined hours** parameter is more then zero then before every searching of card-receiver from the lists, except group.used, card numbers will be deleted. This function is true for card numbers which have the time period from the last call more than **remove numbers older than defined hours** parameter. Also this function was made for automatic excluding of card numbers from the list which haven't got any activity and were removed allegedly from the system.

Note that user can change content of the lists in Registry tab of UTILITY section. It does not depend on active state of automatic phone numbers' rotation.

