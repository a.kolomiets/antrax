## ask for ussd
 
Given script requests for money transfer from random SIM card in the system. Script operates in connective with script ussd action in **Action Provider Script**.

![askforussd](ask-for-ussd.assets/askforussd.png)

### Parameters

| name| description|
| -------- | -------- |
|  event on success | generate the event in case of successful ussd performance |
|  event on failure | generate the event on failure |
|  event | an event which requests for money transfer |
|  action | name of an action which will "request" for money transfer |

The concept of operation is similar to ExecuteAction.

 

