## check balance SMS

Given script is responsible for balance check on incoming SMS and generates the events in accordance with balance.

![check_balance_sms](check-balance-sms.assets/check_balance_sms.png)

### Parameters

| name| description|
| -------- | -------- |
|  minimum amount of money | essential amount of money on balance |
|  balance check data | data essential for script to analyze the reply |
|  event on more | event, which will be generated, if money on balance exceed or equals the index in minimum amount of money field   |
|  parse alternative SMS responses | if the alternative SMS texts require checking |
|  alternative SMS response | alternative expression for SMS check-up |
|  event on less | event, which will be generated, if money on balance is less than index in minimum amount of money field |
