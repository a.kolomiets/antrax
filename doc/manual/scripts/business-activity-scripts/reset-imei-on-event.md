## reset imei on event

After given script is fulfilled the card performs the reset of  IMEI.

![reset_imei_on_event](reset-imei-on-event.assets/reset_imei_on_event.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event on fail** | event on failure of **event** |
|  **event** | event necessary for script activation |
|  **event on success** | event generated on  **event** failure|


 

