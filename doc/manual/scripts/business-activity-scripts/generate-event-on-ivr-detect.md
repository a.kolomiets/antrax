## generate event on ivr detect

Given script is designed to perform any action when received system event 'ivr detect'.

![g_e_on_ivr](generate-event-on-ivr-detect.assets/g_e_on_ivr.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event, which is generated when **amount ivr detects** is reached |
|  **amount ivr detects** | amount of ivr detect to generate event |
|  **consecutive count** | take into account the sequence of ivr detects |
|  **ivr template name pattern** | generate event only for specific ivr template names |

