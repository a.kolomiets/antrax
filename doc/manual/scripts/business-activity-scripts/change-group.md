## change group

The task of the given script is to change the SIM group for the card.

![change_group](change-group.assets/change_group.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | initial event essential for script activation |
|  group | name of SIM group where SIM card can be transferred |
