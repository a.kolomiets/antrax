## generate event on dial error

Given script is designed for any event performance when a particular error was detected when dialing the number.

![g_e_on_dial_error](generate-event-on-dial-error.assets/g_e_on_dial_error.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event, which is generated when **dial errors count** is reached |
|  **dial errors count** | amount of calls with dial error |
|  **call error cause** | dial error code |
