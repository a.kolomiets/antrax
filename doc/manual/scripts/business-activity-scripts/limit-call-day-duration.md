## limit day call duration

Given script is designed for limitation of call duration per day. When particular call duration is reached - call ring off is performed.

![limit_day_call_du](limit-call-day-duration.assets/limit_day_call_dur.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event which is generated when **drop limit** is reached |
|  **duration limit** | call duration |
|  **drop limit** | quantity of rejected calls when **duration limit** is reached |
|  **block of seconds** | proximity of call duration |

