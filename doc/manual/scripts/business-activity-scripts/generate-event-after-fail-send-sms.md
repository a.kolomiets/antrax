## generate event after fail send SMS

Given script is designed to perform any action after definite amount of fail send SMS.

![generate-event-after-fail_send-sms](generate-event-after-fail-send-sms.assets/generate-event-after-fail-send-sms.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | generate this event after the amount of fail send SMS specified by this script |
|  count consecutive sms | count consecutive fail send SMS. For example, if this field is true then 2 fail to send SMS 1 success to send SMS then counter will be reset, if this  field is false, then counting continue |
|  sms limit | limit of send fail sms. This filed allow set a range. At start of count, script generate random value from this range |

