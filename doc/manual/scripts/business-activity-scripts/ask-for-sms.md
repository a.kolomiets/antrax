## ask for SMS
 
Given script requests for incoming SMS from random SIM card in the system. The script operates in conjunction with SMS action in **Action Provider Script**.

![askforsms](ask-for-sms.assets/askforsms.png)

### Parameters

| name| description|
| -------- | -------- |
|  lock on fail | whether to block the card in case of failure |
|  event on success | generate the event in case of successful SMS performance |
|  event on failure | generate the event in case of failure |
|  event | an event which requests for SMS |
|  action | name of the action which will request for SMS |
|  execute time | timeout script execution, after which the script will be canceled |

The concept is very similar to ExecuteAction.
