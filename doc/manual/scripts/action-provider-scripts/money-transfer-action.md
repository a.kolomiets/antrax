## Money transfer action

The script is responsible for money transfer between cards, USSD menu navigation.

![money_transfer](money-transfer-action.assets/money_transfer.png)

### Parameters

| name| description|
| -------- | -------- |
|  bill check data | data necessary for bill check  |
|  amount | an amount of money for transfer |
|  use USSD session | use USSD session instead of separate USSD |
|  provided action | action name initiating this script fulfillment |
|  USSD request | an event essential for script activation |
|  response patterns list | regular expression to analyze requests and generate USSD answers |
|  remains amount | minimum amount of money, which must remain on your balance for successful money transfer |
|  allow check event | an event which allows checking the balance |
|  checking failed event | an action to detect an error when checking |
|  action failed event | an event on unsuccessful script performance |
|  action impossible event | an event when the action is impossible to perform |

 

