## Incoming Call Management Script

Incoming Call Management Script - script is responsible for reception of incoming calls. With its help you can set up the likelihood of incoming call reception as well as its duration. Incoming Call Management Script is carried out on  Voice server.

![menu](incoming-call-management-script.assets/menu.png)

### Chances based

Script is responsible for retention of incoming call by SIM card in accordance with active time framework and interest ratio of incoming call recertion .

![chanses_based](incoming-call-management-script.assets/chanses_based.png)

|  Menu |  Parameter |
| -------- | -------- |
|  Active interval | time period of incoming call retention, that is a period from the moment the receiver was picked up till the end of call, when this interval is over the script will finish the call |
|  Idle interval | dialing time, that is the time from start of the call till the receiver is picked up  |
|  Accept chance | likelihood of incoming call  reception |

For proper script performance idle interval parameter must not exceed 40 seconds (approximate dialing time in GSM). Otherwise GSM will finish the call earlier, than script will be able to pick up the receiver.

### Origination call

![origination_call](incoming-call-management-script.assets/origination_call.png)

|  Menu |  Parameter |
| -------- | -------- |
|  Caller number pattern | Calling number pattern |
|  Caller number replace pattern | Replace pattern of calling number |
|  Caller number | Calling number |
|  Target address | IP for reroute call |
