## CONFIGURATION

Configuration section contains subsections:

| Name of section | Description of subsection |
| -------- | -------- |
| Servers | editing of servers' list  and their interfaces |
| GSM groups | editing GSM groups' list and their connection with SIM groups |
| SIM groups | SIM cards' behavior setting  |
| Scripts | script archive downloading |
| Voip AntiSpam | AntiSpam system setting |

### Servers table

Servers list activity is performed only on condition of open transaction.

![servers](configuration.assets/servers.png)

Servers table contains the following parameters:

| Name | Description |
| -------- | -------- |
| Name | server name |
| Public interface | public interface |
| Voice server | presence of Voice service on server |
| SIM server | presence of SIM service on server |

Buttons and filters for activities with servers'table:

| Button filter name  | Description |
| -------- | -------- |
| ![add_button](configuration.assets/add_button.png) | button of server adding |
| ![del_button](configuration.assets/del_button.png) | button of server removal |
| ![filter_search](configuration.assets/filter_search.jpg) | filter of information displaying in the servers' table on the specified parameters |

### Adding Servers

After clicking the button for adding a new server, a pop-up menu appears:

![add_new_server](configuration.assets/add_new_server.png)

| Name | Description |
| -------- | -------- |
| Server name | name of server |
| Public interface | IP address for link with server |

### Servers' configuration editing

Editing of selected server configuration is carried out on condition of open transaction.

![reconfigure_server](configuration.assets/reconfigure_server.png)

| Name | Description |
| -------- | -------- |
| ![server_name](configuration.assets/server_name.png) | server name |
| ![public_interface](configuration.assets/public_interface.png) | IP address for connection with server |
| ![vs_enable](configuration.assets/vs_enable.png) | Voice Server option |
| ![ss_enable](configuration.assets/ss_enable.png) | Sim Server option |

### Server removal

Visual sequence of activities to remove server from the servers table:

![delete_server](configuration.assets/delete_server.png)

Servers removal is carried out according to the following sequence of actions:
1. Open transaction to make changes in the servers table.
2. Select the server which needs removing.
3. Push the button of server removing.
4. Close transaction with the application of changes made.

### The concept of interfaces

Network interfaces enable any server with the Routing and Remote Access to connect to other computers through private or public networks. Network interfaces are two aspects associated with the Routing and Remote Access: the physical equipment, such as a network adapter, and the network interface configuration. In our case, the network interface is considered as the IP address of the virtual interface or network adapter.

### Public interface

Program modules of the Antrax system may be located on different servers. Therefore, to interact with each other, they should know where to send packets. For this purpose we introduce the concept of public interface - it is the IP address where you can find this module.

#### Devices' interface

Network interfaces enable any server with the Routing and Remote Access to connect to other computers through private or public networks. Network interfaces are two aspects associated with the Routing and Remote Access: the physical equipment, such as a network adapter, and the network interface configuration. In our case, the network interface is considered as an IP address of the virtual interface or network adapter. Hardware modules of Antrax system use tcp/udp packets in their activities.

### GSM groups

![main_gsm_group_table](configuration.assets/main_gsm_group_table.png)

### Parameters of GSM groups

| Button filter name  | Description |
| -------- | -------- |
| ![add](configuration.assets/add.png) | button for GSM group adding |
| ![del](configuration.assets/del.png) |button for GSM group removal  |
| ![copy](configuration.assets/copy.png) |button for GSM group copy  |
| ![name](configuration.assets/name.png) | GSM group name  |
| ![imsi_catcher](configuration.assets/imsi_catcher.png) | IMSI cacher detection parameter |
| ![lock_gsm_channels](configuration.assets/lock_gsm_channels.png) | parameter of GSM channels' blocking on detection on IMSI cacher |
| ![link_gsm_with_sim_groups](configuration.assets/link_gsm_with_sim_groups.png) | menu of connection establishment between GSM groups and SIM groups |
| ![select_all](configuration.assets/select_all.png) | button for connection establishment with all SIM groups |
| ![deselect_all](configuration.assets/deselect_all.png) | button to break the connection with all SIM groups |
| ![filter_search](configuration.assets/filter_search.jpg) |filter of information displaying in servers table on specified parameters |

### Choosing SIM card for GSM channel connection

One of the tasks for Voice server is a formation of voice channels. Voice channel is created by means of joining SIM and GSM channels following the registration in GSM network. Created voice channels are ready to process the calls and other activity (send SMS, top up the balance, etc). Voice channels appear in particular order in the list according to the Voice Server Factor Script.

Formation of voice channels is performed continuously and doesn't depend on presence or absence of calls. Task of Voice server is a connection of all free GSM channels and SIM channels. Voice server repetedly requests SIM cards from SIM server.

Disconnection of voice channel occurs according the settings of SIM group,which contains the given SIM card involved in particular voice channel. Voice server also requests information about time for voice channel disconnection from SIM server.

First channel from the list of ordered voice channels will be active on new call. In case of free voice channels missing, Voice server will reject the call.

![sim_getting](configuration.assets/sim_getting.jpg)

The process of SIM channel selection in order to join it to GSM channel is displayed on the picture. Given Voice channel contains several GSM channels belonging to Agsm and Bgsm groups. As it is shown on the picture below, GSM groups are connected to SIM groups by the following way:  Agsm <>  Asim, Bgsm <> Bsim.

![gsm_groups](configuration.assets/gsm_groups.png)

Lets consider the process of connection of GSM channels from Agsm group with suitable SIM channel. As Agsm group connects with Asim group, therefore SIM channel must belong to the Asim group. As you can see on the picture, Voice server does not have any SIM channels belonging to Asim group. That's why Voice server requests SIM channels from SIM server. SIM server chooses all GSM channels from Asim group and sorts it in accordance with SIM Server Factor Script. First SIM channel will be taken from this sorted list. According to Session Period Script checking process takes place: if selected SIM channel can go to the Voice server at the moment. In case of positive result such checking takes place: if selected SIM channel can go to the current Voice server (Gateway Selector Script). SIM channel is transferred to Voice server if both conditions are true. In case of failure of any condition (Session Period Script or Gateway Selector Script), next SIM channel will be chosen from the selection list and same checking process will take place.

Then system checks if SIM channel can register in GSM network at the moment (Activity Period Script). In case of positive result GSM and SIM channels connect with each other and create new voice channel, which are placed into the list of voice channels on Voice server. If received SIM channel can't register in GSM network at the moment, then Voice server requests SIM server for one SIM channel one more time.

On picture below you can see block-scheme for choosing algorithm of SIM channels.

![enu_block_diagramm](configuration.assets/enu_block_diagramm.jpg)

It is worth mentioning, that SIM channels remain on the Voice server while conditions of Session Period Script are performed. In case of failure of any condition, voice channel is destroyed and specified SIM channel goes back to SIM server.

### SIM group settings

SIM group setting is divided into the following sections:

| Section name | Description of section functions | 
| -------- | -------- |
| Timeouts | statistical intervals during SIM card activity |
| Script Commons | scripts setting simplifying technique |
| IMEI Generator Script | scripts of IMEI generation methods |
| Call filter | scripts that set parameters for out-coming calls to GSM network |
| SMS Filter | scripts that set parameters for out-coming SMS messages to GSM network |
| Buisness Activity Scripts | scripts that set the behavior of SIM card |
| Action Provider Script | supporting activity scripts of Business Activity Scripts section |
| Sim Server Factor Script | scripts that set parameters of SIM card selection for Voice server |
| Voice Server Factor Script | scripts that set parameters for choosing voice channel for the call |
| Gateway Selector Script | scripts that set parameters for choosing Voice server for SIM card |
| Incoming Call Management Script | scripts that set parameters for answering incoming calls |
| Activity Period Script | scripts that manage the activity of SIM card |
| Session Period Script | scripts that manage SIM card session  |
| Description | field for notes in SIM group |

### Timeouts

Timeout is a period of time, during which the voice channel is not used for calls. To set up the SIM card behavior there are several types of timeouts:

| Parameters| Description|
| -------- | -------- |
| Name | name of SIM group |
| Operator selection | **Parameters of selecting GSM network:** <br> **auto** - automatic selection of GSM network <br> **home** - selection of home GSM network <br> **custom** - manual selection of GSM network |
| Can be appointed | if any SIM card can be added to the specified group in SIM CHANNELS tab |
| Enable synthetic ringing | substitute GSM network beeps when calling the subscriber with the beeps built in Antrax system |
| FAS Detection | Checking FAS (block the operator's messages about the lack of money on balance and etc.) |
| Enable extended AT command | block the signal of another incoming call during the conversation ( if cards of the specified SIM group refuse to create the channel, it is required to remove the checkmark) |
| Idle after successfull call | idle time between calls |
| Idle after zero call | idle time after zero call |
| Idle before unregistration | idle time before card's leaving the network |
| Idle after registration | idle time after registration |
| Idle before self call | idle time before conducting the incoming call |
| Idle after self call | idle time after incoming call |
| Self dial factor | quantity of out-coming calls with SIM cards from the the specified SIM groups, after which incoming call is possible |
| Self dial chance | chance of receiving the call from GSM |
| Registration period, minutes | period during which SIM card should register (period is specified in minutes) |
| Registration count by period | quantity of SIM cards that can register for the given period |

### Script Commons

Script Commons - is a mechanism, that enables to simplify the script setting forced by using one common item (commons) for assigning similar settings to different scripts. Commons consists of name (a unique identifier), type (any type of script fields can be used) and value (definite value of given type). Using this mechanism is not obligatory, however it enables to simplify the setting, especially if repeated elements are available, and minimize the errors during the setting.

### Script archive of Antrax system

Parameters Antrax system script archive:

| Parameter | Description/Function |
| -------- | -------- |
| ![import_archive_of_scripts](configuration.assets/import_archive_of_scripts.png) | button to download the script archive of Antrax system |
| File name | name of current script archive of Antrax system |
| Size | size of current script archive of Antrax system |
| Version | version of script archive of Antrax system |
| Check sum | check sum of Antrax system script archive |
| Compilation time | time of establishment of Antrax system script archive |
| Upload time | uploading time of script archive to the system |
| File count | quantity of utilized scripts in current archive |
| Class count | the quantity of utilized classes in the current classes |

![import_scripts](configuration.assets/import_scripts.png)

Antrax system script archive downloading is possible on condition of open transaction, on closing the transaction with application pop-up menu is displayed: 

![script_archve_apply](configuration.assets/script_archve_apply.png)

### VoIP AntiSpam configuration

![voipantispam](configuration.assets/voipantispam.png)

Parameters of Antispam system system filtration:

| Parameter | Description |
| -------- | -------- |
| Enable Voip Anti Spam | option of VoIP AntiSpam system activation |
| Gray list | **Gray list parameter:** <br> **Period** - period of time, during which the quantity of requests for routing (calls) is counted, minutes <br> **Max routing request per period** - maximal quantity of requests for routing (calls) for the specified period of time <br> **Block period** - period of time during which the number is blocked in the grey list, minutes |
| Black list |**Parameters of Black list:** <br> **Period** - period of time, during which the quantity of requests for routing (calls) is counted, minutes <br> **Max routing request per period** - maximal quantity of requests for routing (calls) for the specified period of time <br> **Block period** - maximal quantity of times when the number was blocked in the grey list, after which the number is automatically transferred in the black list |

When the ACD option is enabled:

![adc_antispam](configuration.assets/adc_antispam.png)

| Parameter | Description |
| -------- | -------- |
| ACD Analyze | Option of ACD analysis activation |
| Period | period of time, during which the calls with small duration are counted |
| Max routing request per period | maximal quantity of calls with small duration to be enrolled to grey list |
| Max block count before move to black list | maximal quantity of times being present in grey list |

When the Alerting Analyze option is enabled:

| Parameter | Description |
| -------- | -------- |
| Alerting Analyze | Option to enable Alerting Analysis |
| Alerting time | Period from the beginning of the ringing alarm, during which the cancellation of the call will be taken into account |
| Period | The time interval for which calls are counted |
| Max routing request per period | The maximum number of calls for routing calls for a given period of time |

When the FAS Analyze option is enabled:

| Parameter | Description |
| -------- | -------- |
| FAS Analyze | Option to enable FAS Analyze |
| Period | The time interval for which calls are counted |
| Max routing request per period | The maximum number of calls for routing calls for a given period of time |

### Prefix mark lists

The functions of the mechanism of marking lists are that when a number falls into one of the created groups, add a prefix assigned to it, if a number does not fall into any group, a default prefix should be added, which can be empty.
To perform any actions on the data, you must first select a record from the table.

![prefix_mark_lists](configuration.assets/prefix_mark_lists.png)

| Parameter | Description |
| -------- | -------- |
| Add | Adding a prefix to the list |
| Edit | Editing the tagging prefix in the list |
| Del | Remove a prefix in the list |
| Clear | Cleaning marking list |
| Import | import marking list |
| Export | export marking list |

**Steps to add a prefix:**

  - Add prefix. Press "Add" key, enter "Name" and "Prefix".
  - Select additions prefix from the table, click on the "Import" button. In the menu that appears, choose one of the options for resolving number conflicts (Ignore on duplicate  Update on duplicate).
  - Select file with numbers (csv file), numbers can be separated by any character except digits and "#"
**Ignore on duplicate** - The numbers that already exist in other prefixes will be ignored.
**Update on duplicate** - Numbers that already exist in other prefixes will be added to the current prefix and will be removed from the duplicate.
