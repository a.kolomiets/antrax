## VOICE SERVERS

Monitoring of voice channel status and manual mode activity with channels is carried out in VOICE SERVERS section.

### 1. Manual mode of work with channels

![user_interface](voice-server.assets/user_interface.png)

You will find the element assignment of graphic user interface below.

| Button | Action |
| -------- | -------- |
| ![nr_vs_refresh](voice-server.assets/nr_vs_refresh.jpg) | manual synchronization of working area info |
| ![nr_vs_export_excel](voice-server.assets/nr_vs_export_excel.jpg) | export of working area information to excel file |
| ![enable_call](voice-server.assets/enable_call.png) | enable selected sim-card for making calls |
| ![disable_call](voice-server.assets/disable_call.png) | disable selected sim-card for making calls |
| ![nr_vs_generate_event](voice-server.assets/nr_vs_generate_event.jpg) | manual generating of unique event |
| ![nr_vs_lock_gateway](voice-server.assets/nr_vs_lock_gateway.jpg) | ban on the use of selected GSM channels |
| ![nr_vs_unlock_gateway](voice-server.assets/nr_vs_unlock_gateway.jpg) | lifting the ban on using selected GSM channels |
| ![network_survey](voice-server.assets/network_survey.png) | GSM network survey in manual mode |
| ![arfcn_lock](voice-server.assets/arfcn_lock.png) | setting up the arfcn for the GSM channel |
| ![unlock_arfcn](voice-server.assets/unlock_arfcn.png) | reset of the arfcn setting for the GSM channel |
| ![send_ussd](voice-server.assets/send_ussd.jpg) | manual USSD sending on selected GSM channels |
| ![send_sms](voice-server.assets/send_sms.jpg) | manual SMS sending on selected GSM channels |
| ![send_dtmf](voice-server.assets/send_dtmf.jpg) | manual DTMF setting on selected  GSM channels |

|  Menu/Filter | Action |
| -------- | -------- |
| ![nr_vs_gsm_group](voice-server.assets/nr_vs_gsm_group.jpg) | adding/reassignment to particular GSM  group selected GSM channels in case of open transaction |
| ![nr_vs_show_unvaiable_channels](voice-server.assets/nr_vs_show_unvaiable_channels.jpg) | display filter of unavailable GSM channels |
| ![nr_vs_filter](voice-server.assets/nr_vs_filter.jpg) | Filter of information insertion to working area for the specified parameters |

#### Manual synchronization of working area information

When pressing the working area information synchronization button, the proof-reading of relevant information is performed , you can track the information update status on GUI client status bar.

#### Export of working area information to excel file

Pressing the button current information from working area will be exported to excel file, at the same time pop-up menu is invoked in order to choose the path to file saving location.

![save_excel](voice-server.assets/save_excel.png)

#### Manual generating of unique event

Pressing the button of generating unique event,pop-up menu is invoked, where in Event box you inset the name of event which is required to be called up according to assigned SIM group

![screenshot-generate_event_to_test_sim_group_scripts](voice-server.assets/screenshot-generate_event_to_test_sim_group_scripts.png)

#### Ban on using selected GSM channels

Prior to pressing the button ban on using GSM channels, you need to select particular GSM channel, then pop-up menu is called up, where in the  "Enter lock GSM reason" box insert the reason for ban on using  GSM channel.

![lock](voice-server.assets/lock.png)

#### Lifting the ban on using selected GSM chanels

Pressing the button lifting the ban on using selected GSM channels you need to select required GSM channels , then pop-up menu is called up, where in «Enter unlock GSM reason» box you insert the the reason for lifting the ban on using GSM channels.

![unlock](voice-server.assets/unlock.png)

#### GSM network monitoring in manual mode

When using the button, the current GSM network information is read out from a dedicated GSM channel, after the information is displayed in the GSM-view table according to the server where this function was performed.

#### Setting the absolute radio frequency number (arfcn) for the GSM channel

When using the button, you can set a specific absolute radio frequency number for the selected GSM channels in the pop-up menu.

![arfcn_lock_full](voice-server.assets/arfcn_lock_full.png)

#### Canceling the settings of the absolute radio frequency number (arfcn) for the GSM channel

When using the button, system reset settings absolute radio frequency non-selected GSM channels.

#### Manual USSD sending on selected GSM channels

Pressing the button manual USSD sending, pop-up menu is called up, on previously selected GSM channels . Insert USSD request in pop-up menu, which is required to be called.

![ussd_panel](voice-server.assets/ussd_panel.png)

On successful USSD request sending, a notifying window pops-up.

![sms_sent](voice-server.assets/sms_sent.png)

If the result is a dialog with a response, it is possible to continue to execute USSD requests. Need to press the desired button.

![ussd_dialog_sent](voice-server.assets/ussd-dialog-send-botton.png)

On false USSD request sending, a notifying window pops-up.

![ussd_error_send](voice-server.assets/ussd_error_send.png)

#### Manual SMS sending  on selected GSM channels

Pressing the manual SMS sending button,a pop-up menu is called on selected GSM channels.
In pop-up window:
1. Filter **Request for delivery report**, if the marker is set delivery report will be displayed.
2. In the **"Number"** box insert the number which SMS will be sent to - press **"Add"** button, after that the number is added to the list below.
3. You can clear out the number list with the help of **"Delete"** button and previously selected numbers from the list.
4. Below the number list there is a window to type the message text in.
5. Pressing **"Send"** button send SMS and pressing **"Cancel"** button cancel the sending of  SMS.

![sms_panel](voice-server.assets/sms_panel.png)

On successful SMS sending the notifying window will appear.

![sms_sent](voice-server.assets/sms_sent.png)

On false SMS sending the notifying window will appear.

![sms_error_send](voice-server.assets/sms_error_send.png)

#### Manual DTMF setting on selected GSM channels

Pressing the button manual DTMF setting, pop-up window is called on previously selected GSM channel.
Insert to pop-up menu DTMF setting.

![dtmf_panel](voice-server.assets/dtmf_panel.png)

On successful DTMF setting notifying window will appear.

![dtmf_sent](voice-server.assets/dtmf_sent.png)

On false DTMF setting a notifying window will appear.

![dtmf_error_send](voice-server.assets/dtmf_error_send.png)

#### Adding/reassigning selected GSM channels to particular GSM group

In order to add/reassign selected GSM channels to particular GSM group.

![gsm_channel_change](voice-server.assets/gsm_channel_change.png)

#### Work with display filter of unavailable GSM channels

| ![nr_vs_show_unvaiable_channels](voice-server.assets/nr_vs_show_unvaiable_channels.jpg) | a marker set on filter displays all unavailable GSM channels in VOICE SERVERS table |
| -------- | -------- |

#### Work with information insertion filter

| ![nr_vs_filter](voice-server.assets/nr_vs_filter.jpg) | In order to filter information inserted in working area, you need to insert data required for search in filter box. As you insert the information, the data on the screen will alter |
| -------- | -------- |

### 2. Possible statuses of Voice server

Following statuses are possible for Voice server:

| Icon | Status|
| -------- | -------- |
| ![gateway-started](voice-server.assets/gateway-started.png) | Voice server operates |
| ![gateway-noconnection](voice-server.assets/gateway-noconnection.gif) | Voice server is switched off |


### 3. Description of GSM channel statuses

Select Voice server, you want to receive information voice channel status from. If you select VOICE SERVERS tab, the information about all Voice servers will be displayed in working area.

Following information columns are present in working area:

<table>
	<tr><th>Heading</th><th>Description</th></tr>
	<tr><td>GSM channel</td><td>number of GSM channel</td></tr>
	<tr><td>SIM holder</td><td>number of SIM module and holder of specified SIM card</td></tr>
	<tr><td rowspan="3">E</td></tr>
	<tr><td><img src="voice-server.assets/simstate_enable.png" alt="simstate_enable"> - SIM card is ready to conduct calls</td></tr>
	<tr><td><img src="voice-server.assets/simstate_disable.png" alt="simstate_disable"> - channel with SIM card is blocked</td></tr>
	<tr><td rowspan="3">L</td></tr>
	<tr><td><img src="voice-server.assets/simlockstate_unlocked.gif" alt="simlockstate_unlocked."> - GSM channel is unblocked</td></tr>
	<tr><td><img src="voice-server.assets/simlockstate_l.gif" alt="simlockstate_l"> - GSM channel is blocked</td></tr>
	<tr><td>Lock timeout</td><td>time period of channel block</td></tr>
	<tr><td>(Un)Lock reason</td><td>reason of unblocking</td></tr>
	<tr><td rowspan="14">S</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_business_activity.gif" alt="channelstate_business_activity"> - business activity</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_outgoing_call.png" alt="channelstate_outgoing_call"> - outcoming call to GSM network is conducted</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_incoming_call.png" alt="channelstate_incoming_call"> - incoming call from GSM network is conducted</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_self_call.png" alt="channelstate_self_call"> - outcoming call on the basis of scripts is conducted</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_incoming_script_call.png" alt="channelstate_incoming_script_call"> - incoming call on the basis of scripts is conducted</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_registering_in_gsm.png" alt="channelstate_registering_in_gsm"> - channel is registered in GSM network</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_after_registration.gif" alt="channelstate_idle_after_registration"> - timeout after registration</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_ready_to_call.gif" alt="channelstate_ready_to_call"> - channel is ready to accept the call</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_after_self_call.gif" alt="channelstate_idle_after_self_call"> - waiting of incoming call from the system</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_before_self_call.gif" alt="channelstate_idle_before_self_call"> - preparation for incoming call</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_after_self_call.gif" alt="channelstate_idle_after_self_call"> - end of incoming call</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_between_calls.gif" alt="channelstate_idle_between_calls"> - timeout between calls</td></tr>
	<tr><td><img src="voice-server.assets/channelstate_idle_before_unreg.gif" alt="channelstate_idle_before_unreg"> - channel leaves the GSM network</td></tr>
	<tr><td>S timeout</td><td>time period of channel in stated mode</td></tr>
	<tr><td>adv info</td><td>description of action activity</td></tr>
	<tr><td rowspan="5">CS</td></tr>
	<tr><td><img src="voice-server.assets/callstate_idle.gif" alt="callstate_idle"> - channel is ready to register a SIM card</td></tr>
	<tr><td><img src="voice-server.assets/callstate_dialing.gif" alt="callstate_dialing"> - dial of number is performed</td></tr>
	<tr><td><img src="voice-server.assets/callstate_alerting.gif" alt="callstate_alerting"> - call of subscriber</td></tr>
	<tr><td><img src="voice-server.assets/callstate_active.gif" alt="callstate_active"> - conversation</td></tr>
	<tr><td>CS timeout</td><td>time period of call in stated mode</td></tr>
	<tr><td>Phone number</td><td>telephone number being called to</td></tr>
	<tr><td>Voice Server</td><td>name of Voice server, where the specified channel is located</td></tr>
	<tr><td>SIM group</td><td>SIM group of registered SIM card on channel</td></tr>
	<tr><td>GSM group</td><td>GSM group of the channel</td></tr>
	<tr><td>Signal</td><td>level of GSM signal</td></tr>
	<tr><td rowspan="4">Lock arfcn</td></tr>
	<tr><td>Unsupported - сhannel does not support arfcn settings arfcn</td></tr>
	<tr><td>Default - default settings of arfcn</td></tr>
	<tr><td>950 - arfcn is set up</td></tr>
	<tr><td>OK calls</td><td>quantity of successful calls</td></tr>
	<tr><td>Tatal calls</td><td>overall amount of calls</td></tr>
	<tr><td>Success SMS</td><td>amount of successful SMS</td></tr>
	<tr><td>Success SMS</td><td>overall amount of SMS</td></tr>
	<tr><td>Calls dur.</td><td>overall duration of call on the specified channel</td></tr>
	<tr><td>USSD timeout</td><td>duration of current USSD request</td></tr>
	<tr><td>Last USSD</td><td>text of the last USSD response received from GSM operator</td></tr>
	<tr><td rowspan="3">Status of channel connection</td></tr>
	<tr><td><img src="voice-server.assets/live.png" alt="live"> - channel is connected</td></tr>
	<tr><td><img src="voice-server.assets/unlive.png" alt="unlive"> - channel is connected</td></tr>
	<tr><td rowspan="6">License status and network status</td></tr>
	<tr><td><img src="voice-server.assets/license_correct.png" alt="license_correct"> - license is relevant</td></tr>
	<tr><td><img src="voice-server.assets/license_invalid.png" alt="license_invalid"> - license is not valid</td></tr>
	<tr><td><img src="voice-server.assets/license_expire.png" alt="license_expire"> - license is overdue</td></tr>
	<tr><td><img src="voice-server.assets/set_ip_config_error.png" alt="set_ip_config_error"> - IP address establishment error</td></tr>
	<tr><td><img src="voice-server.assets/ip_config_parameters_invalid.png" alt="ip_config_parameters_invalid"> - wrong IP address settings</td></tr>
</table>

Pop-up menu when clicking the GSM signal level

<table>
	<tr><th>Pop-up window</th><th>Space</th><th>Value</th></tr>
	<tr><td rowspan="5"><img src="voice-server.assets/signal_hint.jpg" alt="signal_hint"></td></tr>
	<tr><td>Reg mode</td><td>network selection mode for registration (set in SIM group adjustments)</td></tr>
	<tr><td>Reg status</td><td>current registration status</td></tr>
	<tr><td>Operator</td><td>current operator</td></tr>
	<tr><td>MCC, LAC, cellID, BSID</td><td>current network settings</td></tr>
</table>
