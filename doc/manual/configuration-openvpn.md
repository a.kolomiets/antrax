## Configuration OpenVPN`s client for Linux/Windows/MacOS

### Configuration OpenVPN for Linux

#### 1. First of all, you need to install the OpenVPN package, for this you need to use the following commands:

_Debian/Ubuntu:_

    apt-get install openvpn

_CentOS 6_, before installing, you need to connect the Epel repository using the command:
    
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm

After we have downloaded this file, we need to install it through the command:

    yum install ./epel-release-latest-*.noarch.rpm

After you can install the OpenVPN package through the command:
    
    yum install openvpn

#### 2. Now you need to create the openvpn.sh file using the command in the terminal (Command line):

    >openvpn.sh

After you add these lines to this file:

##### For Debian/Ubuntu:

    #!/bin/bash
    if [ "$#" -ne 4 ]; then
        echo "Please use like: $BASH_SOURCE 192.168.100.66 client_name root qweqwe"
        echo "where 192.168.100.66 - public ip address of OpenVpn server, root - login to server, qweqwe - password"
        exit 0
    fi
    echo "=============================="
    echo "start install and configure OpenVpn client"
    echo "=============================="
    CUR_PATH=`pwd`
    
    echo "=============================="
    echo "install openvpn and easy-rsa"
    echo "=============================="
    wget -O - https://swupdate.openvpn.net/repos/repo-public.gpg|apt-key add -
    echo "deb http://build.openvpn.net/debian/openvpn/testing jessie main" > /etc/apt/sources.list.d/openvpn-aptrepo.list
    apt update
    apt install -y openvpn sshpass
    
    echo "=============================="
    echo "generate certificates"
    echo "=============================="
    sshpass -p "$4" ssh -oStrictHostKeyChecking=no "$3@$1" 'cd /etc/openvpn/easy-rsa && source ./vars && ./pkitool '"$2"
    
    echo "=============================="
    echo "copy certificates"
    echo "=============================="
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ca.crt /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.crt /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.key /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ta.key /etc/openvpn
    
    echo "=============================="
    echo "configure client"
    echo "=============================="
    cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn
    sed -i -e 's/remote my-server-1 1194/remote '"$1"' 1194/g' /etc/openvpn/client.conf
    sed -i -e 's/cert client.crt/cert '"$2"'.crt/g' /etc/openvpn/client.conf
    sed -i -e 's/key client.key/key '"$2"'.key/g' /etc/openvpn/client.conf
    sed -i -e 's/;tls-auth ta.key 1/tls-auth ta.key 1/g' /etc/openvpn/client.conf
    sed -i -e 's/;cipher x/cipher AES-256-CBC/g' /etc/openvpn/client.conf
    sed -i -e 's/comp-lzo/#comp-lzo/g' /etc/openvpn/client.conf
    
    echo "=============================="
    echo "start OpenVpn"
    echo "=============================="
    systemctl enable openvpn.service
    systemctl start openvpn.service
    
    cd $CUR_PATH

##### For CentOS:

    #!/bin/bash
    if [ "$#" -ne 4 ]; then
        echo "Please use like: $BASH_SOURCE 192.168.100.66 client_name root qweqwe"
        echo "where 192.168.100.66 - public ip address of OpenVpn server, root - login to server, qweqwe - password"
        exit 0
    fi
    echo "=============================="
    echo "start install and configure OpenVpn client"
    echo "=============================="
    CUR_PATH=`pwd`
    
    echo "=============================="
    echo "install openvpn"
    echo "=============================="
    rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
    yum install -y openvpn sshpass
    
    echo "=============================="
    echo "generate certificates"
    echo "=============================="
    sshpass -p "$4" ssh -oStrictHostKeyChecking=no "$3@$1" 'cd /etc/openvpn/easy-rsa && source ./vars && ./pkitool '"$2"
    
    echo "=============================="
    echo "copy certificates"
    echo "=============================="
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ca.crt /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.crt /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.key /etc/openvpn
    sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ta.key /etc/openvpn
    
    echo "=============================="
    echo "configure client"
    echo "=============================="
    cp /usr/share/doc/openvpn-*/sample/sample-config-files/client.conf /etc/openvpn
    sed -i -e 's/remote my-server-1 1194/remote '"$1"' 1194/g' /etc/openvpn/client.conf
    sed -i -e 's/cert client.crt/cert '"$2"'.crt/g' /etc/openvpn/client.conf
    sed -i -e 's/key client.key/key '"$2"'.key/g' /etc/openvpn/client.conf
    
    
    echo "=============================="
    echo "start OpenVpn"
    echo "=============================="
    chkconfig openvpn on
    service openvpn start
    
    cd $CUR_PATH

Save the file and execute the command:

    chmod a+x openvpn.sh

#### 3. Now, use this script to generate certificates for the VPN client, using the command:

    ./openvpn.sh 192.168.100.66 client_name root qweqwe

where:
192.168.100.66 - ip address of the OpenVPN server (This can be a static-public IP with VPS/VDS or local IP from the PC behind the router). If you are using a network topology (configuration) using DMZ, you need to specify the router's IP address.
client_name - the name of the client for which the certificate will be generated.
root qweqwe - login and password for accessing the OpenVpn server via SSH, those that you use to access the server using SSH tools.

#### 4. After successful execution of the script, the OpenVpn client will be configured and started.
#### 5. Client logs will be located in the file with system logs (/var/log/messages). If you want to redirect the logs, change the configuration file that is located at /etc/openvpn/client.conf.

### Configuration OpenVPN for Windows

For Windows, you need to download the OpenVPN application by clicking on the link https://openvpn.net/index.php/open-source/downloads.html and requesting keys for the OpenVPN client from those. support. You need to unzip them and put them in the C:keys/ folder. After we move to the folder C:ProgramFiles/OpenVPN/config/ in this folder, create the client.ovpn file and add the settings to its data:

    client
    dev tun
    proto udp
    remote xxx.xxx.xxx.xxx 1194
    resolv-retry
    infinite
    nobind
    persist-key
    persist-tun
    tls-auth C:\\keys\\ta.key 1
    ca C:\\keys\\ca.crt
    cert C:\\keys\\clientgui.crt
    key C:\\keys\\clientgui.key
    cipher
    AES-256-CBC
    #show-valid-subnets
    verb 3
    auth-nocache
    #log
    C:\\keys\\test_server.log
    mtu-test
    tun-mtu 1500
    tun-mtu-extra 32
    mssfix 1450

where:
xxx.xxx.xxx.xxx - IP address of the VPN-server.
clientgui - name of the VPN client.

### Configuration OpenVPN for MacOS

For MacOS, you need to download the Tunnelblick application by clicking on the link https://tunnelblick.net/downloads.html and requesting keys for the OpenVPN client from those. support. You need to unzip them and put them in your home folder. The configuration file must be created with the extension ovpn.

#### The configuration file for the VPN client:

    client
    remote xxx.xxx.xxx.xxx 1194
    ca "/home/myusername/keys/ca.crt"
    cert "/home/myusername/keys/client.crt"
    key "/home/myusername/keys/client.key"
    auth-user-pass
    tun-mtu 1500
    dev tun
    proto tcp
    nobind
    auth-nocache
    script-security 2
    persist-key
    persist-tun

where:
xxx.xxx.xxx.xxx - IP address of the VPN-server.
myusername - user name.
client - name of the VPN client.










