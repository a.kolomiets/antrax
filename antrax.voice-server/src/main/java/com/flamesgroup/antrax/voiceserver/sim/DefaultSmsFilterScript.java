/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.sim;

import com.flamesgroup.antrax.automation.scripts.SmsFilterScript;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultSmsFilterScript implements SmsFilterScript {
  private final Logger logger = LoggerFactory.getLogger(DefaultSmsFilterScript.class);

  @Override
  public boolean isSmsAccepted(final PhoneNumber phoneNumber) {
    logger.debug("[{}] - rejecting phone number {} as a default call filter.", this, phoneNumber);
    return false;
  }

  @Override
  public PhoneNumber substituteBNumber(final PhoneNumber bPhoneNumber) {
    return bPhoneNumber;
  }

  @Override
  public String substituteText(final String originText) {
    return originText;
  }

  @Override
  public boolean isAcceptsSmsParts(final int parts) {
    return true;
  }

  @Override
  public boolean isAcceptsSmsText(final String text) {
    return true;
  }

}
