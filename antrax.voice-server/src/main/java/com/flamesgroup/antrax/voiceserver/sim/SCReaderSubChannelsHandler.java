/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.sim;

import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SCReaderSubChannelsHandler implements ISCReaderSubChannelsHandler {

  private static final Logger logger = LoggerFactory.getLogger(SCReaderSubChannelsHandler.class);

  private final Map<ChannelUID, ISCReaderSubChannelHandler> scReaderSubChannelHandlers = new ConcurrentHashMap<>();

  @Override
  public void handleAPDUResponse(final ChannelUID channelUID, final APDUResponse response) throws RemoteException, IllegalChannelException {
    if (scReaderSubChannelHandlers.containsKey(channelUID)) {
      scReaderSubChannelHandlers.get(channelUID).handleAPDUResponse(response);
    } else {
      logger.warn("[{}] - can't find SCReaderSubChannelHandler for [{}]", channelUID, response);
      throw new IllegalChannelException(channelUID);
    }
  }

  @Override
  public void handleErrorState(final ChannelUID channelUID, final SCReaderError error, final SCReaderState state) throws RemoteException, IllegalChannelException {
    if (scReaderSubChannelHandlers.containsKey(channelUID)) {
      scReaderSubChannelHandlers.get(channelUID).handleErrorState(error, state);
    } else {
      logger.warn("[{}] - can't find SCReaderSubChannelHandler for error [{}] and [{}]", channelUID, error, state);
      throw new IllegalChannelException(channelUID);
    }
  }

  public void putSCReaderSubChannelHandler(final ChannelUID channelUID, final ISCReaderSubChannelHandler scReaderSubChannelHandler) {
    scReaderSubChannelHandlers.put(channelUID, scReaderSubChannelHandler);
  }

  public void removeSCReaderSubChannelHandler(final ChannelUID channelUID) {
    scReaderSubChannelHandlers.remove(channelUID);
  }

}
