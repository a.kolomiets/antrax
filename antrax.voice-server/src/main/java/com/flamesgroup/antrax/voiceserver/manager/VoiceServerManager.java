/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager;

import static com.flamesgroup.rmi.RemoteServiceExporter.destroyAll;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.voiceserv.utils.delivery.GuaranteedDeliveryProxyServer;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.CallChannelPool;
import com.flamesgroup.antrax.voiceserver.Configuration;
import com.flamesgroup.antrax.voiceserver.ControlServerPingThread;
import com.flamesgroup.antrax.voiceserver.IConfigurator;
import com.flamesgroup.antrax.voiceserver.IGsmChannelManager;
import com.flamesgroup.antrax.voiceserver.IVoiceServerManager;
import com.flamesgroup.antrax.voiceserver.UnitActivator;
import com.flamesgroup.antrax.voiceserver.VSStatus;
import com.flamesgroup.antrax.voiceserver.VoIPPeer;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.engines.AutomationActionsEngine;
import com.flamesgroup.antrax.voiceserver.engines.HttpServerEngine;
import com.flamesgroup.antrax.voiceserver.engines.ReconfigurationEngine;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplate;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplateManager;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class VoiceServerManager implements IVoiceServerManager {

  private static final Logger logger = LoggerFactory.getLogger(VoiceServerManager.class);

  private IActivityLogger activityLogger;

  private IConfigurator configurator;
  private Configuration cfg;
  private ScriptStateSaver scriptStateSaver;
  private CallChannelPool pool;

  private GuaranteedDeliveryProxyServer simServerDeliveryServer;
  private GuaranteedDeliveryProxyServer simHistoryDeliveryServer;
  private GuaranteedDeliveryProxyServer activityRemoteLoggerServer;

  private VoIPPeer voipPeer;

  private UnitActivator unitActivator;
  private IGsmChannelManager channelManager;
  private ReconfigurationEngine reconfigurationEngine;
  private AutomationActionsEngine actionsEngine;
  private HttpServerEngine httpServerEngine;
  private ControlServerPingThread controlServerPingThread;
  private IGsmChannelManagerHandler gsmChannelManagerHandler;
  private IvrTemplateManager ivrTemplateManager;

  private VSStatus antraxStatus;
  private final ConcurrentMap<UUID, VsSmsStatus> sendingSmsStatuses = new ConcurrentHashMap<>();

  @Override
  public synchronized void enableCallChannel(final ICCID simUID) {
    logger.debug("[{}] - going to enable call channel withIMSI={}", this, simUID);
    for (SIMUnit sim : unitActivator.getSIMUnits()) {
      if (sim.getIMSI().getValue().equals(simUID.getValue())) {
        logger.debug("[{}] - enabling {}", this, sim);
        sim.enable(true);
        return;
      }
    }
    logger.warn("[{}] - didn't find call channel withIMSI={}", this, simUID);
  }

  @Override
  public synchronized void disableCallChannel(final ICCID simUID) {
    logger.debug("[{}] - going to disable call channel withIMSI={}", this, simUID);
    for (SIMUnit sim : unitActivator.getSIMUnits()) {
      if (sim.getIMSI().getValue().equals(simUID.getValue())) {
        logger.debug("[{}] - disabling {}", this, sim);
        sim.enable(false);
        return;
      }
    }
    logger.debug("[{}] - didn't find call channel withIMSI={}", this, simUID);
  }

  @Override
  public synchronized void lockSIM(final ICCID simUID) {
    for (SIMUnit sim : unitActivator.getSIMUnits()) {
      if (sim.getUid().equals(simUID)) {
        sim.lock(true, "UserRequest");
        break;
      }
    }
  }

  @Override
  public synchronized void unlockSIM(final ICCID simUID) {
    for (SIMUnit sim : unitActivator.getSIMUnits()) {
      if (sim.getUid().equals(simUID)) {
        sim.lock(false, "UserRequest");
        break;
      }
    }
  }

  @Override
  public synchronized void fireEvent(final ICCID simUID, final String event) throws EventException {
    logger.debug("[{}] - going to fire event \"{}\" to call channel by IMSI \"{}\"", this, event, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - sending Event \"{}\" to CallChannel \"{}\"", this, event, callChannel);
      callChannel.toRegisteredInGSMChannel().fireGenericEvent(event);
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new EventException("Can't find call channel");
    }
  }

  @Override
  public synchronized void lockGsmChannel(final ChannelUID channel, final boolean lock, final String lockReason) throws LockException {
    logger.debug("[{}] - going to {} gsm channel {}", this, lock ? "lock" : "unlock", channel);
    GSMUnit gsmUnit = unitActivator.getGSMUnits().stream().filter(u -> u.getChannelUID().equals(channel)).findFirst().orElse(null);
    if (gsmUnit == null) {
      logger.debug("[{}] - didn't find gsm unit by channel \"{}\"", this, channel);
      throw new LockException("Can't find gsm unit");
    } else {
      gsmUnit.lockGsmChannel(lock, lockReason);
    }
  }

  @Override
  public synchronized void lockGsmChannelToArfcn(final ChannelUID channel, final int arfcn) throws LockArfcnException {
    logger.debug("[{}] - going to {} gsm channel lock to arfcn: {}", this, channel, arfcn);
    GSMUnit gsmUnit = unitActivator.getGSMUnits().stream().filter(u -> u.getChannelUID().equals(channel)).findFirst().orElse(null);
    if (gsmUnit == null) {
      logger.debug("[{}] - didn't find gsm unit by channel \"{}\"", this, channel);
    } else {
      gsmUnit.lockGsmChannelToArfcn(arfcn);
    }
    activityLogger.logGsmUnitLockToArfcn(channel, arfcn);
  }

  @Override
  public synchronized void unLockGsmChannelToArfcn(final ChannelUID channel) throws LockArfcnException {
    logger.debug("[{}] - going to {} gsm channel unLock to default arfcn", this, channel);
    GSMUnit gsmUnit = unitActivator.getGSMUnits().stream().filter(u -> u.getChannelUID().equals(channel)).findFirst().orElse(null);
    if (gsmUnit == null) {
      logger.debug("[{}] - didn't find gsm unit by channel \"{}\"", this, channel);
    } else {
      gsmUnit.unLockGsmChannelToArfcn();
    }
    activityLogger.logGsmUnitLockToArfcn(channel, null);
  }

  @Override
  public synchronized void executeNetworkSurvey(final ChannelUID channel) throws NetworkSurveyException {
    logger.debug("[{}] - going to executeNetworkSurvey for gsm channel {}", this, channel);
    GSMUnit gsmUnit = unitActivator.getGSMUnits().stream().filter(u -> u.getChannelUID().equals(channel)).findFirst().orElse(null);
    if (gsmUnit == null) {
      logger.debug("[{}] - didn't find gsm unit by channel \"{}\"", this, channel);
      throw new NetworkSurveyException("Can't find gsm unit");
    } else {
      gsmUnit.executeNetworkSurvey();
    }
  }

  @Override
  public synchronized void sendUSSD(final ICCID simUID, final String ussd) throws USSDException {
    logger.debug("[{}] - going to send USSD \"{}\" to call channel by IMSI \"{}\"", this, ussd, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - sending USSD \"{}\" to CallChannel \"{}\"", this, ussd, callChannel);
      try {
        callChannel.sendUSSD(ussd);
      } catch (GsmChannelError e) {
        logger.warn("[{}] - failed to send USSD", this, e);
        throw new USSDException(e.getMessage(), e.getCause());
      }
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new USSDException("Can't find call channel");
    }
  }

  @Override
  public synchronized String startUSSDSession(final ICCID simUID, final String ussd) throws USSDException {
    logger.debug("[{}] - going to start USSD session \"{}\" to call channel by IMSI \"{}\"", this, ussd, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - starting USSD session \"{}\" to CallChannel \"{}\"", this, ussd, callChannel);
      try {
        return callChannel.startUSSDSession(ussd);
      } catch (GsmChannelError e) {
        logger.warn("[{}] - failed to start USSD session", this, e);
        throw new USSDException(e.getMessage(), e.getCause());
      }
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new USSDException("Can't find call channel");
    }
  }

  @Override
  public synchronized String sendUSSDSessionCommand(final ICCID simUID, final String command) throws USSDException {
    logger.debug("[{}] - going to send USSD command \"{}\" to call channel by IMSI \"{}\"", this, command, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - sending USSD command \"{}\" to CallChannel \"{}\"", this, command, callChannel);
      try {
        return callChannel.sendUSSDSessionCommand(command);
      } catch (GsmChannelError e) {
        logger.warn("[{}] - failed to send USSD command", this, e);
        throw new USSDException(e.getMessage(), e.getCause());
      }
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new USSDException("Can't find call channel");
    }
  }

  @Override
  public synchronized void endUSSDSession(final ICCID simUID) throws USSDException {
    logger.debug("[{}] - going to end USSD to call channel by IMSI \"{}\"", this, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - ending USSD to CallChannel \"{}\"", this, callChannel);
      callChannel.endUSSDSession();
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new USSDException("Can't find call channel");
    }
  }

  @Override
  public synchronized void sendSMS(final ICCID simUID, final String number, final String smsText) throws SMSException {
    logger.debug("[{}] - going to send SMS \"{}\" to call channel by IMSI \"{}\"", this, smsText, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - sending SMS \"{}\" to CallChannel \"{}\"", this, smsText, callChannel);
      try {
        callChannel.sendSMS(new PhoneNumber(number), smsText);
        logger.debug("[{}] - success sending SMS \"{}\" to CallChannel \"{}\"", this, smsText, callChannel);
      } catch (SMSException e) {
        logger.warn("[{}] - can't send SMS: [{}] at callChannel: [{}]", this, smsText, callChannel, e);
        throw e;
      }
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new SMSException("Can't find call channel");
    }
  }

  @Override
  public VsSmsStatus sendSMS(final List<AlarisSms> alarisSmses, final UUID objectUuid) {
    logger.debug("[{}] - sending AlarisSms: [{}] ", this, alarisSmses);
    PhoneNumber phoneNumber;
    try {
      phoneNumber = new PhoneNumber(alarisSmses.get(0).getDnis());
    } catch (IllegalArgumentException e) {
      logger.warn("[{}] - SMS sending error:", this, e);
      return new VsSmsStatus().setStatus(VsSmsStatus.Status.FAIL).setMessage(e.getMessage());
    }
    String allText = alarisSmses.stream().map(AlarisSms::getMessage).collect(Collectors.joining());

    CallChannel cc = pool.takeForSms(phoneNumber, alarisSmses.size(), allText);
    if (cc != null) {
      VsSmsStatus vsSmsStatus = new VsSmsStatus().setStatus(VsSmsStatus.Status.SENDING);
      new Thread(() -> {
        try {
          sendingSmsStatuses.put(objectUuid, vsSmsStatus);
          UUID uuid = cc.sendSMS(phoneNumber, allText);
          logger.debug("[{}] - SMS was successfully sent by {}. AlarisSms: {}, sendSmsUUIID: [{}]", this, cc, alarisSmses, uuid);
          sendingSmsStatuses.put(objectUuid, new VsSmsStatus().setStatus(VsSmsStatus.Status.SENT).setUuid(uuid));
        } catch (SMSException e) {
          logger.warn("[{}] - can't send AlarisSms: [{}] by [{}]", this, alarisSmses, cc, e);
          sendingSmsStatuses.put(objectUuid, new VsSmsStatus().setStatus(VsSmsStatus.Status.FAIL).setMessage(e.getMessage()));
        } finally {
          cc.untake();
          cc.sendingSms(false);
        }
      }).start();
      return vsSmsStatus;
    } else {
      logger.warn("[{}] - SMS sending error: no free channels left", this);
      return new VsSmsStatus().setStatus(VsSmsStatus.Status.FAIL).setMessage("no free channels left");
    }
  }

  @Override
  public VsSmsStatus getSMSStatus(final UUID objectUuid) {
    logger.debug("[{}] - get SMS status: [{}] ", this, objectUuid);
    return sendingSmsStatuses.get(objectUuid);
  }

  @Override
  public void removeSMSStatus(final UUID objectUuid) {
    logger.debug("[{}] - remove SMS status: [{}] ", this, objectUuid);
    sendingSmsStatuses.remove(objectUuid);
  }

  @Override
  public synchronized void sendDTMF(final ICCID simUID, final String dtmf) throws DTMFException {
    logger.debug("[{}] - going to send DTMF \"{}\" to call channel by IMSI \"{}\"", this, dtmf, simUID);
    CallChannel callChannel = pool.take(simUID);
    if (callChannel != null) {
      logger.debug("[{}] - sending DTMF \"{}\" to CallChannel \"{}\"", this, dtmf, callChannel);
      try {
        callChannel.sendDTMF(dtmf);
      } catch (GsmChannelError e) {
        logger.warn("[{}] - failed to send DTMF", this, e);
        throw new DTMFException(e.getMessage(), e.getCause());
      }
    } else {
      logger.debug("[{}] - didn't find call channel with IMSI \"{}\"", this, simUID);
      throw new DTMFException("Can't find call channel");
    }
  }

  @Override
  public void resetStatistic() {
    logger.info("[{}] - reset statistic", this);
    activityLogger.resetStatistic();
  }

  @Override
  public void updateAudioCaptureEnable(final boolean enable) {
    logger.info("[{}] - update audio capture enable", this);
    cfg.updateAudioCaptureEnabled(enable);
  }

  @Override
  public List<AudioFile> getAudioFiles() {
    Path voiceServerAudioCapturePath = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
    logger.info("[{}] - try load list of audio files from: [{}]", this, voiceServerAudioCapturePath);
    File[] files = new File(voiceServerAudioCapturePath.toUri()).listFiles();
    if (files == null) {
      logger.info("[{}] - not found files at path: {}", this, voiceServerAudioCapturePath);
      return Collections.emptyList();
    }
    return Arrays.stream(files).map(f -> new AudioFile(f.getName(), f.lastModified(), f.length())).collect(Collectors.toList());
  }

  @Override
  public byte[] downloadAudioFileFromServer(final String fileName) {
    Path voiceServerAudioCapturePath = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
    logger.info("[{}] - try read audio file: [{}] from: [{}]", this, fileName, voiceServerAudioCapturePath);

    try {
      return Files.readAllBytes(voiceServerAudioCapturePath.resolve(fileName));
    } catch (IOException e) {
      logger.error("[{}] - can't read file: [{}]", this, fileName, e);
    }
    return new byte[0];
  }

  @Override
  public List<String> getIvrTemplatesSimGroups() {
    return ivrTemplateManager.getSimGroupNames();
  }

  @Override
  public List<IvrTemplateWrapper> getIvrTemplates(final String simGroup) {
    List<IvrTemplate> ivrTemplates = ivrTemplateManager.getIvrTemplates(simGroup);
    return ivrTemplates.stream().map(e -> new IvrTemplateWrapper(e.getName(), e.getCallDropReason().name(), String.valueOf(e.getCallDropReason().getCauseCode() & 0xFF)))
        .collect(Collectors.toList());
  }

  @Override
  public void createIvrTemplateSimGroup(final String simGroup) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for create a new SIM group");
    }
    ivrTemplateManager.createSimGroup(simGroup);
  }

  @Override
  public void removeIvrTemplateSimGroups(final List<String> simGroups) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for remove SIM group");
    }
    ivrTemplateManager.removeIvrTemplateSimGroups(simGroups);
  }

  @Override
  public Map<String, Integer> getCallDropReasons() {
    return Arrays.stream(CallDropReason.values()).collect(Collectors.toMap(CallDropReason::name, e-> e.getCauseCode() & 0xFF));
  }

  @Override
  public void uploadIvrTemplate(final String simGroup, final byte[] bytes, final String fileName) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for upload ivr template");
    }
    ivrTemplateManager.uploadIvrTemplate(simGroup, bytes, fileName);
  }

  @Override
  public void removeIvrTemplate(final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for upload ivr template");
    }
    ivrTemplateManager.removeIvrTemplate(simGroup, ivrTemplates);
  }

  public void setActivityLogger(final IActivityLogger activityLogger) {
    this.activityLogger = activityLogger;
  }

  public void setSimServerDeliveryServer(final GuaranteedDeliveryProxyServer simServerDeliveryServer) {
    this.simServerDeliveryServer = simServerDeliveryServer;
  }

  public void setSimHistoryDeliveryServer(final GuaranteedDeliveryProxyServer simHistoryDeliveryServer) {
    this.simHistoryDeliveryServer = simHistoryDeliveryServer;
  }

  public void setActivityRemoteLoggerServer(final GuaranteedDeliveryProxyServer activityRemoteLoggerServer) {
    this.activityRemoteLoggerServer = activityRemoteLoggerServer;
  }

  public void setConfigurator(final IConfigurator configurator) {
    this.configurator = configurator;
  }

  public void setCfg(final Configuration cfg) {
    this.cfg = cfg;
  }

  public void setScriptStateSaver(final ScriptStateSaver scriptStateSaver) {
    this.scriptStateSaver = scriptStateSaver;
  }

  public void setCallChannelPool(final CallChannelPool pool) {
    this.pool = pool;
  }

  public void setUnitActivator(final UnitActivator unitActivator) {
    this.unitActivator = unitActivator;
  }

  public void setChannelManager(final IGsmChannelManager channelManager) {
    this.channelManager = channelManager;
  }

  public void setReconfigurationEngine(final ReconfigurationEngine reconfigurationEngine) {
    this.reconfigurationEngine = reconfigurationEngine;
  }

  public void setVoipPeer(final VoIPPeer voipPeer) {
    this.voipPeer = voipPeer;
  }

  public void setActionsEngine(final AutomationActionsEngine actionsEngine) {
    this.actionsEngine = actionsEngine;
  }

  public void setHttpServerEngine(final HttpServerEngine httpServerEngine) {
    this.httpServerEngine = httpServerEngine;
  }

  public void setAntraxStatus(final VSStatus antraxStatus) {
    this.antraxStatus = antraxStatus;
  }

  public void setControlServerPingThread(final ControlServerPingThread controlServerPingThread) {
    this.controlServerPingThread = controlServerPingThread;
  }

  public void setGsmChannelManagerHandler(final IGsmChannelManagerHandler gsmChannelManagerHandler) {
    this.gsmChannelManagerHandler = gsmChannelManagerHandler;
  }

  public void setIvrTemplateManager(final IvrTemplateManager ivrTemplateManager) {
    this.ivrTemplateManager = ivrTemplateManager;
  }

  public synchronized void start() throws Exception {
    logger.debug("[{}] - starting voice server", this);
    logger.debug("[{}] - starting guaranteed delivery servers", this);
    simServerDeliveryServer.start();
    simHistoryDeliveryServer.start();
    activityRemoteLoggerServer.start();
    activityLogger.changeServerStatus(ServerStatus.STARTING);

    logger.debug("[{}] - initializing PluginsStore", this);
    configurator.updatePluginsStore(cfg.getPluginsStore());

    logger.debug("[{}] - starting ScriptsStatesSaver", this);
    scriptStateSaver.start();

    logger.debug("[{}] - starting PlugAndPlayEngine", this);
    channelManager.start(gsmChannelManagerHandler);

    logger.debug("[{}] - starting ReconfigurationEngine", this);
    reconfigurationEngine.start();

    logger.debug("[{}] - starting VoIPPeer", this);
    voipPeer.start();

    logger.debug("[{}] - starting ActionsEngine", this);
    actionsEngine.start();

    logger.debug("[{}] - starting HttpServerEngine", this);
    httpServerEngine.start();

    logger.debug("[{}] - starting ControlServerPingThread", this);
    controlServerPingThread.start();

    activityLogger.changeServerStatus(ServerStatus.STARTED);
    logger.info("[{}] - voice server started", this);
  }

  @Override
  public synchronized void shutdown() {
    // TODO: remove monitor object and refactor stopping all threads mechanism
    synchronized (antraxStatus) {
      if (antraxStatus.terminating) {
        return;
      }

      logger.debug("[{}] - marking voice server as terminating", this);
      antraxStatus.terminating = true;
      logger.debug("[{}] - notifying threads about termination", this);
      antraxStatus.notifyAll();
    }
    logger.info("[{}] - terminating voice server", this);
    activityLogger.changeServerStatus(ServerStatus.STOPPING);

    logger.debug("[{}] - terminating ControlServerPingThread", this);
    controlServerPingThread.terminate();
    try {
      controlServerPingThread.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - controlServerPingThread interrupted while join", this, e);
    }

    logger.debug("[{}] - waiting until HttpServerEngine finishes...", this);
    httpServerEngine.stop();

    try {
      logger.debug("[{}] - waiting for actionsEngine to terminate", this);
      actionsEngine.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - actionsEngine interrupted while join", this, e);
    }

    logger.debug("[{}] - force releasing channels", this);
    pool.forceReleaseChannels();

    logger.debug("[{}] - terminating VoIPPeer", this);
    try {
      voipPeer.stop();
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }

    try {
      logger.debug("[{}] - waiting for reconfigurationEngine to terminate", this);
      reconfigurationEngine.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - reconfigurationEngine interrupted while join", this, e);
    }

    logger.debug("[{}] - terminating PlugAndPlayEngine", this);
    channelManager.stop();

    logger.debug("[{}] - terminating ScriptStateSaver", this);
    scriptStateSaver.terminate();

    logger.debug("[{}] - terminating delivery servers", this);
    simServerDeliveryServer.terminate();
    simHistoryDeliveryServer.terminate();
    activityRemoteLoggerServer.terminate();
    logger.debug("[{}] - all done", this);

    destroyAll();
  }

}
