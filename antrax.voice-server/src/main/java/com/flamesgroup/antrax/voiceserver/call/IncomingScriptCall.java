/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.FakeAnswerMediaStream;
import com.flamesgroup.antrax.voiceserver.SilentMediaStream;
import com.flamesgroup.antrax.voiceserver.SpyMediaStream;
import com.flamesgroup.antrax.voiceserver.channels.CyclicBuffer;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IMobileTerminatingCall;
import com.flamesgroup.unit.MobileCallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicBoolean;

public class IncomingScriptCall extends IncomingCall {

  private static final Logger logger = LoggerFactory.getLogger(IncomingScriptCall.class);

  private static final int SLEEP_TIMEOUT = 200;

  private final AtomicBoolean answered = new AtomicBoolean();

  public IncomingScriptCall(final CallChannel callChannel, final CDR cdr, final IMobileTerminatingCall terminatedCall) {
    super(callChannel, cdr, terminatedCall);

    state = CallChannelState.State.INCOMING_SCRIPT_CALL;
  }


  @Override
  protected IMediaStream wrapMediaStream(final IMediaStream mediaStream) {
    IMediaStream mediaStreamLocal = super.wrapMediaStream(mediaStream);
    CyclicBuffer fakeAnswer = callChannel.getGSMUnit().getFakeAnswerSignal();
    if (fakeAnswer != null) {
      FakeAnswerMediaStream fakeAnswerMediaStream = new FakeAnswerMediaStream(mediaStreamLocal, fakeAnswer);
      addCallStatusListener(fakeAnswerMediaStream);
      mediaStreamLocal = fakeAnswerMediaStream;
    }
    // wrapping by SpyMediaStream the last to write fake answer into output stream
    if (callChannel.isAudioCaptureEnabled()) {
      Path path = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
      mediaStreamLocal = new SpyMediaStream(mediaStreamLocal, path, cdr.getCallType(), cdr.getCallerPhoneNumber(), callChannel.toString());
    }
    return mediaStreamLocal;
  }

  @Override
  public void run() {
    try {
      callChannel.getGSMUnit().turnOnAudio(wrapMediaStream(new SilentMediaStream()));
    } catch (GsmChannelError e) {
      logger.warn("[{}] - can't start incoming call", callChannel, e);
      dropGSM(CallDropReason.SWITCH_CONGESTION, CdrDropReason.START_AUDIO_PROBLEM);
      return;
    }

    callChannel.handleCallSetup(cdr.getCallerPhoneNumber());
    callChannel.getSIMUnit().startShowingState(state, "");
    callChannel.getSIMUnit().handleIncomingCall(cdr.getCallerPhoneNumber());

    processAlerting();
    while (!isReleased()) {
      try {
        Thread.sleep(SLEEP_TIMEOUT);
      } catch (InterruptedException e) {
        logger.warn("[{}] - while sleep to process incoming call was interrupted", callChannel, e);
        return;
      }

      if (callChannel.getSIMUnit().shouldDropIncomingCall()) {
        logger.debug("[{}] - script asks to drop incoming call", callChannel);
        drop(CallDropReason.NORMAL_CLEARING, CdrDropReason.VS);
        callChannel.getSIMUnit().dropActionIncomingCall(callChannel.toRegisteredInGSMChannel());
      } else if (callChannel.getSIMUnit().shouldAnswerIncomingCall() && !answered.get()) {
        logger.debug("[{}] - scripts asks to answer incoming call", callChannel);
        processAnswer();
      }
    }
  }

  // IMobileTerminatingCallHandler
  @Override
  public void handleDTMF(final char dtmf) {
  }

  @Override
  public void handleDropped(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    if (compareStateAndSet(State.HANGUP)) {
      notifyCallStatusListeners(ICallStatusListener::handleHangup);
      dropVoIP(reason, report);
    }
  }

  @Override
  protected void processAlerting() {
    if (compareStateAndSet(State.ALERTING)) {
      notifyCallStatusListeners(ICallStatusListener::handleAlerting);
      super.processAlerting();
    }
  }

  @Override
  protected void processAnswer() {
    answered.set(true);
    if (compareStateAndSet(State.ANSWER)) {
      notifyCallStatusListeners(ICallStatusListener::handleAnswer);
      super.processAnswer();
    }
  }

  @Override
  public void drop(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    if (compareStateAndSet(State.HANGUP)) {
      notifyCallStatusListeners(ICallStatusListener::handleHangup);
      dropAll(callDropReason, cdrDropReason);
    }
  }

  private boolean compareStateAndSet(final State state) {
    lock.lock();
    try {
      if (currentState.compareTo(state) < 0) {
        currentState = state;
        return true;
      } else {
        return false;
      }
    } finally {
      lock.unlock();
    }

  }

}
