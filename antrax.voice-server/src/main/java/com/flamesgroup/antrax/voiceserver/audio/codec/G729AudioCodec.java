/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.audio.codec;

import com.flamesgroup.jg729.G729DecoderException;
import com.flamesgroup.jg729.G729EncoderException;
import com.flamesgroup.jg729.IG729Decoder;
import com.flamesgroup.jg729.IG729Encoder;
import com.flamesgroup.jg729.jni.G729;
import com.flamesgroup.jiax2.util.DataUtils;
import com.flamesgroup.media.codec.audio.AudioCodecException;
import com.flamesgroup.media.codec.audio.IAudioCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class G729AudioCodec implements IAudioCodec {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private IG729Decoder decoder;
  private IG729Encoder encoder;

  private boolean kludgeWarn;

  public G729AudioCodec() {
    G729 g729 = new G729();
    try {
      decoder = g729.createDecoder();
      encoder = g729.createEncoder();
    } catch (G729DecoderException | G729EncoderException e) {
      throw new IllegalStateException("Can't create G729AudioCodec encoder and decoder", e);
    }
  }

  @Override
  public byte[] decode(final byte[] data) throws AudioCodecException {
    int adjustedDataLength = (data.length / 10) * 10;
    if (!kludgeWarn && adjustedDataLength != data.length) {
      logger.warn("[{}] - drop frame(s) unsupported by size, future current codec warnings will be suppressed", this);
      kludgeWarn = true;
    }
    if (adjustedDataLength == 0) {
      return DataUtils.EMPTY_DATA;
    } else {
      byte[] pcm = new byte[adjustedDataLength * 16];
      try {
        decoder.decode(data, 0, adjustedDataLength, pcm, 0, pcm.length);
        return pcm;
      } catch (G729DecoderException e) {
        throw new AudioCodecException(e);
      }
    }
  }


  @Override
  public byte[] encode(final byte[] pcm) throws AudioCodecException {
    if (pcm.length % 16 != 0) {
      throw new AudioCodecException("can't process pcm, pcmFrameSize must be divisible by 16");
    }
    byte[] g729Data = new byte[pcm.length / 16];
    try {
      encoder.encode(pcm, 0, pcm.length, g729Data, 0, g729Data.length);
      return g729Data;
    } catch (G729EncoderException e) {
      throw new AudioCodecException(e);
    }
  }

  @Override
  public int getFrameDuration() {
    return 10;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

}
