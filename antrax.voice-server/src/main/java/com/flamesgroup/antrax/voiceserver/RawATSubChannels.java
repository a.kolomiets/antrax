/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannels;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannelsHandler;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IGSMChannel;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.gsmb.IRawATSubChannel;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RawATSubChannels implements IRawATSubChannels {

  private static final Logger logger = LoggerFactory.getLogger(RawATSubChannels.class);

  private final Map<ChannelUID, IGSMEntry> gsmEntries = new ConcurrentHashMap<>();

  @Override
  public void start(final ChannelUID channel, final IRawATSubChannelsHandler atRawSubChannelsHandler) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    IGSMChannel gsmChannel = getGSMChannel(channel);
    gsmChannel.enslave();

    IRawATSubChannel rawATSubChannel = gsmChannel.getRawATSubChannel();
    try {
      rawATSubChannel.start(new RawATSubChannelHandler(channel, atRawSubChannelsHandler));
    } catch (ChannelException | InterruptedException e) {
      gsmChannel.free();
      throw e;
    }
  }

  @Override
  public void stop(final ChannelUID channel) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    IGSMChannel gsmChannel = getGSMChannel(channel);
    try {
      gsmChannel.getRawATSubChannel().stop();
    } finally {
      gsmChannel.free();
    }
  }

  @Override
  public void writeRawATData(final ChannelUID channel, final byte[] atData) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    IGSMChannel gsmChannel = getGSMChannel(channel);
    gsmChannel.getRawATSubChannel().writeRawATData(ByteBuffer.wrap(atData));
  }

  public void addGsmEntry(final ChannelUID channel, final IGSMEntry gsmEntry) {
    gsmEntries.put(channel, gsmEntry);
  }

  public void removeGsmEntry(final ChannelUID channel) {
    gsmEntries.remove(channel);
  }

  private IGSMChannel getGSMChannel(final ChannelUID channel) throws IllegalChannelException {
    IGSMEntry gsmEntry = gsmEntries.get(channel);
    if (gsmEntry == null) {
      throw new IllegalChannelException(channel);
    }

    return gsmEntry.getGSMChannel();
  }

  private class RawATSubChannelHandler implements IRawATSubChannelHandler {

    private final ChannelUID channel;
    private final IRawATSubChannelsHandler rawATSubChannelsHandler;

    public RawATSubChannelHandler(final ChannelUID channel, final IRawATSubChannelsHandler rawATSubChannelsHandler) {
      this.channel = channel;
      this.rawATSubChannelsHandler = rawATSubChannelsHandler;
    }

    @Override
    public void handleRawATData(final ByteBuffer atDataBuffer) {
      try {
        rawATSubChannelsHandler.handleRawATData(channel, toByteArray(atDataBuffer));
      } catch (RemoteException | IllegalChannelException e) {
        logger.warn("[{}] - can't handle raw AT data for [{}]", this, channel, e);
      }
    }

    private byte[] toByteArray(final ByteBuffer atDataBuffer) {
      byte[] bytes = new byte[atDataBuffer.remaining()];
      atDataBuffer.get(bytes);
      return bytes;
    }

  }

}
