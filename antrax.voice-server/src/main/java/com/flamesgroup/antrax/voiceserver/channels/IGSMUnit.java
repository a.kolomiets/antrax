/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmChannelError;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.device.gsmb.atengine.http.IHTTPSession;
import com.flamesgroup.device.gsmb.atengine.ussd.IUSSDSession;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.unit.IIncomingMobileCallHandler;
import com.flamesgroup.unit.IIncomingSMSHandler;
import com.flamesgroup.unit.IMobileOriginatingCallHandler;
import com.flamesgroup.unit.IMobileStationUnitExternalErrorHandler;
import com.flamesgroup.unit.IMobileStationUnitInternalErrorHandler;
import com.flamesgroup.unit.IServiceInfoHandler;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.unit.sms.SMSMessageOutbound;

import java.util.List;

public interface IGSMUnit {

  ChannelUID getChannelUID();

  void connect(SIMUnit simUnit, IIncomingMobileCallHandler incomingMobileCallHandler, IIncomingSMSHandler incomingSMSHandler,
      IServiceInfoHandler serviceInfoHandler, IMobileStationUnitInternalErrorHandler mobileStationUnitInternalErrorHandler, CellEqualizerManager cellEqualizerManager,
      IMobileStationUnitExternalErrorHandler mobileStationUnitExternalErrorHandler, ISCReaderSubChannel screReaderSubChannel, IMEI imei, String description) throws GsmChannelError;

  void disconnect();

  void dropCall() throws GsmChannelError;

  void turnOffAudio();

  void dial(PhoneNumber number, IMobileOriginatingCallHandler originatingCallHandler) throws GsmChannelError;

  void sendDTMF(String dtmfString) throws GsmChannelError;

  void sendDTMF(char dtmf, int duration) throws GsmChannelError;

  void turnOnAudio(IMediaStream mediaStream) throws GsmChannelError;

  IUSSDSession openUSSDSession(String ussd) throws GsmChannelError;

  void activateRinging();

  List<SMSMessageOutbound> sendSMS(PhoneNumber number, String text, boolean needReport) throws SMSException;

  IHTTPSession getHttpSession();

}
