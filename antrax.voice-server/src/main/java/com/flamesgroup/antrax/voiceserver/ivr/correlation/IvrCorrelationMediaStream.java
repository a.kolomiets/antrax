/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class IvrCorrelationMediaStream implements IMediaStream {

  private static final Logger logger = LoggerFactory.getLogger(IvrCorrelationMediaStream.class);

  private final IMediaStream mediaStream;
  private final CallChannel callChannel;

  private final long ivrCorrelationTime;

  private final List<Pair<IvrTemplate, ProcessCorrelationData>> processIvrTemplates;

  private boolean skipCorrelation;
  private long correlationStart = -1; // 0 it's initial value for timestamp

  public IvrCorrelationMediaStream(final IMediaStream mediaStream, final CallChannel callChannel) {
    this.mediaStream = mediaStream;
    this.callChannel = callChannel;

    ivrCorrelationTime = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationTime();

    String simGroupName = callChannel.getSIMUnit().getSimData().getSimGroup().getName();
    List<IvrTemplate> ivrTemplates = callChannel.getIvrTemplateManager().getIvrTemplates(simGroupName);
    processIvrTemplates = ivrTemplates.stream().map(t -> new Pair<>(t, new ProcessCorrelationData(t.getCorrelationData().getLength()))).collect(Collectors.toList());
  }

  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    logger.debug("[{}] - enable correlation", callChannel);
    mediaStream.open(mediaStreamHandler);
  }

  @Override
  public void close() {
    mediaStream.close();
  }

  @Override
  public boolean isOpen() {
    return mediaStream.isOpen();
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    mediaStream.write(timestamp, data, offset, length);
    if (skipCorrelation) {
      return;
    }

    String simGroupName = callChannel.getSIMUnit().getSimData().getSimGroup().getName();
    if (processIvrTemplates.isEmpty()) {
      logger.info("[{}] - not found ivr templates for sim group [{}]", callChannel, simGroupName);
      skipCorrelation = true;
      return;
    }

    if (correlationStart < 0) {
      correlationStart = timestamp;
    }

    if (timestamp - correlationStart >= ivrCorrelationTime) {
      logger.info("[{}] - correlation time was exceeded", callChannel);
      skipCorrelation = true;
      return;
    }

    double processAvgFrame = AudioDataCorrelationHelper.avgAsAbsShort(data, offset, length);

    for (Pair<IvrTemplate, ProcessCorrelationData> processIvrTemplate : processIvrTemplates) {
      IvrTemplate ivrTemplate = processIvrTemplate.first();

      ProcessCorrelationData processCorrelationData = processIvrTemplate.second();
      ICorrelationData templateCorrelationData = ivrTemplate.getCorrelationData();

      processCorrelationData.add(processAvgFrame);
      double correlationCoefficient = Correlator.correlate(templateCorrelationData, processCorrelationData);

      if (correlationCoefficient >= Correlator.CORRELATION_THRESHOLD) {
        logger.info("[{}] - ivr detect with correlation coefficient [{}] by template [{}] in group [{}]", callChannel, correlationCoefficient, ivrTemplate.getName(), simGroupName);
        skipCorrelation = true;

        callChannel.handleIvr(ivrTemplate.getName(), ivrTemplate.getCallDropReason());
        break;
      }
    }

  }

}
