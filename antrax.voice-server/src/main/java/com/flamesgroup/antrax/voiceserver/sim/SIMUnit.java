/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.sim;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.ISmsManager;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.listeners.IvrListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.listeners.SimCallHistoryListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.scripts.CallFilterScript;
import com.flamesgroup.antrax.automation.scripts.ChancesBasedIncomingCallScript;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.automation.scripts.IncomingCallScript;
import com.flamesgroup.antrax.automation.scripts.OriginationIncomingCallScript;
import com.flamesgroup.antrax.automation.scripts.SmsFilterScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.plugins.core.data.SimGroupReferenceImpl;
import com.flamesgroup.antrax.plugins.cuncurrent.ScriptHolder;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.OperatorSelectionMode;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.storage.jooq.tables.SimGroup;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSMessageInbound;
import com.flamesgroup.unit.sms.SMSMessageOutbound;
import com.flamesgroup.unit.sms.SMSStatusReport;
import com.flamesgroup.utils.TransientValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class SIMUnit {

  private final Logger logger = LoggerFactory.getLogger(SIMUnit.class);

  private final SessionSimUnitManager simUnitManager;
  private final IRemoteHistoryLogger historyLogger;
  private final ISmsManager smsManager;
  private final ChannelUID channelUID;
  private final UUID sessionUuid;
  private final ScriptStateSaver scriptStateSaver;
  private final IActivityLogger activityLogger;
  private final RegistryAccess registry;
  private final ISimCallHistory simCallHistory;

  private final BlockingQueue<IEventNotification> eventNotificationQueue = new LinkedBlockingQueue<>();
  private final Map<Integer, SMSStatusReport> cacheSmsStatusReportMap = new ConcurrentHashMap<>();

  private volatile SimData simData;
  private volatile IMSI imsi;
  private volatile boolean locked;
  private volatile boolean enabled;
  private volatile boolean supportOrigination;
  private volatile boolean waitRelease;
  private volatile boolean scReaderChannelError;

  // need to show correct channel state after business activity and incoming call
  private volatile CallChannelState.State showState;
  private volatile CallChannelState.State saveState;
  private volatile String showAdvInfo;
  private volatile String saveAdvInfo;

  private boolean owned;
  private long lastPluginsStoreRevision;
  private EventNotificationThread eventNotificationThread;

  private OperatorSelectionMode operatorSelectionMode;

  private int attemptNumber;

  private volatile String businessActivityDescription;

  private final TransientValue<Boolean> plugged = new TransientValue<Boolean>(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSimPlugedCacheLifetime()) {
    @Override
    protected Boolean readValue() {
      return simUnitManager.isSimUnitPlugged(sessionUuid);
    }
  };

  private final TransientValue<Boolean> activityStart = new TransientValue<Boolean>(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSimActivityStartCacheLifetime()) {
    @Override
    protected Boolean readValue() {
      return simUnitManager.isSimUnitShouldStartActivity(sessionUuid);
    }
  };

  private final TransientValue<Boolean> activityStop = new TransientValue<Boolean>(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSimActivityStopCacheLifetime()) {
    @Override
    protected Boolean readValue() {
      return simUnitManager.isSimUnitShouldStopActivity(sessionUuid);
    }
  };

  private final TransientValue<Boolean> sessionStop = new TransientValue<Boolean>(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSimSessionCacheLifetime()) {
    @Override
    protected Boolean readValue() {
      return simUnitManager.isSimUnitShouldStopSession(sessionUuid);
    }
  };
  private final TransientValue<Boolean> reconfiguration = new TransientValue<Boolean>(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getSimReconfigurationCacheLifetime()) {
    @Override
    protected Boolean readValue() {
      return simUnitManager.isSimUnitRequiresReconfiguration(sessionUuid);
    }
  };

  private final ScriptHolder<BusinessActivityScript> businessScripts = new ScriptHolder<BusinessActivityScript>() {

    @Override
    protected void saveScripts(final BusinessActivityScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.BUSINESS_ACTIVITY_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final BusinessActivityScript script) {
    }
  };
  private final ScriptHolder<IncomingCallScript> incomingCallScript = new ScriptHolder<IncomingCallScript>(new DefaultIncomingCallScript()) {

    @Override
    protected void saveScripts(final IncomingCallScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.INCOMING_CALL_MANAGEMENT_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final IncomingCallScript script) {
    }
  };
  private final ScriptHolder<CallFilterScript> callFilterScripts = new ScriptHolder<CallFilterScript>() {

    @Override
    protected void saveScripts(final CallFilterScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.CALL_FILTER_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final CallFilterScript script) {
    }
  };

  private final ScriptHolder<SmsFilterScript> smsFilterScripts = new ScriptHolder<SmsFilterScript>() {

    @Override
    protected void saveScripts(final SmsFilterScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.SMS_FILTER_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final SmsFilterScript script) {
    }
  };

  private final ScriptHolder<ActionProviderScript> actionProviders = new ScriptHolder<ActionProviderScript>() {

    @Override
    protected void saveScripts(final ActionProviderScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.ACTION_PROVIDER_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final ActionProviderScript script) {
      if (script instanceof SessionListener) {
        ((SessionListener) script).handleSessionStarted(null); // TODO: provide real value
      }
      if (script instanceof ActivityListener) {
        ((ActivityListener) script).handleActivityStarted(null); // TODO: provide real value
      }
      if (script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodStart();
      }
    }

  };
  private final ScriptHolder<FactorEvaluationScript> factorScript = new ScriptHolder<FactorEvaluationScript>(new DefaultFactorEvaluationScript()) {

    @Override
    protected void saveScripts(final FactorEvaluationScript[] scripts) {
      scriptStateSaver.saveScript(simData.getUid(), ScriptType.VS_FACTOR_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final FactorEvaluationScript script) {
    }

  };

  public SIMUnit(final SessionSimUnitManager simUnitManager, final IRemoteHistoryLogger historyLogger, final ISmsManager smsManager, final ChannelUID simChannelUID, final UUID sessionUuid,
      final ScriptStateSaver scriptSaver, final IActivityLogger activityLogger, final RegistryAccess registry, final ISimCallHistory simCallHistory) {
    this.simUnitManager = simUnitManager;
    this.historyLogger = historyLogger;
    this.smsManager = smsManager;
    this.channelUID = simChannelUID;
    this.sessionUuid = sessionUuid;
    this.scriptStateSaver = scriptSaver;
    this.activityLogger = activityLogger;
    this.registry = registry;
    this.simCallHistory = simCallHistory;
  }

  public boolean acceptsCallPhoneNumber(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber) {
    try {
      CallFilterScript[] callFilters = callFilterScripts.takeScriptsForUse();
      if (callFilters.length == 0) {
        logger.warn("[{}] - there is no call filter script in sim group {}", this, simData.getSimGroup());
        return false;
      }

      for (CallFilterScript callFilter : callFilters) {
        if (callFilter.isCallAccepted(callerPhoneNumber, calledPhoneNumber)) {
          return true;
        }
      }
      return false;
    } finally {
      callFilterScripts.untakeScripts();
    }
  }

  public boolean acceptsSmsPhoneNumber(final PhoneNumber phoneNumber) {
    try {
      SmsFilterScript[] smsFilters = smsFilterScripts.takeScriptsForUse();
      if (smsFilters.length == 0) {
        logger.warn("[{}] - there is no sms filter script in sim group {}", this, simData.getSimGroup());
        return false;
      }
      for (SmsFilterScript smsFilter : smsFilters) {
        if (smsFilter.isSmsAccepted(phoneNumber)) {
          return true;
        }
      }
      return false;
    } finally {
      smsFilterScripts.untakeScripts();
    }
  }

  public boolean acceptsSmsParts(final int parts) {
    try {
      SmsFilterScript[] smsFilters = smsFilterScripts.takeScriptsForUse();
      if (smsFilters.length == 0) {
        logger.warn("[{}] - there is no sms filter script in sim group {}", this, simData.getSimGroup());
        return false;
      }
      for (SmsFilterScript smsFilter : smsFilters) {
        if (smsFilter.isAcceptsSmsParts(parts)) {
          return true;
        }
      }
      return false;
    } finally {
      smsFilterScripts.untakeScripts();
    }
  }

  public boolean acceptsSmsText(final String text) {
    try {
      SmsFilterScript[] smsFilters = smsFilterScripts.takeScriptsForUse();
      if (smsFilters.length == 0) {
        logger.warn("[{}] - there is no sms filter script in sim group {}", this, simData.getSimGroup());
        return false;
      }
      for (SmsFilterScript smsFilter : smsFilters) {
        if (smsFilter.isAcceptsSmsText(text)) {
          return true;
        }
      }
      return false;
    } finally {
      smsFilterScripts.untakeScripts();
    }
  }

  public PhoneNumber substituteSmsPhoneNumber(final PhoneNumber number) {
    try {
      SmsFilterScript[] smsFilters = smsFilterScripts.takeScriptsForUse();
      if (smsFilters.length == 0) {
        logger.warn("There is no sms filter script in sim group {}", simData.getSimGroup());
        return number;
      }
      for (SmsFilterScript smsFilter : smsFilters) {
        if (smsFilter.isSmsAccepted(number)) {
          return smsFilter.substituteBNumber(number);
        }
      }
      logger.warn("No one of sms filter script are not accepting phone number in sim group {}", simData.getSimGroup());
      return number;
    } finally {
      smsFilterScripts.untakeScripts();
    }
  }

  public String substituteSmsText(final PhoneNumber number, final String text) {
    try {
      SmsFilterScript[] smsFilters = smsFilterScripts.takeScriptsForUse();
      if (smsFilters.length == 0) {
        logger.warn("There is no sms filter script in sim group {}", simData.getSimGroup());
        return text;
      }
      for (SmsFilterScript smsFilter : smsFilters) {
        if (smsFilter.isSmsAccepted(number)) {
          return smsFilter.substituteText(text);
        }
      }
      logger.warn("No one of sms filter script are not accepting phone number in sim group {}", simData.getSimGroup());
      return text;
    } finally {
      smsFilterScripts.untakeScripts();
    }
  }

  public PhoneNumber substituteNumber(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber) {
    try {
      CallFilterScript[] callFilters = callFilterScripts.takeScriptsForUse();
      if (callFilters.length == 0) {
        logger.warn("[{}] - there is no call filter script in sim group {}", this, simData.getSimGroup());
        return calledPhoneNumber;
      }

      for (CallFilterScript callFilter : callFilters) {
        if (callFilter.isCallAccepted(callerPhoneNumber, calledPhoneNumber)) {
          return callFilter.substituteBNumber(calledPhoneNumber);
        }
      }

      logger.warn("[{}] - no one of the script are not accepting phone calledPhoneNumber [{}] and callerPhoneNumber [{}] in sim group {}", this, calledPhoneNumber, callerPhoneNumber,
          simData.getSimGroup());
      return calledPhoneNumber;
    } finally {
      callFilterScripts.untakeScripts();
    }
  }

  public IMSI getIMSI() {
    return imsi;
  }

  public IMEI getImei() {
    return simData.getImei();
  }

  public ICCID getUid() {
    return simData.getUid();
  }

  // FIXME: use only one method getChannelUID or getSimUnitId
  public ChannelUID getChannelUID() {
    return channelUID;
  }

  public boolean isLocked() {
    return locked;
  }

  public boolean isPlugged() {
    if (scReaderChannelError) {
      return false; // use the same previous mechanism to return sim unit by channel error
    } else {
      return plugged.getValue();
    }
  }

  public ISCReaderSubChannels activate() {
    return simUnitManager.takeSCReaderSubChannels(sessionUuid);
  }

  public void release() {
    simUnitManager.returnSimUnit(sessionUuid);
  }

  public boolean isEnabled() {
    return enabled;
  }

  public boolean isWaitRelease() {
    return waitRelease;
  }

  public void setWaitRelease(final boolean waitRelease) {
    this.waitRelease = waitRelease;
  }

  public void gsmRegistration() {
    simUnitManager.gsmRegistration(sessionUuid, ++attemptNumber);
  }

  public void gsmRegistered() {
    attemptNumber = 0;
    simUnitManager.gsmRegistered(sessionUuid);
  }

  public void lock(final boolean lockStatus, final String reason) {
    locked = true;
    simUnitManager.lockSimUnit(sessionUuid, lockStatus, reason);
    handleEvent(SimEvent.simCardLocked(reason));
  }

  public void enable(final boolean enableStatus) {
    enabled = enableStatus;
    simUnitManager.enableSimUnit(sessionUuid, enableStatus);
  }

  public void handleFailSentSms(final int totalSmsParts, final int successSendSmsParts) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof SMSListener) {
        ((SMSListener) script).handleFailSentSMS(totalSmsParts - successSendSmsParts);
      }
    });
    simUnitManager.handleSimUnitFailSentSMS(simData.getUid(), totalSmsParts, successSendSmsParts);
  }

  public void handleSentSms(final PhoneNumber number, final String text, final int parts) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof SMSListener) {
        ((SMSListener) script).handleSentSMS(number.getValue(), text, parts);
      }
    });
    simUnitManager.handleSimUnitSentSMS(simData.getUid(), parts);
  }

  public void handleMultiPartSentSms(final UUID smsId, final List<SMSMessageOutbound> smsInfoOutbounds) {
    try {
      smsManager.saveMultiPartOutboundSms(getUid(), smsId, smsInfoOutbounds);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't process outbound sms: [{}], for ICCID: [{}]", this, smsInfoOutbounds, getUid(), e);
    }
    for (SMSMessageOutbound smsInfoOutbound : smsInfoOutbounds) {
      SMSStatusReport smsStatusReport = cacheSmsStatusReportMap.remove(smsInfoOutbound.getReferenceNumber());
      if (smsStatusReport != null) {
        handleReceivedSmsStatusReport(smsStatusReport);
      }
    }
  }

  public void handleEvent(final SimEvent event) {
    event.setSim(simData.getUid());
    try {
      historyLogger.handleSimEvent(event);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't handle event {} for {}", this, event, simData.getUid(), e);
    }
  }

  public void handleReceivedSms(final SMSMessageInbound smsMessageInbound) {
    try {
      smsManager.saveReceivedSms(getUid(), smsMessageInbound);
      if (smsMessageInbound.getReferenceNumber() == 0) {
        smsManager.updateReceivedSmsIdForSms(getUid(), smsMessageInbound.getSenderPhoneNumber(), smsMessageInbound.getReferenceNumber());
        handleReceivedSmsEvent(smsMessageInbound.getSenderPhoneNumber(), smsMessageInbound.getMessage(), smsMessageInbound.getSenderTime(), 1);
      } else if (smsManager.isReceivedAllMultiPartSms(getUid(), smsMessageInbound.getSenderPhoneNumber(), smsMessageInbound.getReferenceNumber(), smsMessageInbound.getCountOfSmsParts())) {
        UUID smsId = smsManager.updateReceivedSmsIdForSms(getUid(), smsMessageInbound.getSenderPhoneNumber(), smsMessageInbound.getReferenceNumber());
        Pair<String, Date> fullSmsMessageAndDate = smsManager.getFullReceivedSmsAndDate(smsId);
        if (fullSmsMessageAndDate != null) {
          handleReceivedSmsEvent(smsMessageInbound.getSenderPhoneNumber(), fullSmsMessageAndDate.first(), fullSmsMessageAndDate.second(), smsMessageInbound.getCountOfSmsParts());
        }
      }
    } catch (RemoteException e) {
      logger.warn("[{}] - can't process sms [{}] for ICCID: [{}]", this, smsMessageInbound, getUid(), e);
    }
  }

  public void handleReceivedSmsStatusReport(final SMSStatusReport smsStatusReport) {
    Pair<UUID, SMSMessageOutbound> messageOutboundPair = null;
    try {
      messageOutboundPair = smsManager.saveSmsStatusReport(getUid(), smsStatusReport);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't process sms status report for ICCID: [{}]", this, getUid(), e);
    }
    if (messageOutboundPair == null) {
      cacheSmsStatusReportMap.put(smsStatusReport.getReferenceNumber(), smsStatusReport);
      return;
    }
    UUID smsId = messageOutboundPair.first();
    SMSMessageOutbound sms = messageOutboundPair.second();
    handleEvent(SimEvent.smsStatus(smsStatusReport.getRecipientPhoneNumber(), smsStatusReport.getStatus().toString(), sms.getNumberOfSmsPart(), sms.getCountOfSmsParts(), smsId));

    simUnitManager.handleSimUnitSMSStatus(simData.getUid());
  }

  private void handleReceivedSmsEvent(final String senderPhoneNumber, final String message, final Date senderTime, final int countOfSmsParts) {
    try {
      activityLogger.logCallChannelReceivedSMS(channelUID, senderPhoneNumber, message, countOfSmsParts);
      handleEvent(SimEvent.smsReceived(senderPhoneNumber, message, senderTime, countOfSmsParts));
      handleIncomingSMS(senderPhoneNumber, message, countOfSmsParts);
    } catch (Exception e) {
      logger.warn("[{}] - while saving full SMS", this, e);
    }
  }

  public void handleActivityStarted(final ChannelUID gsmChannel, final GSMGroup gsmGroup) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof ActivityListener) {
        ((ActivityListener) script).handleActivityStarted(gsmGroup);
      }
    });
    simUnitManager.handleSimUnitStartedActivity(sessionUuid, gsmChannel, gsmGroup);
  }

  public void handleGenericEvent(final String event, final Serializable... args) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof GenericEventListener) {
        ((GenericEventListener) script).handleGenericEvent(event, args);
      }
    });

    byte[][] serializedArgs = new byte[args.length][];
    for (int i = 0; i < serializedArgs.length; ++i) {
      serializedArgs[i] = serialize(args[i]);
    }
    simUnitManager.handleGenericEvent(simData.getUid(), event, serializedArgs);
  }

  private byte[] serialize(final Serializable serializable) {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    ObjectOutputStream objectOutputStream;
    try {
      objectOutputStream = new ObjectOutputStream(out);
      objectOutputStream.writeObject(serializable);
      objectOutputStream.close();
    } catch (IOException e) {
      logger.warn("[{}] - while using ObjectOutputStream", this, e);
    }
    return out.toByteArray();
  }

  public void handleActivityStopped(final String stopReason) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof ActivityListener) {
        ((ActivityListener) script).handleActivityEnded();
      }
    });
    simUnitManager.handleSimUnitStoppedActivity(sessionUuid, stopReason);
  }

  public void handleCallEnded(final long callDuration, final int causeCode) {
    simData.setCallDuration(simData.getCallDuration() + callDuration);
    simData.setTotalCallsCount(simData.getTotalCallsCount() + 1);
    if (callDuration > 0) {
      simData.setSuccessfulCallsCount(simData.getSuccessfulCallsCount() + 1);
    }
    activityStop.reset();
    sessionStop.reset();
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleCallEnd(callDuration, causeCode);
      }
    });
    simUnitManager.handleSimUnitCallEnded(simData.getUid(), callDuration);
  }

  public boolean shouldStartActivity() {
    return activityStart.getValue();
  }

  public boolean forceShouldStopActivity() {
    activityStop.reset();
    return activityStop.getValue();
  }

  public boolean shouldStopActivity() {
    return activityStop.getValue();
  }

  public boolean forceShouldStopSession() {
    sessionStop.reset();
    return sessionStop.getValue();
  }

  public boolean shouldStopSession() {
    return sessionStop.getValue();
  }

  public synchronized void connect() {
    eventNotificationThread = new EventNotificationThread();
    eventNotificationThread.start();
  }

  public synchronized void disconnect() {
    if (eventNotificationThread != null) {
      EventNotificationThread eventNotificationThreadLocal = eventNotificationThread;
      eventNotificationThread = null;
      eventNotificationQueue.offer(script -> {
      });
      try {
        eventNotificationThreadLocal.join();
      } catch (InterruptedException e) {
        logger.warn("[{}] - while joining event notification thread was interrupted", this, e);
      }
    }
  }

  public boolean isOwned() {
    return this.owned;
  }

  public void setOwned(final boolean flag) {
    this.owned = flag;
  }

  public SimData getSimData() {
    return simData;
  }

  public synchronized void refreshData(final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    if (pluginsStoreUpdated(pluginsStore) || reconfiguration.getValue() || resetScriptStates) {
      Pair<SimData, IMSI> simDataPair = simUnitManager.getSimData(sessionUuid);
      if (simDataPair == null) { // remote error or illegal session uid
        logger.warn("[{}] - illegal session uuid [{}]", this, sessionUuid);
        return;
      }

      SIMGroup prevSimGroup = simData.getSimGroup();
      simData = simDataPair.first();
      imsi = simDataPair.second();

      reconfiguration.reset();
      reinitData();

      if (simGroupUpdated(simData.getSimGroup(), prevSimGroup) || resetScriptStates) {
        endRefreshData(pluginsStore, resetScriptStates);
      }
    }
  }

  private boolean simGroupUpdated(final SIMGroup simGroup, final SIMGroup prevSimGroup) {
    return !Objects.equals(simGroup, prevSimGroup) || simGroup.getRevision() >= 0 && prevSimGroup.getRevision() != simGroup.getRevision();
  }

  private boolean pluginsStoreUpdated(final AntraxPluginsStore pluginsStore) {
    return lastPluginsStoreRevision < pluginsStore.getRevision();
  }

  public void beginRefreshData() throws Exception {

    Pair<SimData, IMSI> simDataPair = simUnitManager.getSimData(sessionUuid);
    if (simDataPair == null) {
      throw new Exception("failed to refresh sim unit data");
    }

    simData = simDataPair.first();
    imsi = simDataPair.second();
    reinitData();
  }

  private void reinitData() {
    enabled = simData.isEnabled();
    locked = simData.isLocked();
    activityLogger.logSIMChangedEnableStatus(channelUID, enabled);
    SIMGroup group = simData.getSimGroup();
    activityLogger.logSIMChangedGroup(channelUID, group.getName());
  }

  public synchronized void endRefreshData(final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    logger.debug("[{}] - setting up scripts", this);
    setBusinessActivityScripts(getBusinessActivityScripts(pluginsStore), pluginsStore, resetScriptStates);
    setActionProviderScripts(getActionProviderScripts(pluginsStore), pluginsStore, resetScriptStates);
    setFactorScript(getFactorScript(pluginsStore), pluginsStore, resetScriptStates);
    setIncomingCallScript(getIncomingCallScript(pluginsStore), pluginsStore, resetScriptStates);
    setCallFilterScripts(getCallFilterScript(pluginsStore), pluginsStore, resetScriptStates);
    setSmsFilterScripts(getSmsFilterScripts(pluginsStore), pluginsStore, resetScriptStates);
    lastPluginsStoreRevision = pluginsStore.getRevision();
  }

  private CallFilterScript[] getCallFilterScript(final AntraxPluginsStore pluginsStore) {
    ScriptInstance[] scripts = simData.getSimGroup().getCallFilterScripts();
    List<CallFilterScript> retval = new LinkedList<>();
    for (ScriptInstance s : scripts) {
      if (s == null) {
        continue;
      }

      try {
        retval.add(pluginsStore.instantiate(s, CallFilterScript.class, simData.getSimGroup().listScriptCommons()));
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate call filter script", this, e);
      }
    }
    return retval.toArray(new CallFilterScript[retval.size()]);
  }

  private SmsFilterScript[] getSmsFilterScripts(final AntraxPluginsStore pluginsStore) {
    ScriptInstance[] scripts = simData.getSimGroup().getSmsFilterScripts();
    List<SmsFilterScript> retval = new LinkedList<>();
    for (ScriptInstance s : scripts) {
      if (s == null) {
        continue;
      }

      try {
        retval.add(pluginsStore.instantiate(s, SmsFilterScript.class, simData.getSimGroup().listScriptCommons()));
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate SMS filter script", this, e);
      }
    }
    return retval.toArray(new SmsFilterScript[retval.size()]);
  }

  private IncomingCallScript getIncomingCallScript(final AntraxPluginsStore pluginsStore) {
    ScriptInstance script = simData.getSimGroup().getIncomingCallManagementScript();
    if (script == null) {
      return new DefaultIncomingCallScript();
    }

    IncomingCallScript retval;
    try {
      retval = pluginsStore.instantiate(simData.getSimGroup().getIncomingCallManagementScript(), IncomingCallScript.class,
          simData.getSimGroup().listScriptCommons());
    } catch (Throwable e) {
      logger.warn("[{}] - failed to instantiate incoming call management script", this, e);
      retval = new DefaultIncomingCallScript();
    }
    return retval;
  }

  private FactorEvaluationScript getFactorScript(final AntraxPluginsStore pluginsStore) {
    ScriptInstance script = simData.getSimGroup().getVSFactorScript();
    if (script == null) {
      return new DefaultFactorEvaluationScript();
    }

    FactorEvaluationScript retval;
    try {
      retval = pluginsStore.instantiate(simData.getSimGroup().getVSFactorScript(), FactorEvaluationScript.class,
          simData.getSimGroup().listScriptCommons());
    } catch (Throwable e) {
      logger.warn("[{}] - failed to instantiate factor script", this, e);
      retval = new DefaultFactorEvaluationScript();
    }
    return retval;
  }

  private ActionProviderScript[] getActionProviderScripts(final AntraxPluginsStore pluginsStore) {
    ScriptInstance[] scripts = simData.getSimGroup().getActionProviderScripts();
    List<ActionProviderScript> retval = new LinkedList<>();
    for (ScriptInstance s : scripts) {
      if (s == null) {
        continue;
      }

      try {
        retval.add(pluginsStore.instantiate(s, ActionProviderScript.class, simData.getSimGroup().listScriptCommons()));
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate action provider script", this, e);
      }
    }
    return retval.toArray(new ActionProviderScript[retval.size()]);
  }

  private BusinessActivityScript[] getBusinessActivityScripts(final AntraxPluginsStore pluginsStore) {
    ScriptInstance[] scripts = simData.getSimGroup().getBusinessActivityScripts();
    List<BusinessActivityScript> retval = new LinkedList<>();
    for (ScriptInstance s : scripts) {
      if (s == null) {
        continue;
      }

      try {
        retval.add(pluginsStore.instantiate(s, BusinessActivityScript.class, simData.getSimGroup().listScriptCommons()));
      } catch (Throwable e) {
        logger.warn("[{}] - failed to instantiate business activity script", this, e);
      }
    }
    return retval.toArray(new BusinessActivityScript[retval.size()]);
  }

  public void setBusinessActivityScripts(final BusinessActivityScript[] scripts, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(scripts);
    businessScripts.setNewInstance(pluginsStore, resetScriptStates, scripts);
  }

  public void setActionProviderScripts(final ActionProviderScript[] scripts, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(scripts);
    actionProviders.setNewInstance(pluginsStore, resetScriptStates, scripts);
  }

  public void setCallFilterScripts(final CallFilterScript[] scripts, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(scripts);
    callFilterScripts.setNewInstance(pluginsStore, resetScriptStates, scripts);
  }

  public void setSmsFilterScripts(final SmsFilterScript[] scripts, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(scripts);
    this.smsFilterScripts.setNewInstance(pluginsStore, resetScriptStates, scripts);
  }

  public void setFactorScript(final FactorEvaluationScript script, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(script);
    factorScript.setNewInstance(pluginsStore, resetScriptStates, script);
  }

  public void setIncomingCallScript(final IncomingCallScript script, final AntraxPluginsStore pluginsStore, final boolean resetScriptStates) {
    initialize(script);
    incomingCallScript.setNewInstance(pluginsStore, resetScriptStates, script);
    supportOrigination = (script instanceof OriginationIncomingCallScript);
  }

  private void initialize(final Object... scripts) {
    for (Object script : scripts) {
      if (script instanceof SimDataListener) {
        ((SimDataListener) script).setSimData(simData);
      }
      if (script instanceof SimCallHistoryListener) {
        ((SimCallHistoryListener) script).setSimCallHistory(simCallHistory);
      }
      if (script instanceof RegistryAccessListener) {
        ((RegistryAccessListener) script).setRegistryAccess(registry);
      }
    }
  }

  public long countSimFactor() {
    try {
      return factorScript.takeScriptsForUse()[0].countFactor();
    } finally {
      factorScript.untakeScripts();
    }
  }

  @Override
  public String toString() {
    return channelUID.toString();
  }

  public void handleIncomingSMS(final String phoneNumber, final String text, final int parts) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof SMSListener) {
        ((SMSListener) script).handleIncomingSMS(phoneNumber, text);
      }
    });
    simUnitManager.handleSimUnitIncomingSMS(simData.getUid(), parts);
  }

  public void handleCallSetup(final PhoneNumber phoneNumber) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleCallSetup(phoneNumber);
      }
    });
  }

  public void handleIvr(final String ivrTemplateName, final CallDropReason callDropReason) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof IvrListener) {
        ((IvrListener) script).handleIvr(ivrTemplateName, callDropReason.getCauseCode());
      }
    });
  }

  public void handleCallStarted(final PhoneNumber phoneNumber) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleCallStart(phoneNumber);
      }
    });
  }

  public void handleCallForwarding() {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleCallForwarded();
      }
    });
  }

  public void handleFAS() {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleFAS();
      }
    });
  }

  public void handleIncomingCallDropped(final long callDuration) {
    simData.setIncomingCallDuration(simData.getIncomingCallDuration() + callDuration);
    simData.setIncomingTotalCallsCount(simData.getIncomingTotalCallsCount() + 1);
    if (callDuration > 0) {
      simData.setIncomingSuccessfulCallsCount(simData.getSuccessfulCallsCount() + 1);
    }

    eventNotificationQueue.offer(script -> {
      if (script instanceof IncomingCallsListener) {
        synchronized (script) {
          ((IncomingCallsListener) script).handleIncomingCallDropped(callDuration);
        }
      }
    });
    simUnitManager.handleSimUnitIncomingCallEnded(simData.getUid(), callDuration);
  }

  public void handleIncomingCallAnswered() {
    eventNotificationQueue.offer(script -> {
      if (script instanceof IncomingCallsListener) {
        synchronized (script) {
          ((IncomingCallsListener) script).handleIncomingCallAnswered();
        }
      }
    });
  }

  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) {
    long prevGroupId = simData.getSimGroup().getID();
    String prevName = simData.getSimGroup().getName();
    try {
      for (BusinessActivityScript s : businessScripts.takeScriptsForUse()) {
        if (prevGroupId != simData.getSimGroup().getID()) {
          logger.warn("[{}] - while invoke business activity group change: prev-{}, new-{}, will break activity", this, prevName, simData.getSimGroup().getName());
          break;
        }

        if (s.shouldStartBusinessActivity()) {
          try {
            businessActivityDescription = s.describeBusinessActivity();
            logger.debug("[{}] - invoke business activity [{}]", channel, businessActivityDescription);
            handleEvent(SimEvent.businessActivityStarted(businessActivityDescription));
            startShowingState(CallChannelState.State.STARTED_BUSINESS_ACTIVITY, businessActivityDescription);
            s.invokeBusinessActivity(channel);
          } catch (Exception e) {
            logger.warn("[{}] - failed to invoke business activity", this, e);
            channel.lock(e.getMessage());
          } finally {
            finishShowingState(CallChannelState.State.STARTED_BUSINESS_ACTIVITY, CallChannelState.State.READY_TO_CALL);
            handleEvent(SimEvent.businessActivityStopped());
          }
        }
      }
    } finally {
      businessActivityDescription = null;
      businessScripts.untakeScripts();
    }
  }

  public String getBusinessActivityDescription() {
    return businessActivityDescription;
  }

  public boolean isSupportOrigination() {
    return supportOrigination;
  }

  public PhoneNumber getOriginationCalledNumber() {
    try {
      return ((OriginationIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).getCalledNumber();
    } finally {
      incomingCallScript.untakeScripts();
    }
  }

  public String getTargetAddress() {
    try {
      return ((OriginationIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).getTargetAddress();
    } finally {
      incomingCallScript.untakeScripts();
    }
  }


  public PhoneNumber substituteOriginationCallerNumber(final PhoneNumber callerNumber) {
    try {
      return ((OriginationIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).substituteCallerNumber(callerNumber);
    } finally {
      incomingCallScript.untakeScripts();
    }
  }

  public boolean shouldAnswerIncomingCall() {
    try {
      return ((ChancesBasedIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).shouldAnswer();
    } finally {
      incomingCallScript.untakeScripts();
    }
  }

  public boolean shouldDropIncomingCall() {
    try {
      return ((ChancesBasedIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).shouldDrop();
    } finally {
      incomingCallScript.untakeScripts();
    }
  }

  public void dropActionIncomingCall(final RegisteredInGSMChannel channel) {
    try {
      ((ChancesBasedIncomingCallScript) incomingCallScript.takeScriptsForUse()[0]).dropAction(channel);
    } catch (Exception e) {
      logger.warn("[{}] - failed to invoke drop action", this, e);
      channel.lock(e.getMessage());
    } finally {
      incomingCallScript.untakeScripts();
    }
  }

  public boolean shouldStartBusinessActivity() {
    try {
      boolean retval = false;
      for (BusinessActivityScript s : businessScripts.takeScriptsForUse()) {
        if (s.shouldStartBusinessActivity()) {
          retval = true;
          break;
        }
      }
      return retval;
    } finally {
      businessScripts.untakeScripts();
    }
  }

  public boolean isSupportsAction(final String action) {
    try {
      for (ActionProviderScript s : actionProviders.takeScriptsForUse()) {
        if (s.getProvidedAction().equals(action)) {
          return true;
        }
      }
      return false;
    } finally {
      actionProviders.untakeScripts();
    }
  }

  public ActionProviderScript getActionProvider(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    try {
      for (ActionProviderScript s : actionProviders.takeScriptsForUse()) {
        if (s.getProvidedAction().equals(action.getAction())) {
          if (s.canExecuteAction(action, channel)) {
            return s;
          }
        }
      }
      actionProviders.untakeScripts();
      return null;
    } catch (Exception e) {
      actionProviders.untakeScripts();
      throw e;
    }
  }

  public void handleIncomingCall(final PhoneNumber number) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof IncomingCallsListener) {
        synchronized (script) {
          ((IncomingCallsListener) script).handleIncomingCall(number);
        }
      }
    });
  }

  public void handleCallError(final int callControlConnectionManagementCause) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleCallError(callControlConnectionManagementCause);
      }
    });
  }

  public void handleDialError(final String errorStatus) {
    eventNotificationQueue.offer(script -> {
      if (script instanceof CallsListener) {
        ((CallsListener) script).handleDialError(errorStatus);
      }
    });
  }

  // FIXME: use only one method getChannelUID or getSimUnitId
  public ChannelUID getSimUnitId() {
    return channelUID;
  }

  public void handleAllowedInternet(final boolean allowed) {
    simData.setAllowInternet(allowed);
    simUnitManager.handleAllowedInternet(simData.getUid(), allowed);
  }

  private interface IEventNotification {

    void notifyScript(Object script);

  }

  private class EventNotificationThread extends Thread {

    private IEventNotification eventNotification;

    public EventNotificationThread() {
      super("EventNotification:" + getChannelUID());
    }

    @Override
    public void run() {
      Thread thisThread = Thread.currentThread();
      while (thisThread == eventNotificationThread) {
        try {
          eventNotification = eventNotificationQueue.take();

          notifyScripts(businessScripts);
          notifyScripts(actionProviders);
          notifyScripts(factorScript);
          notifyScripts(incomingCallScript);
          notifyScripts(callFilterScripts);
          notifyScripts(smsFilterScripts);
        } catch (InterruptedException e) {
          logger.warn("[{}] - error: ", this, e);
          return;
        }
      }
    }

    private <T> void notifyScripts(final ScriptHolder<T> scripts) {
      try {
        for (Object s : scripts.takeScriptsForUse()) {
          try {
            eventNotification.notifyScript(s);
          } catch (Exception e) {
            logger.warn("[{}] - failed to notify script [{}]", this, s, e);
          }
        }
      } finally {
        scripts.untakeScripts();
      }
    }
  }

  public void executeAction(final ActionProviderScript script, final Action action, final RegisteredInGSMChannel channel, final UUID executionID) throws Exception {
    try {
      startShowingState(CallChannelState.State.STARTED_BUSINESS_ACTIVITY, "action: " + action.getAction());
      handleEvent(SimEvent.automationActionExecutionStarted(action, executionID));
      script.executeAction(action, channel);
      actionProviders.untakeScripts();
      handleEvent(SimEvent.automationActionExecutionStopped());
    } catch (Exception e) {
      handleEvent(SimEvent.automationActionExecutionFailed(e.getMessage()));
      throw e;
    } finally {
      finishShowingState(CallChannelState.State.STARTED_BUSINESS_ACTIVITY, CallChannelState.State.READY_TO_CALL);
    }
  }

  public void changeState(final CallChannelState.State state) {
    showState = saveState = state;
    showAdvInfo = saveAdvInfo = "";
  }

  public void startShowingState(final CallChannelState.State state, final String advInfo) {
    saveState = showState;
    showState = state;
    saveAdvInfo = showAdvInfo;
    showAdvInfo = advInfo;

    activityLogger.logCallChannelChangedState(getSimUnitId(), state, advInfo, -1);
  }

  public void finishShowingState(final CallChannelState.State state, final CallChannelState.State finishState) {
    if (showState == state) {
      showState = saveState;
      showAdvInfo = saveAdvInfo;
      activityLogger.logCallChannelChangedState(getSimUnitId(), showState, showAdvInfo, -1);
    }
    saveState = finishState;
    saveAdvInfo = "";
  }

  public CallChannelState.State getShowState() {
    return this.showState;
  }

  public void addUserMessage(final String message) {
    simUnitManager.addUserMessage(sessionUuid, message);
  }

  public void changeGroup(final SimGroupReference simGroup, final AntraxPluginsStore pluginsStore) throws Exception {
    logger.debug("[{}] - changeGroup requested to {}", this, simGroup);
    SimGroupReferenceImpl group = (SimGroupReferenceImpl) simGroup;
    long prevGroupId = simData.getSimGroup().getID();
    if (prevGroupId == group.getId()) {
      logger.debug("[{}] - no need to change group, it is already {}", this, simGroup);
      return;
    }

    logger.debug("[{}] - asking sim-server to change group from {} to {}", this, prevGroupId, group.getId());
    SIMGroup changedSimGroup = simUnitManager.changeSimGroup(simData.getUid(), group.getId());
    if (changedSimGroup == null) {
      throw new Exception("failed to change sim group");
    }

    simData.setSimGroup(changedSimGroup);
    endRefreshData(pluginsStore, false);

    logger.debug("[{}] - end changing group. New group [{}]", this, changedSimGroup);
  }

  public boolean shouldGenerateIMEI() {
    return simUnitManager.isSimUnitShouldGenerateIMEI(sessionUuid);
  }

  public IMEI generateIMEI() {
    return simUnitManager.generateIMEI(sessionUuid);
  }

  public void resetIMEI() {
    simUnitManager.resetIMEI(sessionUuid, "by 'reset IMEI on event' script");
  }

  public void updatePhoneNumber(final PhoneNumber phoneNumber) {
    simUnitManager.updatePhoneNumber(sessionUuid, phoneNumber);
  }

  public void setTariffPlanEndDate(final long endDate) {
    simUnitManager.setTariffPlanEndDate(simData.getUid(), endDate);
  }

  public void setBalance(final double balance) {
    simUnitManager.setBalance(simData.getUid(), balance);
  }

  public void setOperatorSelectionMode(final OperatorSelectionMode operatorSelectionMode) {
    this.operatorSelectionMode = operatorSelectionMode;
  }

  public OperatorSelectionMode getOperatorSelectionMode() {
    return operatorSelectionMode;
  }

  public void handleSCReaderChannelError() {
    scReaderChannelError = true;
  }

}
