/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.GuardMediaStream;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IOriginatingCallHandler;
import com.flamesgroup.jiax2.ITerminatingCallHandler;
import com.flamesgroup.unit.IExtendedCallErrorReport;
import com.flamesgroup.unit.IMobileOriginatingCallHandler;
import com.flamesgroup.unit.IMobileTerminatingCallHandler;
import com.flamesgroup.unit.MobileCallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

public abstract class Call implements Runnable, IMobileTerminatingCallHandler, IMobileOriginatingCallHandler, ITerminatingCallHandler, IOriginatingCallHandler {

  private static final Logger logger = LoggerFactory.getLogger(Call.class);

  protected enum State {
    DIAL, DTMF, ALERTING, ANSWER, HANGUP
  }

  protected final CallChannel callChannel;
  protected final CDR cdr;

  protected final Lock lock = new ReentrantLock();
  protected final Condition condition = lock.newCondition();

  protected final Queue<State> states = new LinkedList<>();
  protected State currentState = State.DIAL; // initial state

  protected IStateAdapter stateAdapter;

  protected final AtomicBoolean callReleased = new AtomicBoolean();

  private final List<ICallStatusListener> callStatusListeners = new ArrayList<>();

  protected abstract void processAlerting();

  protected abstract void processAnswer();

  protected abstract void dropGSM(CallDropReason callDropReason, CdrDropReason cdrDropReason);

  protected abstract void dropVoIP(MobileCallDropReason reason, IExtendedCallErrorReport report);

  protected abstract void dropAll(CallDropReason callDropReason, CdrDropReason cdrDropReason);

  Call(final CallChannel callChannel, final CDR cdr) {
    this.callChannel = callChannel;
    this.cdr = cdr;
  }

  protected IMediaStream wrapMediaStream(final IMediaStream mediaStream) {
    return new GuardMediaStream(mediaStream);
  }

  // Runnable
  @Override
  public void run() {
    while (!isReleased()) {
      State currentStateLocal;
      lock.lock();
      try {
        if (states.size() <= 0) {
          condition.await();
        }

        currentStateLocal = currentState = states.poll();
      } catch (InterruptedException e) {
        logger.warn("[{}] - while wait state for incoming call was interrupted", callChannel, e);
        return;
      } finally {
        lock.unlock();
      }

      switch (currentStateLocal) {
        case ALERTING:
          notifyCallStatusListeners(ICallStatusListener::handleAlerting);
          processAlerting();
          break;
        case ANSWER:
          notifyCallStatusListeners(ICallStatusListener::handleAnswer);
          processAnswer();
          break;
        case DTMF:
          stateAdapter.processState();
          break;
        case HANGUP:
          notifyCallStatusListeners(ICallStatusListener::handleHangup);
          stateAdapter.processState();
          break;
      }
    }
  }

  // IMobileTerminatingCallHandler
  @Override
  public void handleHeld() {
    logger.debug("[{}] - got HELD", callChannel);
  }

  @Override
  public void handleUnheld() {
    logger.debug("[{}] - got UNHELD", callChannel);
  }

  @Override
  public void handleDropped(final MobileCallDropReason reason, final IExtendedCallErrorReport report) {
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        logger.debug("[{}] - got DROP from GSM with cause [{}]", callChannel, report.getCallControlConnectionManagementCauseDescription());
        states.offer(State.HANGUP);
        stateAdapter = () -> dropVoIP(reason, report);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // IMobileOriginatingCallHandler
  @Override
  public void handleAlerting() {
    lock.lock();
    try {
      if (currentState.compareTo(State.ALERTING) < 0) {
        logger.debug("[{}] - got ALERTING", callChannel);
        states.offer(State.ALERTING);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void handleAnswer() {
    lock.lock();
    try {
      if (currentState.compareTo(State.ANSWER) < 0) {
        logger.debug("[{}] - got ANSWER", callChannel);
        states.offer(State.ANSWER);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // ITerminatingCallHandler
  @Override
  public void handleHangup(final CallDropReason callDropReason) {
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        logger.debug("[{}] - got DROP from VoIP with reason [{}({})]", callChannel, callDropReason, callDropReason.getCauseCode());
        states.offer(State.HANGUP);
        stateAdapter = () -> dropGSM(callDropReason, CdrDropReason.VOIP); // originating and terminating calls
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  // IOriginatingCallHandler
  @Override
  public void handleRinging() {
    handleAlerting();
  }

  @Override
  public void handleAccept() { // originating call
    logger.debug("[{}] - got ACCEPT", callChannel);
  }

  public boolean isReleased() {
    return callReleased.get();
  }

  public void drop(final CallDropReason callDropReason, final CdrDropReason cdrDropReason) {
    lock.lock();
    try {
      if (currentState.compareTo(State.HANGUP) < 0) {
        logger.debug("[{}] - got DROP from VS (scripts or releasing call channel) with reason ", callChannel);
        states.offer(State.HANGUP);
        stateAdapter = () -> dropAll(callDropReason, cdrDropReason);
        condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }

  public void addCallStatusListener(final ICallStatusListener callListener) {
    callStatusListeners.add(callListener);
  }

  public void removeCallStatusListener(final ICallStatusListener callListener) {
    callStatusListeners.remove(callListener);
  }

  public void notifyCallStatusListeners(final Consumer<ICallStatusListener> consumer) {
    callStatusListeners.forEach(consumer);
  }

  protected interface IStateAdapter {

    void processState();

  }

}
