/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

/**
 * Pearson product-moment correlation coefficient for a sample
 * https://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient
 */
public final class Correlator {

  public static final double CORRELATION_THRESHOLD = 0.90;

  private Correlator() {
  }

  public static double correlate(final ICorrelationData x, final ICorrelationData y) {
    assert x.getLength() == y.getLength();

    final int n = x.getLength();

    double sumOfXMultiplyY = 0;
    for (int i = 0; i < n; i++) {
      sumOfXMultiplyY += x.get(i) * y.get(i);
    }

    // covariation
    double cov = n * sumOfXMultiplyY - x.getSum() * y.getSum();
    // standard error of x
    double sigmaX = Math.sqrt(n * x.getSumOfSquares() - x.getSum() * x.getSum());
    // standard error of y
    double sigmaY = Math.sqrt(n * y.getSumOfSquares() - y.getSum() * y.getSum());

    // correlation is just a normalized covariation
    return cov / sigmaX / sigmaY;
  }

}
