/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

public final class TemplateCorrelationData implements ICorrelationData {

  private final double[] data;
  private final double sum;
  private final double sumOfSquares;

  public TemplateCorrelationData(final double[] data) {
    this.data = data;
    double sumLocal = 0;
    double sumOfSquaresLocal = 0;
    for (double value : data) {
      sumLocal += value;
      sumOfSquaresLocal += value * value;
    }
    this.sum = sumLocal;
    this.sumOfSquares = sumOfSquaresLocal;
  }

  @Override
  public int getLength() {
    return data.length;
  }

  @Override
  public double get(final int index) {
    return data[index];
  }

  @Override
  public double getSum() {
    return sum;
  }

  @Override
  public double getSumOfSquares() {
    return sumOfSquares;
  }

}

