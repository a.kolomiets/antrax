/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.configuration.AntraxConfigurationHelper;
import com.flamesgroup.antrax.configuration.ControlServerTimeDifferenceChecker;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannels;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannels;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.voiceserver.manager.VoiceServerManager;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.antrax.voiceserver.statuses.VoiceServerReportBasic;
import com.flamesgroup.antrax.voiceserver.statuses.VoiceServerReportChannels;
import com.flamesgroup.antrax.voiceserver.statuses.VoiceServerReportStatistic;
import com.flamesgroup.antrax.voiceserver.statuses.VoiceServerStatus;
import com.flamesgroup.daemonext.DaemonExt;
import com.flamesgroup.daemonext.IDaemonExtStatus;
import com.flamesgroup.properties.ReloadCommand;
import com.flamesgroup.rmi.RemoteExporter;
import com.flamesgroup.rmi.RemoteServiceExporter;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.utils.BuildInfo;
import com.flamesgroup.utils.BuildInfoUtils;
import com.flamesgroup.utils.OperatorsStorage;
import com.flamesgroup.utils.UncaughtExceptionHandlerWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

public class VoiceServerLauncher extends DaemonExt {

  private final static Logger logger = LoggerFactory.getLogger(VoiceServerLauncher.class);

  private VoiceServerStatus voiceServerStatus;
  private VoiceServerReportBasic reportBasic;
  private VoiceServerReportStatistic reportStatistic;
  private VoiceServerReportChannels reportChannels;
  private ReloadCommand reloadCommand;

  private VoiceServerManager voiceServerManager;

  public static void main(final String[] args) throws Exception {
    VoiceServerLauncher voiceServerLauncher = new VoiceServerLauncher();
    voiceServerLauncher.main(voiceServerLauncher.getClass().getSimpleName(), args);
  }

  @Override
  public List<IDaemonExtStatus> getStatuses() {
    return Arrays.asList(voiceServerStatus, reportBasic, reportStatistic, reportChannels, reloadCommand);
  }

  @Override
  public void start() throws Exception {
    BuildInfo buildInfo = BuildInfoUtils.readBuildInfo(this.getClass());
    logDuplicateInfo("Voice Server start: " + (buildInfo == null ? "buildInfo didn't found at JAR" : buildInfo.toString()));
    try {
      start0();
      super.start();
    } catch (Exception e) {
      logDuplicateError("Voice Server start failed", e);
      System.exit(EXIT_CODE_TO_BE_RESTARTED);
    }
  }

  @Override
  public void stop() throws Exception {
    logDuplicateInfo("Voice Server stop");
    super.stop();
    voiceServerManager.shutdown();
  }

  private void start0() throws Exception {
    ControlServerTimeDifferenceChecker.isAllowable(System.currentTimeMillis());

    reloadCommand = new ReloadCommand();
    reloadCommand.addReloadListener(VoiceServerPropUtils.getInstance());
    //call getInstance() to do the first initialization of the OperatorsStorage
    OperatorsStorage.getInstance();
    VoiceServerBuilder builder = new VoiceServerBuilder();
    IActivityLogger activityLogger = builder.getActivityLogger();
    voiceServerStatus = new VoiceServerStatus(activityLogger);
    reportBasic = new VoiceServerReportBasic(activityLogger);
    reportStatistic = new VoiceServerReportStatistic(activityLogger);
    reportChannels = new VoiceServerReportChannels(activityLogger);
    voiceServerManager = builder.createVoiceServerManager();
    ISCReaderSubChannelsHandler scReaderSubChannelsHandler = builder.getOrCreateSCReaderSubChannelsHandler();
    IRawATSubChannels rawATSubChannels = builder.getOrCreateRawATSubChannels();
    ISCEmulatorSubChannels scEmulatorSubChannels = builder.getOrCreateSCEmulatorSubChannels();

    reloadCommand.addReloadListener(builder.getIvrTemplateManager());

    IServerData serverData = AntraxConfigurationHelper.getInstance().getMyServer();
    System.setProperty("java.rmi.server.hostname", serverData.getPublicAddress().getHostAddress());
    int port = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getRmiRegistryPort();

    RemoteServiceExporter.export(new RemoteExporter.Builder(voiceServerManager, IVoiceServerManager.class).setRegistryPort(port).build());
    RemoteServiceExporter.export(new RemoteExporter.Builder(scReaderSubChannelsHandler, ISCReaderSubChannelsHandler.class).setRegistryPort(port).build());
    RemoteServiceExporter.export(new RemoteExporter.Builder(rawATSubChannels, IRawATSubChannels.class).setRegistryPort(port).build());
    RemoteServiceExporter.export(new RemoteExporter.Builder(scEmulatorSubChannels, ISCEmulatorSubChannels.class).setRegistryPort(port).build());

    voiceServerManager.start();

    Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerWrapper((t, e) -> {
      logger.error("[{}] - Voice Server - Uncaught Exception in thread [{}]", this, t, e);

      logDuplicateInfo("Voice Server stop on Uncaught Exception");
      voiceServerManager.shutdown();

      if (!(e instanceof ControlServerPingException)) {
        System.exit(EXIT_CODE_TO_BE_RESTARTED);
      }

      long controlServerPingTimeout = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getPingControlServerTimeout();
      while (true) {
        try {
          Thread.sleep(controlServerPingTimeout);
        } catch (InterruptedException ignored) {
          throw new AssertionError(e); // actually never can happen, because no one have reference to this thread
        }

        try {
          logDuplicateInfo("Voice Server internal start");
          start0();
        } catch (RemoteAccessException ignored) {
          logDuplicateInfo("Control server is not responding, try more time late");
          continue;
        } catch (Exception ex) {
          logDuplicateError("Voice Server internal start failed", ex);
          System.exit(EXIT_CODE_TO_BE_RESTARTED);
        }
        break;
      }
    }));
  }

  private void logDuplicateInfo(final String message) {
    logger.info(message);
    getOut().println(OffsetDateTime.now() + " - " + message);
  }

  private void logDuplicateError(final String message, final Exception e) {
    logger.error(message, e);
    getOut().println(OffsetDateTime.now() + " - " + message + ": " + e.getMessage());
  }

}
