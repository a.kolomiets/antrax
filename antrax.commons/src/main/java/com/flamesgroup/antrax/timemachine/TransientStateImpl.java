/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.timemachine;

public class TransientStateImpl implements TransientState {

  private final long period;

  private State nextState;

  public TransientStateImpl(final long period) {
    this.period = period;
  }

  @Override
  public long getPeriod() {
    return period;
  }

  @Override
  public void enterState() {
  }

  @Override
  public State getNextState() {
    return nextState;
  }

  public void setNextState(final State state) {
    nextState = state;
  }

  @Override
  public void tickRoutine() {
  }

}
