/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.commons;

import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.IDevice;
import com.flamesgroup.device.commonb.ILoggerChannel;
import com.flamesgroup.device.commonb.ILoggerSubChannelHandler;
import com.flamesgroup.device.commonb.LoggerLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DeviceLoggerHelper {

  private DeviceLoggerHelper() {
  }

  public static void startLogDevice(final IDevice device, final DeviceUID deviceUID) throws ChannelException, InterruptedException {
    Logger deviceLogger = LoggerFactory.getLogger("cpu-logs");
    LoggerLevel loggerLevel = LoggerLevel.LOGGER_INFO;
    if (deviceLogger.isTraceEnabled() || deviceLogger.isDebugEnabled()) {
      loggerLevel = LoggerLevel.LOGGER_DEBUG;
    } else if (deviceLogger.isInfoEnabled()) {
      loggerLevel = LoggerLevel.LOGGER_INFO;
    } else if (deviceLogger.isWarnEnabled()) {
      loggerLevel = LoggerLevel.LOGGER_NOTICE;
    } else if (deviceLogger.isErrorEnabled()) {
      loggerLevel = LoggerLevel.LOGGER_ERROR;
    }
    for (int i = 0; i < device.getCPUEntries().size(); i++) {
      ILoggerChannel loggerChannel = device.getCPUEntries().get(i).getLoggerChannel();
      loggerChannel.enslave();
      try {
        loggerChannel.getLoggerSubChannel().start(loggerLevel, new LoggerSubChannelHandler(deviceLogger, String.format("%s_cpu%02d", deviceUID.getDeviceType().toString().toLowerCase(), i)));
      } catch (ChannelException | InterruptedException e) {
        try {
          loggerChannel.free();
        } catch (final ChannelException ex) {
          ex.addSuppressed(e);
          throw ex;
        }
        throw e;
      }

    }
  }

  public static void stopLogDevice(final IDevice device) throws ChannelException, InterruptedException {
    for (int i = 0; i < device.getCPUEntries().size(); i++) {
      ILoggerChannel loggerChannel = device.getCPUEntries().get(i).getLoggerChannel();
      try {
        loggerChannel.getLoggerSubChannel().stop();
      } catch (ChannelException | InterruptedException e) {
        try {
          loggerChannel.free();
        } catch (final ChannelException ex) {
          ex.addSuppressed(e);
          throw ex;
        }
        throw e;
      }
      loggerChannel.free();
    }
  }

  public static class LoggerSubChannelHandler implements ILoggerSubChannelHandler {

    private final Logger logger;
    private final String fileName;
    private final StringBuilder buffer = new StringBuilder();
    private final StringBuilder data = new StringBuilder();

    public LoggerSubChannelHandler(final Logger logger, final String fileName) {
      this.logger = logger;
      this.fileName = fileName;
    }

    @Override
    public void handleLoggerData(final String loggerData) {
      MDC.put("fileName", fileName);
      data.delete(0, data.length());
      data.append(buffer);
      buffer.delete(0, buffer.length());
      int index = loggerData.lastIndexOf('\n') + 1;
      if (index != loggerData.length()) {
        data.append(loggerData.substring(0, index));
        buffer.append(loggerData.substring(index, loggerData.length()));
      } else {
        data.append(loggerData);
      }
      if (logger.isTraceEnabled() || logger.isDebugEnabled()) {
        logger.debug(data.toString());
      } else if (logger.isInfoEnabled()) {
        logger.info(data.toString());
      } else if (logger.isWarnEnabled()) {
        logger.warn(data.toString());
      } else if (logger.isErrorEnabled()) {
        logger.error(data.toString());
      }
    }

  }

}
