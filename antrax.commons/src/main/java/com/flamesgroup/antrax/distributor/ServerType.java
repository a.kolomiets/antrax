/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.distributor;

import com.flamesgroup.device.DeviceType;

import java.util.EnumSet;
import java.util.Set;

public enum ServerType {

  SIM_SERVER(EnumSet.of(DeviceType.SIMB)),
  VOICE_SERVER(EnumSet.of(DeviceType.GSMB, DeviceType.GSMB2, DeviceType.GSMB3));

  private final Set<DeviceType> deviceTypes;

  ServerType(final EnumSet<DeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  public Set<DeviceType> getDeviceTypes() {
    return deviceTypes;
  }

}
