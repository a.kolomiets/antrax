/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.distributor;

import com.flamesgroup.device.protocol.cudp.ExpireTime;

import java.io.Serializable;
import java.util.Objects;

public final class ChannelConfig implements Serializable {

  private static final long serialVersionUID = 2458902189455513278L;

  private final ExpireTime expireTime;
  private final ChannelState channelState;

  public ChannelConfig(final ExpireTime expireTime, final ChannelState channelState) {
    Objects.requireNonNull(expireTime, "expireTime mustn't be null");
    Objects.requireNonNull(channelState, "channelState mustn't be null");

    this.expireTime = expireTime;
    this.channelState = channelState;
  }

  public ChannelState getChannelState() {
    return channelState;
  }

  public ExpireTime getExpireTime() {
    return expireTime;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ChannelConfig)) {
      return false;
    }
    ChannelConfig that = (ChannelConfig) object;

    return expireTime.equals(that.expireTime) && channelState == that.channelState;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = expireTime.hashCode();
    result = prime * result + (channelState == null ? 0 : channelState.hashCode());
    return result;
  }

  public enum ChannelState {

    ACTIVE("license is active"),
    LICENSE_INVALID("channel has invalid license"),
    LICENSE_EXPIRE("channel has expired license"),

    // only for ethernet devices
    IP_CONFIG_PARAMETERS_INVALID("channel get invalid one of ip config parameters"),
    SET_IP_CONFIG_ERROR("channel has setting ip config error"),

    // only for spi devices
    LICENSE_LOCK("channel has locked license");

    private final String value;

    ChannelState(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

  }

}
