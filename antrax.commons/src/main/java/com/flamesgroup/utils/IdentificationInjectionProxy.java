/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Wraps interface which have methods with first argument as an identifier of
 * session or server etc.
 */
public class IdentificationInjectionProxy {

  public static class InjectionInvHandler<T, K> implements InvocationHandler {

    private final T delegate;
    private final K key;
    private final Class<T> delegateType;
    private final Class<K> keyType;

    @SuppressWarnings("unchecked")
    public InjectionInvHandler(final T delegate, final K key, final Class<K> keyType) {
      this.delegate = delegate;
      this.key = key;
      this.keyType = keyType;
      this.delegateType = (Class<T>) delegate.getClass();
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      Method thatMethod = getThatMethod(method);
      try {
        return thatMethod.invoke(delegate, append(key, args));
      } catch (InvocationTargetException e) {
        throw e.getCause();
      }
    }

    private Method getThatMethod(final Method method) throws SecurityException, NoSuchMethodException {
      return delegateType.getDeclaredMethod(method.getName(), append(keyType, method.getParameterTypes()));
    }

    private Class<?>[] append(final Class<?> a, final Class<?>[] rest) {
      Class<?>[] retval = new Class<?>[rest.length + 1];
      retval[0] = a;
      System.arraycopy(rest, 0, retval, 1, rest.length);
      return retval;
    }

    private Object[] append(final K key, final Object[] rest) {
      if (rest == null) {
        return new Object[] {key};
      } else {
        Object[] retval = new Object[rest.length + 1];
        retval[0] = key;
        System.arraycopy(rest, 0, retval, 1, rest.length);
        return retval;
      }
    }
  }

  @SuppressWarnings("unchecked")
  public static <T, K> T proxy(final Object delegate, final Class<T> clazz, final K key, final Class<K> keyType) {
    return (T) Proxy.newProxyInstance(delegate.getClass().getClassLoader(), new Class[] {clazz},
        new InjectionInvHandler(delegate, key, keyType));
  }

}
