/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;

public final class Pair<F, S> implements Serializable {

  private static final long serialVersionUID = -8233834850858012023L;

  private final F first;
  private final S second;

  public Pair(final F first, final S second) {
    this.first = first;
    this.second = second;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (obj instanceof Pair) {
      Pair<F, S> that = (Pair<F, S>) obj;
      return equals(first, that.first) && equals(second, that.second);
    }
    return false;
  }

  private boolean equals(final Object a, final Object b) {
    if (a == null) {
      return b == null;
    }
    if (b == null) {
      return false;
    }
    return a.equals(b);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 31 * hash + hashCode(first);
    hash *= 15 + hashCode(second);
    return hash;

  }

  private int hashCode(final Object o) {
    return o == null ? 0 : o.hashCode();
  }

  @Override
  public String toString() {
    StringBuilder retval = new StringBuilder();
    retval.append('[').append(first).append(':').append(second).append(']');
    return retval.toString();
  }

  public F first() {
    return first;
  }

  public S second() {
    return second;
  }

}
