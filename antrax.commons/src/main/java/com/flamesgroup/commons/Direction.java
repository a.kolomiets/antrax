/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.Objects;

public class Direction implements Serializable {

  private static final long serialVersionUID = 4496215279881506692L;

  private final String address;
  private final int priority;
  private final int capacity;

  public Direction(final String address, final int priority, final int capacity) {
    Objects.requireNonNull(address, "address mustn't be null");

    this.address = address;
    this.priority = priority;
    this.capacity = capacity;
  }

  public String getAddress() {
    return address;
  }

  public int getPriority() {
    return priority;
  }

  public int getCapacity() {
    return capacity;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof Direction)) {
      return false;
    }
    Direction that = (Direction) object;

    return priority == that.priority
        && capacity == that.capacity
        && address.equals(that.address);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = address.hashCode();
    result = prime * result + priority;
    result = prime * result + capacity;
    return result;
  }

  @Override
  public String toString() {
    return "[" + address + "/" + priority + "/" + capacity + "]";
  }

}
