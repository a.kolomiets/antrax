/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons.voipantispam;

import java.io.Serializable;

public class FasConfig implements Serializable {

  private static final long serialVersionUID = 7803599206405339292L;

  private final int period;
  private final int maxRoutingRequestPerPeriod;

  public FasConfig(final int period, final int maxRoutingRequestPerPeriod) {
    this.period = period;
    this.maxRoutingRequestPerPeriod = maxRoutingRequestPerPeriod;
  }

  public int getPeriod() {
    return period;
  }

  public int getMaxRoutingRequestPerPeriod() {
    return maxRoutingRequestPerPeriod;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof FasConfig)) {
      return false;
    }
    final FasConfig that = (FasConfig) object;

    return period == that.period
        && maxRoutingRequestPerPeriod == that.maxRoutingRequestPerPeriod;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = period;
    result = prime * result + maxRoutingRequestPerPeriod;
    return result;
  }

}
