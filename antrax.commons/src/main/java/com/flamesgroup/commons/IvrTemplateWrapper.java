/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;

public final class IvrTemplateWrapper implements Serializable {

  private static final long serialVersionUID = 4145687401839947937L;

  private final String name;
  private final String callDropReason;
  private final String callDropCode;

  public IvrTemplateWrapper(final String name, final String callDropReason, final String callDropCode) {
    this.name = name;
    this.callDropReason = callDropReason;
    this.callDropCode = callDropCode;
  }

  public String getName() {
    return name;
  }

  public String getCallDropReason() {
    return callDropReason;
  }

  public String getCallDropCode() {
    return callDropCode;
  }

}
