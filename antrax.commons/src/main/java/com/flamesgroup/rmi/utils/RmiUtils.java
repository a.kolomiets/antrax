/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi.utils;

import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.rmi.exception.RemoteConnectFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.rmi.ConnectException;
import java.rmi.ConnectIOException;
import java.rmi.MarshalException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.StubNotFoundException;
import java.rmi.UnknownHostException;
import java.rmi.UnmarshalException;

public class RmiUtils {

  private RmiUtils() {
  }

  private static final Logger logger = LoggerFactory.getLogger(RmiUtils.class);

  public static Exception convertRmiAccessException(final Method method, final RemoteException ex, final boolean isConnectFailure, final String serviceName) {
    if (logger.isDebugEnabled()) {
      logger.debug("[{}] - remote service [{}] threw exception", RmiUtils.class, serviceName, ex);
    }
    if (declaresException(method, ex.getClass())) {
      return ex;
    } else {
      if (isConnectFailure) {
        return new RemoteConnectFailureException("Could not connect to remote service [" + serviceName + "]", ex);
      } else {
        return new RemoteAccessException("Could not access remote service [" + serviceName + "]", ex);
      }
    }
  }

  public static boolean isConnectFailure(final RemoteException ex) {
    return (ex instanceof ConnectException
        || ex instanceof ConnectIOException
        || ex instanceof UnknownHostException
        || ex instanceof NoSuchObjectException
        || ex instanceof StubNotFoundException
        || ex instanceof MarshalException
        || ex instanceof UnmarshalException);
  }

  private static boolean declaresException(final Method method, final Class<?> exceptionType) {
    if (method == null) {
      throw new IllegalArgumentException("Method must not be null");
    }
    Class<?>[] declaredExceptions = method.getExceptionTypes();
    for (int i = 0; i < declaredExceptions.length; i++) {
      Class<?> declaredException = declaredExceptions[i];
      if (declaredException.isAssignableFrom(exceptionType)) {
        return true;
      }
    }
    return false;
  }

}
